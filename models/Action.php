<?php

namespace app\models;

use app\components\MyUploadedFile;
use Yii;

/**
 * This is the model class for table "action".
 *
 * @property int $id
 * @property int $company_id Компания
 * @property string $name Название компании
 * @property int $status Статус
 * @property int $common_price Единая цена
 * @property string $created_at Создан
 * @property string $end_date Дата завершения
 *
 * @property Company $company
 * @property Advert[] $adverts
 */
class Action extends \yii\db\ActiveRecord
{
    public $advs;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'action';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['company_id', 'status', 'common_price'], 'integer'],
            [['created_at', 'end_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['advs'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'name' => 'Название компании',
            'status' => 'Статус',
            'common_price' => 'Единая цена',
            'created_at' => 'Создан',
            'end_date' => 'Дата завершения',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
            $this->created_at = date('Y-m-d H:i:s');
            $this->status = 0;
        }


        return parent::beforeSave($insert);
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->advs != null) {
            $allSubs = Advert::find()->where(['action_id' => $this->id])->all();
            foreach ($allSubs as $sub) {
                if (array_search($sub, $this->advs) !== false) {
                    continue;
                } else {
                    $sub->delete();
                }
            }

            $files = MyUploadedFile::getInstancesByName('advsFiles', true);

//            if(count($this->listFile) == count($files)){
//                $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
//                foreach ($files as $name => $file){
//                    /** @var $file MyUploadedFile */
//
//                    $path = Yii::$app->security->generateRandomString();
//                    $path = "uploads/{$path}.$file->extension";
//
//                    $file->saveAs($path);
//
//                    $scan = new Scan([
//                        'name' => !$name ? date("m.d.y") : $name,
//                        'link' => $path,
//                        'claim_id' => $this->id,
//                    ]);
//                    $scan->save(false);
//                }
//            }

            $counter = 0;
            foreach ($this->advs as $adv) {
                $subgroup2 = Advert::find()->where(['id' => $adv['id'], 'action_id' => $this->id])->one();
                $path = null;

                if (!$subgroup2) {

                    if(isset($files[$counter])){
                        /** @var $file MyUploadedFile */
                        $file = $files[$counter];
                        $path = Yii::$app->security->generateRandomString();
                        $path = "uploads/{$path}.$file->extension";
                        $file->saveAs($path);
                    }

                    (new Advert([
                        'action_id' => $this->id,
                        'company_id' => $adv['company_id'],
                        'accessory_id' => $adv['accessory_id'],
                        'name' => $adv['name'],
                        'description' => $adv['description'],
                        'price' => $adv['price'],
                        'show_date_end' => $adv['show_date_end'],
                        'photo' => $path,
                    ]))->save(false);
                } else {
                    if(isset($files[$counter])){
                        /** @var $file MyUploadedFile */
                        $file = $files[$counter];
                        $path = Yii::$app->security->generateRandomString();
                        $path = "uploads/{$path}.$file->extension";
                        $file->saveAs($path);
                    }

                    $subgroup2->company_id = $adv['company_id'];
                    $subgroup2->accessory_id = $adv['accessory_id'];
                    $subgroup2->name = $adv['name'];
                    $subgroup2->description = $adv['description'];
                    $subgroup2->price = $adv['price'];
                    $subgroup2->show_date_end = $adv['show_date_end'];
                    $subgroup2->photo = $path;
                    $subgroup2->save(false);
                }
                $counter++;
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdverts()
    {
        return $this->hasMany(Advert::className(), ['action_id' => 'id']);
    }
}
