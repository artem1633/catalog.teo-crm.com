<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $region_id Регион
 *
 * @property Region $region
 * @property Metro[] $metros
 * @property PriceCity[] $priceCities
 */
class City extends \yii\db\ActiveRecord
{
    public $mtr;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            [['mtr'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'region_id' => 'Регион',
            'mtr' => 'Метро',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->mtr != null) {
            $allSubs = Metro::find()->where(['city_id' => $this->id])->all();
            foreach ($allSubs as $sub) {
                if (array_search($sub, $this->mtr) !== false) {
                    continue;
                } else {
                    $sub->delete();
                }
            }

            foreach ($this->mtr as $adv) {
                $subgroup2 = Metro::find()->where(['id' => $adv['id']])->one();
                if (!$subgroup2) {
                    (new Metro([
                        'city_id' => $this->id,
                        'name' => $adv['name'],
                    ]))->save(false);
                } else {
                    $subgroup2->name = $adv['name'];
                    $subgroup2->save(false);
                }
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetros()
    {
        return $this->hasMany(Metro::className(), ['city_id' => 'id']);
    }
}
