<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "faq".
*
    * @property string $name Наименование
    * @property string $content Содержимое
*/
class Faq extends \yii\db\ActiveRecord
{
    const TYPE_BUYER = 0;
    const TYPE_PROVIDER = 1;
    const TYPE_UNREGISTERED = 2;


    /** @var \yii\web\UploadedFile */
    public $fileInstance;

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'faq';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['name', 'content', 'grouping'], 'string'],
            [['type'], 'integer'],
            [['fileInstance'], 'file'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
            'content' => 'Содержимое',
            'type' => 'Тип',
            'grouping' => 'Группировка',
        ];
    }

    public function beforeSave($insert)
    {

        if($this->fileInstance) {
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$this->fileInstance->extension;

            $this->fileInstance->saveAs($path);

            $this->file = $path;
        }

        if (parent::beforeSave($insert)) {


            return true;
        }
        return false;
    }

    public static function typeLabels()
    {
        return [
            self::TYPE_PROVIDER => 'Компания',
            self::TYPE_BUYER => 'Покупатель',
            self::TYPE_UNREGISTERED => 'Незарегистрированный пользователь',
        ];
    }
}