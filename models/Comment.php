<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use rmrevin\yii\module\Comments\models\Comment as BaseComment;

/**
 * Class Comment
 * @package app\models
 */
class Comment extends BaseComment
{
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['answer_for'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }
}