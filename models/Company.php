<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $official_name Официальное наименование
 * @property string $official_name_short Сокращенное официальное наименование
 * @property int $organization_type Тип лица
 * @property string $pass пароль
 * @property string $address Фактический адрес
 * @property string $post_index Почтовый индекс

 * @property int $is_super_company Является ли супер компанией

 * @property int $access Доступ (вкл/выкл)
 * @property string $last_activity_datetime Дата и время последней активности
 * @property double $balance Баланс
 * @property string $created_at
 * @property string $code Код
 * @property string $inn ИНН
 * @property string $ogrn ОГРН
 * @property string $kpp КПП
 * @property string $official_address Юридический адрес
 * @property int $address_equals Фактический адрес совпадает с юридическим
 * @property string $director Генеральный директор
 * @property string $email Email
 * @property string $phone Телефон
 * @property string $site Сайт
 * @property string $bank_bik БИК/SWIFT
 * @property string $bank_name Наименование банка
 * @property string $bank_address Адрес банка
 * @property string $bank_correspondent_account Корреспондентский счёт
 * @property string $bank_register_number Регистрационный номер
 * @property string $bank_registration_date Дата регистрации
 * @property string $bank_payment_account Расчетный счет
 * @property string $api_key АПИ ключ
 * @property integer $rate_id Тариф
 * @property string $providers Поставщики
 * @property string $products Товары/Услуги
 * @property string $avatar Аватар
 * @property string $description Описание
 * @property string $accessories Услуги
 * @property string $accessories_other Недостающие услуги
 *
 * @property CompanyFile[] $files


 * @property Position[] $positions
 * @property User[] $users

 */
class Company extends ActiveRecord
{
    const TYPE_BUYER = 0;
    const TYPE_PROVIDER = 1;

    const ORGANIZATION_TYPE_INDIVIDUAL = 1;
    const ORGANIZATION_TYPE_COMPANY = 2;


    public $pass;
    public $patronymic;
    public $last_name;

    const RATE_NO = 0;
    const RATE_NO_BRANCH = 1;
    const RATE_BRANCH = 2;

    /** @var UploadedFile */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['email', 'name', 'pass'], 'required'],
            [['organization_type', 'is_super_company', 'access', 'address_equals', 'type', 'rate_id'], 'integer'],
            [
                [

                    'last_activity_datetime',
                    'created_at',
                    'bank_registration_date',
                    'bank_payment_account',
                    'pass',
                    'providers',
                    'products'
                ],
                'safe'
            ],
            [['balance'], 'number'],
            [
                [
                    'name',
                    'official_name',
                    'official_name_short',
                    'address',
                    'post_index',
                    'code',
                    'inn',
                    'ogrn',
                    'kpp',
                    'official_address',
                    'director',
                    'email',
                    'phone',
                    'site',
                    'bank_bik',
                    'bank_name',
                    'bank_address',
                    'bank_correspondent_account',
                    'bank_register_number',
                    'avatar',
                ],
                'string',
                'max' => 255
            ],
            [['api_key', 'description', 'accessories'], 'string'],
            [['accessories_other'], 'safe'],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование компании',
            'official_name' => 'Официальное наименование',
            'official_name_short' => 'Сокращенное официальное наименование',
            'organization_type' => 'Тип лица',
            'address' => 'Фактический адрес',
            'post_index' => 'Почтовый индекс',
            'is_super_company' => 'Является ли супер компанией',
            'access' => 'Доступ (вкл/выкл)',
            'last_activity_datetime' => 'Дата и время последней активности',
            'balance' => 'Баланс',
            'created_at' => 'Дата и время создания',
            'code' => 'Код',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'kpp' => 'КПП',
            'official_address' => 'Юридический адрес',
            'address_equals' => 'Фактический адрес совпадает с юридическим',
            'director' => 'Генеральный директор',
            'email' => 'Email',
            'phone' => 'Телефон',
            'site' => 'Сайт',
            'type' => 'Тип компании',
            'moderated' => 'Прошла модерацию',
            'bank_bik' => 'БИК/SWIFT',
            'bank_name' => 'Наименование банка',
            'bank_address' => 'Адрес банка',
            'bank_correspondent_account' => 'Корреспондентский счёт',
            'bank_register_number' => 'Регистрационный номер',
            'bank_registration_date' => 'Дата регистрации',
            'bank_payment_account' => 'Расчетный счет',
            'api_key' => 'АПИ ключ',
            'pass' => 'Пароль',
            'rate_id' => 'Тариф',
            'providers' => 'Поставщики',
            'products' => 'Товары/Услуги',
            'avatar' => 'Аватар',
            'file' => 'Аватар',
            'description' => 'Описание',
            'accessories' => 'Услуги',
            'accessories_other' => 'Другие услуги',
        ];
    }

    /**
     * @return array
     */
    public static function rateLabels()
    {
        return [
            self::RATE_NO => 'Без абонентской платы',
            self::RATE_NO_BRANCH => 'С абонентской платой (без филиалов)',
            self::RATE_BRANCH => 'С абонентской платой (с филиалами)',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_PROVIDER => 'Компания',
            self::TYPE_BUYER=> 'Покупатель',
        ];
    }

    /**
     * @return array
     */
    public static function organizationTypeLabels()
    {
        return [
            self::ORGANIZATION_TYPE_INDIVIDUAL => 'ИП',
            self::ORGANIZATION_TYPE_COMPANY => 'Юридическое лицо',
        ];
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->generateCodeRecursive();
            $this->rate_id = 1;
        }

//        if($this->providers != null) {
//            $this->providers = implode(',', $this->providers);
//        }

//        if($this->products != null) {
//            $this->products = implode(',', $this->products);
//        }

        if($this->file) {
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$this->file->extension;

            $this->file->saveAs($path);

            $this->avatar = $path;
        }

        if($this->accessories_other != null && is_array($this->accessories_other)){
            $this->accessories_other = json_encode($this->accessories_other, JSON_UNESCAPED_UNICODE);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        if ($insert) {
            foreach (CompanyFile::typeList() as $type) {
                (new CompanyFile(['company_id' => $this->id, 'type' => $type]))->save(false);
            }
            
            $user = new User();
            $user->detachBehavior('company');
            $user->name = $this->name;
            $user->setPassword($this->pass);
            $user->token = md5($this->email.$this->pass);       
            $user->role = User::ROLE_USER;
            $user->company_id = $this->id;
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->is_company_super_admin = 1;
            $user->is_deletable = 1;
            $user->last_name = $this->last_name;
            $user->patronymic = $this->patronymic;


            $user->save(false);
        }
    }

    /**
     * Отправляет письмо
     * @param $subject
     * @param $htmlBody
     * @param array $attachments
     * @return bool
     */
    /**
     * Отправляет письмо
     * @param $subject
     * @param $htmlBody
     * @param array $attachments
     * @return bool
     */
    public function sendEmailMessage($subject, $htmlBody, $attachments = [])
    {
        if ($this->email == null) {
            return false;
        }

        try {
            $mail = Yii::$app->mailer->compose()
                ->setFrom('beavercrm@yandex.ru')
                ->setTo($this->email)
                ->setSubject($subject)
                ->setHtmlBody($htmlBody);

            if (count($attachments) > 0) {
                foreach ($attachments as $attachmentPath => $attachmentName) {
                    $mail->attach($attachmentPath, ['fileName' => $attachmentName]);
                }
            }

            $mail->send();
        } catch (\Exception $e) {
            Yii::warning($e);
        }
    }

    /**
     * @param integer $i
     * @throws \yii\base\Exception
     */
    private function generateCodeRecursive($i = 1)
    {
        if ($i > 20) {
            return;
        }

        $code = Yii::$app->security->generateRandomString(5);
        $company = self::find()->where(['code' => $code])->one();
        if ($company == null) {
            $this->code = $code;
            return;
        } else {
            $i++;
            $this->generateCodeRecursive($i);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(CompanyFile::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBots()
    {
        return $this->hasMany(Bot::className(), ['company_id' => 'id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */


    /**
     * Получает компанию по токену
     * @param $token
     * @return null|static
     */
    public function getByToken($token)
    {
        return self::findOne(['api_key' => $token]) ?? null;
    }

    /**
     * Генерация нового токена
     * @return string
     * @throws \yii\base\Exception
     */
    public function changeApiKey()
    {
        $this->api_key = Yii::$app->security->generateRandomString();

        $this->save(false);

        return $this->api_key;
    }


	
	
	
	
}
