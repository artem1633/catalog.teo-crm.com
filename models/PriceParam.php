<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "price_param".
 *
 * @property int $id
 * @property int $price_id Прайс
 * @property int $company_id Компания
 * @property string $params Параметры
 * @property int $check Чек
 *
 * @property Company $company
 * @property Price $price
 */
class PriceParam extends \yii\db\ActiveRecord
{
    /** @var int Кол-во записей */
    public $count;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_param';
    }

    // /**
    //  * {@inheritdoc}
    //  */
    // public function behaviors()
    // {
    //     return [
    //         [
    //             'class' => BlameableBehavior::class,
    //             'updatedByAttribute' => null,
    //             'createdByAttribute' => 'company_id',
    //             'value' => Yii::$app->user->isGuest == false ? \Yii::$app->user->identity->company_id : null,
    //         ],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_id', 'company_id', 'check', 'count'], 'integer'],
            [['params'], 'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => Price::className(), 'targetAttribute' => ['price_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_id' => 'Прайс',
            'company_id' => 'Компания',
            'params' => 'Параметры',
            'check' => 'Чек',
            'count' => 'Кол-во создаваемых строк',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(isset($_POST['Param'])){
            $this->params = json_encode($_POST['Param'], JSON_UNESCAPED_UNICODE);
        }

        if($this->company_id == null){
            $this->company_id = Yii::$app->user->isGuest == false ? \Yii::$app->user->identity->company_id : null;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        // $childAccessory = Accessories::find()->where(['id' => $this->price->accessory_id])->one();

        // if($childAccessory){
        //     \Yii::warning($childAccessory->accessories_id, 'ACCESSORY ID');
        //     if(Yii::$app->cache->exists('API_FIELDS_'.$childAccessory->accessories_id)){
        //         Yii::$app->cache->delete('API_FIELDS_'.$childAccessory->accessories_id);
        //     }
        // }
    }

    public function afterDelete()
    {
        parent::afterDelete();


        $childAccessory = Accessories::find()->where(['id' => $this->price->accessory_id])->one();

        if($childAccessory){
            \Yii::warning($childAccessory->accessories_id, 'ACCESSORY ID');
            if(Yii::$app->cache->exists('API_FIELDS_'.$childAccessory->accessories_id)){
                Yii::$app->cache->delete('API_FIELDS_'.$childAccessory->accessories_id);
            }
        }
    }

    /**
     * @param boolean $runValidation
     * @return bool
     */
    public function saveMulti($runValidation = true)
    {
        if($this->validate()){
            if($this->count != null){
                $attributes = $this->attributes;
                $attributes['count'] = null;
                $attributes['id'] = null;
                for ($i = 0; $i < $this->count; $i++){
                    $m = new self($attributes);
                    $m->save($runValidation);
                }
                if($this->isNewRecord == false){
                    $this->save($runValidation);
                }
            } else {
                $this->save($runValidation);
            }
            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(Price::className(), ['id' => 'price_id']);
    }
}
