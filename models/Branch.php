<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "branch".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $company_id Компания
 * @property string $email Email
 * @property string $phone Телефон
 * @property string $address Адрес
 * @property int $city_id Город
 * @property int $metro_id Метро
 * @property string $avatar Аватар
 * @property string $social_site Сайт
 * @property string $social_google Google
 * @property string $social_vk ВК
 * @property string $social_facebook Facebook
 * @property string $social_instagram Instagram
 * @property string $social_youtube Youtube
 * @property int $work_mon_enable ПН
 * @property int $work_tue_enable ВТ
 * @property int $work_wed_enable СР
 * @property int $work_thu_enable ЧТ
 * @property int $work_fri_enable ПТ
 * @property int $work_sat_enable СБ
 * @property int $work_sun_enable ВС
 * @property string $work_mon_hours Часы работы, ПН
 * @property string $work_tue_hours Часы работы, ВТ
 * @property string $work_wed_hours Часы работы, СР
 * @property string $work_thu_hours Часы работы, ЧТ
 * @property string $work_fri_hours Часы работы, ПТ
 * @property string $work_sat_hours Часы работы, СБ
 * @property string $work_sun_hours Часы работы, ВС
 *
 * @property City $city
 * @property Company $company
 * @property Metro $metro
 * @property Price[] $prices
 * @property User[] $users
 */
class Branch extends \yii\db\ActiveRecord
{
    /** @var UploadedFile */
    public $file;

    public $userPassword;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'branch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'city_id', 'metro_id', 'work_mon_enable', 'work_tue_enable', 'work_wed_enable', 'work_thu_enable', 'work_fri_enable', 'work_sat_enable', 'work_sun_enable'], 'integer'],
            [['name', 'email', 'phone', 'address', 'social_site', 'social_google', 'social_vk', 'social_facebook', 'social_instagram', 'social_youtube', 'work_mon_hours', 'work_tue_hours', 'work_wed_hours', 'work_thu_hours', 'work_fri_hours', 'work_sat_hours', 'work_sun_hours', 'avatar', 'userPassword', 'coords'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['metro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Metro::className(), 'targetAttribute' => ['metro_id' => 'id']],
            [['file'], 'file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Филиал',
            'company_id' => 'Компания',
            'email' => 'Email',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'city_id' => 'Город',
            'metro_id' => 'Метро',
            'avatar' => 'Аватар',
            'social_site' => 'Сайт',
            'social_google' => 'Google',
            'social_vk' => 'ВК',
            'social_facebook' => 'Facebook',
            'social_instagram' => 'Instagram',
            'social_youtube' => 'Youtube',
            'work_mon_enable' => 'ПН',
            'work_tue_enable' => 'ВТ',
            'work_wed_enable' => 'СР',
            'work_thu_enable' => 'ЧТ',
            'work_fri_enable' => 'ПТ',
            'work_sat_enable' => 'СБ',
            'work_sun_enable' => 'ВС',
            'work_mon_hours' => 'Часы работы, ПН',
            'work_tue_hours' => 'Часы работы, ВТ',
            'work_wed_hours' => 'Часы работы, СР',
            'work_thu_hours' => 'Часы работы, ЧТ',
            'work_fri_hours' => 'Часы работы, ПТ',
            'work_sat_hours' => 'Часы работы, СБ',
            'work_sun_hours' => 'Часы работы, ВС',
            'file' => 'Файл',
            'userPassword' => 'Пароль'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord && $this->company_id == null){
            $this->company_id = Yii::$app->user->identity->company_id;
        }

        if($this->file) {
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$this->file->extension;

            $this->file->saveAs($path);

            $this->avatar = $path;
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            if($this->userPassword && $this->email){
                $user = User::find()->where(['email' => $this->email])->one();


                if($user){
                    $user->branch_id = $this->id;
                    $user->save(false);
                } else {
                    (new User([
                        'branch_id' => $this->id,
                        'password' => $this->userPassword,
                        'company_id' => $this->company_id,
                        'email' => $this->email,
                        'role' => User::ROLE_USER,
                    ]))->save(false);
                }
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetro()
    {
        return $this->hasOne(Metro::className(), ['id' => 'metro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['branch_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['branch_id' => 'id']);
    }
}