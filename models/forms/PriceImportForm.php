<?php

namespace app\models\forms;

use yii\base\Model;
use SimpleExcel\SimpleExcel;

class PriceImportForm extends Model
{
	public $priceId;

	public $file;

	public function rules()
	{
		return [
			[['priceId'], 'required'],
			[['priceId'], 'integer'],
			[['file'], 'file']
		];
	}

	public function attributesLabel()
	{
		return [
			'priceId' => 'Цена',
			'file' => 'Excel-файл',
		];
	}

	public function upload($runValidation = true)
	{
		if($runValidation){
			if($this->validate() == false){
				return false;
			}
		}

		if(is_dir('uploads') == false){
			mkdir('uploads');
		}

		$path = 'uploads/'.\Yii::$app->security->generateRandomString().'.'.$this->file->extension;

		$this->file->saveAs($path);

		$xl = \SimpleXLSX::parse($path);

		$rows = $xl->rows();

		$price = \app\models\Price::findOne($this->priceId);
		$fields = \app\models\TemplateFields::find()->where(['accessories_id' => $price->accessory_id])->all();

		$newData = [];

		// var_dump($fields);
		// exit;

		$tCount = 0;
		foreach ($rows as $row) {
			$counter = 0;
			$newRow = [];

			if($tCount == 0){
				$tCount++;
				continue;
			}

			foreach ($row as $el) {
				
				if(isset($fields[$counter]) == false){
					continue;
				}

				if($fields[$counter]->type == \app\models\TemplateFields::TYPE_DROPDOWN){
					$data = explode(',', $fields[$counter]->data);

					\Yii::warning($el, 'VALUE');
					\Yii::warning($data, 'data');


					$data = array_combine($data, $data);
					if(in_array($el, $data, false)){
						$newRow[$fields[$counter]->id] = (string) (isset($data[$el]) ? trim($data[$el]) : trim($el));
					} elseif(in_array(' '.$el, $data, false)){
						$newRow[$fields[$counter]->id] = (string) (isset($data[' '.$el]) ? trim($data[' '.$el]) : trim(' '.$el));
					} else {
						// continue 2;
					}


				} else {
					$newRow[$fields[$counter]->id] = trim($el);
				}

				$counter++;
			}

			$newData[] = $newRow;
			$tCount++;
		}

		\app\models\PriceParam::deleteAll(['price_id' => $this->priceId]);

		foreach ($newData as $row) {
			$fld = new \app\models\PriceParam([
				'company_id' => $price->company_id,
				'price_id' => $this->priceId,
				'params' => json_encode($row, JSON_UNESCAPED_UNICODE),
			]);

			$fld->save(false);
		}

		\Yii::warning($newData, 'newData');


		return true;
	}
}

