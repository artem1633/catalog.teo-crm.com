<?php

namespace app\models\forms;

use app\models\Accessories;
use app\models\City;
use app\models\TemplateFields;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class OrderForm
 * @package app\models\forms
 */
class OrderForm extends Model
{
    public $city;

    public $category;

    public $subCategory;

    public $param1;

    public $param2;

    public $height;

    public $width;

    public $count;

    public $comment;

    public $name;

    public $phone;

    public $files;

    public $agree;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['city', 'category', 'subCategory', 'param1', 'param2', 'name', 'phone'], 'required'],
            [['city', 'category', 'subCategory', 'name', 'phone'], 'required'],
            [['count'], 'required', 'when' => function(){
                return !stripos($this->category, 'Широкоформат');
            }],
            [['height', 'width'], 'required', 'when' => function(){
                return stripos($this->category, 'Широкоформат');
            }],
            [['city',  'count', 'agree', 'phone'], 'integer'],
            [['name', 'comment', 'height', 'width', 'param1', 'category', 'subCategory'], 'string'],
            [['files'], 'file'],
            ['files', function(){
                $files = UploadedFile::getInstances($this, 'files');

                if(count($files) > 4){
                    $this->addError('files', 'Максимум может быть 4 файла');
                }
            }],

            ['agree', function($model){
                if($this->agree == false){
                    $this->addError('agree', 'Необходимо дать согласие');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'comment' => 'Комментарий',
            'phone' => 'Телефон',
            'count' => 'Тираж (кол-во штук)',
            'width' => 'Ширина (см)',
            'height' => 'Высота (см)',
            'file' => 'Файлы',
            'agree' => 'Согласие',
            'city' => 'Город',
            'category' => 'Категория',
            'subCategory' => 'Подкатегория',
            'param1' => 'Параметр 1',
            'param2' => 'Параметр 2',
        ];
    }

    /**
     * @return bool
     */
    public function send()
    {
        if($this->validate()){

//            $category = Accessories::findOne($this->category);
//            $param1 = TemplateFields::findOne($this->param1);
//            $subCategory = Accessories::findOne($this->subCategory);
            $city = City::findOne($this->city);

            $info = [];

            if($this->category){
                $info[] = "Категория: {$this->category}";
            }
            if($this->subCategory){
                $info[] = "Подкатегория: {$this->subCategory}";
            }
            // if($this->param1){
            //     $info[] = "Параметр1: {$this->param1}";
            // }
            // if($this->param2){
            //     $info[] = "Параметр2: {$this->param2}";
            // }
//            if($category){
//                $info[] = "Категория: {$category->name}";
//            }
//            if($subCategory){
//                $info[] = "Подкатегория: {$subCategory->name}";
//            }
            if($this->count){
                $info[] = "Тираж: {$this->count}";
            } elseif($this->height || $this->width) {
                $info[] = "Высота: {$this->height}";
                $info[] = "Широта: {$this->width}";
            }

            if($city){
                $info[] = "Город: {$city->name}";
            }
            $info[] = "Имя: {$this->name}";
            $info[] = "Комментарий: {$this->comment}";
            $info[] = "Телефон: {$this->phone}";
            $info[] = "Дата и время: ".date('d.m.Y H:i:s');


            /** @var UploadedFile[] $files */
            $files = UploadedFile::getInstances($this, 'files');

            $filePathes = [];

            foreach ($files as $file)
            {
                if(is_dir('uploads') == false){
                    mkdir('uploads');
                }

                $path = 'uploads/'.\Yii::$app->security->generateRandomString().'.'.$file->extension;

                $filePathes[] = $path;

                $file->saveAs($path);
            }


            $text = implode('<br>', $info);

            \Yii::warning($text, 'text mail message');

            try {
                // $mail = \Yii::$app->mailer->compose()
                //     ->setFrom('order2@stampato.ru')
                //     ->setTo('order@stampato.ru')
                //     ->setSubject("Заявка")
                //     ->setHtmlBody($text);

                $messageParams = [
                  // 'from'    => \Yii::$app->params['adminEmail'],
                  'from'    => 'admin@stampato.ru',
                  'to'      => 'stampato@yandex.ru',
                  'subject' => 'Заявка',
                  'html'    => $text,
                  'attachment' => [],
                ];

                foreach ($filePathes as $path){
                    $messageParams['attachment'][] = ['filePath' => $path];
                }

                \Yii::$app->mailgun->instance->messages()->send('stampato.ru', $messageParams);

                // $mail->send();

            } catch(\Exception $e){
                \Yii::warning($e->getMessage());
            }


            return true;
        }

        return false;
    }
}