<?php
namespace app\models\forms;

use app\models\City;
use app\models\Company;
use yii\base\Model;
use app\models\User;
use yii\behaviors\BlameableBehavior;

/**
 * Signup form
 */
class RequestCompanyForm extends Model
{
    public $companyName;
    public $phone;
    public $city;
    public $branchCount;
    public $contacts;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['companyName'], 'required'],
//            ['oferta', function(){
//                if($this->oferta == 0){
//                    $this->addError('oferta', 'Вы должны согласиться с договором публичной оферты');
//                    return false;
//                }
//            }],
            ['contacts', 'string', 'max' => 255],
            ['phone', 'string', 'max' => 255],
            [['city', 'branchCount'], 'integer'],
//            ['email', 'unique', 'targetClass' => '\app\models\User', 'targetAttribute' => 'email', 'message' => 'Этот email уже зарегистрирован',],
//            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот email уже зарегистрирован',
//                'when' => function($model, $attribute){
//                    if($this->userId != null)
//                    {
//                        $userModel = User::findOne($this->userId);
//                        return $this->{$attribute} !== $userModel->getOldAttribute($attribute);
//                    }
//                    return true;
//                },
//            ],
//            ['password', 'string', 'min' => 6],
        ];
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios['update'] = ['password', 'email'];//Scenario Values Only Accepted
    //     return $scenarios;
    // }

    public function attributeLabels()
    {
        return [
            'companyName' => 'Наименование компании',
            'password' => 'Пароль',
            'email' => 'Email',
            'name' => 'Имя',
            'promo' => 'Промо-код',
            'last_name' => 'Фамилия',
            'type' => 'Тип',
            'patronymic' => 'Отчество',
            'phone' => 'Телефон',
            'agree' => 'agree',
            'city' => 'Город',
            'branchCount' => 'Кол-во филиалов',
            'contacts' => 'Контактное лицо',
        ];
    }

    /**
     * Signs user up.
     *
     * @return true
     */
    public function request()
    {
        if (!$this->validate()) {
            return null;
        }

        try {

            $city = City::findOne($this->city);

            if($city){
                $city = $city->name;
            }

            \Yii::$app->mailer->compose()
                ->setFrom('order2@stampato.ru')
                ->setTo('order@stampato.ru')
                ->setSubject('Заявка на добавление компании')
                ->setHtmlBody("
                    <b>Город:</b> {$city}<br>
                    <b>Название компании:</b> {$this->companyName}<br>
                    <b>Кол-во филиалов:</b> {$this->branchCount}<br>
                    <b>Телефон:</b> {$this->phone}<br>
                    <b>Контактное лицо:</b> {$this->contacts}<br>
                ")->send();

            return true;


        } catch (\Exception $e){
            \Yii::warning($e->getMessage());
        }

    }
}
