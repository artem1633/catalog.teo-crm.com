<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * Class CityCashForm
 * @package app\models\forms
 */
class CityCashForm extends Model
{
    /** @var integer */
    public $city;

    /** @var integer */
    public $metro;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city'], 'required'],
            [['metro'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->city = Yii::$app->session->get('user__city');
        $this->metro = Yii::$app->session->get('user__metro');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city' => 'Город',
            'metro' => 'Метро',
        ];
    }

    /**
     * Сохраняет в кещ данные
     * @return bool
     */
    public function saveInCash()
    {
        if($this->validate()){
            Yii::$app->session->set('user__city', $this->city);
            Yii::$app->session->set('user__metro', $this->metro);

            return true;
        }

        return false;
    }
}