<?php

namespace app\models\forms;

use yii\base\Model;

/**
 * Class ReportFilterForm
 * @package app\models\forms
 */
class ReportFilterForm extends Model
{
    /** @var string */
    public $companyName;

    /** @var int */
    public $accessoryId;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyName'], 'string'],
            [['accessoryId'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'accessoryId' => 'Категория',
        ];
    }
}