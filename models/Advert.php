<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advert".
 *
 * @property int $id
 * @property int $action_id Акция
 * @property int $company_id Компания
 * @property int $price_id Услуга
 * @property string $name Название
 * @property string $description Описание
 * @property double $price Цена
 * @property double $photo Фото
 * @property int $view Кол-во просмотров
 * @property string $created_at Создан
 * @property int $show_date_end Показывать время окончания
 * @property int $accessory_id Категория
 *
 * @property Accessories $accessory
 * @property Action $action
 * @property Company $company
 * @property Price $priceInstance
 */
class Advert extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action_id', 'company_id', 'price_id', 'view', 'show_date_end'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['created_at'], 'safe'],
            [['name', 'photo'], 'string', 'max' => 255],
            [['accessory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accessories::className(), 'targetAttribute' => ['accessory_id' => 'id']],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => Price::className(), 'targetAttribute' => ['price_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'accessory_id' => 'Категория',
            'action_id' => 'Акция',
            'company_id' => 'Компания',
            'price_id' => 'Услуга',
            'name' => 'Название',
            'description' => 'Описание',
            'view' => 'Кол-во просмотров',
            'created_at' => 'Создан',
            'show_date_end' => 'Показывать время окончания',
            'photo' => 'Фото',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessory()
    {
        return $this->hasOne(Accessories::className(), ['id' => 'accessory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceInstance()
    {
        return $this->hasOne(Price::className(), ['id' => 'price_id']);
    }
}
