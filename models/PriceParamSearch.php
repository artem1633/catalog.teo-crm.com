<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PriceParam;

/**
 * PriceParamSearch represents the model behind the search form about `app\models\PriceParam`.
 */
class PriceParamSearch extends PriceParam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price_id', 'company_id'], 'integer'],
            [['params', 'check'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PriceParam::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price_id' => $this->price_id,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'params', $this->params])
            ->andFilterWhere(['like', 'check', $this->check]);

        return $dataProvider;
    }
}
