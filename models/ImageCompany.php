<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image_company".
 *
 * @property int $id
 * @property int $company_id Компания
 * @property string $name Наименование
 * @property string $path Путь
 * @property string $created_at Дата и время загрузки
 *
 * @property Company $company
 */
class ImageCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'path'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'name' => 'Наименование',
            'path' => 'Путь',
            'created_at' => 'Дата и время загрузки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
