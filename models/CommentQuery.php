<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * Class CommentQuery
 * @package app\models
 */
class CommentQuery extends \rmrevin\yii\module\Comments\models\queries\CommentQuery
{
    /**
     * @inheritdoc
     */
    public function all($db = null) {

        if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
            $this->andWhere(['or', ['created_by' => Yii::$app->user->getId()], ['answer_for' => Yii::$app->user->getId()]]);
        }

        return parent::all($db);
    }

}