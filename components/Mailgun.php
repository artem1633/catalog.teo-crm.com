<?php

namespace app\components;

use \yii\base\Component;
use Mailgun\Mailgun as BaseMailgun;

class Mailgun extends Component{
	
	public $apiKey;

	public $instance;


	public function init()
	{
		$this->instance = BaseMailgun::create($this->apiKey, 'https://api.eu.mailgun.net');
	}
}