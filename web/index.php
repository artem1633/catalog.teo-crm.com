<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

error_reporting(-1);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');
ini_set('memory_limit', '2024M'); 

(new yii\web\Application($config))->run();
