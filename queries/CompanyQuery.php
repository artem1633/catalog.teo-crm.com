<?php

namespace app\queries;

use app\models\User;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class CompanyQuery
 * @package app\queries
 */
class CompanyQuery extends ActiveQuery
{
    public $ignoreCompany = false;

    public $onlyMyCompany = false;

    private $user;

    /**
     * CompanyQuery constructor.
     * @param string $modelClass
     * @param User $user
     * @param array $config
     */
    public function __construct($modelClass, $user = null, array $config = [])
    {
        $this->user = $user;
        parent::__construct($modelClass, $config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();


        if(Yii::$app->module != null){
            $moduleId = Yii::$app->module->id;
        } else {
            $moduleId = null;
        }

        if($moduleId == 'api'){
            $this->ignoreCompany = true;
        }
    }

    /**
     * Включает игнорирование компании
     */
    public function ignoreCompany()
    {
        $this->ignoreCompany = true;
        return $this;
    }


    /**
     * Отключает игнорирование компании
     */
    public function withCompany()
    {
        $this->ignoreCompany = false;
        return $this;
    }

    /**
     * Если администратор то отображает только сущности админской компании
     */
    public function onlyMyCompany()
    {
        $this->onlyMyCompany = 1;
        return $this;
    }

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        if($this->user != null && $this->user->isSuperAdmin() == false && $this->ignoreCompany == false){
            $this->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        if($this->user != null && $this->user->isSuperAdmin() && $this->onlyMyCompany){
            $this->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        return parent::all($db);
    }

    /**
     * @param string $q
     * @param null $db
     * @return int|string
     */
    public function count($q = '*', $db = null)
    {
        if($this->user != null && $this->user->isSuperAdmin() == false && $this->ignoreCompany == false){
            $this->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        return parent::count($q, $db);
    }

    public function one($db = null)
    {
        if($this->user != null && $this->user->isSuperAdmin() == false && $this->ignoreCompany == false){
            $this->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        return parent::one($db);
    }
}