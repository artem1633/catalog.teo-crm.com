<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ChatAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'app-assets/vendors/css/vendors.min.css',
        'app-assets/css/bootstrap.min.css',
        'app-assets/css/bootstrap-extended.min.css',
        'app-assets/css/colors.css',
        'app-assets/css/components.min.css',
        'app-assets/css/themes/dark-layout.min.css',
        'app-assets/css/themes/bordered-layout.min.css',
        'app-assets/css/themes/semi-dark-layout.min.css',
        'app-assets/css/core/menu/menu-types/vertical-menu.min.css',
        'app-assets/css/pages/app-chat.min.css',
        'app-assets/css/pages/app-chat-list.css',
        'libs/magnific-popup/magnific-popup.css',
        'css/site.css',
    ];

    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.5.0/perfect-scrollbar.min.js',
//        'app-assets/vendors/js/vendors.min.js',
        'app-assets/js/core/app-menu.min.js',
        'app-assets/js/core/app.min.js',
        'app-assets/js/scripts/customizer.min.js',
        'app-assets/js/scripts/pages/app-chat.min.js',
        'libs/magnific-popup/jquery.magnific-popup.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.pjax/2.0.1/jquery.pjax.min.js',
        'libs/socket.io/socket.io.js',
//        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
        'js/main.js',
    ];

    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
