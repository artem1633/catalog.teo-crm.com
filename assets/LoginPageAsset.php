<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'app-assets/css/pages/app-auth.min.css'
    ];

    public $js = [
//        'app-assets/vendors/js/vendors.min.js',

    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}
