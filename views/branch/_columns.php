<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($model){

            if($model->avatar){
                $path = "/{$model->avatar}";
            } else {
                $path = "/no-photo-available.png";
            }

            $addresses = "";

            if($model->city_id){
                $city = \app\models\City::findOne($model->city_id);
                if($city){
                    $region = \app\models\Region::findOne($city->region_id);
                    $addresses .= "{$region->name}, {$city->name}";
                }
            }

            if($model->metro_id){
                $metro = \app\models\Metro::findOne($model->metro_id);
                if($metro){
                    $addresses .= ", {$metro->name}";
                }
            }

            $output = "<div class=\"d-flex justify-content-left align-items-center\">
                    <div class=\"avatar  mr-1\"><img src=\"{$path}\" width=\"52\" height=\"52\" style='object-fit: cover;'></div><div class=\"d-flex flex-column\"><span class=\"emp_name text-truncate font-weight-bold\">{$model->name}</span><small class=\"emp_post text-truncate text-muted\">{$addresses}</small></div></div>";


            return $output;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'value' => 'company.name',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                            'data-confirm-cancel'=>'Отмена',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   