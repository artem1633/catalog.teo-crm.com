<?php

use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/** @var $model \app\models\Branch */

?>

<style>
    #table-hours tr td {
        vertical-align: top;
    }

    .td-row {
        margin-bottom: 5px;
    }

    .input-not-long {
        display: inline-block;
        width: 200px;
    }
</style>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Часы работы</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?php Pjax::begin(['id' => 'pjax-hours-container']) ?>
                <?php $form = ActiveForm::begin() ?>
                <div class="row">
                    <div class="col-md-12">
                        <table id="table-hours" style="border-collapse: separate; border-spacing: 7px;">
                            <tbody>
                            <?php

                                $days = [
                                    'mon' => 'Понедельник',
                                    'tue' => 'Вторник',
                                    'wed' => 'Среда',
                                    'thu' => 'Четверг',
                                    'fri' => 'Пятница',
                                    'sat' => 'Суббота',
                                    'sun' => 'Воскресение',
                                ];

                            ?>
                            <?php foreach ($days as $key => $label): ?>
                            <tr>
                                <td>
                                    <?= $label ?>
                                </td>
                                <td>
                                    <input name="Branch[work_<?=$key?>_enable]" <?= ArrayHelper::getValue($model, "work_{$key}_enable") ? "checked=''" : '' ?> type="checkbox" style="margin: 0 10px;">
                                </td>
                                <td>
                                    <?php

                                    $data = json_decode(ArrayHelper::getValue($model, "work_{$key}_hours"));

                                    if(is_array($data) == false){
                                        $data = [];
                                    }


                                    ?>
                                    <?php if(count($data) > 0): ?>
                                        <?php $counter = 0; foreach ($data as $row): ?>
                                            <div class="td-row">
                                                <input name="work_<?=$key?>_hours[<?=$counter?>][from]" class="form-control input-not-long" type="text" placeholder="От" value="<?=$row[0]?>"> - <input name="work_<?=$key?>_hours[<?=$counter?>][to]" class="form-control input-not-long" type="text" placeholder="До" value="<?=$row[1]?>">
                                                <?php if($counter > 0): ?>
                                                    <a class="btn btn-danger btn-sm" href="#" onclick="event.preventDefault(); $.get('/branch/delete-hours?id=<?=$model->id?>&attr=work_<?=$key?>_hours&i=<?=$counter?>', function(){ $.pjax.reload('#pjax-hours-container'); })"><i class="feather icon-x"></i></a>
                                                <?php endif; ?>
                                            </div>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <input name="work_<?=$key?>_hours[0][from]" class="form-control input-not-long" type="text" placeholder="От" value=""> - <input name="work_<?=$key?>_hours[0][to]" class="form-control input-not-long" type="text" placeholder="До" value="">
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <a href="#" onclick="event.preventDefault(); $.get('/branch/add-hours?id=<?=$model->id?>&attr=work_<?=$key?>_hours', function(){ $.pjax.reload('#pjax-hours-container'); })">Добавьте часы работы</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>
