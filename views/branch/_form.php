<?php
use app\models\City;
use app\models\Metro;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Branch */
/* @var $form yii\widgets\ActiveForm */


$cityData = [];

foreach (City::find()->joinWith(['region'])->andWhere(['region.name' => 'Санкт-Петербург и Ленинградская область'])->all() as $city)
{
    $cityData[$city->id] = "{$city->name}";
}

?>

<div class="branch-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

   <div class="row">
       <div class="col-md-6">
           <div class="row">
               <div class="col-md-12">
                   <h4>Основное</h4>
               </div>
           </div>
           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
               </div>
           </div>

           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'userPassword')->passwordInput() ?>
               </div>
           </div>

           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
               </div>
           </div>

           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
               </div>
           </div>

           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
               </div>
           </div>

           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'city_id')->widget(Select2::class, [
                       'data' => $cityData,
                       'options' => [
                           'placeholder' => 'Выберите'
                       ],
                   ]) ?>
               </div>
           </div>

           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'metro_id')->widget(Select2::class, [
                       'data' => $model->city ? ArrayHelper::map(Metro::find()->where(['city_id' => $model->city])->all(), 'id', 'name') : [],
                       'options' => [
                           'placeholder' => 'Выберите'
                       ],
                   ]) ?>
               </div>
           </div>

           <div class="row">
               <div class="col-md-12">
                   <div class="hidden">
                       <?= $form->field($model, 'file')->fileInput(['onchange' => '
                            console.log(event);
                            $("#btn-file-file").text("Лого выбран");
                            $("#btn-file-file").removeClass("btn-primary");
                            $("#btn-file-file").addClass("btn-success");
                       ']) ?>
                   </div>
                   <?= Html::a('Выбрать лого', '#', ['id' => 'btn-file-file', 'class' => 'btn btn-primary', 'onclick' => '$("#branch-file").trigger("click");']) ?>
               </div>
           </div>

       </div>
       <div class="col-md-6">
           <div class="row">
               <div class="col-md-12">
                   <h4>Социальные сети</h4>
               </div>
           </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'social_site')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'social_vk')->textInput(['maxlength' => true]) ?>
               </div>
           </div>
           <div class="row">
               <div class="col-md-12">
                   <?= $form->field($model, 'social_instagram')->textInput(['maxlength' => true]) ?>
               </div>
           </div>
           <div class="row">
               <div class="col-md-12">
                   <div id="YMapsID" style="width:400px;height:300px;"></div>


<!--                   <input id="coordinates">-->
                   <div class="hidden">
                       <?= $form->field($model, 'coords')->textInput(['maxlength' => true]) ?>
                   </div>
               </div>
           </div>
       </div>
   </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>



<?php

$startCoords = '59.910470,30.342766';
if($model->coords){
    $startCoords = $model->coords;
}

$script = <<< JS
$("#branch-city_id").change(function(){
    $.get("/city/get-dropdown-data-metro?id="+$(this).val(), function(response){
        if(response.count > 0){
            $("#branch-metro_id").html(response.text);
            // $("#metro-wrapper").show();
        } else {
            // $("#citycashform-metro").html('');
            $("#branch-metro_id").val(null);
            // $("#metro-wrapper").hide();
        }
    });
});

function init () {

  start = '{$startCoords}'; // start coordinates
  zoom = 10;                      // start zoom
  id = 'map';

  start = start.split(',');       // split coordinates to array

  lat = start[0];
  long = start[1];
  coords = [lat, long];
  Map = new ymaps.Map('YMapsID', {    // initialize map
    center: coords,
    zoom: zoom,
    controls: ['zoomControl']
  });
  
    //   var search = new ymaps.control.SearchControl({
    //     options: {
    //         float: 'left',
    //         floatIndex: 100,
    //         noPlacemark: true
    //     }
    // });
    // Map.controls.add(search);

    /* Adding mark on map*/
    mark = new ymaps.Placemark([lat, long],{}, {preset: "islands#redIcon", draggable: true});
    Map.geoObjects.add(mark);
    
    mark.events.add("dragend", function () {
      coords = this.geometry.getCoordinates();
      save();
    }, mark);

    /* Event click */
    Map.events.add('click', function (e) {
      coords = e.get('coords');
      save();
    });
    
    Map.controls.remove('searchControl');


    /* Event search */
    search.events.add("resultselect", function () {
      coords = search.getResultsArray()[0].geometry.getCoordinates();
      save();
    });
}
function save (){
  var new_coords = [coords[0].toFixed(6), coords[1].toFixed(6)];
  mark.getOverlaySync().getData().geometry.setCoordinates(new_coords);
  document.getElementById("branch-coords").value = new_coords;
}
ymaps.ready(init);

JS;

$this->registerJs($script, View::POS_READY);

?>
