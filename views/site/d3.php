<?php


\app\assets\plugins\SnapSvgAsset::register($this);
?>

    <style>
        rect.active {
            fill: #5acce0;
        }
    </style>

<div class="content-wrapper" style="position: relative;">
    <svg id="svg" style="width: 100%; height: 1000px;"></svg>

    <div class="panel panel-inverse" style="position: absolute; top: 100px; left: 200px;" data-rect="2">
        <div class="panel-heading">
            <h4 class="panel-title"></h4>
        </div>
        <div class="panel-body">
            child
            <a href="#" data-role="creator" class="btn btn-success btn-xs"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="panel panel-inverse" style="position: absolute; top: 0;" data-rect="1">
        <div class="panel-heading">
            <h4 class="panel-title"></h4>
        </div>
        <div class="panel-body">
            parent
        </div>
    </div>
</div>

<?php

$script = <<< JS
//
// var style = {
//     fill: '#5acce0',
//     stroke: '#207887',
//     strokeWidth: 3,
// };
//
// var path = paper.path("").attr({stroke: "#222", fill: "transparent", strokeWidth: 1});
//
// // var p = paper.path('M20 50L40 180').attr({stroke: "#222", fill: "transparent", strokeWidth: 1});
//
// paper.click(function(e){
//     if(e.target.tagName == 'svg'){
//         var bigCircle = paper.circle(e.offsetX, e.offsetY, 20);
//         bigCircle.attr(style).drag();
//         var p = paper.path('M'+e.offsetX+' '+e.offsetY+'T40 180').attr({stroke: "#222", fill: "transparent", strokeWidth: 1});
//     }
// });

var activeDragable = null;

var rects = [];

Snap.plugin(function (Snap, Element, Paper, global, Fragment) {
    function dragStart(x, y, e) {
        this.current_transform = this.transform();
    }

    function dragMove(dx, dy, x, y, e) {
        this.transform(this.current_transform+'T'+dx+','+dy);
        this.updatePaths();
    }

    function dragEnd(e) {
        this.current_transform = this.transform();
    }

    function updatePaths() {
        var key;
        for(key in this.paths) {
            this.paths[key][0].attr({"path" : this.getPathString(this.paths[key][1])});
            this.paths[key][0].prependTo(this.paper);
        }
    }

    function getCoordinates() {
        console.log(this.matrix);
        // return [this.offsetX, this.offsetY];
        return [this.matrix.e + (this.node.width.baseVal.value / 2),
          this.matrix.f + (this.node.height.baseVal.value / 2)];
    }
          
    function getPathString(obj) {
        var p1 = this.getCoordinates();
        var p2 = obj.getCoordinates();
        return "M"+p1[0]+","+p1[1]+"L"+p2[0]+","+p2[1];
    }

    function addPath(obj) {
        var id = obj.id;
        var path = this.paper.path(this.getPathString(obj)).attr({fill:'none', stroke:'blue', strokeWidth:1});
        path.prependTo(this.paper);
        this.paths[id] = [path, obj];
        obj.paths[this.id] = [path, this];            
    }
    
    function removePath(obj) {
    		var id = obj.id;
        if (this.paths[id] != null) {
        		this.paths[id][0].remove();
            this.paths[id][1] = null;
            delete this.paths[id];
            
            obj.paths[this.id][1] = null;
            delete obj.paths[this.id];
        }
    }

    Paper.prototype.draggableRect = function (x, y, w, h, id) {
        var rect = this.rect(0,0,w,h).transform("T"+x+","+y);
        rect.paths = {};
        // rect.drag(dragMove, dragStart, dragEnd);
        rect.updatePaths = updatePaths;
        rect.getCoordinates = getCoordinates;
        rect.getPathString = getPathString;
        rect.addPath = addPath;
        rect.removePath = removePath;
        rect.tagId = id;       
        
        rect.attr({'id': id});
        
        rect.click(function(e){
            $('rect.active').removeClass('active');
            rect.addClass('active');
            activeDragable = rect;
        });
        
        rects.push(rect);

        return rect;
    };
});



var paper = Snap("#svg");

paper.click(function(e){
    if(e.target.tagName == 'svg'){
        if(activeDragable != null){
            var rect = paper.draggableRect(e.offsetX, e.offsetY, 40, 40);
        }
    }
});

$('.panel').draggable({
    drag: function(event, ui){
        var rect = $(this).data('rect');
        var rectObj = $('rect#'+rect);
        rectObj.attr('transform', 'matrix(1,0,0,1,'+ui.position.left+','+ui.position.top+')');
        for(var i = 0; i < rects.length; i++){
            var rectObj = rects[i];
            if(rectObj.tagId == rect){
                rectObj.transform('T'+ui.position.left+', '+ui.position.top+'');
                rectObj.updatePaths();
                break;
            }
        }
    },
});

function getRectById(id){
    for(var i = 0; i < rects.length; i++)
        {
            if(rects[i].tagId == id){
                return rects[i];
            }
        }
}

$('[data-role="creator"]').click(function(){
    var currentRect = $(this).parent().parent().data('rect');
    var newPanel = $(this).parent().parent().clone();
    currentRect = getRectById(currentRect);
    var rect = paper.draggableRect(300, 400, 40, 40, 3);
    currentRect.addPath(rect);
    newPanel.css('top', 400);    
    newPanel.css('left', 300);
    newPanel.draggable({
        drag: function(event, ui){
            var rect = $(this).data('rect');
            var rectObj = $('rect#'+rect);
            rectObj.attr('transform', 'matrix(1,0,0,1,'+ui.position.left+','+ui.position.top+')');
            for(var i = 0; i < rects.length; i++){
                var rectObj = rects[i];
                if(rectObj.tagId == rect){
                    rectObj.transform('T'+ui.position.left+', '+ui.position.top+'');
                    rectObj.updatePaths();
                    break;
                }
            }
        },
    });
    newPanel.data('rect', '3');
    $('.content-wrapper').append(newPanel);
});

var rect1 = paper.draggableRect(0,0,40,40, 1);
var rect2 = paper.draggableRect(200,100,40,40, 2);

rect1.addPath(rect2);
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>