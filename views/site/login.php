<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Войти';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

\app\assets\LoginPageAsset::register($this);

?>


<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <!-- Brand logo--><a class="brand-logo" href="javascript:void(0);">
            <img src="/logo.svg" alt="" style="height: 84px; object-fit: contain;">
        </a>
        <!-- /Brand logo-->
        <!-- Left Text-->
        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
            <div class="w-100 px-5 text-center">
<!--                <img class="img-fluid" src="/app-assets/images/pages/login-v2.svg" alt="Login V2">-->
                <h3>Уважаемый пользователь!</h3>
                <h3>Сайт находится в режиме beta-тестирования</h3>
                <h3>По возможным ошибкам присылайте на info@stampato.ru</h3>
            </div>
        </div>
        <!-- /Left Text-->
        <!-- Login-->
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <h2 class="card-title font-weight-bold mb-1">Добро пожаловать!</h2>
                <p class="card-text mb-2">Пожалуйста введите свои данные для авторизации</p>
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
                <?= $form
                    ->field($model, 'username', $fieldOptions1)
                    ->label(false)
                    ->textInput(['placeholder' => 'Логин']) ?>
                <?= $form
                    ->field($model, 'password', $fieldOptions2)
                    ->label(false)
                    ->passwordInput(['placeholder' => 'Пароль']) ?>

                <div class="form-group d-flex justify-content-between align-items-center">
                    <div class="text-left">
                        <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомнить меня') ?>
                    </div>
                </div>
                <button class="btn btn-primary btn-block waves-effect waves-float waves-light" tabindex="4">Войти</button>
                <?php ActiveForm::end() ?>


                <p class="text-center mt-2">
                    <span>Нет аккаунта?</span>
                    <?= Html::a('<span>Зарегистрироваться</span>', ['site/register']) ?>
                </p>

            </div>
        </div>
        <!-- /Login-->
    </div>
</div>