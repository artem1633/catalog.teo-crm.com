<?php

use app\models\City;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model \app\models\RegisterForm
 */

$this->title = 'Регистрация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];
$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-earphone form-control-feedback'></span>"
];
$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
$fieldOptions4 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-asterisk form-control-feedback'></span>"
];
$fieldOptions5 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-phone form-control-feedback'></span>"
];
$fieldOptions6 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon glyphicon-ruble form-control-feedback'></span>"
];

\app\assets\LoginPageAsset::register($this);

$cities = City::find()->joinWith(['region'])->all();

$citiesData = [];

foreach ($cities as $city)
{
    $citiesData[] = ArrayHelper::getValue($city, 'region.name').' / '.$city->name;
}

?>
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <!-- Brand logo--><a class="brand-logo" href="javascript:void(0);">
            <img src="/logo.svg" alt="" style="height: 84px; object-fit: contain;">
        </a>
        <!-- /Brand logo-->
        <!-- Left Text-->
        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
            <div class="w-100 px-5 text-center">
                <!--                <img class="img-fluid" src="/app-assets/images/pages/login-v2.svg" alt="Login V2">-->
                <h3>Уважаемый пользователь!</h3>
                <h3>Сайт находится в режиме beta-тестирования</h3>
                <h3>По возможным ошибкам присылайте на info@stampato.ru</h3>
            </div>
        </div>
        <!-- /Left Text-->
        <!-- Login-->
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <h2 class="card-title font-weight-bold mb-1">Добро пожаловать!</h2>
                <p class="card-text mb-2">Пожалуйста введите свои данные для регистрации</p>
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
                <div class="hidden">
                    <?= $form
                        ->field($model, 'type', $fieldOptions1)
                        ->label(false)
                        ->textInput(['value' => $model->type]) ?>
                    <?= $form
                        ->field($model, 'companyName', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('companyName'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                    <?= $form
                        ->field($model, 'last_name', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('last_name'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                    <?= $form
                        ->field($model, 'name', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('name'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                    <?= $form
                        ->field($model, 'patronymic', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('patronymic'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                </div>
                <?= $form
                    ->field($model, 'city', $fieldOptions1)
                    ->label(false)
                    ->widget(\kartik\select2\Select2::class, [
                        'data' => $citiesData,
                        'options' => [
                            'placeholder' => $model->getAttributeLabel('city'),
                        ],
                    ]) ?>
                <?= $form
                    ->field($model, 'companyName', $fieldOptions1)
                    ->label(false)
                    ->textInput(['placeholder' => $model->getAttributeLabel('companyName'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                <?= $form
                    ->field($model, 'email', $fieldOptions1)
                    ->label(false)
                    ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                <?= $form
                    ->field($model, 'password', $fieldOptions2)
                    ->label(false)
                    ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form
                            ->field($model, 'branchCount', $fieldOptions1)
                            ->label(false)
                            ->textInput(['placeholder' => $model->getAttributeLabel('branchCount'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                    </div>
                </div>

                <?= $form
                    ->field($model, 'phone', $fieldOptions1)
                    ->label(false)
                    ->textInput(['placeholder' => $model->getAttributeLabel('phone'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>

                <?= $form
                    ->field($model, 'contacts', $fieldOptions1)
                    ->label(false)
                    ->textInput(['placeholder' => $model->getAttributeLabel('contacts'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>

                <?= $form
                    ->field($model, 'agree', $fieldOptions1)
                    ->label('Я согласен с '.Html::a('пользовательским соглашением', '/Пользовательское соглашение.pdf', ['target' => '_blank']).' и '.Html::a('политикой конфиденциальности', '/Политика конфиденциальности.pdf', ['target' => '_blank']))
                    ->checkbox() ?>
                <button class="btn btn-primary btn-block waves-effect waves-float waves-light" tabindex="4" onclick="ym(76222834,'reachGoal','registration_user')">Зарегистрироваться</button>
                <?php ActiveForm::end() ?>



            </div>
        </div>
        <!-- /Login-->
    </div>
</div>


