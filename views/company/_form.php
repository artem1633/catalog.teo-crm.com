<?php
use app\models\Company;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */

$model->providers = $model->providers != null ? explode(',', $model->providers) : [];
$model->products = $model->products != null ? explode(',', $model->products) : [];

?>

<div class="company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rate_id')->dropDownList(Company::rateLabels(), ['prompt' => 'Выберите']) ?>

    <?= $form->field($model, 'providers')->widget(\kartik\select2\Select2::class, [
        'data' => [],
        'pluginOptions' => [
            'tags' => true
        ],
        'options' => [
            'multiple' => true,
        ],
    ])?>

    <?= $form->field($model, 'products')->widget(\kartik\select2\Select2::class, [
        'data' => [],
        'pluginOptions' => [
            'tags' => true
        ],
        'options' => [
            'multiple' => true,
        ],
    ])?>

    <?= $form->field($model, 'type')->dropDownList([
        Company::TYPE_BUYER => "Покупатель",
        Company::TYPE_PROVIDER => "Компания",
    ])?>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>