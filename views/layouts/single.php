<?php
use app\models\Accessories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Price;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);


$appContentStyle = '';

if(isset($this->params['filterActive'])){
    if($this->params['filterActive']){
        $appContentStyle = "margin-left: 9% !important;";
    }
}


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  menu-collapsed blank-page blank-page  pace-done">

<?php $this->beginBody() ?>

<style>
    body.vertical-layout.vertical-menu-modern.menu-collapsed .navbar.fixed-top {
        left: 0 !important;
        width: 100%;
    }
    body.vertical-layout.vertical-menu-modern.menu-collapsed .navbar.fixed-top a {
        display: inline-block;
        height: 64px;
        padding: 21px 1%;
        letter-spacing: 0;
    }

    body.vertical-layout.vertical-menu-modern.menu-collapsed .navbar.fixed-top a.active,
    body.vertical-layout.vertical-menu-modern.menu-collapsed .navbar.fixed-top a:hover {
        background: #009fe3;
        color: #fff;
        transition: .25s all;
    }

    #filter-toggle-btn:hover {
        background: #009fe3;
        color: #fff !important;
        transition: .25s all;
    }

    #menu-report a {
        color: #2C2C2C !important;
    }

    #menu-report a.active {
        color: #fff !important;
        background: #009fe3!important;
    }
    #menu-report a:active {
        color: #fff !important;
        background: #009fe3!important;
    }
    #menu-report a:hover {
        color: #fff !important;
        background: #009fe3!important;
    }

    #menu-report a.disabled:hover {
        color: #fff !important;
        background: #cecece!important;
    }

    body.vertical-layout.vertical-menu-modern .main-menu {
         width: 5%;
    }
</style>

<nav class="header-navbar navbar navbar-expand-lg align-items-center navbar-light navbar-shadow fixed-top" style="text-align: center;">
    <div id="header-menu-logo" class="d-flex justify-content-left align-items-center" style="float: left; position: absolute; left: <?php if($this->params['filterEnable']) { if($this->params['filterActive']){echo '15';}else{echo '5';}  } else { echo '1'; } ?>%;">
        <div class="mr-1">
            <img src="/logo.svg" height="52" style="object-fit: cover;">
        </div>
    </div>
    <div id="menu-report" style="width: 100%; letter-spacing: -4px;">
        <a href="<?= Url::toRoute(['report/params']) ?>">К ценам</a>
        <a <?= Yii::$app->controller->action->id == 'contacts' ? 'class="active"' : '' ?> href="<?= Url::toRoute(['report/contacts', 'providerId' => $this->params['providerId']]) ?>">Контакты</a>
        <a <?= Yii::$app->controller->action->id == 'services' ? 'class="active"' : '' ?> href="<?= Url::toRoute(['report/services', 'providerId' => $this->params['providerId']]) ?>">Услуги</a>

        <?php

        $headers = ArrayHelper::getColumn(Accessories::find()
            ->where([
                'id' => ArrayHelper::getColumn(Price::find()->where(['company_id' => $this->params['providerId'], 'is_order' => 0])->all(), 'accessory_id'),
            ])
            ->all(), 'accessories_id');

        $headers = ArrayHelper::getColumn(Accessories::find()
            ->where([
                'id' => ArrayHelper::getColumn(Accessories::find()->where(['id' => $headers])->all(), 'accessories_id'),
            ])
            ->all(), 'id');
        ?>
        <?php if(count($headers) > 0): ?>
            <a <?= Yii::$app->controller->action->id == 'single-page' ? 'class="active"' : '' ?> href="<?= Url::toRoute(['report/single-page', 'providerId' => $this->params['providerId']]) ?>">Цены</a>
        <?php else: ?>
            <a href="#" class="disabled" style="background: #cecece;">Цены</a>
        <?php endif;  ?>
        <a <?= Yii::$app->controller->action->id == 'gallery' ? 'class="active"' : '' ?> href="<?= Url::toRoute(['report/gallery', 'providerId' => $this->params['providerId']]) ?>">Портфолио</a>
    </div>
    <?php
    $providerId = null;
    $branch = null;
    $providerCompany = null;
    if(isset($_GET['providerId'])){
        $providerId = $_GET['providerId'];
        $providerCompany = \app\models\Company::findOne($_GET['providerId']);
        $adminUser = \app\models\User::find()->where(['company_id' => $_GET['providerId'], 'is_company_super_admin' => true])->one();
        if($adminUser){
            $branch = \app\models\Branch::findOne($adminUser->branch_id);
        }
    }
    ?>
    <?php if($providerId): ?>
        <?php

        if($providerCompany->avatar){
            $path = "/{$providerCompany->avatar}";
        } else {
            $path = "/no-photo-available.png";
        }

        ?>
        <div class="d-flex justify-content-left align-items-center" style="float: right; position: absolute; right: 13px;">
            <div class="avatar  mr-1 hidden">
                <img src="<?=$path?>" width="52" height="52" style="object-fit: cover;">
            </div>
            <div class="d-flex flex-column">
                <span class="emp_name text-truncate font-weight-bold"><?=$providerCompany->name?></span>
            </div>
        </div>
    <?php endif; ?>
</nav>
<?php if($this->params['filterEnable']): ?>
    <?php if($this->params['filterActive']): ?>
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow expanded" data-scroll-to-active="true" style="position: fixed; top: 0; touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); overflow: auto;">
            <a id="close-menu-btn" href="#" style="display: inline-block;width: 20%;padding: 10px; background: #009fe3;color: #fff;text-align: center;font-size: 17px; position: absolute; right: 0; top: 0;">X</a>
            <div class="menu-content" style="margin-top: 40%;"><?= isset($this->params['filterContent']) ? $this->params['filterContent'] : null ?>
            </div>
            <a id="filter-toggle-btn" href="#" style="display: none; margin-top: 40vh; font-size: 27px; padding: 27px; color: #009fe3;"><i class="feather icon-filter"></i></a>
        </div>
    <?php else: ?>
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" style="overflow: auto; position: fixed; top: 0; touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
            <a id="close-menu-btn" href="#" style="display: none;width: 20%;padding: 10px; background: #009fe3;color: #fff;text-align: center;font-size: 17px; position: absolute; right: 0; top: 0;">X</a>
            <div class="menu-content" style="display: none; margin-top: 40%;"><?= isset($this->params['filterContent']) ? $this->params['filterContent'] : null ?>
            </div>
            <a id="filter-toggle-btn" href="#" style="display: inline-block; margin-top: 40vh; font-size: 27px; color: rgb(0, 159, 227); width: 100%; text-align: center; line-height: 70px;
}"><i class="feather icon-filter"></i></a>
        </div>
    <?php endif; ?>
<?php endif; ?>

<div class="app-content content" <?php

if($this->params['filterEnable'] == true){
    if($this->params['filterActive']){
        echo 'style="margin-left: 14% !important; margin-top: 72px;"';
    } else {
        echo 'style="margin-left: 4% !important; margin-top: 72px;"';
    }
} else {
    echo 'style="margin-left: 0; margin-top: 72px;"';
}


?>>
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <?= $content ?>
        </div>
    </div>
</div>


<?php

$cookies = Yii::$app->request->cookies;


?>

<?php if($cookies->has('cookies_agree') == false): ?>
    <div class="footerline">
        <p>Мы используются <a href="/Политика использования файлов cookie.pdf" target="_blank">cookie</a>, чтобы сделать сайт удобнее. Продолжая навигацию по сайту, вы соглашаетесь с этим. <i class="feather icon-x-circle" style="font-size: 20px; cursor: pointer;" onclick="$.get('/site/agree-cookies', function() {  }); $(this).parent().hide();"></i></p>
    </div>
<?php endif; ?>


<?php $this->endBody() ?>

<script>

    $("#filter-toggle-btn").click(function(event){
        event.preventDefault();

        $('.main-menu').addClass('expanded');
        $(this).hide();
        $('.menu-content').show();
        $('.app-content').attr("style", "margin-left: 14% !important; margin-top: 72px;");
        $('#header-menu-logo').attr("style", "float: left; position: absolute; left: 15%;");
        $('#close-menu-btn').css('display', 'inline-block');
    });

    $('#close-menu-btn').click(function(event){
        event.preventDefault();

        $('.main-menu').removeClass('expanded');
        $(this).hide();
        $('.menu-content').hide();
        $('.app-content').attr("style", "margin-left: 4% !important; margin-top: 72px;");
        $('#header-menu-logo').attr("style", "float: left; position: absolute; left: 5%;");
        $('#filter-toggle-btn').css('display', 'inline-block');
    });

</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(76222834, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/76222834" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-NR8LM5ZDS4"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-NR8LM5ZDS4');
</script>

</body>
</html>
<?php $this->endPage() ?>
