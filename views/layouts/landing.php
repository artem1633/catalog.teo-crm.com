<!DOCTYPE html>
<html data-wf-page="60994219be79f82113092f6f" data-wf-site="607846553e08cd2e3af7159d" data-wf-status="1">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5SPLMGB');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8" />
    <title>Stampato &#8212; Первый полиграфический кибермаркет</title>
    <meta content="Home v2" property="og:title" />
    <meta content="Home v2" property="twitter:title" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Webflow" name="generator" />
    <link href="607846553e08cd2e3af7159d/css/stampato.webflow.f40a87fbb.css" rel="stylesheet" type="text/css" />
    <script src="ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">
        WebFont.load({
            google: {
                families: [
                    "Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic",
                    "Merriweather:300,300italic,400,400italic,700,700italic,900,900italic",
                ],
            },
        });
    </script>
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif]-->
    <script type="text/javascript">
        !(function (o, c) {
            var n = c.documentElement,
                t = " w-mod-";
            (n.className += t + "js"), ("ontouchstart" in o || (o.DocumentTouch && c instanceof DocumentTouch)) && (n.className += t + "touch");
        })(window, document);
    </script>
    <link href="607846553e08cd2e3af7159d/609d36e98882df8c0e6781c8_favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="607846553e08cd2e3af7159d/609d372f18b9d52132eb6d28_favicon.ico" rel="apple-touch-icon" />
    <meta name="robots" content="noindex, nofollow" />
    <meta name="author" content="wtw" />
    <!-- HEAD CODE -->
    <script src="//code-ya.jivosite.com/widget/25HluUCUhH" async></script>
</head>
<body class="body">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SPLMGB"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="s1-v2">
    <div class="container">
        <div class="logo-cont">
            <div class="logo-block">
                <a href="" class="logo-go-home w-inline-block"><img src="607846553e08cd2e3af7159d/609d39ed2d7a61e09c97a6dd_logo.svg" loading="lazy" width="270" alt="" class="logo-v2" /></a>
            </div>
            <div class="logo-text">
                <div class="div-col-bulk-1"></div>
                <div class="div-col-cont-1">
                    <h1 class="heading-footer-v2">Первый интерактивный справочник полиграфической продукции<br /></h1>
                    <div class="text-block-6">С нами можно быстро найти, наглядно сравнить и заказать на самых выгодных условиях.</div>
                </div>
            </div>
        </div>
        <img
            src="607846553e08cd2e3af7159d/609bb4e936a526e7927fb44e_Group 1847@2x.png"
            loading="lazy"
            width="1198"
            sizes="100vw"
            srcset="607846553e08cd2e3af7159d/609bb4e936a526e7927fb44e_Group 1847@2x-p-500.png 500w, 607846553e08cd2e3af7159d/609bb4e936a526e7927fb44e_Group 1847@2x-p-800.png 800w, 607846553e08cd2e3af7159d/609bb4e936a526e7927fb44e_Group 1847@2x-p-1080.png 1080w, 607846553e08cd2e3af7159d/609bb4e936a526e7927fb44e_Group 1847@2x-p-1600.png 1600w, 607846553e08cd2e3af7159d/609bb4e936a526e7927fb44e_Group 1847@2x-p-2000.png 2000w, 607846553e08cd2e3af7159d/609bb4e936a526e7927fb44e_Group 1847@2x.png 2497w"
            alt=""
            class="image-2"
        />
    </div>
</div>
<div id="s3" class="s2-v2">
    <div class="container">
        <div class="list-cont">
            <h3 class="h2-v2">Как это работает<br /></h3>
            <div class="srv-block-tabs">
                <div class="srv-col">
                    <div class="srv-cont-btn">
                        <div class="btn small space">
                            <div class="text-btn small space">Пользователям<br /></div>
                        </div>
                    </div>
                    <div class="srv-cont-1">
                        <h3 class="text-serv">Ищите услуги<br /></h3>
                        <div class="text-v2">Любые – от печати визиток до изготовления баннеров.<br /></div>
                    </div>
                    <div class="srv-cont-1">
                        <h3 class="text-serv">Сравнивайте цены<br /></h3>
                        <div class="text-v2">Запрашивать прайс и ждать не нужно – цифры доступны сразу.<br /></div>
                    </div>
                    <div class="srv-cont-1">
                        <h3 class="text-serv">Оформляйте заказ<br /></h3>
                        <div class="text-v2">У нас есть чат – там можно обсудить детали с представителем выбранной компании.<br /></div>
                    </div>
                </div>
                <div class="srv-col">
                    <div class="srv-cont-btn">
                        <div class="btn red-sp small space">
                            <div class="text-btn small space">Типографиям<br /></div>
                        </div>
                    </div>
                    <div class="srv-cont-1">
                        <h3 class="text-serv">Добавляйте услуги<br /></h3>
                        <div class="text-v2">Продукцию найдут по характеристикам, которые вы укажете при добавлении.<br /></div>
                    </div>
                    <div class="srv-cont-1">
                        <h3 class="text-serv">Размещайте цены<br /></h3>
                        <div class="text-v2">Для этого у нас есть удобный конструктор прайс-листов.<br /></div>
                    </div>
                    <div class="srv-cont-1">
                        <h3 class="text-serv">Обрабатывайте заказы<br /></h3>
                        <div class="text-v2">Согласуйте детали с клиентами через встроенный чат.<br /></div>
                    </div>
                </div>
            </div>
            <div class="srv-block">
                <div class="srv-col">
                    <a href="/" class="botton-link empty w-inline-block">
                        <div class="btn small space">
                            <div class="text-btn small space">Пользователям<br /></div>
                        </div>
                    </a>
                </div>
                <div class="srv-col">
                    <a href="/" class="botton-link empty w-inline-block">
                        <div class="btn red-sp small space">
                            <div class="text-btn small space">Типографиям<br /></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="srv-block">
                <div class="srv-col">
                    <h3 class="text-serv">Ищите услуги<br /></h3>
                    <div class="text-v2">Любые – от печати этикеток до изготовления баннеров.<br /></div>
                </div>
                <div class="srv-col">
                    <h3 class="text-serv">Добавляйте услуги<br /></h3>
                    <div class="text-v2">Продукцию найдут по характеристикам, которые вы укажите при добавлении.<br /></div>
                </div>
            </div>
            <div class="srv-block">
                <div class="srv-col">
                    <h3 class="text-serv">Сравнивайте цены<br /></h3>
                    <div class="text-v2">Запрашивать прайс и ждать не нужно – цифры доступны сразу.<br /></div>
                </div>
                <div class="srv-col">
                    <h3 class="text-serv">Размещайте цены<br /></h3>
                    <div class="text-v2">Для этого у нас есть удобный конструктор прайс-листов.<br /></div>
                </div>
            </div>
            <div class="srv-block">
                <div class="srv-col">
                    <h3 class="text-serv">Оформляйте заказ<br /></h3>
                    <div class="text-v2">У нас есть чат – там можно обсудить детали с представителем выбранной компании.<br /></div>
                </div>
                <div class="srv-col">
                    <h3 class="text-serv">Обрабатывайте заказы<br /></h3>
                    <div class="text-v2">Согласуйте детали с клиентами через встроенный чат.<br /></div>
                </div>
            </div>
            <h3 class="h2-v2">Почему это удобно<br /></h3>
            <div class="srv-block-tabs">
                <div class="srv-col">
                    <div class="srv-cont-btn">
                        <div class="btn small space">
                            <div class="text-btn small space">Пользователям<br /></div>
                        </div>
                    </div>
                    <div class="srv-cont">
                        <h3 class="text-serv">Экономьте время<br /></h3>
                        <div class="text-v2">Забудьте о многочасовых поисках в интернете – получите всю необходимую информацию на одном экране.<br /></div>
                    </div>
                    <div class="srv-cont">
                        <h3 class="text-serv">Экономьте нервы<br /></h3>
                        <div class="text-v2">Забудьте о бесконечных созвонах и ожидании когда менеджер, наконец, пришлет прайс, ознакомьтесь с ценами сразу.<br /></div>
                    </div>
                    <div class="srv-cont">
                        <h3 class="text-serv">Экономьте деньги<br /></h3>
                        <div class="text-v2">Забудьте о заказах по завышенным ценам – обращайтесь туда, где выгоднее.<br /></div>
                    </div>
                </div>
                <div class="srv-col">
                    <div class="srv-cont-btn">
                        <div class="btn red-sp small space">
                            <div class="text-btn small space">Типографиям<br /></div>
                        </div>
                    </div>
                    <div class="srv-cont">
                        <h3 class="text-serv">Экономьте рекламный бюджет<br /></h3>
                        <div class="text-v2">Получите доступ к «горячей» аудитории, заинтересованной в покупке ваших товаров и услуг.<br /></div>
                    </div>
                    <div class="srv-cont">
                        <h3 class="text-serv">Увеличивайте продажи<br /></h3>
                        <div class="text-v2">Наши пользователи ищут вашу компанию – вы получаете стабильный поток клиентов с выявленной потребностью.<br /></div>
                    </div>
                    <div class="srv-cont">
                        <h3 class="text-serv">Забирайте своих клиентов<br /></h3>
                        <div class="text-v2">Размещайте прайс в разделе «Цены» и получайте целевые переходы из запущенной нами контекстной рекламы. За рекламу платим мы, а клиентов получаете вы. <br /></div>
                    </div>
                </div>
            </div>
            <div class="srv-block">
                <div class="srv-col">
                    <a href="/" class="botton-link empty w-inline-block">
                        <div class="btn small space">
                            <div class="text-btn small space">Пользователям<br /></div>
                        </div>
                    </a>
                </div>
                <div class="srv-col">
                    <a href="/" class="botton-link empty w-inline-block">
                        <div class="btn red-sp small space">
                            <div class="text-btn small space">Типографиям<br /></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="srv-block">
                <div class="srv-col">
                    <h3 class="text-serv">Экономьте время<br /></h3>
                    <div class="text-v2">Забудьте о многочасовых поисках в интернете – получите всю необходимую информацию на одном экране.<br /></div>
                </div>
                <div class="srv-col">
                    <h3 class="text-serv">Экономьте рекламный бюджет<br /></h3>
                    <div class="text-v2">Получите доступ к «горячей» аудитории, заинтересованной в покупке ваших товаров и услуг.<br /></div>
                </div>
            </div>
            <div class="srv-block">
                <div class="srv-col">
                    <h3 class="text-serv">Экономьте нервы<br /></h3>
                    <div class="text-v2">Забудьте о бесконечных созвонах и ожидании, когда менеджер наконец пришлет прайс –ознакомьтесь с ценами сразу.<br /></div>
                </div>
                <div class="srv-col">
                    <h3 class="text-serv">Увеличивайте продажи<br /></h3>
                    <div class="text-v2">Наши пользователи ищут вашу компанию – вы получаете стабильный поток клиентов с выявленной потребностью.<br /></div>
                </div>
            </div>
            <div class="srv-block">
                <div class="srv-col">
                    <h3 class="text-serv">Экономьте деньги<br /></h3>
                    <div class="text-v2">Забудьте о заказах по завышенным ценам – обращайтесь туда, где выгоднее.<br /></div>
                </div>
                <div class="srv-col">
                    <h3 class="text-serv">Забирайте своих клиентов<br /></h3>
                    <div class="text-v2">Размещайте прайс в разделе «Цены» и получайте целевые переходы из запущенной нами контекстной рекламы. За рекламу платим мы, а клиентов получаете вы. <br /></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="s4">
    <div class="container">
        <div class="srv-btn-block">
            <div class="srv-col"><a href="#" target="_blank" class="btn-srv blue">Перейти в каталог</a></div>
            <div class="srv-col"><a href="https://docs.google.com/forms/d/e/1FAIpQLSfS9Ue44rlaCjZ44ronDP2k70cYwbL94oI7EYA9oyChxTMFCw/viewform" target="_blank" class="btn-srv pink">Добавить компанию</a></div>
        </div>
    </div>
</div>
<div class="s5">
    <div class="container">
        <h1 class="heading-footer-v2">С чего все начиналось<span class="dot-b">.</span><span class="dot-r">.</span><span class="dot-y">.</span><br /></h1>
        <div class="text-block-footer">
            <div class="text-footer-v2">Мы сами часто заказывали полиграфию.</div>
            <div class="text-footer-v2">
                Сначала перелопачивали страницы поисковиков, заходя на каждый сайт и выуживая нужную информацию. Потом выбирали несколько типографий и всем писали. Потом очень долго ждали и понимали, что лучше звонить. Потом
                созванивались, уточняли, перезванивали, ждали, пока менеджер рассчитает заказ...
            </div>
            <div class="text-footer-v2">А потом нам надоело и мы запустили сервис Stampato :)</div>
            <div class="text-footer-v2">Удобный, простой и без лишней бюрократии. На который не надо тратить кучу времени и нервов. Доступный для всех, продуманный как для себя.</div>
        </div>
    </div>
</div>
<div class="s7">
    <div class="container">
        <div class="text-block-footer">
            <div class="text-footer-v2-cr">
                ВНИМАНИЕ! Данные, представленные на сайте Stampato.ru, носят исключительно информационный характер и не являются публичной офертой, определяемой положением Статьи 437 Гражданского кодекса Российской Федерации.
                Стоимость услуг наших партнеров является справочной информацией.
            </div>
            <div class="text-footer-v2-cr">ООО «Стампато» @ 2021. Все права защищены.</div>
        </div>
    </div>
</div>
<script src="js/jquery-3.5.1.min.dc5e7f18c8.js?site=607846553e08cd2e3af7159d" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="607846553e08cd2e3af7159d/js/webflow.5729dfdbc.js" type="text/javascript"></script>
<!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
<!-- FOOTER CODE -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(76222834, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/76222834" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
