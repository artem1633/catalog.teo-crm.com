<?php
use yii\helpers\Html;
use app\models\Companies;

/* @var $this \yii\web\View */
/* @var $content string */




if (Yii::$app->controller->action->id === 'login') {
    /**
     * Do not use this code in your template. Remove it.
     * Instead, use the code  $this->layout = '//main-login'; in your controller.
     */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }


//    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5SPLMGB');</script>
        <!-- End Google Tag Manager -->

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(76222834, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/76222834" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->

        <link rel="icon" href="/favicon.ico" type="image/x-icon">


        <meta charset="<?= Yii::$app->charset ?>"/>
<!--        <meta name="viewport" content="width=device-width, initial-scale=1">-->
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script src="//code-ya.jivosite.com/widget/25HluUCUhH" async></script>
    </head>
    <?php
//    $session = Yii::$app->session;
//    if(isset($session['menu'])){
//        if($session['menu'] == 1){
//            $bodyClass = " menu-expanded";
//            $iconClass = " icon-circle";
//        } else {
//            $bodyClass = " menu-collapsed";
//            $iconClass = " icon-disc";
//        }
//    } else {
//        $bodyClass = " menu-collapsed";
//        $iconClass = " icon-disc";
//    }

    if(isset($this->params['left']) == false){
        $this->params['left'] = 0;
    }

    if(isset($this->params['filterEnable']) == false){
        $this->params['filterEnable'] = false;
    }

    if($this->params['left']){
        if($this->params['left'] == 1){
            $bodyClass = " menu-expanded";
            $iconClass = " icon-circle";
        } else {
            $bodyClass = " menu-collapsed";
            $iconClass = " icon-disc";
        }
    } else {
        $bodyClass = " menu-collapsed";
        $iconClass = " icon-disc";
    }

    if($this->params['filterEnable']){
        $bodyClass = " menu-collapsed";
    }

    $appContentStyle = '';

    if(isset($this->params['filterActive'])){
        if($this->params['filterActive']){
            $appContentStyle = "margin-left: 9% !important;";
        }
    }

    ?>
    <body class="vertical-layout vertical-menu-modern 2-columns navbar-floating footer-static pace-done<?= $bodyClass ?>" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SPLMGB"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php $this->beginBody() ?>


    
<div class="preloader-wrapper">
<div class="a" style="--n: 5">
  <div class="dot" style="--i: 0"></div>
  <div class="dot" style="--i: 1"></div>
  <div class="dot" style="--i: 2"></div>
  <div class="dot" style="--i: 3"></div>
  <div class="dot" style="--i: 4"></div>
</div>
</div>

    <?php if($this->params['filterEnable']): ?>
        <?php if($this->params['filterActive']): ?>
            <style>
                body.vertical-layout.vertical-menu-modern.menu-collapsed .header-navbar.floating-nav {
                    width: calc(100vw - (100vw - 100%) - 4.4rem - 250px) !important;
                }
                .content-wrapper {
                    margin-left: 82px !important;
                }
                body.vertical-layout.vertical-menu-modern.menu-collapsed .my-main-menu.expanded {
                    width: 260px;
                }

                body nav.header-navbar {
                    margin-left: 0 !important;
                }
            </style>
        <?php else: ?>
            <style>
                body.vertical-layout.vertical-menu-modern.menu-collapsed .header-navbar.floating-nav {
                    width: calc(100vw - (100vw - 100%) - 4.4rem - 80px) !important;
                }
                .content-wrapper {
                    margin-left: 82px !important;
                }
                body.vertical-layout.vertical-menu-modern.menu-collapsed .my-main-menu.expanded {
                    width: 260px;
                }

                body nav.header-navbar {
                    margin-left: 0 !important;
                }
            </style>
        <?php endif; ?>
    <?php endif; ?>

    <div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div></div>
    <?= $this->render(
        'header.php',
        ['directoryAsset' => $directoryAsset]
    ) ?>



    <?php if($this->params['filterEnable']): ?>
        <?php if($this->params['filterActive']): ?>
            <div class="my-main-menu menu-fixed menu-light menu-accordion menu-shadow expanded" data-scroll-to-active="true" style="z-index: 10; overflow: auto; position: fixed; top: 0; touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                <a id="close-menu-btn" href="#" style="margin-top: 40%; display: inline-block;width: 20%;padding: 10px; background: #009fe3 ;color: #fff;text-align: center;font-size: 17px; position: absolute; right: 0; top: 0;">X</a>
                <div class="menu-content" style="margin-top: 53%;"><?= isset($this->params['filterContent']) ? $this->params['filterContent'] : null ?>
                </div>
                <a id="filter-toggle-btn" href="#" style="display: none; margin-top: 40vh; font-size: 27px; padding: 27px; color: #009fe3;"><i class="feather icon-filter"></i></a>
            </div>
        <?php else: ?>
            <div class="my-main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" style="z-index: 10; overflow: auto; position: fixed; top: 0; touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                <a id="close-menu-btn" href="#" style="margin-top: 40%; display: none;width: 20%;padding: 10px; background: #009fe3;color: #fff;text-align: center;font-size: 17px; position: absolute; right: 0; top: 0;">X</a>
                <div class="menu-content" style="display: none; margin-top: 53%;"><?= isset($this->params['filterContent']) ? $this->params['filterContent'] : null ?>
                </div>
                <a id="filter-toggle-btn" href="#" style="display: inline-block; margin-top: 40vh; font-size: 27px; padding: 27px; color: #009fe3;"><i class="feather icon-filter"></i></a>
            </div>
        <?php endif; ?>

    <?php else: ?>

        <?= $this->render(
            'header-menu.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

    <?php endif; ?>

    <div class="app-content content" style="<?=$appContentStyle?>">

        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>


    <?php

    $cookies = Yii::$app->request->cookies;


    ?>

    <?php if($cookies->has('cookies_agree') == false): ?>
        <div class="footerline">
            <p>Мы используются <a href="/Политика использования файлов cookie.pdf" target="_blank">cookie</a>, чтобы сделать сайт удобнее. Продолжая навигацию по сайту, вы соглашаетесь с этим. <i class="feather icon-x-circle" style="font-size: 20px; cursor: pointer;" onclick="$.get('/site/agree-cookies', function() {  }); $(this).parent().hide();"></i></p>
        </div>
    <?php endif; ?>

    <footer class="footer footer-static footer-light">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">ООО «Стампато» @ 2021. Все права защищены. СПб, ул. Планерная, д.7 (2-й этаж). +7 (812) 934-00-08 info@stampato.ru</span><span class="float-md-right d-none d-md-block"></span>
            <button class="btn btn-primary btn-icon scroll-top waves-effect waves-light" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>


    <?php $this->endBody() ?>


    <script>

        $("#filter-toggle-btn").click(function(event){
            event.preventDefault();

            $('.my-main-menu').addClass('expanded');
            $(this).hide();
            $('.menu-content').show();
            $('.app-content').attr("style", "margin-left: 9% !important");
//            $('.header-navbar').attr("style", "width: calc(100vw - (100vw - 100%) - 4.4rem - 250px) !important;");
            $('.header-navbar').attr("style", "margin-left: 255px;");
            $('#close-menu-btn').css('display', 'inline-block');
        });

        $('#close-menu-btn').click(function(event){
            event.preventDefault();

            $('.my-main-menu').removeClass('expanded');
            $(this).hide();
            $('.menu-content').hide();
            $('.app-content').attr("style", "margin-left: 0 !important");
            $('.header-navbar').attr("style", "margin-left: 73px;");
            $('#filter-toggle-btn').css('display', 'inline-block');
        });

    </script>





    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
