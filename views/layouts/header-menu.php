<?php

use yii\helpers\Url;

if($this->params['left']){
    if($this->params['left'] == 1){
        $hideStyle = '';
    } else {
        $hideStyle = 'display: none; ';
    }
} else {
    $hideStyle = 'display: none; ';
}

?>

<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" style="<?=$hideStyle?>touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); overflow: auto;">
    <div class="shadow-bottom"></div>
    <div class="main-menu-content ps ps--active-y" style="height: 874px !important; overflow: auto !important;;">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <?= isset($this->params['left-content']) ? $this->params['left-content'] : null ?>
        </ul>
        <!--        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 853px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 282px;"></div></div></div>-->
    </div>
</div>