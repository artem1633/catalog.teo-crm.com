<?php

use yii\helpers\Url;

$dashboardActive = Yii::$app->controller->id == 'dashboard' ? ' active' : '';
$orderActive = Yii::$app->controller->id == 'orders' ? ' active' : '';
$clientsActive = Yii::$app->controller->id == 'clients' ? ' open' : '';
$usersActive = Yii::$app->controller->id == 'users' ? ' active' : '';
$atelierActive = Yii::$app->controller->id == 'atelier' ? ' active' : '';
$articlesActive = Yii::$app->controller->id == 'articles' ? ' active' : '';
$clientsGroupActive = Yii::$app->controller->id == 'clients-group' ? ' active' : '';
$advertisingActive = Yii::$app->controller->id == 'advertising' ? ' active' : '';
$productStatusActive = Yii::$app->controller->id == 'product-status' ? ' active' : '';
$orderStatusActive = Yii::$app->controller->id == 'order-status' ? ' active' : '';
$claimStatusesActive = Yii::$app->controller->id == 'claim-statuses' ? ' active' : '';
$usersActive = Yii::$app->controller->id == 'user' ? ' active' : '';
$companyActive = Yii::$app->controller->id == 'company' ? ' active' : '';
$ordersColorActive = Yii::$app->controller->id == 'orders-color' ? ' active' : '';
$typeClothesActive = Yii::$app->controller->id == 'type-clothes' ? ' active' : '';
$quickOrderActive = Yii::$app->controller->id == 'quick-order' ? ' active' : '';
$typePollutionActive = Yii::$app->controller->id == 'type-pollution' ? ' active' : '';
$spotActive = Yii::$app->controller->id == 'spot' ? ' active' : '';
$markingActive = Yii::$app->controller->id == 'marking' ? ' active' : '';
$availableActive = Yii::$app->controller->id == 'available' ? ' active' : '';
$cashboxActive = Yii::$app->controller->id == 'cashbox' ? ' active' : '';
$suppliersActive = Yii::$app->controller->id == 'suppliers' ? ' active' : '';
$listServicesActive = Yii::$app->controller->id == 'list-services' ? ' active' : '';
$productActive = Yii::$app->controller->id == 'product' ? ' active' : '';
$aboutCompanyActive = Yii::$app->controller->id == 'about-company' ? ' active' : '';
$sellingActive = Yii::$app->controller->id == 'selling' ? ' active' : '';
$reportActive = (Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'index') ? ' active' : '';
$reportParamsActive = (Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'params') ? ' active' : '';
$groupActive = Yii::$app->controller->id == 'group' ? ' active' : '';
$manufacturerActive = Yii::$app->controller->id == 'manufacturer' ? ' active' : '';
$accessActive = Yii::$app->controller->id == 'accessories' ? ' active' : '';
$ticketActive = Yii::$app->controller->id == 'ticket' ? ' active' : '';
$templateFields = Yii::$app->controller->id == 'template-fields' ? ' active' : '';
$priceActive = Yii::$app->controller->id == 'price' ? ' active' : '';
$orderActive = Yii::$app->controller->id == 'order' ? ' active' : '';
$myOrderActive = Yii::$app->controller->id == 'my-order' ? ' active' : '';
$branchActive = Yii::$app->controller->id == 'branch' ? ' active' : '';
$actionActive = Yii::$app->controller->id == 'action' ? ' active' : '';
$clientsPersonActive = (Yii::$app->controller->id == 'clients' && Yii::$app->controller->action->id == 'index') ? ' active' : '';
$clientsCompanyActive = (Yii::$app->controller->id == 'clients' && Yii::$app->controller->action->id == 'index-company') ? ' active' : '';


$bookOpen = '';

if(in_array(Yii::$app->controller->id, ['accessories', 'company', 'branch'])){
    $bookOpen = ' open';
}


?>

<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
    <div class="shadow-bottom"></div>
    <div class="main-menu-content ps ps--active-y" style="height: 874px !important; overflow: auto !important;;">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="navigation-header"><span>Меню</span>
            </li>

            <?php if(Yii::$app->user->isGuest == false): ?>
                <?php if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER): ?>
                    <li class="nav-item<?=$reportActive?>">
                        <a href="<?= Url::toRoute(['/report/index']) ?>"><i class="feather icon-file-text"></i><span class="menu-title">Услуги</span></a>
                    </li>
                    <?php if(Yii::$app->user->identity->company->rate_id != \app\models\Company::RATE_NO): ?>
                        <li class="nav-item<?=$reportParamsActive?>">
                            <a href="<?= Url::toRoute(['/report/params']) ?>"><i class="feather icon-file-text"></i><span class="menu-title">Цены</span></a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER || Yii::$app->user->identity->isSuperAdmin()): ?>
                    <li class="nav-item<?=$reportActive?>">
                        <a href="<?= Url::toRoute(['/report/index']) ?>"><i class="feather icon-file-text"></i><span class="menu-title">Услуги</span></a>
                    </li>
                    <li class="nav-item<?=$reportParamsActive?>">
                        <a href="<?= Url::toRoute(['/report/params']) ?>"><i class="feather icon-file-text"></i><span class="menu-title">Цены</span></a>
                    </li>
                <?php endif; ?>
            <?php else: ?>
                <li class="nav-item<?=$reportActive?>">
                    <a href="<?= Url::toRoute(['/report/index']) ?>"><i class="feather icon-file-text"></i><span class="menu-title">Услуги</span></a>
                </li>
                <li class="nav-item<?=$reportParamsActive?>">
                    <a href="<?= Url::toRoute(['/report/params']) ?>"><i class="feather icon-file-text"></i><span class="menu-title">Цены</span></a>
                </li>
            <?php endif; ?>
            <?php if(Yii::$app->user->isGuest == false): ?>
                <?php if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER || Yii::$app->user->identity->isSuperAdmin()): ?>
                    <li class="nav-item<?=$priceActive?>">
                        <a href="<?= Url::toRoute(['/price']) ?>"><i class="feather icon-disc"></i><span class="menu-title">Все прайсы</span></a>
                    </li>
                    <li class="nav-item<?=$actionActive?>">
                        <a href="<?= Url::toRoute(['/action']) ?>"><i class="feather icon-disc"></i><span class="menu-title">Акции</span></a>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
            <?php if(Yii::$app->user->isGuest == false): ?>
                <li class="nav-item<?=$myOrderActive?>">
                    <a href="<?= Url::toRoute(['/my-order']) ?>"><i class="feather icon-disc"></i><span class="menu-title">Мои заказы</span></a>
                </li>
                <?php if(Yii::$app->user->identity->company->rate_id != \app\models\Company::RATE_NO): ?>
                    <li class="nav-item<?=$orderActive?>">
                        <a href="<?= Url::toRoute(['/order']) ?>"><i class="feather icon-disc"></i><span class="menu-title">Заявки</span></a>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
            <?php if(Yii::$app->user->isGuest == false): ?>
                <li class="nav-item<?=$ticketActive?>">
                    <a href="<?= Url::toRoute(['/ticket']) ?>"><i class="feather icon-help-circle"></i><span class="menu-title">Тикеты</span></a>
                </li>
            <?php endif; ?>
            <?php if(Yii::$app->user->isGuest == false): ?>
                <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                    <li class="nav-item has-sub<?=$bookOpen?>"><a href="#"><i class="feather icon-zap"></i><span class="menu-title">Настройки</span></a>
                        <ul class="menu-content" style="">
                            <li class="is-shown<?=$companyActive?>">
                                <a href="<?= Url::toRoute(['/company']) ?>"><i class="feather icon-circle"></i><span class="menu-title">Компании</span></a>
                            </li>
                            <li class="is-shown<?=$usersActive?>">
                                <a href="<?= Url::toRoute(['/user']) ?>"><i class="feather icon-circle"></i><span class="menu-title">Пользователи</span></a>
                            </li>
                            <li class="is-shown<?=$templateFields?>">
                                <a href="<?= Url::toRoute(['/template-fields']) ?>"><i class="feather icon-circle"></i><span class="menu-title">Поля</span></a>
                            </li>
                            <li class="is-shown<?=$accessActive?>"><a href="<?= Url::toRoute(['/accessories']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Категории</span></a>
                            </li>
                            <li class="is-shown<?=$branchActive?>">
                                <a href="<?= Url::toRoute(['/branch']) ?>"><i class="feather icon-circle"></i><span class="menu-title">Филиалы</span></a>
                            </li>
                            <li class="is-shown<?=$groupActive?> hidden"><a href="<?= Url::toRoute(['/group']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Группы пользователей</span></a>
                            </li>
                            <li class="is-shown<?=$listServicesActive?> hidden"><a href="<?= Url::toRoute(['/list-services']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Услуги</span></a>
                            </li>
                            <li class="is-shown<?=$productActive?> hidden"><a href="<?= Url::toRoute(['/product']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Расходники</span></a>
                            </li>
                            <li class="is-shown<?=$atelierActive?> hidden"><a href="<?= Url::toRoute(['/atelier']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Магазины</span></a>
                            </li>
                            <li class="is-shown<?=$suppliersActive?> hidden"><a href="<?= Url::toRoute(['/suppliers']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Поставщики</span></a>
                            </li>
                            <li class="is-shown<?=$manufacturerActive?> hidden"><a href="<?= Url::toRoute(['/manufacturer']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Производители</span></a>
                            </li>
                            <li class="is-shown<?=$typeClothesActive?> hidden"><a href="<?= Url::toRoute(['/type-clothes']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Типы транспорта</span></a>
                            </li>
                            <li class="is-shown<?=$markingActive?> hidden"><a href="<?= Url::toRoute(['/marking']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Марки</span></a>
                            </li>
                            <li class="is-shown<?=$spotActive?> hidden"><a href="<?= Url::toRoute(['/spot']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Модели</span></a>
                            </li>
                            <li class="is-shown<?=$aboutCompanyActive?> hidden"><a href="<?= Url::toRoute(['/about-company/view', 'company_id' => Yii::$app->user->identity->company_id]) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Юридические лица</span></a>
                            </li>
                            <li class="is-shown<?=$clientsGroupActive?> hidden"><a href="<?= Url::toRoute(['/clients-group']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed navbar">Группы клиентов</span></a>
                            </li>
                            <li class="is-shown<?=$advertisingActive?> hidden"><a href="<?= Url::toRoute(['/advertising']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Floating navbar">Реклама</span></a>
                            </li>
                            <li class="is-shown<?=$productStatusActive?> hidden"><a href="<?= Url::toRoute(['/product-status']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Статусы Расходников</span></a>
                            </li>
                            <li class="is-shown<?=$orderStatusActive?> hidden"><a href="<?= Url::toRoute(['/order-status']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Статусы заказов</span></a>
                            </li>
                            <li class="is-shown<?=$claimStatusesActive?> hidden"><a href="<?= Url::toRoute(['/claim-statuses']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Статусы заявок</span></a>
                            </li>
                            <li class="is-shown<?=$ordersColorActive?> hidden"><a href="<?= Url::toRoute(['/orders-color']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Цветовая маркировка</span></a>
                            </li>
                            <li class="is-shown<?=$quickOrderActive?> hidden"><a href="<?= Url::toRoute(['/quick-order']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Срочный заказ</span></a>
                            </li>
                            <li class="is-shown<?=$typePollutionActive?> hidden"><a href="<?= Url::toRoute(['/type-pollution']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Cтепень и тип загрязнения</span></a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
        </ul>
        <!--        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 853px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 282px;"></div></div></div>-->
    </div>
</div>