<?php
use app\models\Branch;
use app\models\ChatMessage;
use app\models\JobList;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\Soglosovaniya;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Url;
use app\models\Orders;
use yii\widgets\Pjax;

$dashboardActive = Yii::$app->controller->id == 'dashboard' ? 'bg-primary' : 'bg-white';
$orderActive = Yii::$app->controller->id == 'orders' ? 'bg-primary' : 'bg-white';
$clientsActive = Yii::$app->controller->id == 'clients' ? ' open' : 'bg-white';
$usersActive = Yii::$app->controller->id == 'users' ? 'bg-primary' : 'bg-white';
$atelierActive = Yii::$app->controller->id == 'atelier' ? 'bg-primary' : 'bg-white';
$articlesActive = Yii::$app->controller->id == 'articles' ? 'bg-primary' : 'bg-white';
$clientsGroupActive = Yii::$app->controller->id == 'clients-group' ? 'bg-primary' : 'bg-white';
$advertisingActive = Yii::$app->controller->id == 'advertising' ? 'bg-primary' : 'bg-white';
$productStatusActive = Yii::$app->controller->id == 'product-status' ? 'bg-primary' : 'bg-white';
$orderStatusActive = Yii::$app->controller->id == 'order-status' ? 'bg-primary' : 'bg-white';
$claimStatusesActive = Yii::$app->controller->id == 'claim-statuses' ? 'bg-primary' : 'bg-white';
$usersActive = Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id != 'chat' ? 'bg-primary' : 'bg-white';
$companyActive = Yii::$app->controller->id == 'company' ? 'bg-primary' : 'bg-white';
$ordersColorActive = Yii::$app->controller->id == 'orders-color' ? 'bg-primary' : 'bg-white';
$typeClothesActive = Yii::$app->controller->id == 'type-clothes' ? 'bg-primary' : 'bg-white';
$quickOrderActive = Yii::$app->controller->id == 'quick-order' ? 'bg-primary' : 'bg-white';
$typePollutionActive = Yii::$app->controller->id == 'type-pollution' ? 'bg-primary' : 'bg-white';
$spotActive = Yii::$app->controller->id == 'spot' ? 'bg-primary' : 'bg-white';
$markingActive = Yii::$app->controller->id == 'marking' ? 'bg-primary' : 'bg-white';
$availableActive = Yii::$app->controller->id == 'available' ? 'bg-primary' : 'bg-white';
$cashboxActive = Yii::$app->controller->id == 'cashbox' ? 'bg-primary' : 'bg-white';
$suppliersActive = Yii::$app->controller->id == 'suppliers' ? 'bg-primary' : 'bg-white';
$listServicesActive = Yii::$app->controller->id == 'list-services' ? 'bg-primary' : 'bg-white';
$productActive = Yii::$app->controller->id == 'product' ? 'bg-primary' : 'bg-white';
$aboutCompanyActive = Yii::$app->controller->id == 'about-company' ? 'bg-primary' : 'bg-white';
$sellingActive = Yii::$app->controller->id == 'selling' ? 'bg-primary' : 'bg-white';
$reportActive = (Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'index') ? 'bg-primary' : 'bg-white';
$reportParamsActive = (Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'params') ? 'bg-primary' : 'bg-white';
$groupActive = Yii::$app->controller->id == 'group' ? 'bg-primary' : 'bg-white';
$manufacturerActive = Yii::$app->controller->id == 'manufacturer' ? 'bg-primary' : 'bg-white';
$accessActive = Yii::$app->controller->id == 'accessories' ? 'bg-primary' : 'bg-white';
$ticketActive = Yii::$app->controller->id == 'ticket' ? 'bg-primary' : 'bg-white';
$templateFields = Yii::$app->controller->id == 'template-fields' ? 'bg-primary' : 'bg-white';
$priceActive = Yii::$app->controller->id == 'price' ? 'bg-primary' : 'bg-white';
$orderActive = Yii::$app->controller->id == 'order' ? 'bg-primary' : 'bg-white';
$myOrderActive = Yii::$app->controller->id == 'my-order' ? 'bg-primary' : 'bg-white';
$branchActive = Yii::$app->controller->id == 'branch' ? 'bg-primary' : 'bg-white';
$actionActive = Yii::$app->controller->id == 'action' ? 'bg-primary' : 'bg-white';
$regionActive = Yii::$app->controller->id == 'region' ? 'bg-primary' : 'bg-white';
$cityActive = Yii::$app->controller->id == 'city' ? 'bg-primary' : 'bg-white';
$faqActive = (Yii::$app->controller->action->id == 'faq' or Yii::$app->controller->id == 'faq') ? 'bg-primary' : 'bg-white';
$clientsPersonActive = (Yii::$app->controller->id == 'clients' && Yii::$app->controller->action->id == 'index') ? 'bg-primary' : 'bg-white';
$clientsCompanyActive = (Yii::$app->controller->id == 'clients' && Yii::$app->controller->action->id == 'index-company') ? 'bg-primary' : 'bg-white';
$userProfileActive = (Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'profile') ? 'bg-primary' : 'bg-white';
$branchActive = (Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'chat') ? 'bg-primary' : 'bg-white';

?>

<style>
    .header-navbar .navbar-container ul.navbar-nav li.dropdown-notification .media-body .media-heading {
        color: #6E6B7B;
        margin-bottom: 0;
        line-height: 1.2;
    }
    .header-navbar .navbar-container ul.navbar-nav li.dropdown-cart .notification-text, .header-navbar .navbar-container ul.navbar-nav li.dropdown-notification .notification-text {
        margin-bottom: .5rem;
        font-size: 15px;
        color: #B9B9C3;
    }

    nav.header-navbar {
        margin-left: 0;
    }



</style>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-lg-block">
                            <div class="d-flex justify-content-start flex-wrap">
                                <a href="/"><img src="/logo.svg" style="height: 35px; margin-top: 4px; margin-right: 12px;" alt=""></a>
                                <?php if(Yii::$app->user->isGuest == false): ?>
                                    <?php if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER): ?>
                                            <a href="<?= Url::toRoute(['user/profile']) ?>" class="text-center <?=$userProfileActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                                <span class="align-middle">Профиль компании</span>
                                            </a>
                                    <?php endif; ?>
                                    <?php if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER || Yii::$app->user->identity->isSuperAdmin()): ?>
                                        <a class="text-center bg-white colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow" href="<?=Url::toRoute(['user/send-request'])?>" style="padding: -4px 9px !important;"><span class="align-middle">Оставить заявку</span></a>

                                        <a href="<?= Url::toRoute(['report/params']) ?>" class="text-center <?=$reportParamsActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Все цены типографий</span>
                                        </a>
                                        <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                                        <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['faq/index']) ?>" class="text-center <?=$faqActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">FAQ</span>
                                        </a>
                                        <?php elseif(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER): ?>
                                            <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['user/faq']) ?>" class="text-center <?=$faqActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">FAQ</span>
                                        </a>
                                        <?php endif; ?>
                                        <a href="<?= Url::toRoute(['report/index']) ?>" class="text-center <?=$reportActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Каталог типографий</span>
                                        </a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a href="<?= Url::toRoute(['report/index']) ?>" class="text-center <?=$reportActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                        <span class="align-middle">Каталог типографий</span>
                                    </a>
                                    <a href="<?= Url::toRoute(['report/params']) ?>" class="text-center <?=$reportParamsActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                        <span class="align-middle">Все цены типографий</span>
                                    </a>
                                <?php endif; ?>
                                <?php if(Yii::$app->user->isGuest == false): ?>
                                    <?php if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER || Yii::$app->user->identity->isSuperAdmin()): ?>
                                        <a href="<?= Url::toRoute(['price/index']) ?>" class="text-center <?=$priceActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Цены</span>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <a href="<?= Url::toRoute(['action/index']) ?>" style="display: none !important;" class="text-center <?=$actionActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                    <span class="align-middle">Акции</span>
                                </a>
                                <?php if(Yii::$app->user->isGuest == false): ?>
                                <a href="<?= Url::toRoute(['my-order/index']) ?>" class="text-center <?=$myOrderActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                    <span class="align-middle">Заказы</span>
                                </a>
                                    <a href="<?= Url::toRoute(['order/index']) ?>" style="display: none !important;" class="text-center <?=$orderActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                        <span class="align-middle">Заявки</span>
                                    </a>
                                <?php endif; ?>
                                <?php if(Yii::$app->user->isGuest == false): ?>
                                    <a href="<?= Url::toRoute(['ticket/index']) ?>" style="display: none !important;" class="text-center <?=$ticketActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                        <span class="align-middle">Тикеты</span>
                                    </a>
                                <?php endif; ?>
                                <?php if(Yii::$app->user->isGuest == false): ?>
                                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                                        <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['company/index']) ?>" class="text-center <?=$companyActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Компании</span>
                                        </a>
                                        <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['user/index']) ?>" class="text-center <?=$usersActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Пользователи</span>
                                        </a>
                                        <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['template-fields/index']) ?>" class="text-center <?=$templateFields?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Поля</span>
                                        </a>
                                        <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['accessories/index']) ?>" class="text-center <?=$accessActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Категории</span>
                                        </a>
                                        <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['region/index']) ?>" class="text-center <?=$regionActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Регионы</span>
                                        </a>
                                        <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['city/index']) ?>" class="text-center <?=$cityActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle">Города</span>
                                        </a>
                                    <?php endif; ?>
                                    <a aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['user/chat']) ?>" class="text-center <?=$branchActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow" <?=$branchActive == 'bg-primary' ? "style='background-color: #009fe3 !important;'" : '' ?> >
                                        <span class="align-middle">Чат</span>
                                    </a>
                                <?php endif; ?>
                                <?php if(Yii::$app->user->isGuest): ?>
                                    <a class="text-center bg-white colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow" href="<?=Url::toRoute(['user/send-request'])?>" style="padding: -4px 9px !important;"><span class="align-middle">Оставить заявку</span></a>
                                    <a  aria-haspopup="true" aria-expanded="false" href="<?= Url::toRoute(['user/faq']) ?>" class="text-center <?=$faqActive?> colors-container text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                        <span class="align-middle">FAQ</span>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <!--   option Chat-->
                            <!--   option email-->
                            <!--   option todo-->
                            <!--   option Calendar-->
                        </li>
                    </ul>
                </div>
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                <ul class="nav navbar-nav float-right">
                            <li class="dropdown dropdown-notification nav-item">
                                <?php Pjax::begin(['id' => 'user-city-container']) ?>

                                <?php

                                $city = Yii::$app->session->get('user__city');
                                $metro = Yii::$app->session->get('user__metro');

                                $output = [];

                                if($city){
                                    $city = \app\models\City::findOne($city);
                                    if($city){
                                        $output[] = $city->name;
                                    }
                                }

                                if($metro){
                                    $metro = \app\models\Metro::findOne($metro);
                                    if($metro){
                                        $output[] = $metro->name;
                                    }
                                }

                                if($output){
                                    echo Html::a(implode(' | ', $output), ['user/edit-city'], ['class' => 'nav-link nav-link-label', 'role' => 'modal-remote']);
                                } else {
                                    echo Html::a('<i>Указать город</i>', ['user/edit-city'], ['class' => 'nav-link nav-link-label', 'role' => 'modal-remote']);
                                }

                                ?>

                                <?php Pjax::end() ?>
                            </li>
                    <?php if(Yii::$app->user->isGuest == false): ?>
                    <li class="nav-item dropdown dropdown-notification mr-25">
                        <?php

                        $users = User::find()->where(['!=', 'user.id', Yii::$app->user->getId()])->joinWith(['company'])->asArray()->all();

                        array_walk($users, function(&$user){

                            $lastMessage = ChatMessage::find()->where([
                                'or',
                                ['sender_id' => Yii::$app->user->getId(), 'receiver_id' => $user['id']],
                                ['sender_id' => $user['id'], 'receiver_id' => Yii::$app->user->getId()]
                            ])->orderBy('created_at desc')->one();

                            $branch = Branch::findOne($user['branch_id']);

                            $user['avatar'] = $user['company']['avatar'];
                            $user['branch_name'] = $branch ? $branch->name : null;

                            $user['unread_messages'] = ChatMessage::find()->where(['receiver_id' => Yii::$app->user->getId(), 'sender_id' => $user['id'], 'is_read' => false])->count();

                            if($lastMessage){
                                $user['last_message_content'] = $lastMessage->content;
                                $user['last_message_datetime'] = $lastMessage->created_at;
                            } else {
                                $user['last_message_content'] = " — ";
                                $user['last_message_datetime'] = null;
                            }

                        });


                        $users = array_filter($users, function($model){
                            return $model['unread_messages'] > 0;
                        });


                        ?>
                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon feather icon-bell"></i><?= (count($users) ? "<span class=\"badge badge-pill badge-danger badge-up\">".count($users)."</span>" : null) ?>

                            <!--                                <span class="badge badge-pill badge-primary badge-up">5</span>-->
                        </a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex" style="margin-top: unset;">
                                    <h4 class="notification-title mb-0 mr-auto">Уведомления</h4>
                                </div>
                            </li>
                            <li class="scrollable-container media-list ps">
                                    <?php foreach ($users as $user): ?>
                                        <a style="padding: 0 !important;" class="d-flex" href="<?= Url::toRoute(['user/chat', 'userId' => $user['id']]) ?>">
                                            <div class="media d-flex align-items-start">
                                                <div class="media-left">
                                                    <div class="avatar"><img src="/<?= ($user['avatar'] ? $user['avatar'] : 'img/nouser.png') ?>" alt="avatar" width="32" height="32"></div>
                                                </div>
                                                <div class="media-body">
                                                    <p class="media-heading">
                                                        <span class="font-weight-bolder"><?= $user['branch_name'] ?></span></p>
                                                        <small class="notification-text"> <?= $user['last_message_content'] ?></small>
                                                </div>
                                            </div>
                                        </a>
                                    <?php endforeach; ?>
                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></li>
                            <li class="dropdown-menu-footer"></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if(Yii::$app->user->isGuest == false): ?>

                        <li class="dropdown dropdown-notification nav-item open">
                            <a class="nav-link nav-link-label" href="<?= Url::toRoute(['user/profile']) ?>" style="margin-top: 3px; margin-right: 19px;"><i class="feather icon-user"></i></a>
                        </li>
                        <li class="dropdown dropdown-notification nav-item open">
                            <a class="nav-link nav-link-label" href="<?= Url::toRoute(['site/logout']) ?>" data-method="POST" style="margin-top: 3px; margin-right: 19px;"><i class="feather icon-power"></i> Выйти</a>
                        </li>

                    <?php else: ?>
                        <li class="dropdown dropdown-notification nav-item open">
                                <a class="nav-link nav-link-label" href="<?= Url::toRoute(['site/login']) ?>" style="margin-top: 3px; margin-right: 19px;"><i class="feather icon-log-in"></i> Войти</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</nav>
