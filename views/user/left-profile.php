<?php

use yii\helpers\Url;

/**
 * @param string $url
 * @return string
 */
function activeClass($url)
{
    $url = explode('/', $url);

    if(count($url) != 2)
        return null;

    $controllerId = $url[0];
    $actionId = $url[1];

    return Yii::$app->controller->id == $controllerId && Yii::$app->controller->action->id == $actionId ? " active" : null;
}


?>
<?php if(Yii::$app->user->isGuest == false): ?>
    <?php if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER): ?>
        <li class="nav-item<?=activeClass('user/profile')?>">
            <a href="<?= Url::toRoute(['/user/profile']) ?>"><span class="menu-title">Администратор</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/description')?>">
            <a href="<?= Url::toRoute(['/user/description']) ?>"><span class="menu-title">Описание</span></a>
        </li>
        <div class="hidden">
            <li class="nav-item">
                <a href="<?= Url::toRoute(['/user/requisites']) ?>"><span class="menu-title">Реквизиты</span></a>
            </li>
            <li class="nav-item">
                <a href="<?= Url::toRoute(['/branch/work-hours']) ?>"><span class="menu-title">Часы работы</span></a>
            </li>
            <li class="nav-item">
                <a href="<?= Url::toRoute(['/action']) ?>"><span class="menu-title">Соцсети</span></a>
            </li>
            <li class="nav-item">
                <a href="<?= Url::toRoute(['/user/accessories']) ?>"><span class="menu-title">Основные услуги</span></a>
            </li>
            <li class="nav-item">
                <a href="<?= Url::toRoute(['/user/accessories-other']) ?>"><span class="menu-title">Другие услуги</span></a>
            </li>
            <li class="nav-item">
                <a href="<?= Url::toRoute(['/price/index']) ?>"><span class="menu-title">Мои прайсы</span></a>
            </li>
        </div>
        <li class="nav-item<?=activeClass('user/accessories')?>">
            <a href="<?= Url::toRoute(['/user/accessories']) ?>"><span class="menu-title">Услуги</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/accessories-other')?>">
            <a href="<?= Url::toRoute(['/user/accessories-other']) ?>"><span class="menu-title">Другие услуги</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/gallery')?>">
            <a href="<?= Url::toRoute(['/user/gallery']) ?>"><span class="menu-title">Галерея</span></a>
        </li>
        <li class="nav-item<?=activeClass('branch/index')?>">
            <a href="<?= Url::toRoute(['/branch/index']) ?>"><span class="menu-title">Филиалы</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/index')?>">
            <a href="<?= Url::toRoute(['/user/index']) ?>"><span class="menu-title">Пользователи</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/tariff')?>" style="display: none;">
            <a href="<?= Url::toRoute(['/user/tariff']) ?>"><span class="menu-title">Тарифы</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/faq')?>">
            <a href="<?= Url::toRoute(['/user/faq']) ?>"><span class="menu-title">FAQ</span></a>
        </li>
    <?php else: ?>
        <li class="nav-item<?=activeClass('user/profile')?>">
            <a href="<?= Url::toRoute(['/user/profile']) ?>"><span class="menu-title">Профиль</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/my-providers')?>">
            <a href="<?= Url::toRoute(['/user/my-providers']) ?>"><span class="menu-title">Избранное</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/tariff')?>" style="display: none;">
            <a href="<?= Url::toRoute(['/user/tariff']) ?>"><span class="menu-title">Тарифы</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/faq')?>">
            <a href="<?= Url::toRoute(['/user/faq']) ?>"><span class="menu-title">FAQ</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/question')?>">
            <a href="<?= Url::toRoute(['/user/question']) ?>"><span class="menu-title">Задать вопрос</span></a>
        </li>
        <li class="nav-item<?=activeClass('user/accessories-other')?>">
            <a href="<?= Url::toRoute(['site/logout']) ?>" data-method="POST"><span class="menu-title">Выход</span></a>
        </li>
    <?php endif; ?>
<?php endif; ?>