<?php
use app\models\City;
use app\models\Metro;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\CityCashForm */
/* @var $form yii\widgets\ActiveForm */

$citiesData = [];
$cities = City::find()->joinWith(['region'])->andWhere(['region.name' => 'Санкт-Петербург и Ленинградская область'])->all();

foreach ($cities as $city){
    $citiesData[$city->id] = $city->name;
}

?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city')->widget(\kartik\select2\Select2::class, [
        'data' => $citiesData,
        'options' => [
            'placeholder' => 'Выберите'
        ],
    ]) ?>

    <div id="metro-wrapper"<?=(Metro::find()->where(['city_id' => $model->city])->count() == 0 ? "style='display: none';" : null)?>>
        <?= $form->field($model, 'metro')->dropDownList($model->city ? ArrayHelper::map(Metro::find()->where(['city_id' => $model->city])->all(), 'id', 'name') : [], ['prompt' => 'Выберите']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS
$("#citycashform-city").change(function(){
    $.get("/city/get-dropdown-data-metro?id="+$(this).val(), function(response){
        if(response.count > 0){
            $("#citycashform-metro").html(response.text);
            $("#metro-wrapper").show();
        } else {
            $("#citycashform-metro").val(null);
            $("#metro-wrapper").hide();
        }
    });
});
JS;

$this->registerJs($script, View::POS_READY);

?>