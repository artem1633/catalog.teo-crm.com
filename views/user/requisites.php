<?php

use app\models\City;
use app\models\Company;
use app\models\Metro;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * @var $model User
 */


$this->title = 'Реквизиты';

?>

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Реквизиты</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'organization_type')->radioList(Company::organizationTypeLabels())->label(false) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'official_name')->textInput() ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'official_name_short')->textInput() ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'inn')->textInput() ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'ogrn')->textInput() ?>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <?= \yii\helpers\Html::submitButton('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>

                    </div>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>

