<?php

use app\models\Accessories;
use app\models\User;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * @var $model User
 */

$this->title = 'Недостающие услуги';

$accessData = [];
$accessories = Accessories::find()->where(['accessories_id' => null])->all();

if($model->accessories_other){
    $accessoriesOther = json_decode($model->accessories_other);
} else {
    $accessoriesOther = [];
}

$model->accessories_other = $accessoriesOther;

//$allChildAccessories = ArrayHelper::map(Accessories::find()->where(['accessories_id' => ArrayHelper::getColumn($accessories, 'id')])->all());


//foreach ($accessories as $accessory){

//    $childAccessories = Accessories::find()->where(['accessories_id' => $accessory->id])->all();

//    $accessData[$accessory->name] = ArrayHelper::map($childAccessories, 'name', 'name');

//}

//if($model->accessories_other != null && is_array($model->accessories_other) == false){
//    $model->accessories_other = json_decode($model->accessories_other);
//}

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Другие услуги</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?php $form = ActiveForm::begin() ?>
            <?= $form->field($model, 'accessories_other')->widget(\kartik\select2\Select2::class, [
                'data' => array_combine($accessoriesOther, $accessoriesOther),
                'options' => [
                    'placeholder' => 'Выберите',
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'tags' => true,
                ],
            ]) ?>
            <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


