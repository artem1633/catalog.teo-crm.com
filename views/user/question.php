<?php

use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

/** @var $this \yii\web\View */

$this->title = 'Задать вопрос';

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Задать вопрос</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?php $form = ActiveForm::begin() ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Заголовок</label>
                        <?= \yii\helpers\Html::textInput('subject', '', ['class' => 'form-control']) ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Идея</label>
                        <?= \yii\helpers\Html::textarea('content', '', ['class' => 'form-control']) ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <?= \yii\helpers\Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
                </div>
            </div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

