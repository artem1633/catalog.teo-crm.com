<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Пользователи";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Пользователи</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <p>
                <?= Html::a('Создать', ['create'],
                    ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']) ?>
            </p>
            <?= GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-bordered'],
                'responsiveWrap' => false,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'panelBeforeTemplate' => '',
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'responsiveWrap' => false,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after' => BulkButtonWidget::widget([
                            'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                ["bulk-delete"],
                                [
                                    "class" => "btn btn-danger btn-xs",
                                    'role' => 'modal-remote-bulk',
                                    'data-confirm' => false,
                                    'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Вы уверены?',
                                    'data-confirm-message' => 'Вы действительно хотите удалить данный элемент?'
                                ]),
                        ]) .
                        '<div class="clearfix"></div>',
                ]
            ]) ?>

        </div>
    </div>
</div>



<?php Modal::begin([
    "id" => "ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
