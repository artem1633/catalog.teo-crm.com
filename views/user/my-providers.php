<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Избранные";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Избранные</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?= GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-bordered'],
                'responsiveWrap' => false,
                'pjax' => true,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'content' => function($model){
                            return Html::a($model->name, ['report/contacts', 'providerId' => $model->id]);
                        },
                    ],
                    [
                        'attribute' => 'name',
                        'label' => 'Удалить',
                        'content' => function($model){
                            return Html::a('<i class="feather icon-x"></i>', ['report/toogle-my', 'id' => $model->id], [
                                'onclick' => 'event.preventDefault();
                                    
                                     $.get("/report/toggle-my?id='.$model->id.'", function(response){
                                        $.pjax.reload("#crud-datatable-pjax");
                                    });
                                    '
                            ]);
                        },
                        'hAlign' => GridView::ALIGN_CENTER,
                        'width' => '10px',
                    ],
//                    [
//                        'class' => 'kartik\grid\ActionColumn',
//                        'dropdown' => false,
//                        'vAlign'=>'middle',
//                        'template' => '{toggle-my}',
//                        'buttons' => [
//                            'toggle-my' => function($url, $model){
//                                return Html::a('<i class="feather icon-x"></i>', $url, [
//                                    'onclick' => 'event.preventDefault();
//
//                                     $.get("/report/toggle-my?id='.$model->id.'", function(response){
//                                        $.pjax.reload("#crud-datatable-pjax");
//                                    });
//                                    '
//                                ]);
//                            },
//                        ],
//                    ],
                ],
                'panelBeforeTemplate' => '',
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'responsiveWrap' => false,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after' =>''.
                        '<div class="clearfix"></div>',
                ]
            ]) ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
