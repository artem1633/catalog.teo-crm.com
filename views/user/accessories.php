<?php

use app\models\User;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

/**
 * @var $model User
 */


$this->title = 'Основные услуги';

$accessories = \app\models\Accessories::find()->where(['accessories_id' => null])->all();

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Основные услуги</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?php $form = ActiveForm::begin() ?>
            <?php foreach ($accessories as $accessory): ?>
                <div class="card shadow-none bg-transparent border-primary" style="border: none !important;">
                    <div class="card-body">
                        <h4 class="card-title"><?= $accessory->name ?></h4>
                        <?php
                            $childAccessories = \app\models\Accessories::find()->where(['accessories_id' => $accessory->id])->all();
                        ?>
                        <div class="row">
                            <?php foreach ($childAccessories as $childAccessory): ?>
                                <?php
                                    $checked = false;

                                    $data = json_decode($model->accessories);

                                    if(is_array($data)){
                                        $checked = in_array($childAccessory->id, $data);
                                    }
                                ?>
                                <div class="col-md-3"><?=$childAccessory->name?><input name="AccessoriesArr[<?=$childAccessory->id?>]" <?=($checked ? "checked='checked'" : null)?> type="checkbox" style="margin-left: 5px; margin-bottom: 25px;"></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


