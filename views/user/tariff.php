<?php

/** @var $this \yii\web\View */

$this->title = 'Тарифы';

?>
<style>
    .plan-price {
        margin-bottom: 14px !important;
    }

    .pricing-basic-value, .pricing-standard-value, .pricing-enterprise-value {
        font-size: 21px !important;
    }
</style>
<div class="row pricing-card">
    <div class="col-12 col-sm-offset-2 col-sm-10 col-md-12 col-lg-offset-2 col-lg-10 mx-auto">
        <div class="row">
            <!-- basic plan -->
            <div class="col-12 col-md-6">
                <div class="card basic-pricing text-center">
                    <div class="card-body">
                        <h3>Basic</h3>
                        <p class="card-text">A simple start for everyone</p>
                        <div class="annual-plan">
                            <div class="plan-price mt-2">
                                <sup class="font-medium-1 font-weight-bold text-primary">$</sup>
                                <span class="pricing-basic-value font-weight-bolder text-primary">0</span>
                                <sub class="pricing-duration text-body font-medium-1 font-weight-bold">/month</sub>
                            </div>
                            <small class="annual-pricing d-none text-muted"></small>
                        </div>
                        <ul class="list-group list-group-circle text-left">
                            <li class="list-group-item">100 responses a month</li>
                            <li class="list-group-item">Unlimited forms and surveys</li>
                            <li class="list-group-item">Unlimited fields</li>
                            <li class="list-group-item">Basic form creation tools</li>
                            <li class="list-group-item">Up to 2 subdomains</li>
                        </ul>
                        <button class="btn btn-block btn-outline-success mt-2 waves-effect">Your current plan</button>
                    </div>
                </div>
            </div>
            <!--/ basic plan -->

            <!-- standard plan -->
            <div class="col-12 col-md-6">
                <div class="card standard-pricing popular text-center">
                    <div class="card-body">
                        <div class="pricing-badge text-right">
                            <div class="badge badge-pill badge-light-primary">Popular</div>
                        </div>
                        <h3>Standard</h3>
                        <p class="card-text">For small to medium businesses</p>
                        <div class="annual-plan">
                            <div class="plan-price mt-2">
                                <sup class="font-medium-1 font-weight-bold text-primary">$</sup>
                                <span class="pricing-standard-value font-weight-bolder text-primary">49</span>
                                <sub class="pricing-duration text-body font-medium-1 font-weight-bold">/month</sub>
                            </div>
                            <small class="annual-pricing text-muted d-none">USD 480 / year</small>
                        </div>
                        <ul class="list-group list-group-circle text-left">
                            <li class="list-group-item">Unlimited responses</li>
                            <li class="list-group-item">Unlimited forms and surveys</li>
                            <li class="list-group-item">Instagram profile page</li>
                            <li class="list-group-item">Google Docs integration</li>
                            <li class="list-group-item">Custom “Thank you” page</li>
                        </ul>
                        <button class="btn btn-block btn-primary mt-2 waves-effect waves-float waves-light">Upgrade</button>
                    </div>
                </div>
            </div>
            <!--/ standard plan -->

            <!-- enterprise plan -->
<!--            <div class="col-12 col-md-4">-->
<!--                <div class="card enterprise-pricing text-center">-->
<!--                    <div class="card-body">-->
<!--                        <h3>Enterprise</h3>-->
<!--                        <p class="card-text">Solution for big organizations</p>-->
<!--                        <div class="annual-plan">-->
<!--                            <div class="plan-price mt-2">-->
<!--                                <sup class="font-medium-1 font-weight-bold text-primary">$</sup>-->
<!--                                <span class="pricing-enterprise-value font-weight-bolder text-primary">99</span>-->
<!--                                <sub class="pricing-duration text-body font-medium-1 font-weight-bold">/month</sub>-->
<!--                            </div>-->
<!--                            <small class="annual-pricing text-muted d-none">USD 960 / year</small>-->
<!--                        </div>-->
<!--                        <ul class="list-group list-group-circle text-left">-->
<!--                            <li class="list-group-item">PayPal payments</li>-->
<!--                            <li class="list-group-item">Logic Jumps</li>-->
<!--                            <li class="list-group-item">File upload with 5GB storage</li>-->
<!--                            <li class="list-group-item">Custom domain support</li>-->
<!--                            <li class="list-group-item">Stripe integration</li>-->
<!--                        </ul>-->
<!--                        <button class="btn btn-block btn-outline-primary mt-2 waves-effect">Upgrade</button>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <!--/ enterprise plan -->
        </div>
    </div>
</div>