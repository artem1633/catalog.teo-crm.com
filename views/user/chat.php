<?php

use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/** @var $this \yii\web\View */
/** @var $userId integer */
/** @var $userReceiver \app\models\User */
/** @var $users array */
/** @var $messages \app\models\ChatMessage[] */

$this->title = "Чат";

$this->params['appContentClass'] = "app-content content chat-application";


//$this->registerCssFile('/app-assets/css/pages/app-chat.min.css', ['depends' => \app\assets\AppAsset::class]);
//$this->registerCssFile('/app-assets/css/pages/app-chat-list.min.css', ['depends' => \app\assets\AppAsset::class]);

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<style>
    .chat-application .sidebar-content .chat-user-list-wrapper li .chat-info {
        width: calc(100% - 85px - 46px) !important;
    }
    .chat-app-window .chats .chat-body .chat-content {
        background: #009fe3;
    }
    .btn-primary {
        border-color: #009fe3 !important;
        background-color: #009fe3 !important;
        color: #FFFFFF;
        border-radius: 0;
    }
    .chat-application .sidebar-content .chat-list-title {
        color: #0b0b0b;
    }
</style>


<div class="sidebar-left">
    <div class="sidebar"><!-- Admin user profile area -->
        <!--/ Admin user profile area -->

        <!-- Chat Sidebar area -->
        <div class="sidebar-content">
  <span class="sidebar-close-icon">
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
  </span>
            <!-- Sidebar header start -->
            <div class="chat-fixed-search">
                <div class="d-flex align-items-center w-100">
                    <div>
                        <div class="avatar avatar-border" style="cursor: auto;">
                            <img src="/<?= (Yii::$app->user->identity->company->avatar ? Yii::$app->user->identity->company->avatar : 'img/nouser.png') ?>" alt="user_avatar" height="42" width="42">
                        </div>
                    </div>
                    <!--                        <div class="input-group input-group-merge ml-1 w-100">-->
                    <!--                            <div class="input-group-prepend">-->
                    <!--                                <span class="input-group-text round"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search text-muted"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg></span>-->
                    <!--                            </div>-->
                    <!--                            <input type="text" class="form-control round" id="chat-search" placeholder="Search or start a new chat" aria-label="Search..." aria-describedby="chat-search">-->
                    <!--                        </div>-->
                </div>
            </div>
            <!-- Sidebar header end -->

            <!-- Sidebar Users start -->
            <div id="users-list" class="chat-user-list-wrapper list-group ps ps--active-y">
                <h4 class="chat-list-title">Пользователи</h4>
                <ul class="chat-users-list chat-list media-list">
                    <?php Pjax::begin(['id' => 'user-list-pjax-container']) ?>
                    <?php foreach($users as $user): ?>
                        <li class="<?= ($userId == $user['id']) ? "active" : "" ?>" onclick="window.location.href='<?= \yii\helpers\Url::toRoute(['user/chat', 'userId' => $user['id']]) ?>'">
        <span class="avatar"><img src="/<?= ($user['avatar'] ? $user['avatar'] : 'img/nouser.png') ?>" style="object-fit: cover;" height="42" width="42" alt="Generic placeholder image">

        </span>
                            <div class="chat-info flex-grow-1">
                                <h5 class="mb-0">

                                    <?php

                                    $receiverChatName = null;

                                    if($user['company']['type'] == \app\models\Company::TYPE_BUYER){
                                        $receiverChatName = $user['name'];
                                    } else {
                                        $receiverChatName = $user['branch_name'];
                                    }


                                    ?>
                                    <?=$receiverChatName?></h5>
                                <p class="card-text text-truncate">
                                    <?= ($user['last_message_content'] ? $user['last_message_content'] : 'Файл') ?>
                                </p>
                            </div>
                            <div class="chat-meta text-nowrap">
                                <small class="float-right mb-25 chat-time"><?php

                                    if($user['last_message_datetime']){
                                        if(time() - strtotime($user['last_message_datetime']) > 172800){
                                            echo Yii::$app->formatter->asDatetime($user['last_message_datetime']);
                                        } else {
                                            echo Yii::$app->formatter->asRelativeTime($user['last_message_datetime']);
                                        }
                                    }

                                    ?></small>
                                <?php if($user['unread_messages'] > 0): ?>
                                    <span class="badge badge-danger badge-pill float-right"><?= $user['unread_messages'] ?></span>
                                <?php endif; ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    <?php Pjax::end() ?>
                </ul>

                <div class="ps__rail-x" style="left: 0px; bottom: -111px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 111px; height: 371px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 46px; height: 156px;"></div></div></div>
            <!-- Sidebar Users end -->
        </div>
        <!--/ Chat Sidebar area -->

    </div>
</div>
<div class="content-right">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><div class="body-content-overlay"></div>
            <!-- Main chat area -->
            <section class="chat-app-window">
                <!-- To load Conversation -->
                <div class="start-chat-area d-none">
                    <div class="mb-1 start-chat-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-square"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>
                    </div>
                    <h4 class="sidebar-toggle start-chat-text">Start Conversation</h4>
                </div>
                <!--/ To load Conversation -->

                <!-- Active Chat -->
                <div class="active-chat">
                    <!-- Chat Header -->
                    <div class="chat-navbar">
                        <header class="chat-header">
                            <div class="d-flex align-items-center">
                                <div class="sidebar-toggle d-block d-lg-none mr-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu font-medium-5"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                                </div>
                                <div class="avatar avatar-border user-profile-toggle m-0 mr-1">
                                    <img src="/<?= ($userReceiver['avatar'] ? $userReceiver['avatar'] : 'img/nouser.png') ?>" style="border-radius: 2px; object-fit: cover;" alt="avatar" height="45" width="70">

                                </div>
                                <?php

                                $receiverBranchName = null;
                                $receiverName = null;

                                if($userReceiver){
                                    $receiverBranch = \app\models\Branch::findOne($userReceiver->branch_id);

                                    if($receiverBranch){
                                        $receiverBranchName = $receiverBranch->name;
                                    }

                                    $receiverName = $receiverBranchName;

                                    if($userReceiver->company->type == \app\models\Company::TYPE_BUYER){
                                        $receiverName = $userReceiver->name;
                                    }
                                }

                                ?>
                                <h6 class="mb-0"><?= $receiverName ?></h6>
                            </div>
                        </header>
                    </div>
                    <!--/ Chat Header -->

                    <!-- User Chat messages -->
                    <div id="user-chats" class="user-chats ps ps--active-y">

                        <div id="chats" class="chats">
                            <?php Pjax::begin(['id' => 'chat-pjax-container']) ?>
                            <?php if($userId != null): ?>
                                <?php for ($i = 0; $i < count($messages); $i++): ?>
                                    <?php

                                    $message = $messages[$i];
                                    $lastMessage = $i > 0 ? $messages[($i - 1)] : null;

                                    ?>
                                    <?php if($userId != $message->sender_id): ?>
                                        <div class="chat">
                                            <div class="chat-avatar">
                                            </div>
                                            <div class="chat-body">
                                                <div class="chat-content">
                                                    <?= $message->content ?>
                                                    <?php if($message->attachment): ?>
                                                        <?php

                                                        $attachmentArr = explode('.', $message->attachment);
                                                        $extension = null;

                                                        if(isset($attachmentArr[count($attachmentArr) - 1])){
                                                            $extension = $attachmentArr[count($attachmentArr) - 1];
                                                        }

                                                        ?>

                                                        <?php if(false): ?>
                                                            <a class="image-popup-vertical-fit"  href="/<?= $message->attachment ?>"><img src="/<?= $message->attachment ?>" style="display: block; width: 450px; height: 350px; object-fit: cover;"></a>
                                                        <?php else: ?>
                                                            <div style="background: #5c50d4; padding: 6px 7px; border-radius: 5px; font-size: 15px;">
                                                                <i class="feather icon-file"></i> <?= \yii\helpers\Html::a($message->attachment_base_name, \yii\helpers\Url::toRoute(['user/chat-download', 'id' => $message->id], true), ['style' => 'color: #fff;', 'onclick' => 'event.preventDefault(); window.location.href = "'.\yii\helpers\Url::toRoute(['user/chat-download', 'id' => $message->id], true).'"; return false;', 'target' => '_blank']) ?>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div style="display: block; text-align: right; margin-top: 52px; margin-right: 15px; color: #a6a6a6;"><?= Yii::$app->formatter->asDate($message->created_at, 'php:d.m.Y H:i') ?></div>

                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="chat chat-left">
                                            <div class="chat-avatar">
                                            </div>
                                            <div class="chat-body">
                                                <div class="chat-content">
                                                    <?= $message->content ?>
                                                    <?php if($message->attachment): ?>
                                                        <?php

                                                        $attachmentArr = explode('.', $message->attachment);
                                                        $extension = null;

                                                        if(isset($attachmentArr[count($attachmentArr) - 1])){
                                                            $extension = $attachmentArr[count($attachmentArr) - 1];
                                                        }

                                                        ?>

                                                        <?php if(false): ?>
                                                            <a class="image-popup-vertical-fit"  href="/<?= $message->attachment ?>"><img src="/<?= $message->attachment ?>" style="display: block; width: 450px; height: 350px; object-fit: cover;"></a>
                                                        <?php else: ?>
                                                            <div style="background: #ededed; padding: 6px 7px; border-radius: 5px; font-size: 15px;">
                                                                <i class="feather icon-file"></i> <?= \yii\helpers\Html::a($message->attachment_base_name, \yii\helpers\Url::toRoute(['user/chat-download', 'id' => $message->id], true), ['style' => 'color: #6E6B7B;', 'onclick' => 'event.preventDefault(); window.location.href = "'.\yii\helpers\Url::toRoute(['user/chat-download', 'id' => $message->id], true).'"; return false;', 'target' => '_blank']) ?>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div style="display: block; text-align: left; margin-top: 64px; margin-left: 17px; color: #a6a6a6;"><?= Yii::$app->formatter->asDate($message->created_at, 'php:d.m.Y H:i') ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            <?php else: ?>

                            <?php endif; ?>
                            <?php Pjax::end() ?>
                        </div>
                        <div class="ps__rail-x" style="left: 0px; bottom: -165px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 165px; right: 0px; height: 650px;"><div class="ps__thumb-y" tabindex="0" style="top: 132px; height: 518px;"></div></div></div>
                    <!-- User Chat messages -->

                    <!-- Submit Chat form -->
                    <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'chat-app-form', 'options' => ['class' => 'chat-app-form', 'enctype' => 'multipart/form-data']]) ?>
                    <div class="input-group input-group-merge mr-1 form-send-message">
                        <input name="chatMessageContent" type="text" class="form-control message" placeholder="Введите текст">
                        <input name="receiverId" value="<?= $userId ?>" type="hidden">
                        <div class="hidden">
                            <input name="fileAttachment" type="file" id="fileMessage" onchange="$('button[type=\'submit\']').trigger('click');">
                        </div>

                        <div class="input-group-append" onclick="$('#fileMessage').trigger('click')">
          <span class="input-group-text">
            <label for="attach-doc" class="attachment-icon mb-0" onclick="return false;">
              <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-image cursor-pointer lighten-2 text-secondary"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><circle cx="8.5" cy="8.5" r="1.5"></circle><polyline points="21 15 16 10 5 21"></polyline></svg>
               </label></span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" onclick="event.preventDefault(); $('#chat-app-form').submit(); $(this).attr('disabled', 'disabled');">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-send d-lg-none"><line x1="22" y1="2" x2="11" y2="13"></line><polygon points="22 2 15 22 11 13 2 9 22 2"></polygon></svg>
                        <span class="d-none d-lg-block">Отправить</span>
                    </button>
                    <?php \yii\widgets\ActiveForm::end() ?>
                    <!--/ Submit Chat form -->
                </div>
                <!--/ Active Chat -->
            </section>
            <!--/ Main chat area -->

            <!-- User Chat profile right area -->
            <div class="user-profile-sidebar">
                <header class="user-profile-header">
    <span class="close-icon">
      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
    </span>
                    <!-- User Profile image with name -->
                    <div class="header-profile-sidebar">
                        <?php if($userReceiver): ?>
                            <div class="avatar box-shadow-1 avatar-border avatar-xl">
                                <img src="/<?= ($userReceiver['avatar'] ? $userReceiver['avatar'] : 'img/nouser.png') ?>" style="border-radius: 2px; object-fit: cover; width: 100px;" alt="user_avatar" height="80" width="115">

                            </div>
                            <h4 class="chat-user-name"><?= $userReceiver['name'] ?></h4>
                            <span class="user-post"><?= $receiverBranchName ?></span>
                        <?php endif; ?>
                    </div>
                    <!--/ User Profile image with name -->
                </header>
                <div class="user-profile-sidebar-area ps">
                    <!-- About User -->
                    <!--                    <h6 class="section-label mb-1">About</h6>-->
                    <!--                    <p>Toffee caramels jelly-o tart gummi bears cake I love ice cream lollipop.</p>-->
                    <!-- About User -->
                    <!-- User's personal information -->
                    <?php if($userReceiver): ?>
                        <div class="personal-info">
                            <h6 class="section-label mb-1 mt-3">Информация</h6>
                            <?php if($userReceiver->company->type == \app\models\Company::TYPE_PROVIDER): ?>
                                <?php

                                $branch = \app\models\Branch::findOne($userReceiver->branch_id);

                                ?>
                                <?php if($branch): ?>
                                    <ul class="list-unstyled">
                                        <li class="mb-1">
                                            <i class="fa fa-address-book-o"></i>
                                            <span class="align-middle"><?= $branch->email ?></span>
                                        </li>
                                        <li class="mb-1">
                                            <i class="fa fa-phone"></i>
                                            <span class="align-middle"><?= $branch->phone ?></span>
                                        </li>
                                        <li class="mb-1">
                                            <i class="fa fa-map-marker"></i>
                                            <span class="align-middle"><?= $branch->address ?></span>
                                        </li>
                                        <li class="mb-1">
                                            <i class="fa fa-building"></i>
                                            <span class="align-middle"><?= \yii\helpers\ArrayHelper::getValue($branch, 'city.name') ?></span>
                                        </li>
                                        <li class="mb-1">
                                            <i class="fa fa-train"></i>
                                            <span class="align-middle"><?= \yii\helpers\ArrayHelper::getValue($branch, 'metro.name') ?></span>
                                        </li>
                                        <li class="mb-1">
                                            <i class="fa fa-sitemap"></i>
                                            <span class="align-middle"><?= $branch->social_site ?></span>
                                        </li>
                                        <li class="mb-1">
                                            <i class="fa fa-vk"></i>
                                            <span class="align-middle"><?= $branch->social_vk ?></span>
                                        </li>
                                        <li class="mb-1">
                                            <i class="fa fa-instagram"></i>
                                            <span class="align-middle"><?= $branch->social_instagram ?></span>
                                        </li>
                                    </ul>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php

                                $company = \app\models\Company::findOne($userReceiver->company_id);

                                ?>
                                <?php if($company): ?>
                                    <ul class="list-unstyled">
                                        <li class="mb-1">
                                            <i class="fa fa-address-book-o"></i>
                                            <span class="align-middle"><?= $company->email ?></span>
                                        </li>
                                        <li class="mb-1">
                                            <i class="fa fa-phone"></i>
                                            <span class="align-middle"><?= $company->phone ?></span>
                                        </li>
                                    </ul>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <!--/ User's personal information -->

                    <!-- User's Links -->
                    <div class="more-options" style="display: none;">
                        <h6 class="section-label mb-1 mt-3">Options</h6>
                        <ul class="list-unstyled">
                            <li class="cursor-pointer mb-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag font-medium-2 mr-50"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg>
                                <span class="align-middle">Add Tag</span>
                            </li>
                            <li class="cursor-pointer mb-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star font-medium-2 mr-50"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                <span class="align-middle">Important Contact</span>
                            </li>
                            <li class="cursor-pointer mb-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-image font-medium-2 mr-50"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><circle cx="8.5" cy="8.5" r="1.5"></circle><polyline points="21 15 16 10 5 21"></polyline></svg>
                                <span class="align-middle">Shared Media</span>
                            </li>
                            <li class="cursor-pointer mb-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 mr-50"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                <span class="align-middle">Delete Contact</span>
                            </li>
                            <li class="cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-slash font-medium-2 mr-50"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg>
                                <span class="align-middle">Block Contact</span>
                            </li>
                        </ul>
                    </div>
                    <!--/ User's Links -->
                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
            </div>
            <!--/ User Chat profile right area -->

        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>



<?php

$script = <<< JS

window.onload = function () {
setTimeout(function(){
 var elem = document.getElementById('user-chats');
  elem.scrollTop = elem.scrollHeight;
}, 3000);
}





JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>