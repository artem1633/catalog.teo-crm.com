<?php

use app\models\ImageCompany;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/** @var $this \yii\web\View */

$this->title = "Галерея";

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<div class="card">
    <div class="card-content">
        <div class="card-body">
            <?= Html::a('Добавить фото', ['user/upload-company-photo', 'companyId' => Yii::$app->user->identity->company_id], ['class' => 'btn btn-primary', 'role' => 'modal-remote']) ?>
        </div>
    </div>
</div>

<?php Pjax::begin(['id' => 'pjax-gallery-container', 'enablePushState' => false]) ?>
    <div class="clearfix">
        <?php foreach (ImageCompany::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all() as $model): ?>
            <div style="width: 300px; height: 300px; margin: 8px; float: left; position: relative;">
                <a class="image-popup-vertical-fit" href="/<?=$model->path?>"><img src="/<?=$model->path?>" alt="" style="width: 100%; height: 100%; object-fit: cover; position: absolute;"></a>
                <?= Html::a('<i class="feather icon-trash-2"></i>', ['user/delete-company-photo', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'style' => 'position: absolute; top: 10px; right: 15px; font-size: 14px;',
                    'onclick' => 'event.preventDefault(); if(confirm("Вы уверены что хотите удалить изображение?")){ var url = $(this).attr("href"); $.get(url, function(response){ $.pjax.reload("#pjax-gallery-container"); }); }',
                ]) ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		 closeOnContentClick: false,
	});

$(document).on('pjax:complete' , function(event) {
	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		 closeOnContentClick: false,
	});
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>