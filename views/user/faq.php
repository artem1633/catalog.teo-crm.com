<?php

/** @var $this \yii\web\View */
use yii\bootstrap\Modal;

/** @var $faqs app\models\Faq[] */

$this->title = 'FAQ';

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>


<div class="collapse-margin collapse-icon mt-2" id="faq-payment-qna">
    <?php $counter = 1; foreach ($faqs as $faq): ?>

        <div class="card">
            <div class="card-header collapsed" id="paymentOne" role="button" data-target="#faq-payment-<?=$counter?>" aria-expanded="false" onclick="$(this).parent().find('.collapse').slideToggle();" style="cursor: pointer;">
                <span class="lead collapse-title"><?= $faq->name ?></span>
            </div>

            <div id="faq-payment-<?=$counter?>" class="collapse" aria-labelledby="paymentOne" data-parent="#faq-payment-qna" style="">
                <div class="card-body">
                    <?= $faq->content ?>
                </div>
            </div>
        </div>

        <?php $counter++; ?>
    <?php endforeach; ?>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

