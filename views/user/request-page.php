<?php
use app\models\City;
use app\models\Metro;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\forms\OrderForm */
/* @var $form yii\widgets\ActiveForm */

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];


$cities = City::find()->joinWith(['region'])->andWhere(['region.name' => 'Санкт-Петербург и Ленинградская область'])->all();

$citiesData = [];

foreach ($cities as $city)
{
    // $citiesData[] = ArrayHelper::getValue($city, 'region.name').' / '.$city->name;
    $citiesData[$city->id] = $city->name;
}


$json = json_decode(file_get_contents('params.json'), true);

$firstParam = array_combine(array_keys($json), array_keys($json));




?>

    <style>
        .checkbox-label {
            display: inline-block;
            width: 85%;
            vertical-align: -webkit-baseline-middle;
        }
    </style>

<div class="card" style="width: 40%; margin: auto;">
    <div class="card-header">

    </div>
    <div class="card-content" style="padding: 0 40px;">
        <div class="user-form">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'enableClientValidation' => false]); ?>

            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-center" style="margin-bottom: 20px;">Заявка</h3>
                </div>
            </div>


            <?php if(Yii::$app->session->hasFlash('success')): ?>
                <p class="text-success text-center"><b><?= Yii::$app->session->getFlash('success') ?></b></p>
            <?php endif; ?>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'city')->widget(\kartik\select2\Select2::class, [
                        'data' => $citiesData,
                        'options' => [
                            'placeholder' => 'Выберите',
                        ],
                    ])->label(false) ?>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'category')->dropDownList($firstParam, ['prompt' => $model->getAttributeLabel('category')])->label(false) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'subCategory')->dropDownList([], ['prompt' => $model->getAttributeLabel('subCategory')])->label(false) ?>
                </div>
            </div>

            <div class="row" style="display: none;">
                <div class="col-md-12">
                    <?= $form->field($model, 'param1')->dropDownList([], ['prompt' => $model->getAttributeLabel('param1')])->label(false) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'param2')->dropDownList([], ['prompt' => $model->getAttributeLabel('param2')])->label(false) ?>
                </div>
            </div>




            <div class="count-wrapper" <?=strstr($model->category, 'Широкоформат') ? 'style="display: none;"' : ''?>>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'count')->input('number', ['placeholder' => $model->getAttributeLabel('count')])->label(false) ?>
                    </div>
                </div>
            </div>


            <div class="params-wrapper" <?=strstr($model->category, 'Широкоформат') ? '' : 'style="display: none;"'?>>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'height')->input('number', ['placeholder' => $model->getAttributeLabel('height')])->label(false) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'width')->input('number', ['placeholder' => $model->getAttributeLabel('width')])->label(false) ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'comment')->textInput(['placeholder' => $model->getAttributeLabel('comment')])->label(false) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput(['placeholder' => $model->getAttributeLabel('name')])->label(false) ?>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => $model->getAttributeLabel('phone')])->label(false) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= \yii\helpers\Html::a('Выбрать файлы', '#', ['id' => 'attach-files-btn', 'class' => 'btn btn-success btn-block']) ?>
                    <div class="hidden">
                        <?= $form->field($model, 'files[]')->fileInput(['multiple' => true]) ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        <?= $form
                            ->field($model, 'agree', $fieldOptions1)
                            ->label('<span class="checkbox-label">Я согласен с '.Html::a('пользовательским соглашением', '/Пользовательское соглашение.pdf', ['target' => '_blank', 'style' => 'color: #009fe3']).' и '.Html::a('политикой конфиденциальности', '/Политика конфиденциальности.pdf', ['target' => '_blank', 'style' => 'color: #009fe3']).'</span>')
                            ->checkbox() ?>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= \yii\helpers\Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-block', 'style' => "margin-bottom: 20px;"]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>



<?php

$script = <<< JS

$("#orderform-category").change(function(){
    $.get('/user/get-subcategories?id='+$(this).val(), function(html){
        $('#orderform-subcategory').html(html);
    });

    // $.get('/user/get-category?id='+$(this).val(), function(html){
        if($("#orderform-category option[value='"+$(this).val()+"']").text().includes('Широкоформат')){
            $('.count-wrapper').hide();
            $('.params-wrapper').show();
        } else {
            $('.count-wrapper').show();
            $('.params-wrapper').hide();
        }
        
        
    $("#orderform-param1").html('<option value="">Параметр 1</option>');
    $("#orderform-param2").html('<option value="">Параметр 2</option>');
    
    // });
});

$("#orderform-subcategory").change(function(){
    $.get('/user/get-params?first='+$("#orderform-category").val()+'&second='+$(this).val(), function(html){
        $('#orderform-param1').html(html);
    });
    
    $("#orderform-param2").html('<option value="">Параметр 2</option>');
});


$("#orderform-param1").change(function(){
    $.get('/user/get-params-data?first='+encodeURIComponent($("#orderform-category").val())+'&second='+encodeURIComponent($("#orderform-subcategory").val())+'&third='+encodeURIComponent($(this).val()), function(html){
        $('#orderform-param2').html(html);
    });
});

$('#attach-files-btn').click(function(e){
    e.preventDefault();
    
    $('#orderform-files').trigger('click');
});

$('#orderform-files').change(function(e){
    if($('#orderform-files')[0].files.length > 4){
        alert('Вы можете загрузить максимум 4 файла');
    } else {
        $('#attach-files-btn').removeClass('btn-success');
        $('#attach-files-btn').addClass('btn-primary');
        $('#attach-files-btn').text('Файлы загружены ('+$('#orderform-files')[0].files.length+' шт.)');        
    }
});

//$('#orderform-name').on('keypress', function() {
//    var that = this;
//
//    setTimeout(function() {
//        var res = /[^а-яА-Я ]/g.exec(that.value);
//        console.log(res);
//        that.value = that.value.replace(res, '');
//    }, 0);
//});

JS;

$this->registerJs($script, View::POS_READY);

?>