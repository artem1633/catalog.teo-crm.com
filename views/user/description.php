<?php

use app\models\City;
use app\models\Company;
use app\models\Metro;
use app\models\User;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * @var $model User
 */


$this->title = 'Описание';

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<style>
    #cke_21 {
        display: none;
    }
</style>

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Описание</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <?php $form = ActiveForm::begin() ?>

                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'description')->widget(\mihaildev\ckeditor\CKEditor::class, [
                                    'editorOptions' => [
                                        'preset' => 'basic',
                                    ],
                                ])->label(false)?>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <?= \yii\helpers\Html::submitButton('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>

                    </div>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

