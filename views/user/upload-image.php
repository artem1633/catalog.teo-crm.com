<?php
use app\models\File;
use app\models\FileAwait;
use kato\DropZone;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImageCompany */
/* @var $form yii\widgets\ActiveForm */
/* @var $companyId int */


?>


<div class="user-form">
    <?php ActiveForm::begin() ?>

    <?= DropZone::widget([
        'id'        => 'dzImage',
        'uploadUrl' => Url::toRoute([ 'user/upload-company-photo-total', 'companyId' => $companyId]),
        'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
        'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
        'options' => [
            'dictDefaultMessage' => 'Нажмите чтобы загрузить файлы'
        ],
    ]);?>

    <?php ActiveForm::end() ?>
</div>

