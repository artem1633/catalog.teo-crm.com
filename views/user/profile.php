<?php

use app\models\City;
use app\models\Company;
use app\models\Metro;
use app\models\User;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * @var $model User
 */


$this->title = 'Профиль';

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Настройки</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                <?php if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER): ?>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <img class="img-fluid rounded" src="/<?= isset($model->avatar) ? $model->avatar : 'img/nouser.png' ?>" style="display: inline-block; height: 115px; width: 115px; object-fit: cover;">
                                        <?= \yii\helpers\Html::a('Выбрать лого', '#', ['id' => 'load-file-btn', 'class' => 'btn btn-primary', 'onclick' => '
                                            event.preventDefault();
                                                
                                            $("#company-file").trigger("click");
                                                                               
                                            
                                        ']) ?>
                                    </div>
                                    <div style="display: none">
                                        <?= $form->field($model, 'file')->fileInput(['accept' => 'image/*'])->label(false) ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'email')->textInput() ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'name')->textInput() ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'phone')->textInput() ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                    //                                    echo $form->field($model, 'company_id')->dropDownList(ArrayHelper::map(Company::find()->all(), 'id', 'name'))
                                    ?>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <?= \yii\helpers\Html::submitButton('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success']) ?>
                                </div>
                            </div>

                        </div>
                    </div>

                <?php else: ?>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <img class="img-fluid rounded" src="/<?= isset($model->avatar) ? $model->avatar : 'img/nouser.png' ?>" style="display: inline-block; height: 115px; width: 115px; object-fit: cover;">
                                        <?= \yii\helpers\Html::a('Выбрать лого', '#', ['id' => 'load-file-btn', 'class' => 'btn btn-primary', 'onclick' => '
                                            event.preventDefault();
                                                
                                            $("#company-file").trigger("click");
                                                                               
                                            
                                        ']) ?>
                                    </div>
                                    <div style="display: none">
                                        <?= $form->field($model, 'file')->fileInput(['accept' => 'image/*'])->label(false) ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($modelUser, 'email')->textInput() ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'name')->textInput()->label('Имя') ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'phone')->textInput() ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($modelUser, 'password')->textInput() ?>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <?= \yii\helpers\Html::submitButton('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success']) ?>
                                </div>
                            </div>

                        </div>
                    </div>

                <?php endif; ?>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>



<?php

$script = <<< JS
    $('[data-role="profile-image-select"]').click(function(){
        $('#avatar-form input').trigger('click');
    });

    $('#avatar-form input').change(function(){
        $('#avatar-form').submit();
    });
    
    $('#avatar-form').submit(function(e){
        var formData = new FormData(this);
        $.ajax({
            type: "POST",
            url: $('#avatar-form').attr('action'),
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                if(response.success === 1){
                    var path = '/'+response.path;
                    $('[data-role="avatar-view"]').each(function(i){
                        $(this).attr('src', path);
                    });
                    $('[data-role="profile-image-select"]').each(function(i){
                        $(this).attr('src', path);
                    });
                }
            }
        });
        e.preventDefault();
    });
    
    $("#citycashform-city").change(function(){
        $.get("/city/get-dropdown-data-metro?id="+$(this).val(), function(response){
            if(response.count > 0){
                $("#citycashform-metro").html(response.text);
                $("#metro-wrapper").show();
            } else {
                // $("#citycashform-metro").html('');
                $("#citycashform-metro").val(null);
                $("#metro-wrapper").hide();
            }
        });
    });
    
    
    $("#company-file").change(function(){
        $('#load-file-btn').text('Файл выбран');
        $('#load-file-btn').removeClass('btn-primary');
        $('#load-file-btn').addClass('btn-outline-primary');
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

