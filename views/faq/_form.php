<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput() ?>
            </div>
        </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'type')->dropDownList(\app\models\Faq::typeLabels()) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'grouping')->textInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'fileInstance')->fileInput()->label('Файл') ?>


        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'content')->textarea() ?>
            </div>
        </div>



    <?php ActiveForm::end(); ?>

</div>
