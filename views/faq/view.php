<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model Faq */
?>
<div class="faq-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'content',
        ],
    ]) ?>

</div>
