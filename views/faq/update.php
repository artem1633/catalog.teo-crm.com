<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Faq */
?>
<div class="faq-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
