<?php
use app\models\Accessories;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use \app\models\TemplateFields;


$list = [];
$type = Accessories::find()->where(['is', 'accessories_id', null])->all();
foreach ($type as $item) {
    $kings = Accessories::find()->where(['accessories_id' => $item->id])->all();
    foreach ($kings as $king) {
//                $list[$item->name][$king->name] = ArrayHelper::map(Subgroups::find()->where(['work_kind_id' => $king->id])->all(), 'id', 'name');
        $list[$item->name][$king->name] = ArrayHelper::map(Accessories::find()->where(['accessories_id' => $king->id])->all(), 'id', 'name');
    }
}


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'accessories_id',
        'value' => 'template.name',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'data' => $list,
            'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => 'Выберите шаблон'
            ],
        ],
        'value' => function($model){
            $output = [ArrayHelper::getValue($model, 'template.name')];

            $one = Accessories::findOne($model->accessories_id);

            if($one){
                $two = Accessories::findOne($one->accessories_id);
                $output[] = "{$two->name}";

                if($two){
                    $three = Accessories::findOne($two->accessories_id);
                    if($three){
                        $output[] = "{$three->name}";
                    }
                }
            }

            $output = array_reverse($output);
            $output = implode('/', $output);

            return $output;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'label',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'data' => TemplateFields::getTypes(),
            'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => 'Выберите тип'
            ],
        ],
        'content' => function($model){
            if(isset(TemplateFields::getTypes()[$model->type]))
                return TemplateFields::getTypes()[$model->type];
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-cancel'=>'Отмена',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            }
        ],
    ],

];