<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemplateFieldsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Поля";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

    <style>
        thead, tbody { display: block; }

        tbody {
            height: 70vh;       /* Just for the demo          */
            overflow-y: auto;    /* Trigger vertical scroll    */
            overflow-x: hidden;  /* Hide the horizontal scroll */
            width: 100%;
        }

        table {
            width: 100%; /* Optional */
        }

        tbody td, thead th {
            width: 20%;  /* Optional */
        }
    </style>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Поля</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_columns.php'),
                'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                        ['role'=>'modal-remote','title'=> 'Добавить поле','class'=>'btn btn-success']).'&nbsp;'.
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Вы уверены?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
                ]
            ])?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    'options' => [
        'class' => 'modal-super-slg',
        'tabindex' => false
    ],
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
// Change the selector if needed
var table = $('table'),
    bodyCells = table.find('tbody tr:first').children(),
    colWidth;

// Get the tbody columns width array
colWidth = bodyCells.map(function() {
    return $(this).width();
}).get();

// Set the width of thead columns
table.find('thead tr').children().each(function(i, v) {
    $(v).width(colWidth[i]);
});    
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>