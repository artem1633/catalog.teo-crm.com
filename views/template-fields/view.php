<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TemplateFields */
?>
<div class="template-fields-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'accessories_id',
            'name',
            'label',
            'type',
            'data',
        ],
    ]) ?>

</div>
