<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TemplateFields */

?>
<div class="template-fields-create">
    <?= $this->render('_form', [
        'model' => $model,
        'hideTemplate' => $hideTemplate,
    ]) ?>
</div>
