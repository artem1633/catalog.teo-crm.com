<?php
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Accessories;

/* @var $this yii\web\View */
/* @var $model app\models\TemplateFields */
/* @var $form yii\widgets\ActiveForm */

// if($model->data != null && !is_array($model->data))
//     $model->data = explode(',', $model->data);

$list = [];
$type = Accessories::find()->where(['is', 'accessories_id', null])->all();
foreach ($type as $item) {
    $kings = Accessories::find()->where(['accessories_id' => $item->id])->all();
    foreach ($kings as $king) {
//                $list[$item->name][$king->name] = ArrayHelper::map(Subgroups::find()->where(['work_kind_id' => $king->id])->all(), 'id', 'name');
        $list[$item->name][$king->name] = ArrayHelper::map(Accessories::find()->where(['accessories_id' => $king->id])->all(), 'id', 'name');
    }
}


?>

<div class="template-fields-form">

    <?php $form = ActiveForm::begin(); ?>

    <div style="<?= $hideTemplate ? 'display: none;' : '' ?>">
        <?= $form->field($model, 'accessories_id')->widget(\kartik\select2\Select2::class, [
            'data' => $list,
            'pluginEvents' => [
                'change' => "function(){ var value = $(this).val(); $.get('/template-fields/get-parents?id={$model->id}&accessory_id='+value, function(response){
                    $('#templatefields-parent_id').html(response);
                }) }"
            ],
        ]) ?>
    </div>

    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map($model->isNewRecord ? [] : TemplateFields::find()->where(['!=', 'id', $model->id])->andWhere(['accessories_id' => $model->accessories_id])->all(), 'id', 'name'), ['prompt' => 'Выберите']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(TemplateFields::getTypes()) ?>

    <div id="field-data-wrapper" style="display: none;">

        <?= $form->field($model, 'data')->textInput(['maxlength' => true]) ?>

    </div>

    <?= $form->field($model, 'hint_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->input('number') ?>

    <?= $form->field($model, 'hint_content')->textarea() ?>

    <?= $form->field($model, 'is_main')->checkbox() ?>
    <?= $form->field($model, 'massive_edit')->checkbox() ?>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS
    $('#templatefields-type').change(function(){
        if($(this).val() === 'dropdown')
        {
             $('#field-data-wrapper').show();
        } else
        {
             $('#field-data-wrapper').hide();
        }
    });

    $('#templatefields-type').trigger('change');
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
