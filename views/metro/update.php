<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Metro */
?>
<div class="metro-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
