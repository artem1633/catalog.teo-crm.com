<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Metro */
?>
<div class="metro-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'city_id',
        ],
    ]) ?>

</div>
