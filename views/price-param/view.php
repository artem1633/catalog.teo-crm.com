<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PriceParam */
?>
<div class="price-param-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'price_id',
            'company_id',
            'params:ntext',
            'check',
        ],
    ]) ?>

</div>
