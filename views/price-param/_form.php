<?php
use app\models\Price;
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PriceParam */
/* @var $form yii\widgets\ActiveForm */

$price = Price::findOne($model->price_id);


?>

<div class="price-param-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="hidden">
        <?= $form->field($model, 'price_id')->hiddenInput() ?>
    </div>

    <?php

    /** @var TemplateFields[] $fields */
    $fields = TemplateFields::find()->where(['accessories_id' => $price->accessory_id])->all();
    $params = json_decode($model->params, true);

    ?>



    <?php foreach ($fields as $field): ?>

        <?php if($field->type == TemplateFields::TYPE_TEXT): ?>
            <div class="form-group">
                <label class="control-label"><?= $field->label ?> <?= $field->hint_title ? Html::a('<i class="fa fa-question-circle"></i>', '#', ['title' => $field->hint_title."\n".$field->hint_content]) : null ?></label>
                <?= Html::input('text', "Param[{$field->id}]", ArrayHelper::getValue($params, $field->id), ['class' => 'form-control', 'required' => $field->is_main ? 'true' : 'false']) ?>
            </div>
        <?php elseif($field->type == TemplateFields::TYPE_NUMBER): ?>
            <div class="form-group">
                <label class="control-label"><?= $field->label ?> <?= $field->hint_title ? Html::a('<i class="fa fa-question-circle"></i>', '#', ['title' => $field->hint_title."\n".$field->hint_content]) : null ?></label>
                <?= Html::input('number', "Param[{$field->id}]", ArrayHelper::getValue($params, $field->id), ['class' => 'form-control',  'required' => $field->is_main ? 'true' : 'false']) ?>
            </div>
        <?php elseif($field->type == TemplateFields::TYPE_DROPDOWN): ?>
            <div class="form-group">
                <label class="control-label"><?= $field->label ?> <?= $field->hint_title ? Html::a('<i class="fa fa-question-circle"></i>', '#', ['title' => $field->hint_title."\n".$field->hint_content]) : null ?></label>
                <?php
                    $items = explode(',', $field->data);
                    $items = array_combine($items, $items);

                ?>
                <?php
//                    echo Html::dropDownList("Param[{$field->id}]", ArrayHelper::getValue($params, $field->id), $items, ['class' => 'form-control', 'prompt' => 'Выберите'])

                $value = null;

                if(in_array(ArrayHelper::getValue($params, $field->id), $items)){
                    $value = ArrayHelper::getValue($params, $field->id);
                } elseif(in_array(' '.ArrayHelper::getValue($params, $field->id), $items)){
                    $value = ' '.ArrayHelper::getValue($params, $field->id);
                } elseif(in_array(trim(ArrayHelper::getValue($params, $field->id)), $items)){
                    $value = trim(ArrayHelper::getValue($params, $field->id));
                }

                ?>
                <?= \kartik\select2\Select2::widget([
                        'id' => "ParamSelect2_{$field->id}",
                    'name' => "Param[{$field->id}]",
                    'data' => $items,
                    // 'value' => ArrayHelper::getValue($params, $field->id),
                    'value' => $value,
                    'options' => [
                        'placeholder' => $field->is_main ? null : 'Выберите',
                    ],
                    'pluginOptions' => [
                        'allowClear' => $field->is_main ? false : true,
                    ],
                ]) ?>
            </div>
        <?php endif; ?>

    <?php endforeach; ?>

        <?php if(Yii::$app->user->identity->company->type != \app\models\Company::TYPE_BUYER): ?>
            <?= $form->field($model, 'count')->textInput([ 'required' => $field->is_main ? 'true' : 'false']) ?>
        <?php endif; ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
