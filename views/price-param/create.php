<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PriceParam */

?>
<div class="price-param-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
