<?php
use app\models\Metro;
use app\models\Region;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\City */
/* @var $form yii\widgets\ActiveForm */


if($model->isNewRecord == false){
    $model->mtr = Metro::find()->where(['city_id' => $model->id])->asArray()->all();
}

?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map(Region::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mtr')->widget(\unclead\multipleinput\MultipleInput::className(), [
        'addButtonOptions' => [
            'class' => 'btn btn-sm btn-primary btn-my-square',
            'label' => '+',
        ],
        'removeButtonOptions' => [
            'class' => 'btn btn-sm btn-danger btn-my-square',
            'label' => 'x',
        ],
        'columns' => [
            [
                'name'  => 'id',
                'options' => ['style' => 'display: none;'],
            ],
            [
                'name'  => 'name',
                'title' => 'Наименование',
            ],
        ]
    ])->label(false);
    ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
