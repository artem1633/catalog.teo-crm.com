<?php

use app\models\ImageCompany;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/** @var $this \yii\web\View */

$this->title = "Галерея";

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<?php Pjax::begin(['id' => 'pjax-gallery-container', 'enablePushState' => false]) ?>
    <div class="clearfix">
        <?php foreach ($images as $model): ?>
            <div style="width: 300px; height: 300px; margin: 8px; float: left; position: relative;">
                <a class="image-popup-vertical-fit" href="/<?=$model->path?>"><img src="/<?=$model->path?>" alt="" style="width: 100%; height: 100%; object-fit: cover; position: absolute;"></a>
            </div>
        <?php endforeach; ?>
    </div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		 closeOnContentClick: false,
	});

$(document).on('pjax:complete' , function(event) {
	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		 closeOnContentClick: false,
	});
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>