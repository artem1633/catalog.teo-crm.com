<?php

/** @var $this \yii\web\View */
/** @var $branches \app\models\Branch[] */

?>

<h4>Выберите филиал</h4>
<div class="list-group">
    <?php foreach ($branches as $branch): ?>
        <?php
        $user = \app\models\User::find()->where(['branch_id' => $branch->id])->one();

        if($user == null)
            continue;
        ?>
        <a href="<?= \yii\helpers\Url::to(['report/single-page', 'headerAccessoryId' => $headerAccessoryId, 'accessoryId' => $accessoryId, 'providerId' => $providerId, 'FldPrice[cities]' => $branch->city_id, 'FldPrice[metro]' => $branch->metro_id]) ?>" class="list-group-item list-group-item-action">
            <p style="margin-bottom: 2px;">
                <?= $branch->name ?>
            </p>
            <p class="text-secondary"><?php

                $data = [];

                if($branch->city_id){
                    $data[] = \yii\helpers\ArrayHelper::getValue($branch, 'city.name');
                }

                if($branch->metro_id){
                    $data[] = \yii\helpers\ArrayHelper::getValue($branch, 'metro.name');
                }

                echo implode(', ', $data);

                ?></p>
        </a>
    <?php endforeach; ?>
</div>

