<?php

/** @var $headerAccessoryId int */
/** @var $accessoryId int */

use app\models\Accessories;
use app\models\TemplateFields;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div style="text-align: left;">
                <?php

//                $headerAccessories = ArrayHelper::map(Accessories::find()->where(['is', 'accessories_id', null])->andWhere(['is_main' => true])->all(), 'id', 'name');
                $headerAccessories = ArrayHelper::map(Accessories::find()->where(['is', 'accessories_id', null])->all(), 'id', 'name');



                ?>
                <?php foreach ($headerAccessories as $id => $name): ?>
                    <?php if(in_array($id, $headers) == false) { continue; }?>
                    <?= Html::a($name, ['report/single-page', 'headerAccessoryId' => $id, 'providerId' => isset($_GET['providerId']) ? $_GET['providerId'] : null], ['class' => $headerAccessoryId == $id ? 'btn btn-primary' : 'btn btn-white']) ?>
                <?php endforeach; ?>
            </div>
            <div class="card" style="margin-top: 10px;">
                <div class="card-header">

                    <?php if(Yii::$app->session->hasFlash('success')): ?>
                        <p class="text-success"><?= Yii::$app->session->getFlash('success') ?></p>
                    <?php endif; ?>

                    <h4 class="card-title"><?php

                        if($searchModel->accessoryId != null){
                            $access = Accessories::findOne($searchModel->accessoryId);
                            if($access){
//                                echo $access->name;
                            }
                        }

                        ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php if($headerAccessoryId): ?>
                            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                <?php

                                $accessories = ArrayHelper::map(Accessories::find()->where(['accessories_id' => $headerAccessoryId])->all(), 'id', 'name');



                                ?>
                                <?php foreach ($accessories as $id => $name): ?>
                                    <?php

//                                    $price = \app\models\Price::find()->where(['company_id' => Yii::$app->user->identity->company_id, 'accessory_id' => $id])->one();
//                                    $price = \app\models\Price::find()->where(['company_id' => $_GET['providerId'], 'accessory_id' => $id])->one();
                                    $price = \app\models\Price::find()->where(['accessory_id' => $id])->one();

                                    if($price == null){
                                        continue;
                                    }

                                    ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?= $accessoryId == $id ? 'active' : '' ?>" id="home-tab-fill" href="<?= Url::toRoute(['report/single-page', 'headerAccessoryId' => $headerAccessoryId, 'accessoryId' => $id, 'providerId' => isset($_GET['providerId']) ? $_GET['providerId'] : null]) ?>" aria-controls="home-fill" aria-selected="true"><?=$name?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
<!--                        $accessories = ArrayHelper::map(Accessories::find()->where(['accessories_id' => $access->accessories_id])->all(), 'id', 'name');-->
                        <?php if($searchModel->accessoryId == null): ?>
                            <b>Выберите категорию</b>
                        <?php else: ?>


                            <?php


                            $childAccess = Accessories::find()->where(['accessories_id' => $searchModel->accessoryId])->one();



                            ?>

                            <?php if($childAccess): ?>


                                <?php


                                /** @var TemplateFields[] $fields */
                                $fields = TemplateFields::find()->where(['accessories_id' => $childAccess->id])->orderBy('sort asc')->all();

                                $this->params['filterContent'] = $this->render('_search', [
                                    'fields' => $fields,
                                    'pricesPks' => $pricesPks,
                                    'providerId' => $providerId,
                                    'headerAccessoryId' => $headerAccessoryId,
                                    'accessoryId' => $accessoryId,
                                    'query' => $query,
                                    'cityFilterValue' => $cityFilterValue,
                                    'metroFilterValue' => $metroFilterValue,
                                    'dataProvider' => $dataProvider,
                                    'searchCitiesPks' => $searchCitiesPks,
                                ]);

                                ?>



                                <?php

                                $attributes = [
                                    [
                                        'label' => 'Добавить в корзину',
                                        'width' => '20px',
                                        'content' => function($model){
//                                            return '<input type="checkbox" class="kv-row-checkbox" name="selection[]" value="'.$model['id'].'">';
                                            return Html::a('Заказать', '#', ['class' => 'btn btn-primary btn-block', 'onclick' => '
                                                event.preventDefault();
                                                
                                                $("[name=\'orders_items\']").val('.$model['id'].');
                                                $("[name=\'orders_branch\']").val('.ArrayHelper::getValue($model, 'price.branch_id').');
                                                    
                                                $("#order-form").submit();
                                            ']);
                                        },
                                    ],
                                    [
                                        'label' => 'Филиал',
                                        'content' => function($model){
                                            $branch = \app\models\Branch::findOne(ArrayHelper::getValue($model, 'price.branch_id'));

                                            if($branch){
                                                return $branch->name;
                                            }
                                        },
                                    ],
                                    [
                                        'label' => 'Метро',
                                        'content' => function($model){
                                            $branch = \app\models\Branch::findOne(ArrayHelper::getValue($model, 'price.branch_id'));

                                            if($branch){
                                                return ArrayHelper::getValue($branch, 'metro.name');
                                            }
                                        },
                                    ],
                                ];

                                foreach ($fields as $field){

                                    $label = $field->label;

                                    if(strlen($label) > 10){
                                        $label = iconv_substr($label, 0, 10, "UTF-8" ).'...';
                                    }

                                    $attributes[] = [
                                        'label' => $label,
                                        'content' => function($model, $key, $index) use($field, $label){
                                            $params = json_decode($model['params'], true);

                                            $output = ArrayHelper::getValue($params, $field->id);

                                            if(mb_strlen($output) > 10){
                                                $output = iconv_substr($output, 0, 10, "UTF-8" ).'...';
                                            }

                                            return $output;
                                        },
                                    ];
                                }


                                ?>



                                <?=GridView::widget([
                                'id'=>'crud-datatable',
                                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                                'pjax'=>true,
//                'columns' => require(__DIR__.'/_param_columns.php'),
                                'columns' => $attributes,
                                'toolbar' => null,
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'containerOptions' => ['style' => 'overflow-x: auto;'],
                                'panel' => '',
                            ])?>

                                <hr>

                                <?php $form = ActiveForm::begin(['id' => 'order-form', 'action' => ['report/order']]) ?>

                                <input type="hidden" name="orders_items">
                                <input type="hidden" name="orders_branch">

                                <input type="hidden" name="redirect" value="<?=Url::toRoute(['report/single-page', 'accessoryId' => $searchModel->accessoryId, 'providerId' => $providerId])?>">

                                <?php

                                $accessory = Accessories::findOne($searchModel->accessoryId);

                                ?>

                                <input type="hidden" name="accessory" value="<?=$accessory->id?>">

                                <input type="hidden" name="providerId" value="<?=$providerId?>">

                                <?php ActiveForm::end() ?>

                                <?php

//                                echo Html::a('Создать заказ', '#', ['class' => 'btn btn-primary', 'onclick' => '
//                                    event.preventDefault();
//
//                                        var selectedIds = [];
//                                        $(\'input:checkbox[name="selection[]"]\').each(function () {
//                                            if (this.checked)
//                                                selectedIds.push($(this).val());
//                                        });
//
//                                        selectedIds = selectedIds.join(",");
//                                        console.log(selectedIds);
//                                        $("[name=\'orders_items\']").val(selectedIds);
//
//                                        $("#order-form").submit();
//                                '])
                                ?>

                            <?php endif; ?>
                        <?php endif; ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>