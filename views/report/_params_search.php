<?php

use yii\widgets\ActiveForm;
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var $this \yii\web\View */
/** @var $fields TemplateFields */

?>

<div class="container">
    <?php $searchForm = ActiveForm::begin(['id' => 'search-form', 'method' => "GET"]) ?>
    <div class="row">
        <?php foreach ($fields as $field): ?>

            <?php if($field->type != TemplateFields::TYPE_DROPDOWN) continue; ?>

            <?php


            $fldValue = null;

            if(isset($_GET['Fld'])){
                $fldValue = ArrayHelper::getValue($_GET['Fld'], $field->id);
            }

            $parent = $field->parent;

            $disabled = false;

            if($parent){
                $disabled = true;
                if(isset($_GET['Fld'])){
                    if(isset($_GET['Fld'][$parent->id])){
                        if($_GET['Fld'][$parent->id]){
                            $disabled = false;
                        }
                    }
                }
            }

//                            \yii\helpers\VarDumper::dump($_GET, 10, true);

            $data = explode(',', $field->data);

           $data = array_filter($data, function($value) use ($query, $field, $pricesPks, $parent, $fldValue){

               $query = clone $query;

               if($parent){
                   $filterValue = null;

                   if(isset($_GET['Fld'])){
                       if(isset($_GET['Fld'][$parent->id])){
                           if($_GET['Fld'][$parent->id]){
                               $filterValue = $_GET['Fld'][$parent->id];
                           }
                       }
                   }


                   $prices = $query->andWhere(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['like', 'params', '"'.$parent->id.'":"'.$filterValue.'"'])->count();
               } else {
                   $prices = $query->andWhere(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->count();
               }

               if($fldValue){
                   return $fldValue == $value;
               }


//                 $prices = \app\models\PriceParam::find()->where(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['price_id' => $pricesPks, 'company.id' => $providerId])->count();

               return $prices > 0;
           });

            $data = array_combine($data,$data);

           $data = array_filter($data, function($model){
                return $model != "Нет" && $model != "";
           });

           // array_walk($data, function(&$model){
           //     if($model == null){
           //         $model = 'Нет';
           //     }
           // });

        // foreach($data as $key => $value)
        // {
        //     if($value == null){
        //         unset($data[$key]);
        //         $data[null] = "Нет";
        //     }
        // }

           $data[-1] = "Нет";

            ?>

            <div class="col-md-12">
                <div class="form-group">
                    <label for=""><?=$field->label?> <?= $field->hint_title ? Html::a('<i class="fa fa-question-circle"></i>', '#', ['title' => $field->hint_title."\n".$field->hint_content, 'style' => 'font-size: 16px;']) : null ?></label>
                    <?= Html::dropDownList("Fld[{$field->id}]", $fldValue, $data, ['class' => 'form-control', 'disabled' => $disabled, 'prompt' => 'Выберите']) ?>
                </div>
            </div>

        <?php endforeach; ?>




    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo Html::a('Очистить', '#', ['class' => 'btn btn-primary', 'style' => 'margin-top: 20px; background:009fe3!important;',
                'onclick' => 'event.preventDefault(); $("#search-form input, #search-form select").each(function(){
                    $(this).val(null);
                });
                $("#search-form").submit();
                '
                ])
            ?>
            <?php 
            // echo Html::a('Режим PRO', '#', ['class' => 'btn btn-primary', 'style' => 'margin-top: 20px; width: 144px; background: #e6007e!important;',
            //     'onclick' => 'event.preventDefault(); $("#search-form input, #search-form select").each(function(){
                
            //         if($(this).attr("name") != "headerAccessoryId" && $(this).attr("name") != "accessoryId"){
            //             $(this).val(null);
            //         }
                
            //     });
                
                
                
            //     $("#search-form").submit();
            //     '
            // ])
            ?>
            <?php
//                echo Html::submitButton('Поиск', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px;', 'onclick' => '$("#search-form").attr("action", "/report/params");'])
            ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>


<?php

$script = <<< JS

$("#search-form input, #search-form select").change(function(){
    ym(76222834,'reachGoal','site prices (use left filters)');
    $("#search-form").submit();
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>