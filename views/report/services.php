<?php

/** @var $this \yii\web\View */
/** @var $parentAccessories \app\models\Accessories[] */
/** @var $otherAccessories \app\models\Accessories[] */
/** @var $accessories \app\models\Accessories[] */
/** @var $company \app\models\Company */


$this->title = 'Услуги';

?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Основные услуги</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?php foreach ($parentAccessories as $parentAccessory): ?>
                <div class="card shadow-none bg-transparent border-primary" style="border: none !important;">
                    <div class="card-body">
                        <h4 class="card-title"><?= $parentAccessory->name ?></h4>
                        <div class="row">
                            <?php foreach ($accessories as $childAccessory): ?>
                                <?php

                                if($childAccessory->accessories_id != $parentAccessory->id){
                                    continue;
                                }

                                ?>
                                <div class="col-md-3"><?=$childAccessory->name?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Дополнительные услуги</h4>
    </div>
    <div class="card-content">
        <div class="card-body" style="font-size: 17px;">

            <?php

            $accessoriesHtml = [];

            foreach ($otherAccessories as $accessory){
                $accessoriesHtml[] = $accessory;
            }

            echo implode(', ', $accessoriesHtml);

            ?>
        </div>
    </div>
</div>


