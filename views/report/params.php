<?php
use app\models\Accessories;
use app\models\Company;
use app\models\Price;
use app\models\PriceParam;
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\forms\ReportFilterForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $price \app\models\Price */

$this->title = "Общий прайс";


CrudAsset::register($this);


$list = [];
$type = Accessories::find()->where(['is', 'accessories_id', null])->andFilterWhere(['id' => $headerAccessoryId])->all();
foreach ($type as $item) {
    $kings = Accessories::find()->where(['accessories_id' => $item->id])->all();

    $realKings = [];

    foreach ($kings as $king)
    {
        $childs = Accessories::find()->where(['accessories_id' => $king->id])->all();

        $add = false;

        foreach ($childs as $child){
            $pricesCount = \app\models\Price::find()->where(['accessory_id' => $child->id])->count();

            if($pricesCount > 0){
                $add = true;
            }
        }

        if($add){
            $realKings[] = $king;
        }

    }

    if(count($realKings) > 0){
        $list[$item->name] = ArrayHelper::map($realKings, 'id', 'name');
    }
}



?>



    <style>

        tbody {
            height: 70vh;       /* Just for the demo          */
            overflow-y: auto;    /* Trigger vertical scroll    */
            overflow-x: hidden;  /* Hide the horizontal scroll */
            width: 100%;
        }

        table {
            width: 100%; /* Optional */
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
        }

        /*        tbody td, thead th {
                    width: 20%;  /* Optional */
        /*}*/


        .table th {
            font-size: 1rem;
        }

        .rc-handle-container {
            display: none !important;
        }

    </style>

        <?php
            // \yii\helpers\VarDumper::dump(Yii::$app->request->queryParams, 10, true);

            $firstData = isset(Yii::$app->request->queryParams['ReportFilterForm']) ? Yii::$app->request->queryParams['ReportFilterForm'] : [];
            $secondData = isset(Yii::$app->request->queryParams['headerAccessoryId']) ? Yii::$app->request->queryParams['headerAccessoryId'] : '';
            $threeData = isset(Yii::$app->request->queryParams['Fld']) ? Yii::$app->request->queryParams['Fld'] : [];

            $data = [];

            foreach($firstData as $key => $value){
                $data[] = "{$key}={$value}";
            }

            foreach($threeData as $key => $value){
                $data[] = "{$key}={$value}";
            }

            $data[] = 'headerAccessoryId='.$secondData;

            $cashId =  implode(',', $data);
        ?>

    <?php
    // echo if($this->beginCache($cashId, ['duration' => 3600]))
    ?>

    <div class="card hidden">
        <div class="card-content">
            <div class="card-body">
                <?php $form = ActiveForm::begin(['method' => 'GET']); ?>

                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'accessoryId')->widget(\kartik\select2\Select2::class, [
                            'data' => $list,
                        ]) ?>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= Html::submitButton('Применить', ['class' => 'btn btn-primary', 'style' => 'margin-top: 16px;']) ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 20px; text-align: left;">
        <?php

//        $headerAccessories = ArrayHelper::map(Accessories::find()->where(['is', 'accessories_id', null])->andWhere(['is_main' => true])->all(), 'id', 'name');
        $headerAccessories = ArrayHelper::map(Accessories::find()->where(['is', 'accessories_id', null])->all(), 'id', 'name');



        ?>
        <?php foreach ($headerAccessories as $id => $name): ?>
            <?= Html::a($name, ['report/params', 'headerAccessoryId' => $id], ['class' => $headerAccessoryId == $id ? 'btn btn-primary' : 'btn btn-white']) ?>
        <?php endforeach; ?>
    </div>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><?php

                if($searchModel->accessoryId != null){
                    $access = Accessories::findOne($searchModel->accessoryId);
                    if($access){
//                        echo $access->name;
                    }
                }

                ?></h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <?php if($headerAccessoryId): ?>
                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                        <?php foreach ($list as $parentName => $childs): ?>
                            <?php foreach ($childs as $id => $name): ?>

                                <?php


                                $pricesPks = ArrayHelper::getColumn(Accessories::find()->where(['accessories_id' => $id])->all(), 'id');
                                $pricesModel = Price::find()
                                    ->where(['accessory_id' => $pricesPks])
                                    ->andWhere(['order_status' => Price::ORDER_STATUS_PUBLISHED])
                                    ->andWhere(['!=', 'price.company_id', 1])
                                    ->joinWith(['priceCities', 'priceMetros', 'branch']);

                                if($cityFilterValue){
                                    $pricesModel->andFilterWhere(['branch.city_id' => $cityFilterValue]);
                                    if($cityFilterValue){
                                    }
                                }
                                if($metroFilterValue){
                                    $pricesModel->andFilterWhere(['branch.metro_id' => $metroFilterValue]);
                                    if($metroFilterValue){
                                    }
                                }

                                $pricesModel = $pricesModel->all();

                                $pricesPks = ArrayHelper::getColumn($pricesModel,'id');


                                $params = PriceParam::find()
                                    ->select('price_param.*, company.name as company_name, company.rate_id as company_rate_id')
                                    ->where(['price_id' => $pricesPks])
                                    ->andWhere(['!=', 'company.type', Company::TYPE_BUYER])
                                    ->joinWith('price.accessory as accessory')
                                    ->joinWith('price.company as company');


                                if(Yii::$app->user->isGuest){
                                    $params->andWhere(['!=', 'company.rate_id', 0]);
                                } else {
                                    if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER){
                                        if(Yii::$app->user->identity->company->rate_id !== Company::RATE_BRANCH){
                                            $params->andWhere(['!=', 'company.rate_id', 0]);
                                        }
                                    }
                                }



                                if(isset($_GET['Fld'])){
                                    foreach ($_GET['Fld'] as $ids => $value){
                                        if($value == null) continue;
                                        $params->andWhere(['like', 'params', '"'.$ids.'":"'.$value.'"']);
                                    }
                                }

                                $params = $params->all();


                                if(count($params) == 0){
                                    continue;
                                }

                                ?>

                                <li class="nav-item">
                                    <a class="nav-link <?= $searchModel->accessoryId == $id ? 'active' : '' ?>" id="home-tab-fill" href="<?= Url::toRoute(['report/params', 'ReportFilterForm[accessoryId]' => $id, 'headerAccessoryId' => $headerAccessoryId]) ?>" aria-controls="home-fill" aria-selected="true"><?=$name?></a>
                                </li>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <?php if($searchModel->accessoryId == null): ?>
                    <b>Выберите категорию</b>
                <?php else: ?>


                    <?php


                    $childAccess = Accessories::find()->where(['accessories_id' => $searchModel->accessoryId])->one();

                    /** @var TemplateFields[] $fields */
                    $fields = TemplateFields::find()->where(['accessories_id' => $childAccess->id])->orderBy('sort asc')->all();

                    $this->params['filterContent'] = $this->render('_params_search', [
                        'fields' => $fields,
                        'query' => $query,
                        'pricesPks' => $pricesPks,
                        'cityFilterValue' => $cityFilterValue,
                        'metroFilterValue' => $metroFilterValue,
                    ]);

                    ?>




                    <?php

                    $attributes = [
                        [
                            'label' => '',
                            'content' => function($model){

                                $pAccessory = Accessories::findOne($model['price']['accessory']['accessories_id']);
                                $orderUrl = ['report/single-page', 'providerId' => $model['company_id'], 'headerAccessoryId' => $pAccessory->accessories_id, 'accessoryId' => $model['price']['accessory']['accessories_id']];

                                $filterData = isset($_GET['Fld']) ? $_GET['Fld'] : [];

                                foreach ($filterData as $name => $value){
                                    $orderUrl["Fld[$name]"] = $value;
                                }

//                                return Html::a('Заказать', $orderUrl, ['data-pjax' => 0]);
                                return Html::a('Заказать', ['report/params-order', 'accessory' => $model['price']['accessory']['accessories_id'], 'providerId' => $model['company_id'], 'branch_id' => $model['price']['branch_id']], ['class' => 'btn btn-primary', 'data-pjax' => 0, 'onclick' => "ym(76222834,'reachGoal','site prices (clicks table orders)')"]);
                            },
                        ],
                        [
                            'label' => 'Филиал',
                            'content' => function($model){
                                $branch = \app\models\Branch::findOne($model['price']['branch_id']);

                                if($branch){
                                    return $branch->name;
                                }
                            },
                        ],
                        [
                            'label' => 'Метро',
                            'content' => function($model){
                                $metro = \app\models\Metro::findOne($model['price']['branch']['metro_id']);

                                if($metro){
                                    return $metro->name;
                                }
                            },
                        ]
                    ];

//                    usort($fields, function($a, $b){
//                        return $a->sort < $b->sort;
//                    });


                    foreach ($fields as $field){


                        $label = $field->label;

                        if(strlen($label) > 10){
                            $label = iconv_substr($label, 0, 10, "UTF-8" ).'...';
                        }

//                        $filter = null;
//
//                        if($field->type == TemplateFields::TYPE_DROPDOWN){
//                            $fldValue = null;
//
//                            $data = explode(',', $field->data);
//
//                            if(isset($_GET['Fld'])){
//                                ArrayHelper::getValue($_GET['Fld'], $field->id);
//                            }
//
//                            $filter = Html::dropDownList("Fld[{$field->id}]", $fldValue, array_combine($data,$data), ['class' => 'form-control', 'prompt' => 'Выберите']);
//                        }



                        $attributes[] = [
                            'label' => $label,
//                            'filter' => $filter,
                            'attribute' => 'attr_'.$field->id,
                            'content' => function($model, $key, $index) use($field, $label){
                                $params = json_decode($model['params'], true);

                                $output = ArrayHelper::getValue($params, $field->id);

                                if($output == null){
                                    $output = "";
                                }

                                if(mb_strlen($output) > 10){
                                    $output = iconv_substr($output, 0, 10, "UTF-8" ).'...';
                                }

                                return "<span title='".ArrayHelper::getValue($params, $field->id)."'>$output</span>";
                            },
                        ];
                    }


                    // \yii\helpers\VarDumper::dump($attributes, 10, true);



                    ?>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <?= Html::submitButton('Скачать Excel', ['class' => 'btn btn-pink', 'style' => 'margin-top: -20px; background: #60b800!important;position: fixed;right: 52px;z-index: 100000;', 'onclick' => '$("#search-form").attr("action", "/report/download-params-excel"); $("#search-form").submit();']) ?>
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'summary' => false,
                        'rowOptions' => function($model){
                            if(Yii::$app->user->isGuest == false){
                                if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER && Yii::$app->user->identity->company->rate_id != Company::RATE_NO){
                                    if($model['company_rate_id'] != Company::RATE_NO){
                                        return ['class' => 'table-info'];
                                    }
                                }

                                if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER) {
                                    if (Yii::$app->user->identity->company->rate_id == Company::RATE_BRANCH) {
                                        if($model['company_rate_id'] != Company::RATE_NO){
                                            return ['class' => 'table-info'];
                                        }
                                    }
                                }
                            }
                        },
//                'filterModel' => $searchModel,
                        'pjax'=>true,
//                'columns' => require(__DIR__.'/_param_columns.php'),
                        'columns' => $attributes,
                        'toolbar' => null,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'containerOptions' => ['style' => 'overflow-x: auto;'],
                        'panel' => '',
                    ])?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php
// $this->endCache(); endif;
     ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

if(isset($fields)){
    $px = 300 * count($fields);

    $script = <<< JS



// $('#crud-datatable-container table').DataTable({
//     paging: false,
//     searching: false,
//     fixedHeader: false,
//     // "scrollY": "50vh",
//     // "scrollCollapse": true,
// });


JS;



    $this->registerJs($script, \yii\web\View::POS_READY);


}



?>