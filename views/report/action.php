<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccessoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Акции';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<style>
    .label.label-success {
        background: #affad0;
        color: #26ab60;
        padding: 3px 5px;
        border-radius: 20px;
    }
    .card-body-content {
        padding: 1.5rem;
    }
</style>

<div class="row">
    <?php foreach ($dataProvider->models as $model): ?>
        <div class="col-md-3">
            <div class="card">
                <div class="card-content">
                    <div class="card-body" style="padding: 0;">
                        <img src="/<?= ($model->photo ? $model->photo : 'img/no-photo.png') ?>" style="width: 100%; height: 300px; object-fit: cover;"
                             alt="">
                        <div class="card-body-content">
                            <h4 class="card-title" style="font-weight: 700; font-size: 17px;"><?= $model->name ?></h4>
                            <span class="label label-success">+ <?= Yii::$app->formatter->asCurrency($model->price, 'rub') ?></span>
                            <span class="pull-right" style="font-size: 13px; color: #c42fbf; font-weight: 600;">До окончания 1 день</span>
                            <p style="margin-top: 10px;"><?php

                                echo "Москва";
                                ?> <img class="pull-right" src="/<?= ($model->company->avatar ? $model->company->avatar : '/img/no-photo.png') ?>" style="width: 30px; height: 30px; border-radius: 100%;"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => [
        'class' => 'modal-slg'
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
