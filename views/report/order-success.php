<?php

use app\models\Accessories;
use app\models\TemplateFields;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                </div>
                <div class="card-content">
                    <div class="card-body">

                        <p class="text-success" style="width: 100%; font-size: 22px; text-align: center;"><?= Yii::$app->session->getFlash('success') ?></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>