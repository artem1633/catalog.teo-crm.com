<?php
use app\models\Accessories;
use app\models\Company;
use app\models\Price;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PriceParamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Справочник';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if($searchModel->accessoryId){
    $firstAccess = Accessories::find()->where(['id' => $searchModel->accessoryId])->andWhere(['is', 'accessories_id', null])->all();
    $secondAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($firstAccess, 'id')])->all();
    $thirdAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($secondAccess, 'id')])->all();


    $cityFilter = Yii::$app->session->get('user__city');
    $metroFilter = Yii::$app->session->get('user__metro');

    $branches = \app\models\Branch::find()->andFilterWhere(['city_id' => $cityFilter, 'metro_id' => $metroFilter])->all();

    $companiesPks = array_unique(ArrayHelper::getColumn($branches, 'company_id'));

    $companies = Company::find()->where(['type' => Company::TYPE_PROVIDER])->andFilterWhere(['like', 'name', $searchModel->companyName])->andWhere(['id' => $companiesPks])->andWhere(['!=', 'id', 1])->all();
} else {
    $firstAccess = [];
    $secondAccess = [];
    $thirdAccess = [];
    $branches = [];

    $companies = [];
}

//\yii\helpers\VarDumper::dump(\yii\helpers\ArrayHelper::getColumn($firstAccess, 'name'), 10, true);
//\yii\helpers\VarDumper::dump(\yii\helpers\ArrayHelper::getColumn($secondAccess, 'name'), 10, true);
//\yii\helpers\VarDumper::dump(\yii\helpers\ArrayHelper::getColumn($thirdAccess, 'name'), 10, true);

//$list = [];
//$type = Accessories::find()->where(['is', 'accessories_id', null])->all();
//foreach ($type as $item) {
//    $kings = Accessories::find()->where(['accessories_id' => $item->id])->all();
//
//    $realKings = [];
//
//    foreach ($kings as $king)
//    {
//        $childs = Accessories::find()->where(['accessories_id' => $king->id])->all();
//
//        $add = false;
//
//        foreach ($childs as $child){
//            $pricesCount = \app\models\Price::find()->where(['accessory_id' => $child->id])->count();
//
//            if($pricesCount > 0){
//                $add = true;
//            }
//        }
//
//        if($add){
//            $realKings[] = $king;
//        }
//
//    }
//
//    if(count($realKings) > 0){
//        $list[$item->name] = ArrayHelper::map($realKings, 'id', 'name');
//    }
//}

if(Yii::$app->user->isGuest == false){
    $user = Yii::$app->user->identity;
} else {
    $user = null;
}

?>

<style>
    .table {
        -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none;   /* Chrome/Safari/Opera */
        -khtml-user-select: none;    /* Konqueror */
        -moz-user-select: none;      /* Firefox */
        -ms-user-select: none;       /* Internet Explorer/Edge */
        user-select: none;           /* Non-prefixed version, currently */
    }

    table.dataTable thead th, table.dataTable thead td {
        padding: 10px 18px;
        border-bottom: none !important;
        border-bottom-width: 1px;
        border-bottom-style: solid;
        border-bottom-color: none !important;
    }

    .dataTables_wrapper.no-footer .dataTables_scrollBody {
        border-bottom: none !important;
        border-bottom-width: 1px;
        border-bottom-style: solid;
        border-bottom-color: none !important;
    }

    .dataTables_wrapper {
        overflow: auto !important;
    }

    /*nav.header-navbar {*/
        /*margin-left: 0 !important;*/
    /*}*/
</style>

<div class="card hidden">
    <div class="card-content">
        <div class="card-body">
            <?php $form = ActiveForm::begin(['method' => 'GET']); ?>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($searchModel, 'accessoryId')->widget(\kartik\select2\Select2::class, [
                        'data' => ArrayHelper::map(Accessories::find()->where(['is', 'accessories_id', null])->all(), 'id', 'name'),
                    ]) ?>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= Html::submitButton('Применить', ['class' => 'btn btn-primary', 'style' => 'margin-top: 16px;']) ?>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
<div class="card">
    <div class="card-content">
        <div class="card-body">
            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                <?php
                $accessories = Accessories::find()->where(['is', 'accessories_id', null])->andWhere(['is_main' => true])->all();


                $accessories = ArrayHelper::map($accessories, 'id', 'name');

                ?>
                <?php foreach ($accessories as $id => $name): ?>
                    <li class="nav-item">
                        <a class="nav-link <?= $searchModel->accessoryId == $id ? 'active' : '' ?>" id="home-tab-fill" href="<?= Url::toRoute(['report/index', 'ReportFilterForm[accessoryId]' => $id]) ?>" aria-controls="home-fill" aria-selected="true"><?=$name?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
                <?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'GET']) ?>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($searchModel, 'companyName')->textInput(['placeholder' => 'Компания', 'onchange' => '$("#search-form").submit();'])->label(false) ?>
                        </div>
                    </div>
                <?php ActiveForm::end() ?>
                <p>
                    <?= Html::a('Скачать Excel', ['report/download-index-excel', 'headerAccessoryId' => $searchModel->accessoryId], ['class' => 'btn btn-green', 'style' => 'position: fixed; right: 56px; margin-top: -41px; z-index: 1000;', 'download' => true]) ?>
                </p>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <?php if(Yii::$app->user->isGuest == false): ?>
                        <th></th>
                    <?php endif; ?>
                    <th>Компания</th>
                    <?php foreach ($thirdAccess as $model): ?>
                        <th><?php
                            //                            $output = [$model->name];
                            $one = $model;

                            if($one){
                                $two = Accessories::findOne($one->accessories_id);
                                $output = "{$two->name}";

                                if($two){
                                    $three = Accessories::findOne($two->accessories_id);
                                    if($three){
//                                        $output[] = "{$three->name}";
                                    }
                                }
                            }

                            //                            $output = array_reverse($output);
                            //                            $output = implode('/', $output);

                            if(strlen($output) >= 30){
                                $output = iconv_substr($output, 0, 30, "UTF-8" ).'...';
                            }

                            echo $output;

                            ?></th>
                    <?php endforeach;; ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($companies as $company): ?>
                    <?php

                    $output = $company->name;

                    if(strlen($output) >= 30){
                        $output = iconv_substr($output, 0, 30, "UTF-8" ).'...';
                    }

                    $output = Html::a($company->name, ['report/contacts', 'providerId' => $company->id]);

//                    $output .= '<img src="/'.$company->avatar.'" style="display: inline-block; width: 50px; height: 50px;">'.$output;

                    $img = '';

//                    if($company->avatar){
//                        $img = "<img src=\"/{$company->avatar}\" width=\"35\" height=\"35\" style='object-fit: cover;'>";
//                    }

                    $output = "<div class=\"d-flex justify-content-left align-items-center\">
                    <div class=\"avatar  mr-1\">{$img}</div><div class=\"d-flex flex-column\"><span class=\"emp_name text-truncate font-weight-bold\">{$output}</span></div></div>";

                    $trClass = null;

                    if(Yii::$app->user->isGuest){
                        if($company->rate_id != Company::RATE_NO){
                            $trClass .= "table-tarif";
                        }
                    } else {
                        if((Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER) || Yii::$app->user->identity->company->type == Company::TYPE_BUYER){
                            if($company->rate_id != Company::RATE_NO){
                                $trClass .= "table-tarif";
                            }
                        }
                    }

                    ?>
                    <tr <?=($trClass ? 'class="'.$trClass.'"' : null)?>>
                        <?php if(Yii::$app->user->isGuest == false): ?>
                            <td style="text-align: center; font-size: 20px; width: 5%;">
                                <?php
                                echo Html::a('<i class="fa fa-heart '.($user->isMyProvider($company->id) ? 'text-pink' : 'text-secondary').'"></i>', '#', ['class' => 'text-primary', 'title' => 'Добавить в избранные', 'onclick' => '
                                    event.preventDefault();
                                    
                                    var self = this;
                                    
                                    $.get("/report/toggle-my?id='.$company->id.'", function(response){
                                        if(response.toggle == 1){
                                            $(self).find("i").removeClass("text-secondary").addClass("text-pink");
                                            $(self).attr("title", "Убрать из избранных");
                                        } else {
                                            $(self).find("i").addClass("text-secondary").removeClass("text-pink");
                                            $(self).attr("title", "Добавить в избранные");
                                        }
                                    });
                                
                                ']);
                                ?>
                            </td>
                        <?php endif; ?>
                        <td>
                            <?php



                            echo $output;

                            ?>
                        </td>
                        <?php foreach ($thirdAccess as $access): ?>
                            <td style="text-align: center;">
                                <?php

                                $branchesPks = ArrayHelper::getColumn($branches, 'id');

                                $price = Price::find()->where(['accessory_id' => $access->id, 'company_id' => $company->id, 'branch_id' => $branchesPks])->one();

                                $pAccess = Accessories::find()->where(['id' => $access->accessories_id])->one();

                                if($price){
//                                    $priceLink = $user ? Html::a('Расчитать', ['report/single-page', 'accessoryId' => $access->id, 'providerId' => $company->id], ['target' => '_blank']) : null;
                                    if($price->order_status == Price::ORDER_STATUS_PUBLISHED) // Если прайс опубликован
                                    {
                                        $priceLink = Html::a('Рассчитать', ['report/branch-single-page',  'headerAccessoryId' => $pAccess->accessories_id, 'accessoryId' => $access->accessories_id, 'providerId' => $company->id], ['role' => 'modal-remote', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table orders)')"]);
                                        echo $priceLink;

                                    } else {
//                                        $user = \app\models\User::find()->where(['company_id' => $company->id])->one();
//                                        if($user) {
//                                            $priceLink = Html::a('Консультация', ['report/branch-user-chat', 'providerId' => $company->id], ['class' => 'text-warning', 'role' => 'modal-remote', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table specify)')"]);
//                                        } else {
//                                            $priceLink = Html::a('Консультация', '#', ['class' => 'text-warning', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table specify)')"]);
//                                        }

                                        // Уточняем есть ли в услугах компании данная услуга
                                        if($company->accessories == null){
                                            $dataAccessories = [];
                                        } else {
                                            $dataAccessories = json_decode($company->accessories);
                                        }

                                        if(in_array($access->accessories_id, $dataAccessories)){
                                            $user = \app\models\User::find()->where(['company_id' => $company->id])->one();
                                            if($user) {
                                            	if(Yii::$app->user->isGuest){
                                            		echo Html::a('Консультация', ['report/contacts', 'providerId' => $company->id], ['class' => 'text-warning', 'data-pjax' => '0', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table specify)')"]);
                                            	} else {
                                            		echo Html::a('Консультация', ['user/branch-user-chat', 'providerId' => $company->id], ['class' => 'text-warning', 'role' => 'modal-remote', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table specify)')"]);
                                            	}
                                                
                                            } else {
                                                echo Html::a('Консультация', '#', ['class' => 'text-warning', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table specify)')"]);
                                            }
                                        } else {
                                            echo "<span style='color: #666666;'>—</span>";
                                        }
                                    }

                                } else {

                                    // Уточняем есть ли в услугах компании данная услуга
                                    if($company->accessories == null){
                                        $dataAccessories = [];
                                    } else {
                                        $dataAccessories = json_decode($company->accessories);
                                    }

                                    if(in_array($access->accessories_id, $dataAccessories)){
                                        $user = \app\models\User::find()->where(['company_id' => $company->id])->one();
                                        if($user) {
                                        	if(Yii::$app->user->isGuest){
                                            	echo Html::a('Консультация', ['report/contacts', 'providerId' => $company->id], ['class' => 'text-warning', 'data-pjax' => '0', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table specify)')"]);
                                            } else {
                                            	echo Html::a('Консультация', ['report/branch-user-chat', 'providerId' => $company->id], ['class' => 'text-warning', 'role' => 'modal-remote', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table specify)')"]);
                                            }
                                           
                                        } else {
                                            echo Html::a('Консультация', '#', ['class' => 'text-warning', 'onclick' => "ym(76222834,'reachGoal','site services (clicks table specify)')"]);
                                        }
                                    } else {
                                        echo "<span style='color: #666666;'>—</span>";
                                    }

                                }

                                ?>

                            </td>
                        <?php endforeach;; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>		
	</div>

</div>


<?php

$script = <<< JS



$('table').DataTable({
    paging: false,
    searching: false,
    // "scrollY": "50vh",
    "scrollCollapse": true,
    "bInfo": false,
});

JS;



$this->registerJs($script, \yii\web\View::POS_READY);

?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
