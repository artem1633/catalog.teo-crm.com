<?php

/** @var $this \yii\web\View */
/** @var $branches \app\models\Branch[] */

?>

    <h4>Выберите филиал</h4>
    <div class="list-group">
<?php foreach ($branches as $branch): ?>
        <?php
            $user = \app\models\User::find()->where(['branch_id' => $branch->id])->one();

            if($user == null)
                continue;
        ?>
        <a href="<?= \yii\helpers\Url::to(['user/chat', 'userId' => $user->id]) ?>" class="list-group-item list-group-item-action">
            <p style="margin-bottom: 2px;">
                <?= $branch->name ?>
            </p>
            <p class="text-secondary"><?php

                $data = [];

                if($branch->city_id){
                    $data[] = \yii\helpers\ArrayHelper::getValue($branch, 'city.name');
                }

                if($branch->metro_id){
                    $data[] = \yii\helpers\ArrayHelper::getValue($branch, 'metro.name');
                }

                echo implode(', ', $data);

                ?></p>
        </a>
<?php endforeach; ?>
    </div>

