<?php

/** @var $this \yii\web\View */
use kartik\grid\GridView;

/** @var $provider \app\models\Company */

$this->title = "Контакты";

$startCoords = '59.910470,30.342766';

$branchesData = '';

$counter = 1;
foreach ($branches as $branch)
{
    $branchesData .= " var myPlacemark{$counter} = new ymaps.Placemark([".$branch->coords."], {
       content: 'Филиал',
       balloonContent: '{$branch->name}, {$branch->address}',
    });
    myMap.geoObjects.add(myPlacemark{$counter});
    
    ";

    $counter++;
}


?>

<style>
    #branch-datatable tbody tr td {
        font-size: 14px;
    }

    #branch-datatable thead tr th a {
        color: #626262 !important;
    }
</style>

<div class="card" style="margin-top: 30px;">
    <div class="card-header">
    </div>
    <div class="card-content">
        <div class="card-body">
            <div class="content-wrapper" style="padding-left: 10px; min-height: 400px; display: inline-block; width: 49%; vertical-align: top;">
                <h2><?= $provider->name ?></h2>
                <p><?=$provider->address?></p>
                <?= $provider->description ?>
            </div>
            <div class="map-wrapper" style="display: inline-block; width: 49%; padding-right: 10px;">
                <div id="YMapsID" style="width:100%;height:400px;"></div>

                <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU">
                </script>

                <?php
                $script = <<< JS
ymaps.ready(function () { 
 
    
    var myMap = new ymaps.Map("YMapsID", {
        center: [59.910470, 30.342766],
        zoom: 9,
    });
 
    myMap.controls.remove('searchControl');
    myMap.controls.remove('trafficControl');
    myMap.controls.remove('typeSelector');
    
    {$branchesData}
    
});
JS;
                $this->registerJs($script, \yii\web\View::POS_READY);
                ?>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    <h4>Контакты</h4>
                    <?=GridView::widget([
                        'id'=>'branch-datatable',
                        'dataProvider' => $branchDataProvider,
//                        'filterModel' => $branchSearchModel,
                        'summary' => false,
                        'pjax'=>false,
                        'columns' => [
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'name',
                                'content' => function($model){

                                    if($model->avatar){
                                        $path = "/{$model->avatar}";
                                    } else {
                                        $path = "/no-photo-available.png";
                                    }

                                    $addresses = "";

                                    if($model->city_id){
                                        $city = \app\models\City::findOne($model->city_id);
                                        if($city){
                                            $region = \app\models\Region::findOne($city->region_id);
                                            $addresses .= "{$region->name}, {$city->name}";
                                        }
                                    }

                                    if($model->metro_id){
                                        $metro = \app\models\Metro::findOne($model->metro_id);
                                        if($metro){
                                            $addresses .= ", {$metro->name}";
                                        }
                                    }

                                    $output = "<div class=\"d-flex justify-content-left align-items-center\">
                    <div class=\"avatar  mr-1\"></div><div class=\"d-flex flex-column\"><span class=\"emp_name text-truncate font-weight-bold\">{$model->name}</span><small class=\"emp_post text-truncate text-muted\"></small></div></div>";


                                    return $output;
                                }
                            ],
                            [
                                'attribute' => 'city_id',
                                'value' => 'city.name',
                            ],
                            [
                                'attribute' => 'metro_id',
                                'value' => 'metro.name',
                            ],
                            [
                                'attribute' => 'address',
                                'value' => 'address',
                            ],
                            [
                                'attribute' => '',
                                'content' => function($model){

                                    $user = \app\models\User::find()->where(['company_id' => $model->company_id, 'branch_id' => $model->id])->one();

                                    if($user){
                                        return \yii\helpers\Html::a('Написать сообщение', ['user/chat', 'userId' => $user->id], ['class' => 'btn btn-primary']);
                                    } else {
                                        return \yii\helpers\Html::a('Написать сообщение', '#', ['class' => 'btn btn-secondary', 'title' => 'Нет активных пользователей']);
                                    }
                                },
                            ],
                            [
                                'attribute' => 'phone',
                                'format' => 'raw',
                                'value' => function($model){
                                    if($model->phone == null){
                                        return null;
                                    }

                                    return "<div>".\yii\helpers\Html::a('Показать телефон', '#', ['onclick' => 'event.preventDefault(); ym(76222834,\'reachGoal\',\'site page_company (show phone number)\'); $(this).parent().find("span").show(); $(this).hide();'])."<span style='display: none;'>".$model->phone."</span></div>";
                                },
                            ],
                            [
                                'attribute' => 'social_vk',
                                'content' => function($model){
                                    if($model->social_vk){
                                        return \yii\helpers\Html::a('Перейти', $model->social_vk, ['target' => '_blank']);
                                    }
                                }
                            ],
                            [
                                'attribute' => 'social_instagram',
                                'content' => function($model){
                                    if($model->social_instagram){
                                        return \yii\helpers\Html::a('Перейти', $model->social_instagram, ['target' => '_blank']);
                                    }
                                }
                            ],
                            [
                                'attribute' => 'social_site',
                                'content' => function($model){
                                    if($model->social_site){
                                        return \yii\helpers\Html::a('Перейти', $model->social_site, ['target' => '_blank']);
                                    }
                                },
                            ],
                        ],
                        'striped' => false,
                        'condensed' => false,
                        'responsive' => true,
                        'panel' => '',
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>
