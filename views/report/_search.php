<?php

use yii\widgets\ActiveForm;
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var $this \yii\web\View */
/** @var $fields TemplateFields */

?>

<div class="container">
    <?php $searchForm = ActiveForm::begin(['id' => 'search-form', 'method' => "GET"]) ?>
    <div class="row">
        <?php foreach ($fields as $field): ?>

            <?php if($field->type != TemplateFields::TYPE_DROPDOWN) continue; ?>

            <?php

            $data = explode(',', $field->data);

            $data = array_filter($data, function($value) use ($query, $field, $pricesPks, $providerId){

                $query = clone $query;

//                $prices = \app\models\PriceParam::find()->where(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['price_id' => $pricesPks, 'company.id' => $providerId])->joinWith('price.company as company')->count();
                $prices = $query->andWhere(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['price_id' => $pricesPks, 'company.id' => $providerId])->joinWith('price.company as company')->count();

                return $prices > 0;
            });


            $fldValue = null;

            if(isset($_GET['Fld'])){
                $fldValue = ArrayHelper::getValue($_GET['Fld'], $field->id);
            }

            $data = array_combine($data,$data);

            $data[-1] = "Нет";

            ?>

            <div class="col-md-12">
                <div class="form-group">
                    <label for=""><?=$field->label?> <?= $field->hint_title ? Html::a('<i class="fa fa-question-circle"></i>', '#', ['title' => $field->hint_title."\n".$field->hint_content, 'style' => 'font-size: 16px;']) : null ?></label>
                    <?= Html::dropDownList("Fld[{$field->id}]", $fldValue, $data, ['class' => 'form-control', 'prompt' => 'Выберите']) ?>
                </div>
            </div>



        <?php endforeach; ?>

        <div class="col-md-12 hidden">
            <div class="form-group">
                <label for="">Город</label>
                <?php

                $citiesData = [];



                foreach(\app\models\City::find()->where(['city.id' => $searchCitiesPks])->joinWith(['region'])->andWhere(['region.name' => 'Санкт-Петербург и Ленинградская область'])->all() as $city){
                    if($city->region){
                        $citiesData[$city->id] = $city->name;
                    } else {
                        $citiesData[$city->id] = $city->name;
                    }
                }


                ?>
                <?= \kartik\select2\Select2::widget([
                    'name' => 'FldPrice[cities]',
                    'data' => $citiesData,
                    'value' => $cityFilterValue,
                    'options' => [
                        'placeholder' => 'Выберите',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'pluginEvents' => [
                        'change' => 'function(){ $("#search-form").submit(); }'
                    ],
                ]) ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="">Метро</label>
                <?php

                $metroData = [];

                if($cityFilterValue){
                    foreach(\app\models\Metro::find()->andWhere(['city_id' => $cityFilterValue])->all() as $metro){
                        if($metro->city){
                            $metroData[$metro->id] = $metro->city->name.' / '.$metro->name;
                        } else {
                            $citiesData[$metro->id] = $metro->name;
                        }
                    }
                }

                ?>
                <?= \kartik\select2\Select2::widget([
                    'name' => 'FldPrice[metro]',
                    'data' => $metroData,
                    'value' => $metroFilterValue,
                    'options' => [
                        'placeholder' => 'Выберите',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'pluginEvents' => [
                        'change' => 'function(){ $("#search-form").submit(); }'
                    ],
                ]) ?>
            </div>
        </div>

<!--        'headerAccessoryId' => $headerAccessoryId,-->
<!--        'accessoryId' => $accessoryId,-->

        <div class="hidden">
            <input type="text" name="headerAccessoryId" value="<?= $headerAccessoryId ?>">
            <input type="text" name="accessoryId" value="<?= $accessoryId ?>">

        </div>

        <div class="col-md-2">
            <?= Html::a('Очистить', '#', ['class' => 'btn btn-primary', 'style' => 'margin-top: 20px;',
                'onclick' => 'event.preventDefault(); $("#search-form input, #search-form select").each(function(){
                
                    if($(this).attr("name") != "headerAccessoryId" && $(this).attr("name") != "accessoryId"){
                        $(this).val(null);
                    }
                
                });
                
                
                
                $("#search-form").submit();
                '
            ]) ?>
            <?php

//            echo Html::a('Режим PRO', '#', ['class' => 'btn btn-primary', 'style' => 'margin-top: 20px; width: 144px; background: #e6007e!important;',
//                'onclick' => 'event.preventDefault(); $("#search-form input, #search-form select").each(function(){
//
//                    if($(this).attr("name") != "headerAccessoryId" && $(this).attr("name") != "accessoryId"){
//                        $(this).val(null);
//                    }
//
//                });
//
//
//
//                $("#search-form").submit();
//                '
//            ])
            ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>

<?php

$script = <<< JS

$("#search-form input, #search-form select").change(function(){
    $("#search-form").submit();
});


// $("[name='FldPrice[cities]']").change(function(){
//     $.get("/city/get-dropdown-data-metro?id="+$(this).val(), function(response){
//         if(response.count > 0){
//             $("[name='FldPrice[metro]']").html(response.text);
//         } else {
//             $("[name='FldPrice[metro]']").val(null);
//         }
//     });
// });

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>