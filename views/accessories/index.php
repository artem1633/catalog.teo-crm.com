<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccessoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Категории</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <p>
                <?= Html::a('Создать', ['create'],
                    ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']) ?>
            </p>
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_columns.php'),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => '',
            ])?>

        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
