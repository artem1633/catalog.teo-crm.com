<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Accessories */

?>
<div class="accessories-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
