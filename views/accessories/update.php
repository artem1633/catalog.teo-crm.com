<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Accessories */
?>
<div class="accessories-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
