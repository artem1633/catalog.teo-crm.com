<?php
use app\models\Accessories;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Accessories */
/* @var $form yii\widgets\ActiveForm */


$firstAccess = Accessories::find()->where(['is', 'accessories_id', null])->all();
$secondAccess = Accessories::find()->where(['accessories_id' => ArrayHelper::getColumn($firstAccess, 'id')])->all();
$thirdAccessPks = ArrayHelper::getColumn(Accessories::find()->where(['accessories_id' => ArrayHelper::getColumn($secondAccess, 'id')])->all(), 'id');


$data = [];

/** @var Accessories[] $accessData */
$accessData = Accessories::find()->where(['not in', 'id', $thirdAccessPks])->all();

//foreach ($accessData as $access)
//{
//    $names = [$access->name];
//    if($access->accessories_id){
//        $parentAccess = Accessories::findOne($access->accessories_id);
//        $names[] = " ";
//        if($parentAccess){
//            if($parentAccess->accessories_id) {
//                $parentAccess = Accessories::findOne($parentAccess->accessories_id);
//                if($parentAccess){
//                    $names[] = $parentAccess->name;
//                }
//            }
//        }
//    }
//
//    $data[$access->id] = implode('->', array_reverse($names));
//}

$mainAccessories = Accessories::find()->where(['accessories_id' => null])->all();

foreach ($mainAccessories as $accessory)
{
    $data[$accessory->id] = $accessory->name;

    $childs = Accessories::find()->where(['accessories_id' => $accessory->id])->all();

    foreach ($childs as $child)
    {
        $data[$child->id] = "   -> ".$child->name;

        $subChilds = Accessories::find()->where(['accessories_id' => $child->id])->all();

        foreach ($subChilds as $subChild)
        {
            $data[$subChild->id] = "   ->   -> ".$subChild->name;
        }
    }
}


?>

<div class="accessories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accessories_id')->dropDownList($data,['prompt' => 'Выберите категорию'])?>

    <?= $form->field($model, 'is_main')->checkbox() ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
