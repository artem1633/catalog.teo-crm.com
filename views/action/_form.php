<?php
use app\models\Accessories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Action */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
    $model->advs = \app\models\Advert::find()->where(['action_id' => $model->id])->asArray()->all();
}

$list = [];
$type = Accessories::find()->where(['is', 'accessories_id', null])->all();
foreach ($type as $item) {
    $kings = Accessories::find()->where(['accessories_id' => $item->id])->all();

    $realKings = [];

    foreach ($kings as $king)
    {
        $childs = Accessories::find()->where(['accessories_id' => $king->id])->all();

        $add = false;

        foreach ($childs as $child){
            $pricesCount = \app\models\Price::find()->where(['accessory_id' => $child->id])->count();

            if($pricesCount > 0){
                $add = true;
            }
        }

        if($add){
            $realKings[] = $king;
        }

    }

    if(count($realKings) > 0){
        $list[$item->name] = ArrayHelper::map($realKings, 'id', 'name');
    }
}

$data = [];

foreach ($list as $parentName => $childs){
    foreach ($childs as $id => $name){
        $data[$id] = "{$parentName} / {$name}";
    }
}

?>

<div class="action-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'common_price')->checkbox() ?>

    <?= $form->field($model, 'advs')->widget(\unclead\multipleinput\MultipleInput::className(), [
            'max' => 5,
        'addButtonOptions' => [
            'class' => 'btn btn-sm btn-primary btn-my-square',
            'label' => '+',
        ],
        'removeButtonOptions' => [
            'class' => 'btn btn-sm btn-danger btn-my-square',
            'label' => 'x',
        ],
        'columns' => [
            [
                'name'  => 'id',
                'options' => ['style' => 'display: none;'],
            ],
//            [
//                'name'  => 'company_id',
//                'type'  => 'dropDownList',
//                'title' => 'Компания',
//                'items' => \yii\helpers\ArrayHelper::map(\app\models\Company::find()->all(), 'id', 'name')
//            ],
            [
                'name'  => 'accessory_id',
                'type'  => 'dropDownList',
                'title' => 'Услуга',
//                'items' => \yii\helpers\ArrayHelper::map(\app\models\Price::find()->all(), 'id', 'name')
                'items' => $data,
            ],
            [
                'name'  => 'name',
                'title' => 'Наименование',
            ],
            [
                'name'  => 'description',
                'title' => 'Описание',
            ],
            [
                'name'  => 'price',
                'title' => 'Цена',
            ],
            [
                'name'  => 'advsFiles',
                'title' => 'Фото',
                'type' => 'fileInput'
            ],
            [
                'name'  => 'show_date_end',
                'type' => 'checkbox'
            ],
        ]
    ])->label(false);
    ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
