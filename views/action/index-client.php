<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccessoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Акции';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<?php foreach ($dataProvider->models as $model): ?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title" style="font-weight: 700; font-size: 27px;"><?= $model->name ?></h4>
        </div>
        <div class="card-content">
            <div class="card-body" style="padding-top: 0;">
                <p style="margin-bottom: 0; font-size: 12px;"><?= ArrayHelper::getValue($model, 'price.name') ?></p>
                <hr style="margin-top: 3px;">
                <p><?= $model->description ?></p>
            </div>
        </div>
    </div>
<?php endforeach; ?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => [
        'class' => 'modal-slg'
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
