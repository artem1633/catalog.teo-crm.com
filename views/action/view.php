<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Action */
?>
<div class="action-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'name',
            'status',
            'common_price',
            'created_at',
            'end_date',
        ],
    ]) ?>

</div>
