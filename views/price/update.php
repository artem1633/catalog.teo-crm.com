<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Price */
?>
<div class="price-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
