<?php
use app\models\Accessories;
use app\models\Branch;
use app\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Price */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="price-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= Html::a('Загрузить файл', '#', [
        'id' => 'btn-file',
        'class' => 'btn btn-block btn-primary',
        'onclick' => 'event.preventDefault(); $("#priceimportform-file").trigger("click");',
    ]) ?>

    <div class="hidden">
        <?= $form->field($model, 'file')->fileInput() ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$script = <<< JS

$("#priceimportform-file").change(function(e){
    e.preventDefault();

    $("#btn-file").removeClass("btn-primary");
    $("#btn-file").addClass("btn-info");
    $("#btn-file").text($("#priceimportform-file")[0].files[0].name);
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>