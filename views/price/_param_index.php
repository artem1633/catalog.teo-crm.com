<?php
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccessoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $price \app\models\Price */

CrudAsset::register($this);


/** @var TemplateFields[] $fields */
$fields = TemplateFields::find()->where(['accessories_id' => $price->accessory_id])->all();

?>
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Параметры</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <p>
                <?= Html::a('Создать', ['price-param/create', 'price_id' => $price->id],
                    ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']) ?>
                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                <?= Html::a('Импорт <i class="fa fa-cloud-upload"></i>', ['price/import', 'id' => $price->id], ['role' => 'modal-remote', 'class' => 'btn btn-info pull-right']) ?>
            <?php endif; ?>
                <?= Html::a('Экспорт <i class="fa fa-cloud-download"></i>', ['price/export', 'id' => $price->id], ['data-pjax' => 0, 'download' => true, 'class' => 'btn btn-info pull-right', 'style' => 'margin-right: 5px;']) ?>

            </p>


            <?php

            $attributes = [
                [
                    'class' => 'kartik\grid\CheckboxColumn',
                    'width' => '20px',
                ],
            ];

            foreach ($fields as $field){

                if($field->massive_edit){
                    $massiveEditBtn = "<a href='".Url::toRoute(['/price/fld', 'id' => $field->id, 'priceId' => $price->id])."' role='modal-remote' class='btn btn-sm btn-success' style='margin-top: -6px; padding: 0.3rem 1.2rem;'><i class='fa fa-pencil'></i></a>";
                } else {
                    $massiveEditBtn = "";
                }

                $attributes[] = [
                    'header' => "{$field->label} <input type='checkbox' data-update='".$field->id."' style='margin-left: 7px; height: 14px;'> {$massiveEditBtn}",
                    'content' => function($model, $key, $index) use($field){
                        $params = json_decode($model->params, true);

                        $output = "<span data-model-value='".$model->id."_".$field->id."'>".ArrayHelper::getValue($params, $field->id)."</span>";

                        if($field->type == TemplateFields::TYPE_DROPDOWN){
//                            $output .= "<select tabindex='{$key}' style='display: none;' class='form-control' data-model-update='".$model->id."' data-fld-update='".$field->id."' name='fld".$field->id."' value='".ArrayHelper::getValue($params, $field->id)."'>";

                            $data = explode(',', $field->data);

                            if(is_array($data)){
                                foreach ($data as $row) {
                                    $value = ArrayHelper::getValue($params, $field->id);
//                                    $output .= "<option value='{$row}' ".($value == $row ? 'selected' : null).">".$row."</option>";
                                }
                            }

//                            $output .= "</select>";


                            $output .= '<div class="select2-wrapper" style="display: none;">'.\kartik\select2\Select2::widget([
                                'name' => "fld{$field->id}",
                                'data' => array_combine($data, $data),
                                'value' => ArrayHelper::getValue($params, $field->id),
                                'options' => [
                                    'tabindex' => $key,
                                    'data-model-update' => $model->id,
                                    'data-fld-update' => $field->id,
                                ],
                            ]).'</div>';

                        } else {
                            if($field->type == TemplateFields::TYPE_NUMBER){
                                $output .= "<input tabindex='".($index+1)."' type='number' style='display: none;' class='form-control' data-model-update='".$model->id."' data-fld-update='".$field->id."' name='fld".$field->id."' value='".ArrayHelper::getValue($params, $field->id)."'>";
                            } else {
                                $output .= "<input tabindex='".($index+1)."' style='display: none;' class='form-control' data-model-update='".$model->id."' data-fld-update='".$field->id."' name='fld".$field->id."' value='".ArrayHelper::getValue($params, $field->id)."'>";
                            }
                        }

                        return $output;
                    },
                ];
            }

            $attributes = ArrayHelper::merge($attributes, [
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) {
                        return Url::to(["price-param/{$action}",'id'=>$key]);
                    },
                    'buttons' => [
                        'update' => function($url, $model){
                            return Html::a('<i class="feather icon-edit"></i>', $url, [
                                'role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
                            ]);
                        },
                        'delete' => function($url, $model){
                            return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                                'role'=>'modal-remote','title'=>'Удалить',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-toggle'=>'tooltip',
                                'data-confirm-title'=>'',
                                'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                                'style' => 'font-size: 16px;',
                            ]);
                        },
                    ],
                ],
            ]);

            ?>

            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'pjax'=>true,
//                'columns' => require(__DIR__.'/_param_columns.php'),
                'columns' => $attributes,
                'toolbar' => null,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'containerOptions' => ['style' => 'overflow-x: auto;'],
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after' => BulkButtonWidget::widget([
                            'buttons' => Html::a('<i class="feather icon-delete"></i>&nbsp; Удалить',
                                ["price-param/bulk-delete"],
                                [
                                    "class" => "btn btn-pink btn-xs",
                                    'role' => 'modal-remote-bulk',
                                    'data-confirm' => false,
                                    'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Вы уверены?',
                                    'data-confirm-message' => 'Вы действительно хотите удалить данный элемент?'
                                ]) .' '. Html::a('<i class="feather icon-grid"></i>&nbsp; Копировать',
                                    ["price-param/bulk-copy"],
                                    [
                                        "class" => "btn btn-info btn-xs",
                                        'role' => 'modal-remote-bulk',
                                        'data-confirm' => false,
                                        'data-method' => false,// for overide yii data api
                                        'data-request-method' => 'post',
                                        'data-confirm-title' => 'Вы уверены?',
                                        'data-confirm-message' => 'Вы действительно хотите скопировать данный элемент?'
                                    ]),
                        ]) .
                        '<div class="clearfix"></div>',
                ]
            ])?>

        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS

$("[data-update]").change(function(){
    var id = $(this).data('update');

    var colNumber = $(this).parent().data("col-seq");
    
    if($(this).is(":checked")){
        $('td[data-col-seq="'+colNumber+'"] input').show();
        $('td[data-col-seq="'+colNumber+'"] .select2-wrapper').show();
        $('td[data-col-seq="'+colNumber+'"] span[data-model-value]').hide();
        $.get("/price-param/update-ajax?attr=check&value=1&id="+id, function(){ });
    } else {
         $('td[data-col-seq="'+colNumber+'"] input').hide();
         $('td[data-col-seq="'+colNumber+'"] .select2-wrapper').hide();
         $('td[data-col-seq="'+colNumber+'"] span[data-model-value]').show();
        $.get("/price-param/update-ajax?attr=check&value=0&id="+id, function(){ });
    }
});


$("[data-model-update]").change(function(){
    var id = $(this).data('model-update');
    var fldId = $(this).data('fld-update');
    var value = $(this).val();
    
    $.get("/price-param/update-ajax?id="+id+"&fldId="+fldId+"&value="+value, function(){
        $("[data-model-value='"+id+"_"+fldId+"']").text(value);
    });
});


JS;

$this->registerJs($script);

?>