<?php
use app\models\Accessories;
use app\models\Price;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'accessory_id',
        'value' => function($model){
            $output = [ArrayHelper::getValue($model, 'accessory.name')];

            $one = Accessories::findOne($model->accessory_id);

            if($one){
                $two = Accessories::findOne($one->accessories_id);
                if($two){
                    $output[] = "{$two->name}";

//                    if($two){
//                        $three = Accessories::findOne($two->accessories_id);
//                        if($three){
//                            $output[] = "{$three->name}";
//                        }
//                    }
                }
            }

            $output = array_reverse($output);
            $output = implode('/', $output);

            return $output;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'value' => 'company.name',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{publish} {in-black} {view} {copy} {update} {delete}',
        'buttons' => [
            'in-black' => function($url, $model){
                if($model->order_status != Price::ORDER_STATUS_BLACK){
                    return Html::a('<i class="feather icon-corner-up-left"></i>', $url, [
                        'role'=>'modal-remote','title'=>'В черновик',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-cancel'=>'Отмена',
                        'data-confirm-message'=>'Вы уверены что хотите вернуть в черновик этот прайс?',
                        'style' => 'font-size: 16px;',
                    ]);
                }
            },
            'publish' => function($url, $model){
                if($model->order_status != Price::ORDER_STATUS_PUBLISHED){
                    return Html::a('<i class="feather icon-check-square"></i>', $url, [
                        'role'=>'modal-remote','title'=>'Опубликовать',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-cancel'=>'Отмена',
                        'data-confirm-message'=>'Вы уверены что хотите опубликовать этот прайс?',
                        'style' => 'font-size: 16px;',
                    ]);
                }
            },
            'copy' => function($url, $model){
                return Html::a('<i class="feather icon-copy"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Копировать',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-cancel'=>'Отмена',
                    'data-confirm-message'=>'Вы уверены что хотите копировать этот прайс?',
                    'style' => 'font-size: 16px;',
                ]);
            },
            'view' => function($url, $model){
//                return Html::a('<i class="feather icon-eye"></i>', $url, [
//                    'data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
//                ]);
            },
            'update' => function($url, $model){
                return Html::a('<i class="feather icon-edit"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
                ]);
            },
            'delete' => function($url, $model){
                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-cancel'=>'Отмена',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                    'style' => 'font-size: 16px;',
                ]);
            },
        ],
    ],

];   