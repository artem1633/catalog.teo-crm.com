<?php
use app\models\Accessories;
use app\models\Branch;
use app\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Price */
/* @var $form yii\widgets\ActiveForm */

$list = [];
$type = Accessories::find()->where(['is', 'accessories_id', null])->all();
foreach ($type as $item) {
    $kings = Accessories::find()->where(['accessories_id' => $item->id])->all();
    foreach ($kings as $king) {
        $item = Accessories::find()->where(['accessories_id' => $king->id])->one();
        if($item){
            $list[$item->name][$item->id] = $king->name;
        }
    }
}


$branches = Branch::find();
if(Yii::$app->user->identity->isSuperAdmin() == false){
    $branches = $branches->where(['company_id' => Yii::$app->user->identity->company_id]);
}
$branches = ArrayHelper::map($branches->all(), 'id', 'name');



if($model->isNewRecord == false) {
    $model->cities = ArrayHelper::getColumn(\app\models\PriceCity::find()->where(['price_id' => $model->id])->all(), 'city_id');
    $model->cities = ArrayHelper::getColumn(\app\models\City::find()->where(['id' => $model->cities])->all(), 'id');

    $model->metros = ArrayHelper::getColumn(\app\models\PriceMetro::find()->where(['price_id' => $model->id])->all(), 'metro_id');
    $model->metros = ArrayHelper::getColumn(\app\models\Metro::find()->where(['id' => $model->metros])->all(), 'id');
}


$citiesData = [];

foreach(\app\models\City::find()->all() as $city){
    if($city->region){
        $citiesData[$city->id] = $city->region->name.' / '.$city->name;
    } else {
        $citiesData[$city->id] = $city->name;
    }
}


$metroData = [];

foreach(\app\models\Metro::find()->all() as $metro){
    if($metro->city){
        $metroData[$metro->id] = $metro->city->name.' / '.$metro->name;
    } else {
        $citiesData[$metro->id] = $metro->name;
    }
}

//ArrayHelper::map(\app\models\City::find()->all(), 'id', 'name');

?>

<div class="price-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accessory_id')->widget(\kartik\select2\Select2::class, [
        'data' => $list,
        'options' => [
            'placeholder' => 'Выберите',
        ],
    ]) ?>

    <?= $form->field($model, 'branch_id')->dropDownList($branches, ['prompt' => 'Выберите']) ?>


    <div class="hidden">

        <?= $form->field($model, 'cities')->widget(Select2::className(), [
            'data' => $citiesData,
            'options' => [
                'multiple' => true,
            ],
            'pluginOptions' => [
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ]) ?>

        <?= $form->field($model, 'metros')->widget(Select2::className(), [
            'data' => $metroData,
            'options' => [
                'multiple' => true,
            ],
            'pluginOptions' => [
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ]) ?>


        <?= $form->field($model, 'order_status')->dropDownList(\app\models\Price::orderStatusLabels(), ['prompt' => 'Выберите']) ?>
    </div>



    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
