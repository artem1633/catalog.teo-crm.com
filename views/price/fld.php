<?php
use app\models\Accessories;
use app\models\Branch;
use app\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Price */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="price-form">

    <?php $form = ActiveForm::begin(); ?>

    	<div class="form-group">
    		<label>Поле</label>
    		<textarea class="form-control" name="changes"></textarea>
    	</div>

    </div>



    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
