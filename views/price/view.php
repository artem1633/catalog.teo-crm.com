<?php

use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Price */


CrudAsset::register($this);


?>
<div class="price-view">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <?php \yii\widgets\Pjax::begin(['id' => 'pjax-detail-view-container', 'enablePushState' => false]) ?>
                            <p>
                                <?php if($model->order_status != \app\models\Price::ORDER_STATUS_PUBLISHED): ?>
                                    <?= Html::a('Опубликовать', ['price/publish', 'id' => $model->id, 'reloadPjaxContainer' => '#pjax-detail-view-container'], [
                                        'class' => 'btn btn-pink',
                                        'role'=>'modal-remote','title'=>'Опубликовать',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-toggle'=>'tooltip',
                                        'data-confirm-cancel'=>'Отмена',
                                        'data-confirm-message'=>'Вы уверены что хотите опубликовать этот прайс?',
                                        'style' => 'font-size: 16px;',
                                    ]); ?>

                                <?php endif; ?>
                                <?php if($model->order_status != \app\models\Price::ORDER_STATUS_BLACK): ?>
                                    <?= Html::a('Отмена публикации', ['price/in-black', 'id' => $model->id, 'reloadPjaxContainer' => '#pjax-detail-view-container'], [
                                        'class' => 'btn btn-primary',
                                        'role'=>'modal-remote','title'=>'В черновик',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-toggle'=>'tooltip',
                                        'data-confirm-cancel'=>'Отмена',
                                        'data-confirm-message'=>'Вы уверены что хотите вернуть в черновик этот прайс?',
                                        'style' => 'font-size: 16px;',
                                    ]); ?>
                                <?php endif; ?>
                            </p>

                            <?php
//                            echo DetailView::widget([
//                                'model' => $model,
//                                'attributes' => [
//                                    'id',
//                                    'name',
//                                    [
//                                        'attribute' => 'accessory_id',
//                                        'value' => function($model){
//                                            return ArrayHelper::getValue($model, 'accessory.name');
//                                        },
//                                    ],
//                                    [
//                                        'attribute' => 'company_id',
//                                        'value' => function($model){
//                                            return ArrayHelper::getValue($model, 'company.name');
//                                        },
//                                    ],
//                                ],
//                            ])
                            ?>
                        <?php \yii\widgets\Pjax::end(); ?>
                    </div>
                </div>
            </div>

            <?= $this->render('_param_index', [
                'searchModel' => $paramSearchModel,
                'dataProvider' => $paramDataProvider,
                'price' => $model,
            ]) ?>
        </div>
    </div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

