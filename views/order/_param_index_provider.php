<?php
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccessoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $price \app\models\Price */

CrudAsset::register($this);


/** @var TemplateFields[] $fields */
$fields = TemplateFields::find()->where(['accessories_id' => $price->accessory_id])->all();

?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Параметры</h4>
        </div>
        <div class="card-content">
            <div class="card-body">


                <?php

                $attributes = [

                ];

                foreach ($fields as $field){
                    $attributes[] = [
                        'header' => "{$field->label}",
                        'content' => function($model, $key, $index) use($field){
                            $params = json_decode($model->params, true);

                            $output = ArrayHelper::getValue($params, $field->id);


                            return $output;
                        },
                    ];
                }
                ?>

                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                    'pjax'=>true,
//                'columns' => require(__DIR__.'/_param_columns.php'),
                    'columns' => $attributes,
                    'toolbar' => null,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'containerOptions' => ['style' => 'overflow-x: auto;'],
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after' => '',
                    ]
                ])?>

            </div>
        </div>
    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
