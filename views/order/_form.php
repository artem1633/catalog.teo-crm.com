<?php
use app\models\Accessories;
use app\models\Branch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Price */
/* @var $form yii\widgets\ActiveForm */

$list = [];
$type = Accessories::find()->where(['is', 'accessories_id', null])->all();
foreach ($type as $item) {
    $kings = Accessories::find()->where(['accessories_id' => $item->id])->all();
    foreach ($kings as $king) {
//        $list[$item->name][$king->name] = ArrayHelper::map(Accessories::find()->where(['accessories_id' => $king->id])->all(), 'id', 'name');
        $accessory = Accessories::find()->where(['accessories_id' => $king->id])->one();
        if($accessory){
            $list[$item->name][$accessory->id] = $king->name;
        }
    }
}


$branches = Branch::find();
if(Yii::$app->user->identity->isSuperAdmin() == false){
    $branches = $branches->where(['company_id' => Yii::$app->user->identity->company_id]);
}
$branches = ArrayHelper::map($branches->all(), 'id', 'name');

?>

<div class="price-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accessory_id')->widget(\kartik\select2\Select2::class, [
        'data' => $list,
    ]) ?>

    <?= $form->field($model, 'order_status')->dropDownList(\app\models\Price::orderStatusLabels(), ['prompt' => 'Выберите']) ?>



    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
