<?php
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Accessories;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccessoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $price \app\models\Price */

CrudAsset::register($this);


$accessory = Accessories::find()->where(['accessories_id' => $price->accessory_id])->one();


/** @var TemplateFields[] $fields */
$fields = TemplateFields::find()->where(['accessories_id' => $accessory->id])->all();



?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Параметры</h4>
        </div>
        <div class="card-content">
            <div class="card-body">


                <?php

                $attributes = [];

                foreach ($fields as $field){
                    $attributes[] = [
                        'header' => "{$field->label}",
                        'content' => function($model, $key, $index) use($field){
                            $params = json_decode($model->params, true);

                            $output = ArrayHelper::getValue($params, $field->id);

                            return $output;
                        },
                    ];
                }

                //                $attributes = ArrayHelper::merge($attributes, [
                //                    [
                //                        'class' => 'kartik\grid\ActionColumn',
                //                        'dropdown' => false,
                //                        'vAlign'=>'middle',
                //                        'urlCreator' => function($action, $model, $key, $index) {
                //                            return Url::to(["price-param/{$action}",'id'=>$key]);
                //                        },
                //                        'buttons' => [
                //                            'update' => function($url, $model){
                //                                return Html::a('<i class="feather icon-edit"></i>', $url, [
                //                                    'role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
                //                                ]);
                //                            },
                //                            'delete' => function($url, $model){
                //                                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                //                                    'role'=>'modal-remote','title'=>'Удалить',
                //                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                //                                    'data-request-method'=>'post',
                //                                    'data-toggle'=>'tooltip',
                //                                    'data-confirm-title'=>'',
                //                                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                //                                    'style' => 'font-size: 16px;',
                //                                ]);
                //                            },
                //                        ],
                //                    ],
                //                ]);

                ?>

                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                    'pjax'=>true,
//                'columns' => require(__DIR__.'/_param_columns.php'),
                    'columns' => $attributes,
                    'toolbar' => null,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'containerOptions' => ['style' => 'overflow-x: auto;'],
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after' => '',
                    ]
                ])?>

            </div>
        </div>
    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS

$("[data-update]").change(function(){
    var id = $(this).data('update');

    var colNumber = $(this).parent().data("col-seq");
    
    if($(this).is(":checked")){
        $('td[data-col-seq="'+colNumber+'"] input').show();
        $('td[data-col-seq="'+colNumber+'"] select').show();
        $('td[data-col-seq="'+colNumber+'"] span').hide();
        $.get("/price-param/update-ajax?attr=check&value=1&id="+id, function(){ });
    } else {
         $('td[data-col-seq="'+colNumber+'"] input').hide();
         $('td[data-col-seq="'+colNumber+'"] select').hide();
         $('td[data-col-seq="'+colNumber+'"] span').show();
        $.get("/price-param/update-ajax?attr=check&value=0&id="+id, function(){ });
    }
});


$("[data-model-update]").change(function(){
    var id = $(this).data('model-update');
    var fldId = $(this).data('fld-update');
    var value = $(this).val();
    
    $.get("/price-param/update-ajax?id="+id+"&fldId="+fldId+"&value="+value, function(){
        $("[data-model-value='"+id+"_"+fldId+"']").text(value);
    });
});


JS;

$this->registerJs($script);

?>