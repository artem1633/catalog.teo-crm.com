<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccessoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои заказы';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

    <style>
        [data-key] {
            cursor: pointer !important;
        }
    </style>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Мои заказы</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions' => function($model){
                    return ['data-key' => $model->id];
                },
                'pjax'=>true,
                'columns' => require(__DIR__.'/_columns.php'),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => '',
            ])?>

        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'modal-super-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php
$script = <<< JS
$('[data-key]').click(function(e){
    if($(e.target).is('td')){
        var id = $(this).data('key');
        window.location = '/my-order/view?id='+id;
    }
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);
?>