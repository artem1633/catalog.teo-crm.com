<?php

use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Price */
?>
<div class="price-view">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'name',
                                [
                                    'attribute' => 'accessory_id',
                                    'value' => function($model){
                                        return ArrayHelper::getValue($model, 'accessory.name');
                                    },
                                ],
                                [
                                    'attribute' => 'company_id',
                                    'value' => function($model){
                                        return ArrayHelper::getValue($model, 'company.name');
                                    },
                                ],
                                [
                                    'attribute' => 'created_at',
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

            <?php if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER): ?>
                <?= $this->render('_param_index_provider', [
                    'searchModel' => $paramSearchModel,
                    'dataProvider' => $paramDataProvider,
                    'price' => $model,
                ]) ?>
            <?php else: ?>
                <?= $this->render('_param_index', [
                    'searchModel' => $paramSearchModel,
                    'dataProvider' => $paramDataProvider,
                    'price' => $model,
                ]) ?>
            <?php endif; ?>

            <div class="card hidden">
                <div class="card-content">
                    <div class="card-body">

                        <?php echo \rmrevin\yii\module\Comments\widgets\CommentListWidget::widget(['entity' => (string) 'price-'.$model->id,]);?>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
