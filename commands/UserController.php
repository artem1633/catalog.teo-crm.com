<?php

namespace app\commands;

use \yii\console\Controller;
use \app\models\User;

class UserController extends Controller
{
	public function actionGenerateTokens()
	{
		$users = \app\models\User::find()->all();

		foreach ($users as $user) {
			$user->token = md5($this->email.$this->pass);
			$user->save(false);
		}
	}
}
