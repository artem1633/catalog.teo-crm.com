<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', \app\bootstrap\AppBootstrap::class],
//    'defaultRoute' => 'report/index',
    'defaultRoute' => 'user/redirect',
    'timeZone' => 'Europe/Moscow',
    'name' => 'CRM',
    'language' => 'ru-RU',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'GhAcZ2j2hHCv9-XMRK1mi0wYRu29SWwu',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [
                'request' => 'user/send-request',
            ],
        ],
        'mailgun' => [
            'class' => 'app\components\Mailgun',
            'apiKey' => '6cd8a48219b67d1aaf07b0e1a0d55a16-45f7aa85-490a9d4d',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@vendor/rmrevin/yii2-comments/widgets/views' => '@app/views/comments',
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
//                'yii\bootstrap\BootstrapPluginAsset' => [
//                    'js'=>[]
//                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
//                'yii\web\JqueryAsset' => [
//                    'js'=>[]
//                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@runtime/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                // 'username' => 'hrsoft@mmbusiness.ru',
                // 'password' => 'mmbusiness95',
                'username' => 'order2@stampato.ru',
//                'username' => 'hh.notify@yandex.ru',
//                'password' => 'jw*in29nAlw',
//                'password' => 'sYg65FV00&hbrt5$FC',
                'password' => 'L$sAPO3xHbO4',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
//        'elephantio' => [
//            'class' => 'sammaye\elephantio\ElephantIo',
//            'host' => 'http://debtprice.teo-crm.com:3002'
//        ],
        'formatter' => [
            'dateFormat' => 'php:d.m.Y',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => '₽',
            'defaultTimeZone' => 'GMT+3'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'api' => [
            'class' => 'app\modules\api\Api',
        ],
        'comments' => [
            // 'class' => 'teo_crm\yii\module\Comments\Module',
            // 'userIdentityClass' => 'app\models\User',
            // 'useRbac' => false,
            'class' => 'rmrevin\yii\module\Comments\Module',
            'userIdentityClass' => 'app\models\User',
            'useRbac' => false,
            'modelMap' => [
                'Comment' => \app\models\Comment::class,
                'CommentCreateForm' => \app\models\forms\CommentCreateForm::class,
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => ['My' => '@app/vendor/yiisoft/yii2-gii/generators/crud/admincolor']
            ]
        ],
        'allowedIPs' => ['127.0.0.1'],
    ];
}

return $config;
