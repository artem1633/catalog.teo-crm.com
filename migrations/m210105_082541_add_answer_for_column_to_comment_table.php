<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%comment}}`.
 */
class m210105_082541_add_answer_for_column_to_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('comment', 'answer_for', $this->integer()->after('updated_by'));

        $this->createIndex(
            'idx-comment-answer_for',
            'comment',
            'answer_for'
        );

        $this->addForeignKey(
            'fk-comment-answer_for',
            'comment',
            'answer_for',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-comment-answer_for',
            'comment'
        );

        $this->dropIndex(
            'idx-comment-answer_for',
            'comment'
        );

        $this->dropColumn(
            'comment', 'answer_for'
        );
    }
}
