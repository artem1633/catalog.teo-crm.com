<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chat_message}}`.
 */
class m210406_150638_create_chat_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chat_message}}', [
            'id' => $this->primaryKey(),
            'sender_id' => $this->integer()->comment('Отправитель'),
            'receiver_id' => $this->integer()->comment('Получатель'),
            'content' => $this->binary()->comment('Сообщение'),
            'attachment' => $this->string()->comment('Приложение'),
            'attachment_base_name' => $this->string()->comment('Имя приложения'),
            'is_read' => $this->boolean()->defaultValue(false)->comment('Прочитано'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-chat_message-sender_id',
            'chat_message',
            'sender_id'
        );

        $this->addForeignKey(
            'fk-chat_message-sender_id',
            'chat_message',
            'sender_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-chat_message-receiver_id',
            'chat_message',
            'receiver_id'
        );

        $this->addForeignKey(
            'fk-chat_message-receiver_id',
            'chat_message',
            'receiver_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-chat_message-receiver_id',
            'chat_message'
        );

        $this->dropIndex(
            'idx-chat_message-receiver_id',
            'chat_message'
        );

        $this->dropForeignKey(
            'fk-chat_message-sender_id',
            'chat_message'
        );

        $this->dropIndex(
            'idx-chat_message-sender_id',
            'chat_message'
        );

        $this->dropTable('{{%chat_message}}');
    }
}
