<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m201228_105608_add_branch_id_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'branch_id', $this->integer()->comment('Филиал'));

        $this->createIndex(
            'idx-user-branch_id',
            'user',
            'branch_id'
        );

        $this->addForeignKey(
            'fk-user-branch_id',
            'user',
            'branch_id',
            'branch',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user-branch_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-branch_id',
            'user'
        );

        $this->dropColumn('user', 'branch_id');
    }
}
