<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m200602_234109_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->comment('Email'),
            'last_name' => $this->string()->comment('Фамилия'),
            'name' => $this->string()->comment('Имя'),
            'patronymic' => $this->string()->comment('Отчество'),
            'company_id' => $this->integer()->comment('Компания'),
            'phone' => $this->string()->comment('Телефон'),
            'role' => $this->integer()->comment('Роль'),
            'position_id' => $this->integer()->comment('Должность'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'created_at' => $this->dateTime(),
            'avatar'=> $this->string()->comment('Путь до аватара'),
            'is_company_super_admin' => $this->boolean()->comment('Является ли администратором компании'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
        ]);

        $this->insert('user', [
            'email' => 'admin@admin.com',
            'role' => \app\models\User::ROLE_ADMIN,
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'is_deletable' => false,
            'is_company_super_admin' => true,
            'company_id' => 1,
        ]);

        $this->createIndex(
            'idx-user-company_id',
            'user',
            'company_id'
        );

        $this->addForeignKey(
            'fk-user-company_id',
            'user',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-user-position_id',
            'user',
            'position_id'
        );

        $this->addForeignKey(
            'fk-user-position_id',
            'user',
            'position_id',
            'position',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user-position_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-position_id',
            'user'
        );

        $this->dropForeignKey(
            'fk-user-company_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-company_id',
            'user'
        );

        $this->dropTable('user');
    }
}
