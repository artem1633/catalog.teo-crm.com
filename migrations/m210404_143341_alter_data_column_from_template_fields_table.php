<?php

use yii\db\Migration;

/**
 * Class m210404_143341_alter_data_column_from_template_fields_table
 */
class m210404_143341_alter_data_column_from_template_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('template_fields', 'data', $this->text()->comment('Вспомогательные данные'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('template_fields', 'data', $this->text()->comment('Вспомогательные данные'));
    }
}
