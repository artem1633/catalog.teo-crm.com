<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%branch}}`.
 */
class m201228_105111_create_branch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%branch}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'company_id' => $this->integer()->comment('Компания'),
        ]);

        $this->createIndex(
            'idx-branch-company_id',
            'branch',
            'company_id'
        );

        $this->addForeignKey(
            'fk-branch-company_id',
            'branch',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-branch-company_id',
            'branch'
        );

        $this->dropIndex(
            'idx-branch-company_id',
            'branch'
        );

        $this->dropTable('{{%branch}}');
    }
}
