<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%price}}`.
 */
class m210106_071553_add_provider_id_column_to_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('price', 'provider_id', $this->integer()->comment('Поставщик'));

        $this->createIndex(
            'idx-price-provider_id',
            'price',
            'provider_id'
        );

        $this->addForeignKey(
            'fk-price-provider_id',
            'price',
            'provider_id',
            'company',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-price-provider_id',
            'price'
        );

        $this->dropIndex(
            'idx-price-provider_id',
            'price'
        );

        $this->dropColumn('price', 'provider_id');
    }
}
