<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%advert}}`.
 */
class m210405_141133_add_price_column_to_advert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('advert', 'price', $this->float()->after('description')->comment('Цена'));
        $this->addColumn('advert', 'photo', $this->string()->after('price')->comment('Фото'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('advert', 'price');
        $this->dropColumn('advert', 'photo');
    }
}
