<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210326_111215_create_faq_table`.
 */
class m210326_111215_create_faq_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('faq', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'content' => $this->text()->comment('Содержимое'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('faq');
    }
}
