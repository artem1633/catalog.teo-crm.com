<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%company}}`.
 */
class m201228_120954_add_providers_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'providers', $this->text()->comment('Поставщики'));
        $this->addColumn('company', 'products', $this->text()->comment('Товары/Услуги'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'providers');
        $this->dropColumn('company', 'products');
    }
}
