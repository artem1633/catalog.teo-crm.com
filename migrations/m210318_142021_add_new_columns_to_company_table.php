<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%company}}`.
 */
class m210318_142021_add_new_columns_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'official_name', $this->string()->after('name')->comment('Официальное наименование'));
        $this->addColumn('company', 'official_name_short', $this->string()->after('official_name')->comment('Сокращенное официальное наименование'));
        $this->addColumn('company', 'organization_type', $this->integer()->after('official_name_short')->comment('Тип лица'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'official_name');
        $this->dropColumn('company', 'official_name_short');
        $this->dropColumn('company', 'organization_type');
    }
}
