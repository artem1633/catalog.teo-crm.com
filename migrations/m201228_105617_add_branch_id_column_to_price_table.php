<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%price}}`.
 */
class m201228_105617_add_branch_id_column_to_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('price', 'branch_id', $this->integer()->comment('Филиал'));

        $this->createIndex(
            'idx-price-branch_id',
            'price',
            'branch_id'
        );

        $this->addForeignKey(
            'fk-price-branch_id',
            'price',
            'branch_id',
            'branch',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-price-branch_id',
            'price'
        );

        $this->dropIndex(
            'idx-price-branch_id',
            'price'
        );

        $this->dropColumn('price', 'branch_id');
    }
}
