<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%price_metro}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%price}}`
 * - `{{%metro}}`
 */
class m210425_073652_create_junction_table_for_price_and_metro_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%price_metro}}', [
            'id' => $this->primaryKey(),
            'price_id' => $this->integer(),
            'metro_id' => $this->integer(),
        ]);

        // creates index for column `price_id`
        $this->createIndex(
            '{{%idx-price_metro-price_id}}',
            '{{%price_metro}}',
            'price_id'
        );

        // add foreign key for table `{{%price}}`
        $this->addForeignKey(
            '{{%fk-price_metro-price_id}}',
            '{{%price_metro}}',
            'price_id',
            '{{%price}}',
            'id',
            'CASCADE'
        );

        // creates index for column `metro_id`
        $this->createIndex(
            '{{%idx-price_metro-metro_id}}',
            '{{%price_metro}}',
            'metro_id'
        );

        // add foreign key for table `{{%metro}}`
        $this->addForeignKey(
            '{{%fk-price_metro-metro_id}}',
            '{{%price_metro}}',
            'metro_id',
            '{{%metro}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%price}}`
        $this->dropForeignKey(
            '{{%fk-price_metro-price_id}}',
            '{{%price_metro}}'
        );

        // drops index for column `price_id`
        $this->dropIndex(
            '{{%idx-price_metro-price_id}}',
            '{{%price_metro}}'
        );

        // drops foreign key for table `{{%metro}}`
        $this->dropForeignKey(
            '{{%fk-price_metro-metro_id}}',
            '{{%price_metro}}'
        );

        // drops index for column `metro_id`
        $this->dropIndex(
            '{{%idx-price_metro-metro_id}}',
            '{{%price_metro}}'
        );

        $this->dropTable('{{%price_metro}}');
    }
}
