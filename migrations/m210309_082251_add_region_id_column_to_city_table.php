<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%city}}`.
 */
class m210309_082251_add_region_id_column_to_city_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('city', 'region_id', $this->integer()->comment('Регион'));

        $this->createIndex(
            'idx-city-region_id',
            'city',
            'region_id'
        );

        $this->addForeignKey(
            'fk-city-region_id',
            'city',
            'region_id',
            'region',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-city-region_id',
            'city'
        );

        $this->dropIndex(
            'idx-city-region_id',
            'city'
        );

        $this->dropColumn(
            'city',
                'region_id'
        );
    }
}
