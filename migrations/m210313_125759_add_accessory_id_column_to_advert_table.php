<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%advert}}`.
 */
class m210313_125759_add_accessory_id_column_to_advert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('advert', 'accessory_id', $this->integer()->after('id')->comment('Категория'));

        $this->createIndex(
            'idx-advert-accessory_id',
            'advert',
            'accessory_id'
        );

        $this->addForeignKey(
            'fk-advert-accessory_id',
            'advert',
            'accessory_id',
            'accessories',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-advert-accessory_id',
            'advert'
        );

        $this->dropIndex(
            'idx-advert-accessory_id',
            'advert'
        );

        $this->dropColumn('advert', 'accessory_id');
    }
}
