<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%branch}}`.
 */
class m210318_145840_add_new_columns_to_branch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('branch', 'email', $this->string()->comment('Email'));
        $this->addColumn('branch', 'phone', $this->string()->comment('Телефон'));
        $this->addColumn('branch', 'address', $this->string()->comment('Адрес'));
        $this->addColumn('branch', 'city_id', $this->integer()->comment('Город'));
        $this->addColumn('branch', 'metro_id', $this->integer()->comment('Метро'));
        $this->addColumn('branch', 'avatar', $this->string()->comment('Аватар'));

        $this->addColumn('branch', 'social_site', $this->string()->comment('Сайт'));
        $this->addColumn('branch', 'social_google', $this->string()->comment('Google'));
        $this->addColumn('branch', 'social_vk', $this->string()->comment('ВК'));
        $this->addColumn('branch', 'social_facebook', $this->string()->comment('Facebook'));
        $this->addColumn('branch', 'social_instagram', $this->string()->comment('Instagram'));
        $this->addColumn('branch', 'social_youtube', $this->string()->comment('Youtube'));

        $this->addColumn('branch', 'work_mon_enable', $this->boolean()->comment('ПН'));
        $this->addColumn('branch', 'work_tue_enable', $this->boolean()->comment('ВТ'));
        $this->addColumn('branch', 'work_wed_enable', $this->boolean()->comment('СР'));
        $this->addColumn('branch', 'work_thu_enable', $this->boolean()->comment('ЧТ'));
        $this->addColumn('branch', 'work_fri_enable', $this->boolean()->comment('ПТ'));
        $this->addColumn('branch', 'work_sat_enable', $this->boolean()->comment('СБ'));
        $this->addColumn('branch', 'work_sun_enable', $this->boolean()->comment('ВС'));

        $this->addColumn('branch', 'work_mon_hours', $this->string()->comment('Часы работы, ПН'));
        $this->addColumn('branch', 'work_tue_hours', $this->string()->comment('Часы работы, ВТ'));
        $this->addColumn('branch', 'work_wed_hours', $this->string()->comment('Часы работы, СР'));
        $this->addColumn('branch', 'work_thu_hours', $this->string()->comment('Часы работы, ЧТ'));
        $this->addColumn('branch', 'work_fri_hours', $this->string()->comment('Часы работы, ПТ'));
        $this->addColumn('branch', 'work_sat_hours', $this->string()->comment('Часы работы, СБ'));
        $this->addColumn('branch', 'work_sun_hours', $this->string()->comment('Часы работы, ВС'));

        $this->createIndex(
            'idx-branch-city_id',
            'branch',
            'city_id'
        );

        $this->addForeignKey(
            'fk-branch-city_id',
            'branch',
            'city_id',
            'city',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-branch-metro_id',
            'branch',
            'metro_id'
        );

        $this->addForeignKey(
            'fk-branch-metro_id',
            'branch',
            'metro_id',
            'metro',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-branch-city_id',
            'branch'
        );

        $this->dropIndex(
            'idx-branch-city_id',
            'branch'
        );

        $this->dropForeignKey(
            'fk-branch-metro_id',
            'branch'
        );

        $this->dropIndex(
            'idx-branch-metro_id',
            'branch'
        );

        $this->dropColumn('branch', 'work_mon_hours');
        $this->dropColumn('branch', 'work_tue_hours');
        $this->dropColumn('branch', 'work_wed_hours');
        $this->dropColumn('branch', 'work_thu_hours');
        $this->dropColumn('branch', 'work_fri_hours');
        $this->dropColumn('branch', 'work_sat_hours');
        $this->dropColumn('branch', 'work_sun_hours');

        $this->dropColumn('branch', 'work_mon_enable');
        $this->dropColumn('branch', 'work_tue_enable');
        $this->dropColumn('branch', 'work_wed_enable');
        $this->dropColumn('branch', 'work_thu_enable');
        $this->dropColumn('branch', 'work_fri_enable');
        $this->dropColumn('branch', 'work_sat_enable');
        $this->dropColumn('branch', 'work_sun_enable');

        $this->dropColumn('branch', 'social_site');
        $this->dropColumn('branch', 'social_google');
        $this->dropColumn('branch', 'social_vk');
        $this->dropColumn('branch', 'social_facebook');
        $this->dropColumn('branch', 'social_instagram');
        $this->dropColumn('branch', 'social_youtube');

        $this->dropColumn('branch', 'email');
        $this->dropColumn('branch', 'phone');
        $this->dropColumn('branch', 'address');
        $this->dropColumn('branch', 'city_id');
        $this->dropColumn('branch', 'metro_id');
        $this->dropColumn('branch', 'avatar');
    }
}
