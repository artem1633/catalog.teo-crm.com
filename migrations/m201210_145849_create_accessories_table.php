<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%accessories}}`.
 */
class m201210_145849_create_accessories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('accessories', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'accessories_id' => $this->integer()->comment('Родительская категория'),
            'eye' => $this->boolean()->defaultValue(1)->comment('Показ'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('accessories');
    }
}
