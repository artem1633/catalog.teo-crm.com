<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%template_fields}}`.
 */
class m210716_104544_add_sort_column_to_template_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('template_fields', 'sort', $this->integer()->defaultValue(0)->comment('Сортировка'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('template_fields', 'sort');
    }
}
