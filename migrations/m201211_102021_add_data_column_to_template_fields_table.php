<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%template_fields}}`.
 */
class m201211_102021_add_data_column_to_template_fields_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('template_fields', 'data', $this->string()->comment('Вспомогательные данные'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('template_fields', 'data');
    }
}
