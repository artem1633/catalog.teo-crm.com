<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%template_fields}}`.
 */
class m211022_111855_add_massive_edit_column_to_template_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('template_fields', 'massive_edit', $this->boolean()->defaultValue(false)->comment('Массовое редактирование'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('template_fields', 'massive_edit');
    }
}
