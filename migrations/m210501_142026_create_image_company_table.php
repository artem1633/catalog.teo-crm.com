<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%image_company}}`.
 */
class m210501_142026_create_image_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%image_company}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'name' => $this->string()->comment('Наименование'),
            'path' => $this->string()->comment('Путь'),
            'created_at' => $this->dateTime()->comment('Дата и время загрузки'),
        ]);

        $this->createIndex(
            'idx-image_company-company_id',
            'image_company',
            'company_id'
        );

        $this->addForeignKey(
            'fk-image_company-company_id',
            'image_company',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-image_company-company_id',
            'image_company'
        );

        $this->dropIndex(
            'idx-image_company-company_id',
            'image_company'
        );

        $this->dropTable('{{%image_company}}');
    }
}
