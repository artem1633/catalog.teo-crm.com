<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%metro}}`.
 */
class m210309_082725_create_metro_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%metro}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'city_id' => $this->integer()->comment('Город'),
        ]);

        $this->createIndex(
            'idx-metro-city_id',
            'metro',
            'city_id'
        );

        $this->addForeignKey(
            'fk-metro-city_id',
            'metro',
            'city_id',
            'city',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-metro-city_id',
            'metro'
        );

        $this->dropIndex(
            'idx-metro-city_id',
            'metro'
        );

        $this->dropTable('{{%metro}}');
    }
}
