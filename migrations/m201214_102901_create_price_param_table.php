<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%price_param}}`.
 */
class m201214_102901_create_price_param_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%price_param}}', [
            'id' => $this->primaryKey(),
            'price_id' => $this->integer()->comment('Прайс'),
            'company_id' => $this->integer()->comment('Компания'),
            'params' => $this->text()->comment('Параметры'),
            'check' => $this->boolean()->defaultValue(false)->comment('Чек'),
        ]);

        $this->createIndex(
            'idx-price_param-price_id',
            'price_param',
            'price_id'
        );

        $this->addForeignKey(
            'fk-price_param-price_id',
            'price_param',
            'price_id',
            'price',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-price_param-company_id',
            'price_param',
            'company_id'
        );

        $this->addForeignKey(
            'fk-price_param-company_id',
            'price_param',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-price_param-price_id',
            'price_param'
        );

        $this->dropIndex(
            'idx-price_param-price_id',
            'price_param'
        );

        $this->dropForeignKey(
            'fk-price_param-company_id',
            'price_param'
        );

        $this->dropIndex(
            'idx-price_param-company_id',
            'price_param'
        );

        $this->dropTable('{{%price_param}}');
    }
}
