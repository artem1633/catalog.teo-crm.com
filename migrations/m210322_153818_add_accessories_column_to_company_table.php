<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%company}}`.
 */
class m210322_153818_add_accessories_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'accessories', $this->text()->comment('Услуги'));
        $this->addColumn('company', 'accessories_other', $this->text()->comment('Недостающие услуги'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'accessories_other');
        $this->dropColumn('company', 'accessories');
    }
}
