<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%price}}`.
 */
class m210105_153241_add_new_columns_to_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('price', 'is_provider_order', $this->boolean()->defaultValue(false)->comment('Заказ'));
        $this->addColumn('price', 'order_items', $this->text()->comment('Тело заказа'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('price', 'is_provider_order');
        $this->dropColumn('price', 'order_items');
    }
}
