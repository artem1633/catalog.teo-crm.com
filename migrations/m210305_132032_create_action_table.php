<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%action}}`.
 */
class m210305_132032_create_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%action}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'name' => $this->string()->comment('Название компании'),
            'status' => $this->integer()->comment('Статус'),
            'common_price' => $this->boolean()->defaultValue(false)->comment('Единая цена'),
            'created_at' => $this->dateTime()->comment('Создан'),
            'end_date' => $this->date()->comment('Дата завершения'),
        ]);

        $this->createIndex(
            'idx-action-company_id',
            'action',
            'company_id'
        );

        $this->addForeignKey(
            'fk-action-company_id',
            'action',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-action-company_id',
            'action'
        );

        $this->dropIndex(
            'idx-action-company_id',
            'action'
        );

        $this->dropTable('{{%action}}');
    }
}
