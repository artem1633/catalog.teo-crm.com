<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%template_fields}}`.
 */
class m210304_140118_add_new_columns_to_template_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('template_fields', 'hint_title', $this->string()->comment('Заголовок подсказки'));
        $this->addColumn('template_fields', 'hint_content', $this->text()->comment('Описание подсказки'));
        $this->addColumn('template_fields', 'is_main', $this->boolean()->defaultValue(false)->comment('Основной'));
        $this->addColumn('template_fields', 'parent_id', $this->integer()->comment('Основной'));

        $this->createIndex(
            'idx-template_fields-parent_id',
            'template_fields',
            'parent_id'
        );

        $this->addForeignKey(
            'fk-template_fields-parent_id',
            'template_fields',
            'parent_id',
            'template_fields',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-template_fields-parent_id',
            'template_fields'
        );

        $this->dropIndex(
            'idx-template_fields-parent_id',
            'template_fields'
        );

        $this->dropColumn('template_fields', 'hint_title');
        $this->dropColumn('template_fields', 'hint_content');
        $this->dropColumn('template_fields', 'is_main');
        $this->dropColumn('template_fields', 'parent_id');
    }
}
