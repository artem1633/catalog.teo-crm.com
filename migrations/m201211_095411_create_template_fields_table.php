<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%template_fields}}`.
 */
class m201211_095411_create_template_fields_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('template_fields', [
            'id' => $this->primaryKey(),
            'accessories_id' => $this->integer()->comment('Категория'),
            'name' => $this->string()->comment('Наименование (на англ.)'),
            'label' => $this->string()->comment('Лейбел поля'),
            'type' => $this->string()->comment('Тип поля'),
        ]);
        $this->addCommentOnTable('template_fields', 'Поля шаблона');

        $this->createIndex(
            'idx-template_fields-accessories_id',
            'template_fields',
            'accessories_id'
        );

        $this->addForeignKey(
            'fk-template_fields-accessories_id',
            'template_fields',
            'accessories_id',
            'accessories',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-template_fields-accessories_id',
            'template_fields'
        );

        $this->dropIndex(
            'idx-template_fields-accessories_id',
            'template_fields'
        );

        $this->dropTable('template_fields');
    }
}
