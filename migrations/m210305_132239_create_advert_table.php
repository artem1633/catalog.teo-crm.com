<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advert}}`.
 */
class m210305_132239_create_advert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advert}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer()->comment('Акция'),
            'company_id' => $this->integer()->comment('Компания'),
            'price_id' => $this->integer()->comment('Услуга'),
            'name' => $this->string()->comment('Название'),
            'description' => $this->text()->comment('Описание'),
            'view' => $this->integer()->comment('Кол-во просмотров'),
            'created_at' => $this->dateTime()->comment('Создан'),
            'show_date_end' => $this->boolean()->defaultValue(false)->comment('Показывать время окончания'),
        ]);

        $this->createIndex(
            'idx-advert-action_id',
            'advert',
            'action_id'
        );

        $this->addForeignKey(
            'fk-advert-action_id',
            'advert',
            'action_id',
            'action',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-advert-company_id',
            'advert',
            'company_id'
        );

        $this->addForeignKey(
            'fk-advert-company_id',
            'advert',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-advert-price_id',
            'advert',
            'price_id'
        );

        $this->addForeignKey(
            'fk-advert-price_id',
            'advert',
            'price_id',
            'price',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-advert-price_id',
            'advert'
        );

        $this->dropIndex(
            'idx-advert-price_id',
            'advert'
        );

        $this->dropForeignKey(
            'fk-advert-company_id',
            'advert'
        );

        $this->dropIndex(
            'idx-advert-company_id',
            'advert'
        );

        $this->dropForeignKey(
            'fk-advert-action_id',
            'advert'
        );

        $this->dropIndex(
            'idx-advert-action_id',
            'advert'
        );

        $this->dropTable('{{%advert}}');
    }
}
