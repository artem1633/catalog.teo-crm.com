<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%price}}`.
 */
class m201214_094532_create_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%price}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'accessory_id' => $this->integer()->comment('Категория'),
            'company_id' => $this->integer()->comment('Компания'),
        ]);

        $this->createIndex(
            'idx-price-accessory_id',
            'price',
            'accessory_id'
        );

        $this->addForeignKey(
            'fk-price-accessory_id',
            'price',
            'accessory_id',
            'accessories',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-price-company_id',
            'price',
            'company_id'
        );

        $this->addForeignKey(
            'fk-price-company_id',
            'price',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-price-accessory_id',
            'price'
        );

        $this->dropIndex(
            'idx-price-accessory_id',
            'price'
        );

        $this->dropForeignKey(
            'fk-price-company_id',
            'price'
        );

        $this->dropIndex(
            'idx-price-company_id',
            'price'
        );

        $this->dropTable('{{%price}}');
    }
}
