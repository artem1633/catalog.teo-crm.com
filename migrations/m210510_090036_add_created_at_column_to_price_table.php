<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%price}}`.
 */
class m210510_090036_add_created_at_column_to_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('price', 'created_at', $this->dateTime()->comment('Дата и время'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('price', 'created_at');
    }
}
