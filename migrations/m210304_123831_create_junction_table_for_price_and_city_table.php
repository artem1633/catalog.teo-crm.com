<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%price_city}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%price}}`
 * - `{{%city}}`
 */
class m210304_123831_create_junction_table_for_price_and_city_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%price_city}}', [
            'id' => $this->primaryKey(),
            'price_id' => $this->integer(),
            'city_id' => $this->integer(),
        ]);

        // creates index for column `price_id`
        $this->createIndex(
            '{{%idx-price_city-price_id}}',
            '{{%price_city}}',
            'price_id'
        );

        // add foreign key for table `{{%price}}`
        $this->addForeignKey(
            '{{%fk-price_city-price_id}}',
            '{{%price_city}}',
            'price_id',
            '{{%price}}',
            'id',
            'CASCADE'
        );

        // creates index for column `city_id`
        $this->createIndex(
            '{{%idx-price_city-city_id}}',
            '{{%price_city}}',
            'city_id'
        );

        // add foreign key for table `{{%city}}`
        $this->addForeignKey(
            '{{%fk-price_city-city_id}}',
            '{{%price_city}}',
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%price}}`
        $this->dropForeignKey(
            '{{%fk-price_city-price_id}}',
            '{{%price_city}}'
        );

        // drops index for column `price_id`
        $this->dropIndex(
            '{{%idx-price_city-price_id}}',
            '{{%price_city}}'
        );

        // drops foreign key for table `{{%city}}`
        $this->dropForeignKey(
            '{{%fk-price_city-city_id}}',
            '{{%price_city}}'
        );

        // drops index for column `city_id`
        $this->dropIndex(
            '{{%idx-price_city-city_id}}',
            '{{%price_city}}'
        );

        $this->dropTable('{{%price_city}}');
    }
}
