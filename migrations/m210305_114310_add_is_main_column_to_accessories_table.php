<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%accessories}}`.
 */
class m210305_114310_add_is_main_column_to_accessories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('accessories', 'is_main', $this->boolean()->defaultValue(false)->comment('Основная'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('accessories', 'is_main');
    }
}
