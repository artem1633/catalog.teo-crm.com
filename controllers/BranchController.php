<?php

namespace app\controllers;

use Yii;
use app\models\Branch;
use app\models\BranchSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * BranchController implements the CRUD actions for Branch model.
 */
class BranchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Branch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('@app/views/user/left-profile');

        $searchModel = new BranchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddHours($id, $attr)
    {
        $model = $this->findModel($id);

        $data = json_decode($model->$attr);

        if(is_array($data) == false){
            $data = [];
        }

        if(count($data) == 0){
            $data[] = [null, null];
            $data[] = [null, null];
        } else {
            $data[] = [null, null];
        }

        $model->$attr = json_encode($data);

        $model->save(false);
    }


    public function actionDeleteHours($id, $attr, $i)
    {
        $model = $this->findModel($id);

        $data = json_decode($model->$attr);

        $newData = [];

        for ($j = 0; $j < count($data); $j++)
        {
            if($j != $i){
                $newData[] = $data[$j];
            }
        }

        $model->$attr = json_encode($newData);

        $model->save(false);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionWorkHours($id)
    {
        $request = Yii::$app->request;
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('@app/views/user/left-profile');

        $model = $this->findModel($id);


        if($request->isPost) {
            $model->work_mon_hours = $this->saveHoursFromPost('work_mon_hours');
            $model->work_tue_hours = $this->saveHoursFromPost('work_tue_hours');
            $model->work_wed_hours = $this->saveHoursFromPost('work_wed_hours');
            $model->work_thu_hours = $this->saveHoursFromPost('work_thu_hours');
            $model->work_fri_hours = $this->saveHoursFromPost('work_fri_hours');
            $model->work_sat_hours = $this->saveHoursFromPost('work_sat_hours');
            $model->work_sun_hours = $this->saveHoursFromPost('work_sun_hours');

            $model->work_mon_enable = ArrayHelper::getValue($_POST, 'Branch.work_mon_enable') == 'on' ? 1 : 0;
            $model->work_tue_enable = ArrayHelper::getValue($_POST, 'Branch.work_tue_enable') == 'on' ? 1 : 0;
            $model->work_wed_enable = ArrayHelper::getValue($_POST, 'Branch.work_wed_enable') == 'on' ? 1 : 0;
            $model->work_thu_enable = ArrayHelper::getValue($_POST, 'Branch.work_thu_enable') == 'on' ? 1 : 0;
            $model->work_fri_enable = ArrayHelper::getValue($_POST, 'Branch.work_fri_enable') == 'on' ? 1 : 0;
            $model->work_sat_enable = ArrayHelper::getValue($_POST, 'Branch.work_sat_enable') == 'on' ? 1 : 0;
            $model->work_sun_enable = ArrayHelper::getValue($_POST, 'Branch.work_sun_enable') == 'on' ? 1 : 0;


            $model->save(false);
        }

        return $this->render('hours-work', [
            'model' => $model,
        ]);
    }

    private function saveHoursFromPost($attribute)
    {
        if(isset($_POST[$attribute])){
            $data = [];
            foreach ($_POST[$attribute] as $row){
                $data[] = [$row['from'], $row['to']];
            }
            return json_encode($data);
        }
    }

    /**
     * Displays a single Branch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Сохранить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Branch model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Branch();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate()){

                $model->file = UploadedFile::getInstance($model, 'file');
                $model->save(false);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "",
                    'content'=>'<span class="text-success">Создание успешно завершено</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Branch model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->validate()){

                $model->file = UploadedFile::getInstance($model, 'file');
                $model->save(false);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Сохранить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Branch model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Branch model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Branch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Branch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Branch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
