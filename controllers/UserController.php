<?php

namespace app\controllers;

use app\models\Accessories;
use app\models\Branch;
use app\models\ChatMessage;
use app\models\City;
use app\models\Company;
use app\models\Faq;
use app\models\forms\AvatarForm;
use app\models\forms\CityCashForm;
use app\models\forms\OrderForm;
use app\models\ImageCompany;
use app\models\TemplateFields;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\forms\ResetPasswordForm;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => ['edit-city', 'redirect', 'faq', 'send-request', 'get-subcategories', 'get-params', 'get-params-data'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->isSuperAdmin() == false){
            if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
                $this->view->params['left'] = true;
                $this->view->params['left-content'] = $this->renderAjax('@app/views/user/left-profile');
            }

            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array
     */
    public function actionEditCity()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new CityCashForm();

        if($request->isGet){
            return [
                'title'=> "",
                'content'=>$this->renderAjax('edit-city', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else if($model->load($request->post()) && $model->saveInCash()){
            return [
                'title'=> "",
                'content'=> "<span class='text-success'>Сохранение <i class='fa fa-spinner fa-spin'></i></span><script>window.location.reload();</script>",
                'footer'=> '',
            ];
        }else{
            return [
                'title'=> "",
                'content'=>$this->renderAjax('edit-city', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    /**
     *
     */
    public function actionProfile()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');
        $request = Yii::$app->request;


        /** @var Company $model */
        $model = Yii::$app->user->identity->company;
        $modelUser = Yii::$app->user->identity;

        if($model->load($request->post()) && $model->validate()){

            $model->file = UploadedFile::getInstance($model, 'file');

            $model->save(false);

            Yii::$app->session->setFlash('success', 'Изменения сохранены');

            if($modelUser->load($request->post()) && $modelUser->save()){

            }

            return $this->render('profile', [
                'model' => $model,
                'modelUser' => $modelUser,
            ]);
        }

        return $this->render('profile', [
            'model' => $model,
            'modelUser' => $modelUser,
        ]);
    }


    /**
     *
     */
    public function actionDescription()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');
        $request = Yii::$app->request;

        /** @var Company $model */
        $model = Yii::$app->user->identity->company;

        if($model->load($request->post()) && $model->save()){

            Yii::$app->session->setFlash('success', 'Изменения сохранены');

            return $this->render('description', [
                'model' => $model,
            ]);
        }

        return $this->render('description', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionGallery()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');
        $request = Yii::$app->request;

        /** @var Company $model */
        $model = Yii::$app->user->identity->company;

        return $this->render('gallery', [
            'model' => $model,
        ]);
    }


    public function actionDeleteCompanyPhoto($id = null)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = ImageCompany::findOne($id);

        if($model){
            $model->delete();
        }

        return [
            'forceReload' => '#pjax-gallery-container',
            'forceClose' => true,
        ];
    }

    public function actionUploadCompanyPhoto($companyId)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($request->isPost) {
            return [
                'forceReload' => '#pjax-gallery-container',
                'forceClose' => true,
            ];
        }

        return [
            'title' => "Загрузить файл",
            'content' => $this->renderAjax('upload-image', [
                'companyId' => $companyId,
            ]),
            'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                Html::button('Готово', ['class' => 'btn btn-primary', 'type' => "submit"])
        ];
    }

    public function actionUploadCompanyPhotoTotal($companyId = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $fileName = Yii::$app->security->generateRandomString();
        if (is_dir('uploads') == false) {
            mkdir('uploads');
        }
        $uploadPath = 'uploads';
        if (isset($_FILES['file'])) {
            $file = UploadedFile::getInstanceByName('file');
            $path = $uploadPath . '/' . $fileName . '.' . $file->extension;

            $name = "{$file->baseName}.{$file->extension}";


            if ($file->saveAs($path)) {
//
                $file = new ImageCompany([
                    'company_id' => $companyId,
                    'name' => $name,
                    'path' => $path,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
                $file->save(false);

                return $path;
            }
        }
    }


    /**
     *
     */
    public function actionFaq()
    {
        if(Yii::$app->user->isGuest == false){
            $this->view->params['left'] = true;
            $this->view->params['left-content'] = $this->renderAjax('left-profile');
        }

        $faqs = Faq::find();


        if(Yii::$app->user->isGuest){
            $faqs->andWhere(['type' => Faq::TYPE_UNREGISTERED]);
        } else {
            if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER){
                $faqs->andWhere(['type' => Faq::TYPE_BUYER]);
            } elseif(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER) {
                $faqs->andWhere(['type' => Faq::TYPE_PROVIDER]);
            }
        }

        $faqs = $faqs->all();

        return $this->render('faq', [
            'faqs' => $faqs,
        ]);
    }

    /**
     *
     */
    public function actionTariff()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');


        return $this->render('tariff', [
        ]);
    }

    /**
     *
     */
    public function actionIdea()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');


        return $this->render('idea', [
        ]);
    }

    /**
     *
     */
    public function actionQuestion()
    {
        $request = Yii::$app->request;
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');

        if($request->isPost){
            Yii::$app->session->addFlash('success', 'Письмо успешно отправлено');

            $subject = isset($_POST['subject']) ? $_POST['subject'] : null;
            $content = isset($_POST['content']) ? $_POST['content'] : null;

            if($subject && $content){
                try {
                    Yii::$app->mailer->compose()
//                        ->setFrom('hh.notify@yandex.ru')
                        ->setFrom('order2@stampato.ru')
                        ->setTo('info@stampato.ru')
                        ->setSubject('Вопрос от пользоватля ('.$subject.')')
                        ->setTextBody($content)
                        ->send();
                } catch (\Exception $e) {
                    \Yii::warning($e->getMessage());
                }
            }
        }

        return $this->render('question', [
        ]);
    }


    /**
     *
     */
    public function actionMyProviders()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');
        $request = Yii::$app->request;

        /** @var Company $model */
        $model = Yii::$app->user->identity->company;

        $providers = $model->providers;

        if($providers == null){
            $providersPks = [];
        } else {
            $providersPks = explode(',', $model->providers);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Company::find()->where(['type' => Company::TYPE_PROVIDER])->andWhere(['id' => $providersPks]),
        ]);

        return $this->render('my-providers', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *
     */
    public function actionRequisites()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');
        $request = Yii::$app->request;

        /** @var Company $model */
        $model = Yii::$app->user->identity->company;

        if($model->load($request->post()) && $model->save()){

            Yii::$app->session->setFlash('success', 'Изменения сохранены');

            return $this->render('requisites', [
                'model' => $model,
            ]);
        }

        return $this->render('requisites', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionAccessories()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');
        $request = Yii::$app->request;

        /** @var Company $model */
        $model = Yii::$app->user->identity->company;

        if($request->isPost){

            $data = isset($_POST['AccessoriesArr']) ? $_POST['AccessoriesArr'] : [];

            $model->accessories = json_encode(array_keys($data));
            $model->save(false);

            Yii::$app->session->setFlash('success', 'Изменения сохранены');

            return $this->render('accessories', [
                'model' => $model,
            ]);
        }

        return $this->render('accessories', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionAccessoriesOther()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('left-profile');
        $request = Yii::$app->request;

        /** @var Company $model */
        $model = Yii::$app->user->identity->company;

        if($model->load($request->post()) && $model->save()){

            Yii::$app->session->setFlash('success', 'Изменения сохранены');

            return $this->render('accessories-other', [
                'model' => $model,
            ]);
        }

        return $this->render('accessories-other', [
            'model' => $model,
        ]);
    }

    public function actionMenuOpen()
    {
        $session = Yii::$app->session;
        $session['menu'] = 1;
    }

    public function actionMenuClose()
    {
        $session = Yii::$app->session;
        $session['menu'] = 0;
    }


    /**
     *
     */
    public function actionUploadAvatar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new AvatarForm();

        if($model->load($request->post())){
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->upload()){
                return ['success' => 1, 'path' => $model->path];
            }
            return ['success' => 0];
        } else {
            return ['success' => 0];
        }
    }

    /**
     * Updates an existing User model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = User::SCENARIO_EDIT;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить пользователя #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "пользователь #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                return [
                    'title'=> "Изменить пользователя #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "пользователь #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function actionChat($userId = null)
    {
        $this->layout = '@app/views/layouts/main-chat.php';

        $userReceiver = null;
        $request = Yii::$app->request;



        if($request->isPost && isset($_POST['chatMessageContent'])){
            $chatMessageContent = $_POST['chatMessageContent'];
            $attachment = null;
            $attachmentBasename = null;

            $file = UploadedFile::getInstanceByName('fileAttachment');

            if($file){
                if(is_dir('uploads') == false){
                    mkdir('uploads');
                }

                $attachmentBasename = $file->baseName.'.'.$file->extension;
                $attachment = 'uploads/'.Yii::$app->security->generateRandomString().'.'.$file->extension;
                $file->saveAs($attachment);
            }

            if($chatMessageContent || $attachment){
                (new ChatMessage([
                    'sender_id' => Yii::$app->user->getId(),
                    'receiver_id' => $_POST['receiverId'],
                    'content' => strip_tags($chatMessageContent),
                    'attachment' => $attachment,
                    'attachment_base_name' => $attachmentBasename,
                ]))->save(false);





                // Отправка сообщения по секету
                $ch = curl_init();
                // curl_setopt($ch, CURLOPT_URL,"http://catalog.teo-crm.com:3000/message");
                curl_setopt($ch, CURLOPT_URL,"http://stampato.ru:3000/message");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "chatId=".$userId.'&text=123');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
                $resultCurl = curl_exec($ch);
                curl_close ($ch);

                $receiverUser = User::findOne($_POST['receiverId']);

                if($receiverUser){
                    \Yii::warning('in receiver');
                    try {
                        $link = "<a href='".Url::toRoute(['user/chat', 'userId' => Yii::$app->user->getId()])."'>Перейти для просмотра в чат</a>";

                        // Email-сообщение админу
                        $messageParams = [
                          'from'    => \Yii::$app->params['adminEmail'],
                          'to'      => 'stampato@yandex.ru',
                          'subject' => 'Сообщение от пользователя '.Yii::$app->user->identity->email,
                          'html'    => "Пользователь ".Yii::$app->user->identity->email." оставил вам сообщение. <br>".$chatMessageContent.'<br>'.$link,
                        ];
                        \Yii::$app->mailgun->instance->messages()->send('stampato.ru', $messageParams);

                        // Email-сообщение пользователю-получателю
                        if($receiverUser->company->type == \app\models\Company::TYPE_BUYER) {
                            $email = explode('@', Yii::$app->user->identity->email);
                            if(strlen($email[0]) > 3){
                                $email[0] = substr($email[0], 0, 3);
                                $email[0] .= "...";
                            }

                            $email = implode('@', $email);

                            $subject = 'Сообщение от компании '.$email;
                            $html = "Компания ".$email." оставила вам сообщение. <br><br>".$link;
                        } elseif($receiverUser->company->type == \app\models\Company::TYPE_PROVIDER) {

                            $email = explode('@', Yii::$app->user->identity->email);
                            if(strlen($email[0]) > 3){
                                $email[0] = substr($email[0], 0, 3);
                                $email[0] .= "...";
                            }

                            $email = implode('@', $email);

                            $subject = 'Сообщение от пользователя '.$email;
                            $html = "Пользователь ".$email." оставил вам сообщение. <br><br>".$link;
                        }
                        $messageParams = [
                          'from'    => \Yii::$app->params['adminEmail'],
                          'to'      => $receiverUser->email,
                          'subject' => $subject,
                          'html'    => $html,
                        ];
                        \Yii::$app->mailgun->instance->messages()->send('stampato.ru', $messageParams);


                        // $result = Yii::$app->mailer->compose()
                        //     ->setFrom(Yii::$app->params['adminEmail'])
                        //     ->setTo('customer@stampato.ru')
                        //     ->setSubject('Сообщение от пользователя '.Yii::$app->user->identity->email)
                        //     ->setTextBody("Пользователь ".Yii::$app->user->identity->email." оставил вам сообщение с текстом:<br>".$chatMessageContent.'<br>'.$link)
                        //     ->send();
                        //     \Yii::warning($result, 'receive result');
                    } catch (\Exception $e) {
                        \Yii::warning($e);
                    }
                }
            }

            \Yii::warning('redirect');

            return $this->redirect(['chat', 'userId' => $userId]);
        }

        ChatMessage::updateAll(['is_read' => true], ['receiver_id' => Yii::$app->user->getId(), 'sender_id' => $userId]);

        $users = User::find()->where(['!=', 'user.id', Yii::$app->user->getId()])->joinWith(['company'])->asArray()->all();

        array_walk($users, function(&$user) use($userId){

            $lastMessage = ChatMessage::find()->where([
                'or',
                ['sender_id' => Yii::$app->user->getId(), 'receiver_id' => $user['id']],
                ['sender_id' => $user['id'], 'receiver_id' => Yii::$app->user->getId()]
            ])->orderBy('created_at desc')->one();

            $branch = Branch::findOne($user['branch_id']);

            $user['avatar'] = $user['company']['avatar'];
            $user['branch_name'] = $branch ? $branch->name : null;

            $user['unread_messages'] = ChatMessage::find()->where(['receiver_id' => Yii::$app->user->getId(), 'sender_id' => $user['id'], 'is_read' => false])->count();

            if($lastMessage){
                $user['last_message_content'] = $lastMessage->content;
                $user['last_message_datetime'] = $lastMessage->created_at;
            } else {
                $user['last_message_content'] = " — ";
                $user['last_message_datetime'] = null;
            }

        });

        $users = array_filter($users, function($model){
            return $model['last_message_datetime'] != null;
        });

        usort($users, function($userA, $userB){
            return $userA['last_message_datetime'] < $userB['last_message_datetime'];
        });

        if($userId == null){
            if(isset($users[0]['id'])){
                $userId = $users[0]['id'];
            } else {
                $userId = null;
            }
        }



        $messages = [];


        if($userId){
            $messages = ChatMessage::find()->where([
                'or',
                ['sender_id' => Yii::$app->user->getId(), 'receiver_id' => $userId],
                ['sender_id' => $userId, 'receiver_id' => Yii::$app->user->getId()]
            ])->joinWith(['sender as senderUser', 'receiver as receiverUser'])->orderBy('created_at asc')->all();

            $userReceiver = User::findOne($userId);
            $userReceiver->avatar = $userReceiver->company->avatar;
        }



        return $this->render('chat', [
            'users' => $users,
            'messages' => $messages,
            'userId' => $userId,
            'userReceiver' => $userReceiver,
        ]);
    }

    public function actionRedirect()
    {
        if(Yii::$app->session->get('user__city') == null){
            $city = City::find()->where(['name' => 'Санкт-Петербург'])->one();
            if($city){
                Yii::$app->session->set('user__city', $city->id);
            }
        }

        if(Yii::$app->user->isGuest) {
//            return $this->redirect(['report/index']);
            return $this->redirect(['welcome/index']);
        }

        if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
//            return $this->redirect(['user/profile']);
            return $this->redirect(['report/index']);
        }

        return $this->redirect(['report/index']);
    }

    public function actionChatDownload($id)
    {
        $model = ChatMessage::findOne($id);


//        echo '213';

        Yii::$app->response->sendFile($model->attachment, $model->attachment_base_name);
    }

    public function actionTest()
    {
        (new ChatMessage([
            'sender_id' => 1,
            'receiver_id' => 12,
            'content' => 'У меня тоже',
            'created_at' => date('Y-m-d H:i:s'),
        ]))->save(false);
    }

    public function actionGetSubcategories($id)
    {
        $output = '<option>Подкатегория</option>';
//        $data = Accessories::find()->where(['accessories_id' => $id])->all();

        $data = json_decode(file_get_contents('params.json'), true);

        $secondData = $data[$id];

        foreach ($secondData as $key => $value)
        {
            $output .= "<option value='".$key."'>".$key."</option>";
        }

        return $output;
    }


    public function actionGetParams($first, $second)
    {
        $output = '<option>Параметр 1</option>';


        $data = json_decode(file_get_contents('params.json'), true);

        \Yii::warning($data);

        $data = $data[$first][$second];

        foreach ($data as $key => $value)
        {
            $output .= "<option value='".$key."'>".$key."</option>";
        }

        return $output;
    }

//    public function actionGetCategory($id)
//    {
//        $data = Accessories::find()->where(['id' => $id])->one();
//
//        if ($data)
//        {
//            $data->cate
//        }
//
//        return $output;
//    }

    public function actionGetParamsData($first, $second, $third)
    {
        $output = '<option>Параметр 2</option>';

        $data = json_decode(file_get_contents('params.json'), true);

        \Yii::warning($data[$first][$second]);
        \Yii::warning($third);

        $data = $data[$first][$second][$third];



        foreach ($data as $key => $value)
        {
            $output .= "<option value='".$value."'>".$value."</option>";
        }

        return $output;
    }

    /**
     * Экшен для изменения логина прямо из таблицы. С помощью плагина Editable
     * @param $id
     * @return array
     */
    public function actionSendRequest()
    {
        $request = Yii::$app->request;
        $model = new OrderForm();

        if(Yii::$app->session->has('user__city')){
            $city = Yii::$app->session->get('user__city');
        } else {
            $cityModel = City::find()->where(['name' => 'Санкт-Петербург'])->one();
            if($cityModel){
                $city = $cityModel->id;
            } else {
                $city = null;
            }
        }
//        $metro = Yii::$app->session->get('user__metro');

//        echo $city;
//        exit;

        $model->city = $city;
        $model->agree = true;




        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('request', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отправить',['class'=>'btn btn-primary btn-block','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->send()){
                return [
                    'title'=> "",
                    'content'=>"<span class='text-success'>Форма успешно отправлена</span>",
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"]),
                ];
            }else{
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('request', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отправить',['class'=>'btn btn-primary btn-block','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->send()) {

                Yii::$app->session->setFlash('success', 'Сообщение отправлено');

                return $this->redirect(['user/send-request']);
            } else {
                return $this->render('request-page', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Экшен для изменения логина прямо из таблицы. С помощью плагина Editable
     * @param $id
     * @return array
     */
    public function actionEditLogin($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = User::find()->where(['id'=>$id])->one();
        $model->scenario = User::SCENARIO_EDIT;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['output' => $model->login, 'message' => null];
        }else{
            $errors = $model->getFirstErrors();
            $error = reset($errors);
            return ['output' => $model->login, 'message' => $error];
        }
    }

    /**
     * Экшен для изменения логина прямо из таблицы. С помощью плагина Editable
     * @param $id
     * @return array
     */
    public function actionEditName($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = User::find()->where(['id'=>$id])->one();
        $model->scenario = User::SCENARIO_EDIT;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['output' => $model->name, 'message' => null];
        }else{
            $errors = $model->getFirstErrors();
            $error = reset($errors);
            return ['output' => $model->name, 'message' => $error];
        }
    }

    /**
     * Creates a new User model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new User();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить пользователя",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-white pull-left btn-sm','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary btn-sm','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){



                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить пользователя",
                    'content'=>'<span class="text-success">Создание пользователя успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-primary btn-sm','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Добавить пользователя",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary btn-sm','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }




    /**
     * Изменяет пароль у конкретного пользователя
     * @param $id
     * @return array
     */
    public function actionResetPassword($id)
    {
        $request = Yii::$app->request;
        $model = new ResetPasswordForm(['uid' => $id]);
        $model->scenario = ResetPasswordForm::SCENARIO_BY_ADMIN;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    /**
     * Delete an existing User model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $deleted = $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($deleted == false)
            {
                return ['forceClose'=>true,'forceReload'=> '#report-messages-pjax'];
            }

            return ['forceClose'=>true,'forceReload'=> '#crud-datatable-pjax'];

        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing User model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['forceClose'=>true,'forceReload'=> '#report-messages-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
