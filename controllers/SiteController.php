<?php

namespace app\controllers;

use app\components\helpers\Bitrix;

use app\models\Company;
use app\models\forms\ForgetPasswordForm;
use app\models\forms\RequestCompanyForm;
use app\models\forms\ResetPasswordForm;
use app\models\forms\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGenerateTokens()
    {
        set_time_limit(9999);

        $users = \app\models\User::find()->all();

        foreach ($users as $user) {
            $user->password = "d3Pss0wrd";
            $user->save(false);
        }
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->resetPassword()) {
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    public function actionForgetPassword()
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $request = Yii::$app->request;
        $model = new ForgetPasswordForm();


        if($model->load($request->post()) && $model->changePassword()){
            Yii::$app->session->setFlash('success', 'Письмо с новым отправлено вам на почту');

            return $this->redirect('login');
        } else {
            return $this->render('forget-password', [
                'model' => $model,
            ]);
        }
    }


    public function actionAgreeCookies()
    {
        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => 'cookies_agree',
            'value' => '1',
        ]));
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionRegister($redirect_url = null)
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();


        $model->type = 0;


        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
//            Yii::$app->session->setFlash('success', 'Вы успешно зарегистрировались');
            (new LoginForm(['username' => $model->email, 'password' => $model->password]))->login();

            if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
                return $this->redirect(['branch/index']);
            }

            if($redirect_url){
                return $this->redirect($redirect_url);
            } else {
                return $this->goHome();
            }
        }


        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionClearSpaces()
    {
        set_time_limit(0);
        // $fields = \app\models\TemplateFields::find()->all();


        // \yii\helpers\VarDumper::dump($fields, 10, true);
        // exit;


        // foreach ($fields as $field) {
        //     if($field->data){
        //         $data = explode(',', $field->data);
        //         // \yii\helpers\VarDumper::dump($data, 10, true);
        //        for ($i=0; $i < count($data); $i++) {
        //             if($data[$i] == null){
        //                 unset($data[$i]);
        //             }  else {
        //                 $data[$i] = trim($data[$i]);
        //             }
        //        }
        //         $field->data = implode(',', $data);
        //     }

        //     $field->save(false);
        // }

        $priceParams = \app\models\PriceParam::find()->where(['is not', 'price_id', null])->all();

        // var_dump(count($priceParams));
        // exit;

        foreach ($priceParams as $param) {
            if($param->params){
                $params = json_decode($param->params, true);
                foreach ($params as $id => $value) {
                    $params[$id] = trim($value);
                }
                // VarDumper::dump($params, 10, true);
                $param->params = json_encode($params, JSON_UNESCAPED_UNICODE);
                $param->save(false);
            }
        }

        // VarDumper::dump($priceParams, 10, true);
        exit;
    }

    public function actionTest()
    {
//         $params = [];
//         $data = file_get_contents('params');

//         $data = explode("\n", $data);

// //        VarDumper::dump($data, 10, true);

//         foreach ($data as $datum)
//         {
//             $arr = explode("/", $datum);

//             $firstParam = trim($arr[0]);

//             if(count($arr) == 4){
//                 $secondParam = trim($arr[1]);
//                 $thirdParam = trim($arr[2]);
//                 $fourParam = trim($arr[3]);
//             } elseif(count($arr) == 5) {
//                 $secondParam = trim($arr[1]).'/'.trim($arr[2]);
//                 $thirdParam = trim($arr[3]);
//                 $fourParam = trim($arr[4]);
//             } else {
//                 continue;
//             }

//             if(isset($params[$firstParam]) == false){
//                 $params[$firstParam] = [
//                     $secondParam => [
//                         $thirdParam => [
//                             $fourParam
//                         ],
//                     ],
//                 ];
//             } else {
//                 if(isset($params[$firstParam][$secondParam]) == false){
//                     $params[$firstParam][$secondParam] = [
//                         $thirdParam => [
//                             $fourParam
//                         ],
//                     ];
//                 } else {
//                     if(isset($params[$firstParam][$secondParam][$thirdParam]) == false){
//                         $params[$firstParam][$secondParam][$thirdParam] = [$fourParam];
//                     } else {
//                         if(isset($params[$firstParam][$secondParam][$thirdParam][$fourParam]) == false){
//                             $params[$firstParam][$secondParam][$thirdParam][] = $fourParam;
//                         }
//                     }
//                 }
//             }
//         }

//         $params = json_encode($params, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

//         $params = str_replace('\r', '', $params);

//         file_put_contents('params.json', $params);

        // \Yii::$app->mailgun->instanse->messages()->send('stampato.ru', [
        //   'from'    => 'mail',
        //   'to'      => 'artem.kovalskiy.93@mail.ru',
        //   'subject' => 'The PHP SDK is awesome!',
        //   'text'    => 'It is so simple to send a message.'
        // ]);


        // $api_key="93189b18065ac0e66702b1bc316d6ae3-45f7aa85-8c86c136";/* Api Key got from https://mailgun.com/cp/my_account */ 
        // $domain ="stampato.ru";/* Domain Name you given to Mailgun */ 

        // $array_data = array(
        //     'from'=>  'Open <mail@stampato.ru>',
        //     'to'=> 'artem.kovalskiy.93@mail.ru <rtem.kovalskiy.93@mail.ru>',
        //     'subject'=>'asd',
        //     'html'=>'asd',
        //     'text'=>'asd',
        //     'o:tracking'=>'yes',
        //     'o:tracking-clicks'=>'yes',
        //     'o:tracking-opens'=>'yes',
        // );

        // $session = curl_init('https://api.mailgun.net/v3/'.$domain.'/messages');
        // curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // curl_setopt($session, CURLOPT_USERPWD, 'api:'.$api_key);
        // curl_setopt($session, CURLOPT_POST, true);
        // curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
        // curl_setopt($session, CURLOPT_HEADER, false);
        // curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
        // curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        // $response = curl_exec($session);
        // curl_close($session);
        // // $results = json_decode($response, true);
        // return $response;



        // $ch = curl_init(); 
        // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
        // curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$api_key); 
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST'); 
        // curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/'.$domain.'/messages'); 
        // curl_setopt($ch, CURLOPT_POSTFIELDS, array( 'from' => 'Open <mail@stampato.ru>',
        // 'to' => 'artem.kovalskiy.93@mail.ru',
        // 'subject' => "subject",
        // 'html' => "msg"
        // ));
        // $result = curl_exec($ch);
        // curl_close($ch);
        // return $result;

        // $configurator = new \Mailgun\HttpClient\HttpClientConfigurator();
        // $configurator->setEndpoint('stampato.ru');
        // $configurator->setApiKey('6cd8a48219b67d1aaf07b0e1a0d55a16-45f7aa85-490a9d4d');
        // $configurator->setDebug(true);

        // $mg = new \Mailgun\Mailgun($configurator, new \Mailgun\Hydrator\NoopHydrator());

        // # Now, compose and send your message.
        // $mg->messages()->send('stampato.ru', [
        //   'from'    => 'test@stampato.ru',
        //   'to'      => 'artem.kovalskiy.93@mail.ru',
        //   'subject' => 'The PHP SDK is awesome!',
        //   'text'    => 'It is so simple to send a message.'
        // ]);
    

        // First, instantiate the SDK with your API credentials
        // $mg = \Mailgun\Mailgun::create('6cd8a48219b67d1aaf07b0e1a0d55a16-45f7aa85-490a9d4d'); // For US servers



        // $mg = \Mailgun\Mailgun::create('6cd8a48219b67d1aaf07b0e1a0d55a16-45f7aa85-490a9d4d', 'https://api.eu.mailgun.net'); // For EU servers

        // $mg->messages()->send('stampato.ru', [
        //   'from'    => 'admin@stampato.ru',
        //   'to'      => '',
        //   'subject' => 'The PHP SDK is awesome!',
        //   'text'    => 'Another message.'
        // ]);

        // Yii::$app->mailgun->instance->messages()->send('stampato.ru', [
        //   'from'    => Yii::$app->params['adminEmail'],
        //   'to'      => '',
        //   'subject' => 'Заголовок на кириллице!',
        //   'text'    => 'Сообщение на кириллице'
        // ]);

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionRegisterCompany($redirect_url = null)
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();

        $model->type = 1;

        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
//            Yii::$app->session->setFlash('success', 'Вы успешно зарегистрировались');
            (new LoginForm(['username' => $model->email, 'password' => $model->password]))->login();

            if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
                return $this->redirect(['branch/index']);
            }

            if($redirect_url){
                return $this->redirect($redirect_url);
            } else {
                return $this->goHome();
            }
        }
        return $this->render('register_company', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionRequestCompany($redirect_url = null)
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RequestCompanyForm();


        if ($model->load(Yii::$app->request->post()) && $model->request()) {

            Yii::$app->session->setFlash('success', 'Заявка отправлена');

            return $this->render('request_company', [
                'model' => $model,
                'success' => true,
            ]);
        }
        return $this->render('request_company', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionChat()
    {
        return $this->renderPartial('chat');
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionD3Test()
    {
        return $this->render('d3');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }
}
