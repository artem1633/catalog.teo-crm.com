<?php

namespace app\controllers;

use app\models\Accessories;
use app\models\ActionSearch;
use app\models\Advert;
use app\models\Branch;
use app\models\BranchSearch;
use app\models\ChatMessage;
use app\models\City;
use app\models\Company;
use app\models\forms\ReportFilterForm;
use app\models\ImageCompany;
use app\models\Metro;
use app\models\Price;
use app\models\PriceCity;
use app\models\PriceMetro;
use app\models\TemplateFields;
use app\models\User;
use PHPUnit\Framework\Constraint\Count;
use Yii;
use app\models\PriceParam;
use app\models\PriceParamSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ReportController implements the CRUD actions for PriceParam model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => \yii\filters\AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PriceParam models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest == false){
            if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
                return $this->redirect(['price/index']);
            }
        }

//        $accessories = Accessories::find()->where(['is', 'accessories_id', null])->andWhere(['is_main' => true])->all();
        $accessories = Accessories::find()->where(['is', 'accessories_id', null])->all();


        $searchModel = new ReportFilterForm();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $searchModel->load(Yii::$app->request->queryParams);

        if($searchModel->accessoryId == null){
            if(isset($accessories[0])){
                $searchModel->accessoryId = $accessories[0]->id;
            }
        }


        return $this->render('index', [
            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PriceParam models.
     * @return mixed
     */
    public function actionParams($headerAccessoryId = null)
    {
        $searchModel = new ReportFilterForm();
        $searchModel->load(Yii::$app->request->queryParams);
        $this->view->params['filterEnable'] = true;
        $this->view->params['filterActive'] = true;


        $cityFilterValue = Yii::$app->session->get('user__city');
        $metroFilterValue = Yii::$app->session->get('user__metro');

        if(isset($_GET['Fld'])){
            foreach ($_GET['Fld'] as $fldName => $fldValue){
                if($fldValue){
                    $this->view->params['filterActive'] = true;
                }
            }
        }

        if($headerAccessoryId == null){
            $headerAccessories = Accessories::find()->where(['is', 'accessories_id', null])->andFilterWhere(['id' => $headerAccessoryId])->all();
            if(isset($headerAccessories[0])){
                $headerAccessoryId = $headerAccessories[0]->id;
                if($searchModel->accessoryId == null){
                    $kings = Accessories::find()->where(['accessories_id' => $headerAccessories[0]->id])->all();
                    if(isset($kings[0])){
                        $searchModel->accessoryId = $kings[0]->id;
                    }
                }
            }
        }

        $pricesPks = [];

        $query = null;

        if($searchModel->accessoryId){
            $pricesPks = ArrayHelper::getColumn(Accessories::find()->where(['accessories_id' => $searchModel->accessoryId])->all(), 'id');
            $pricesModel = Price::find()
                ->where(['accessory_id' => $pricesPks])
                ->andWhere(['order_status' => Price::ORDER_STATUS_PUBLISHED])
                ->andWhere(['!=', 'price.company_id', 1])
                ->joinWith(['priceCities', 'priceMetros', 'branch']);
//                ->andFilterWhere(['price_city.city_id' => Yii::$app->session->get('user__city')])


//            if(isset($_GET['FldPrice'])) {
            if($cityFilterValue){
                $pricesModel->andFilterWhere(['branch.city_id' => $cityFilterValue]);
//                    $cityFilterValue = $_GET['FldPrice']['cities'];
                if($cityFilterValue){
//                    $this->view->params['filterActive'] = true;
                }
            }
            if($metroFilterValue){
                $pricesModel->andFilterWhere(['branch.metro_id' => $metroFilterValue]);
//                    $metroFilterValue = $_GET['FldPrice']['metro'];
                if($metroFilterValue){
//                    $this->view->params['filterActive'] = true;
                }
            }
//            }

            $pricesModel = $pricesModel->all();


//            VarDumper::dump([$cityFilterValue, $metroFilterValue], 10, true);
//            exit;





            $pricesPks = ArrayHelper::getColumn($pricesModel,'id');



           // \yii\helpers\VarDumper::dump($pricesPks, 10, true);
           // exit;

            $params = PriceParam::find()
                ->select('price_param.*, company.name as company_name, company.rate_id as company_rate_id')
                ->where(['price_id' => $pricesPks])
                ->andWhere(['!=', 'company.type', Company::TYPE_BUYER])
                ->joinWith('price.accessory as accessory')
                ->joinWith('price.branch as branch')
                ->joinWith('price.company as company');


            if(Yii::$app->user->isGuest){
                $params->andWhere(['!=', 'company.rate_id', 0]);
            } else {
                if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER){
                    if(Yii::$app->user->identity->company->rate_id !== Company::RATE_BRANCH){
                        $params->andWhere(['!=', 'company.rate_id', 0]);
                    }
                }
            }



            if(isset($_GET['Fld'])){
//                VarDumper::dump($_GET['Fld'], 10, true);
//                exit;
                foreach ($_GET['Fld'] as $id => $value){
                    if($value == null) continue;
                    if($value != -1){
                        $params->andWhere(['like', 'params', '"'.$id.'":"'.$value.'"']);
                    } else {
                        $params->andWhere(['like', 'params', '"'.$id.'":""']);
                    }
                }
            }

            $query = clone $params;



//            echo $params->createCommand()->getRawSql();
//            exit;


            $params = $params->asArray()->all();

//            VarDumper::dump($params, 10, true);
//            exit;



        $childAccess = Accessories::find()->where(['accessories_id' => $searchModel->accessoryId])->one();


        if($childAccess){
            /** @var TemplateFields[] $fields */
            $fields = TemplateFields::find()->where(['accessories_id' => $childAccess->id])->orderBy('sort asc')->all();


                        foreach ($fields as $field){


                            $label = $field->label;

                            if(strlen($label) > 10){
                                $label = iconv_substr($label, 0, 10, "UTF-8" ).'...';
                            }

                            $attributes['attr_'.$field->id] = [
                                'asc' => ['attr_'.$field->id => SORT_ASC, 'attr_'.$field->id => SORT_ASC],
                                'desc' => ['attr_'.$field->id => SORT_DESC, 'attr_'.$field->id => SORT_DESC],
                                'default' => SORT_DESC,
                                'label' => 'Name',
                            ];
                        }

                $dataProvider = new ArrayDataProvider([
                        'allModels' => $params,
                        'sort' => [
                            'attributes' => $attributes,
                        ],
                    ]);
                // $dataProvider->setModels($params);
            } else {
                $fields = [];

                $dataProvider = new ArrayDataProvider([
                    'allModels' => [],
                ]);
            }
        } else {
                $fields = [];

                $dataProvider = new ArrayDataProvider([
                    'allModels' => [],
                ]);
        }
        // \yii\helpers\VarDumper::dump($dataProvider->models, 10, true);
        // exit;





        // \yii\helpers\VarDumper::dump($query->createCommand()->getRawSql(), 10, true);
        // exit;

        return $this->render('params', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pricesPks' => $pricesPks,
            'query' => $query,
            'headerAccessoryId' => $headerAccessoryId,
            'cityFilterValue' => $cityFilterValue,
            'metroFilterValue' => $metroFilterValue,
        ]);
    }

    public function actionBranchUserChat($providerId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $users = User::find()->where(['company_id' => $providerId])->all();
        $bracnhes = Branch::find()->where(['id' => ArrayHelper::getColumn($users, 'branch_id')])->all();


        return [
            'title'=> "",
            'content'=>$this->renderAjax('branches_list', [
                'branches' => $bracnhes,
            ]),
            'footer'=> '',

        ];
    }

    public function actionBranchSinglePage($headerAccessoryId, $accessoryId, $providerId)
    {
        $users = User::find()->where(['company_id' => $providerId])->all();
        $bracnhes = Branch::find()->where(['id' => ArrayHelper::getColumn($users, 'branch_id')])->all();


        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "",
                'content'=>$this->renderAjax('branches_list_single', [
                    'branches' => $bracnhes,
                    'headerAccessoryId' => $headerAccessoryId,
                    'accessoryId' => $accessoryId,
                    'providerId' => $providerId,
                ]),
                'footer'=> '',

            ];
        } else {
            return $this->render('branches_list_single', [
                'branches' => $bracnhes,
                'headerAccessoryId' => $headerAccessoryId,
                'accessoryId' => $accessoryId,
                'providerId' => $providerId,
            ]);
        }
    }

    public function actionDownloadIndexExcel($headerAccessoryId = null)
    {
        $mainAccessories = Accessories::find()->where(['is', 'accessories_id', null])->andWhere(['is_main' => true])->all();



        $firstAccess = Accessories::find()->where(['id' => $headerAccessoryId])->andWhere(['is', 'accessories_id', null])->all();
        $secondAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($firstAccess, 'id')])->all();
        $thirdAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($secondAccess, 'id')])->all();


        $cityFilter = Yii::$app->session->get('user__city');
        $metroFilter = Yii::$app->session->get('user__metro');

        $branches = \app\models\Branch::find()->andFilterWhere(['city_id' => $cityFilter, 'metro_id' => $metroFilter])->all();

        $companiesPks = array_unique(ArrayHelper::getColumn($branches, 'company_id'));

        $companies = Company::find()->where(['type' => Company::TYPE_PROVIDER])->andWhere(['id' => $companiesPks])->andWhere(['!=', 'id', 1])->all();


        $data[0][] = "Компания";


        foreach ($mainAccessories as $mainAccessory)
        {
            $firstAccess = Accessories::find()->where(['id' => $mainAccessory->id])->andWhere(['is', 'accessories_id', null])->all();
            $secondAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($firstAccess, 'id')])->all();
            $thirdAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($secondAccess, 'id')])->all();

            foreach ($thirdAccess as $model){
                if($model){
                    $two = Accessories::findOne($model->accessories_id);
                    $output = "{$two->name}";
                }

                $data[0][] = $output;
            }
        }


        $counter = 1;
        foreach ($companies as $company)
        {
//            $metroNames = implode(', ', ArrayHelper::getColumn(Metro::find()->where(['id' => ArrayHelper::getColumn($branches, 'metro_id')])->all(), 'name'));

//            $data[$counter][0] = $metroNames;
            $data[$counter][0] = $company->name;

            foreach ($mainAccessories as $mainAccessory){

                $firstAccess = Accessories::find()->where(['id' => $mainAccessory->id])->andWhere(['is', 'accessories_id', null])->all();
                $secondAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($firstAccess, 'id')])->all();
                $thirdAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($secondAccess, 'id')])->all();

                foreach ($thirdAccess as $access)
                {
                    $branchesPks = ArrayHelper::getColumn($branches, 'id');

                    $price = Price::find()->where(['accessory_id' => $access->id, 'company_id' => $company->id, 'branch_id' => $branchesPks])->one();

                    $pAccess = Accessories::find()->where(['id' => $access->accessories_id])->one();

                    if($price){
//                                    $priceLink = $user ? Html::a('Заказать', ['report/single-page', 'accessoryId' => $access->id, 'providerId' => $company->id], ['target' => '_blank']) : null;
//                    $priceLink = Html::a('Заказать', ['report/single-page',  'headerAccessoryId' => $pAccess->accessories_id, 'accessoryId' => $access->accessories_id, 'providerId' => $company->id], ['target' => '_blank']);


                        if($price->order_status == Price::ORDER_STATUS_PUBLISHED) // Если прайс опубликован
                        {
                            $priceLink = 'Заказать';
                        } else {
                            $user = \app\models\User::find()->where(['company_id' => $company->id])->one();
                            if($user) {
                                $priceLink = 'Уточнить';
                            } else {
                                $priceLink = 'Уточнить';
                            }
                        }

                        $data[$counter][] = $priceLink;
                    } else {

                        if($company->accessories == null){
                            $dataAccessories = [];
                        } else {
                            $dataAccessories = json_decode($company->accessories);
                        }

                        if(in_array($access->accessories_id, $dataAccessories)){
                            $user = \app\models\User::find()->where(['company_id' => $company->id])->one();
                            if($user) {
                                $data[$counter][] = 'Уточнить';
                            } else {
                                $data[$counter][] = 'Уточнить';
                            }
                        } else {
                            $data[$counter][] = "—";
                        }

                    }
                }
            }

            $counter++;
        }



        // Генерация Excel
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $objGet = $objPHPExcel->getActiveSheet();

        $styleForHeaders = array('font' => array('size' => 11,'bold' => true,'color' => array('rgb' => '000000')));

//        $objGet->getStyle('B'.$index)->applyFromArray($styleForHeaders);

//        $objGet->setCellValueByColumnAndRow(0, 1, 'Привет как дела');

        $objGet->getColumnDimension('A')->setWidth(30);
        $objGet->getColumnDimension('B')->setWidth(30);
        $objGet->getColumnDimension('C')->setWidth(30);
        $objGet->getColumnDimension('D')->setWidth(30);
        $objGet->getColumnDimension('E')->setWidth(30);
        $objGet->getColumnDimension('F')->setWidth(30);
        $objGet->getColumnDimension('G')->setWidth(30);
        $objGet->getColumnDimension('H')->setWidth(30);
        $objGet->getColumnDimension('I')->setWidth(30);
        $objGet->getColumnDimension('J')->setWidth(30);
        $objGet->getColumnDimension('L')->setWidth(30);
        $objGet->getColumnDimension('M')->setWidth(30);
        $objGet->getColumnDimension('N')->setWidth(30);
        $objGet->getColumnDimension('O')->setWidth(30);
        $objGet->getColumnDimension('P')->setWidth(30);
        $objGet->getColumnDimension('Q')->setWidth(30);
        $objGet->getColumnDimension('R')->setWidth(30);
        $objGet->getColumnDimension('S')->setWidth(30);
        $objGet->getColumnDimension('T')->setWidth(30);
        $objGet->getColumnDimension('U')->setWidth(30);
        $objGet->getColumnDimension('V')->setWidth(30);
        $objGet->getColumnDimension('W')->setWidth(30);
        $objGet->getColumnDimension('X')->setWidth(30);
        $objGet->getColumnDimension('Y')->setWidth(30);
        $objGet->getColumnDimension('Z')->setWidth(30);
        $objGet->getColumnDimension('AA')->setWidth(30);
        $objGet->getColumnDimension('AB')->setWidth(30);
        $objGet->getColumnDimension('AC')->setWidth(30);

        for ($i = 0; $i <= count($data); $i++)
        {
            if(isset($data[$i]) == false){
                continue;
            }

            $row = $data[$i];

            for ($j = 0; $j <= count($row); $j++)
            {
                if(isset($row[$j])){
                    $value = $row[$j];

                    $objGet->setCellValueByColumnAndRow($j, ($i + 1), $value);
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->getAlignment()->setWrapText(true);
                }

                if($i == 0){
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->applyFromArray($styleForHeaders);
                }
            }
        }

        $filename = 'test.xlsx';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('test.xlsx');

        Yii::$app->response->sendFile('test.xlsx');
    }

    public function actionDownloadParamsExcel($headerAccessoryId = null)
    {
        $searchModel = new ReportFilterForm();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = new ArrayDataProvider();
        $dataProvider->pagination = false;
        $this->view->params['filterEnable'] = true;
        $this->view->params['filterActive'] = false;
    
        ini_set('memory_limit', '500M');

        $cityFilterValue = Yii::$app->session->get('user__city');
        $metroFilterValue = Yii::$app->session->get('user__metro');


        if(isset($_GET['Fld'])){
            foreach ($_GET['Fld'] as $fldName => $fldValue){
                if($fldValue){
                    $this->view->params['filterActive'] = true;
                }
            }
        }

        if($headerAccessoryId == null){
            $headerAccessories = Accessories::find()->where(['is', 'accessories_id', null])->andFilterWhere(['id' => $headerAccessoryId])->all();
            if(isset($headerAccessories[0])){
                $headerAccessoryId = $headerAccessories[0]->id;
                if($searchModel->accessoryId == null){
                    $kings = Accessories::find()->where(['accessories_id' => $headerAccessories[0]->id])->all();
                    if(isset($kings[0])){
                        $searchModel->accessoryId = $kings[0]->id;
                    }
                }
            }
        }

        $pricesPks = [];

        $query = null;

        if($searchModel->accessoryId){
            $pricesPks = ArrayHelper::getColumn(Accessories::find()->where(['accessories_id' => $searchModel->accessoryId])->all(), 'id');
            // $pricesPks = ArrayHelper::getColumn(Price::find()->where(['accessory_id' => $pricesPks])->andWhere(['!=', 'company_id', 1])->all(), 'id');
            $pricesPks = Price::find()->where(['accessory_id' => $pricesPks])->joinWith('branch')->andWhere(['!=', 'price.company_id', 1]);


            if($cityFilterValue){
                $pricesPks->andFilterWhere(['branch.city_id' => $cityFilterValue]);
            }
            if($metroFilterValue){
                $pricesPks->andFilterWhere(['branch.metro_id' => $metroFilterValue]);
            }

            $pricesPks = ArrayHelper::getColumn($pricesPks->all(), 'id');

            $params = PriceParam::find()
                ->select('price_param.*, company.name as company_name, company.rate_id as company_rate_id')
                ->where(['price_id' => $pricesPks])
                ->andWhere(['!=', 'company.type', Company::TYPE_BUYER])
                ->joinWith('price.accessory as accessory')
                ->joinWith('price.company as company');

            if(Yii::$app->user->isGuest){
                $params->andWhere(['!=', 'company.rate_id', 0]);
            } else {
                if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER){
                    if(Yii::$app->user->identity->company->rate_id !== Company::RATE_BRANCH){
                        $params->andWhere(['!=', 'company.rate_id', 0]);
                    }
                }
            }

            $query = clone $params;


            if(isset($_GET['Fld'])){
//                VarDumper::dump($_GET['Fld'], 10, true);
//                exit;
                foreach ($_GET['Fld'] as $id => $value){
                    if($value == null) continue;
                    $params->andWhere(['like', 'params', '"'.$id.'":"'.$value.'"']);
                }
            }
//            echo $params->createCommand()->getRawSql();
//            exit;


            $params = $params->asArray()->all();


            array_walk($params, function(&$model){

                $modelBranch = Branch::findOne($model['price']['branch_id']);

                $model['branch'] = $modelBranch;

                $model['cities'] = implode(', ', ArrayHelper::getColumn(City::find()
                    ->where([
                        'id' => $modelBranch->city_id,
                    ])->all(), 'name'));

                $model['metros'] = implode(', ', ArrayHelper::getColumn(Metro::find()
                    ->where([
                        'id' => $modelBranch->metro_id,
                    ])->all(), 'name'));
            });

//            VarDumper::dump($params, 10, true);


//            exit;
//            VarDumper::dump($params, 10, true);
//            exit;

            $dataProvider->setModels($params);
        }


        $childAccess = Accessories::find()->where(['accessories_id' => $searchModel->accessoryId])->one();

        /** @var TemplateFields[] $fields */
        $fields = TemplateFields::find()->where(['accessories_id' => $childAccess->id])->all();

        $data = [];


        $data[] = [
        ];

        $data[0][] = "Города";
        $data[0][] = "Метро";
        $data[0][] = "Филиал";
        $data[0][] = "Адрес";
        $data[0][] = "Телефон";


        foreach ($fields as $field){
            $data[0][] = $field->label;
        }

        $data[0][] = "Компания";



        $counter = 1;
        foreach ($dataProvider->models as $model)
        {
            $data[$counter][] = $model['cities'];
            $data[$counter][] = $model['metros'];
            $data[$counter][] = $model['branch']->name;
            $data[$counter][] = $model['branch']->address;
            $data[$counter][] = $model['branch']->phone;

            $pAccessory = Accessories::findOne($model['price']['accessory']['accessories_id']);

            foreach ($fields as $field){
                $params = json_decode($model['params'], true);

                $output = ArrayHelper::getValue($params, $field->id);

                if($output == null){
                    $output = "Нет";
                }

                $data[$counter][] = $output;
            }

            $data[$counter][] = Url::toRoute(['report/single-page', 'providerId' => $model['company_id'], 'headerAccessoryId' => $pAccessory->accessories_id, 'accessoryId' => $model['price']['accessory']['accessories_id']], true);


            $counter++;
        }



        // Генерация Excel
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $objGet = $objPHPExcel->getActiveSheet();

        $styleForHeaders = array('font' => array('size' => 11,'bold' => true,'color' => array('rgb' => '000000')));

//        $objGet->getStyle('B'.$index)->applyFromArray($styleForHeaders);

//        $objGet->setCellValueByColumnAndRow(0, 1, 'Привет как дела');

        $objGet->getColumnDimension('A')->setWidth(20);
        $objGet->getColumnDimension('B')->setWidth(20);
        $objGet->getColumnDimension('C')->setWidth(20);
        $objGet->getColumnDimension('D')->setWidth(20);
        $objGet->getColumnDimension('E')->setWidth(20);
        $objGet->getColumnDimension('F')->setWidth(20);
        $objGet->getColumnDimension('G')->setWidth(20);
        $objGet->getColumnDimension('H')->setWidth(20);
        $objGet->getColumnDimension('I')->setWidth(20);
        $objGet->getColumnDimension('J')->setWidth(20);
        $objGet->getColumnDimension('L')->setWidth(20);
        $objGet->getColumnDimension('M')->setWidth(20);
        $objGet->getColumnDimension('N')->setWidth(20);
        $objGet->getColumnDimension('O')->setWidth(20);
        $objGet->getColumnDimension('P')->setWidth(20);
        $objGet->getColumnDimension('Q')->setWidth(20);
        $objGet->getColumnDimension('R')->setWidth(20);
        $objGet->getColumnDimension('S')->setWidth(20);
        $objGet->getColumnDimension('T')->setWidth(20);
        $objGet->getColumnDimension('U')->setWidth(20);
        $objGet->getColumnDimension('V')->setWidth(20);
        $objGet->getColumnDimension('W')->setWidth(20);
        $objGet->getColumnDimension('X')->setWidth(20);
        $objGet->getColumnDimension('Y')->setWidth(20);
        $objGet->getColumnDimension('Z')->setWidth(20);
        $objGet->getColumnDimension('AA')->setWidth(20);
        $objGet->getColumnDimension('AB')->setWidth(20);
        $objGet->getColumnDimension('AC')->setWidth(20);

        for ($i = 0; $i <= count($data); $i++)
        {
            if(isset($data[$i]) == false){
                continue;
            }

            $row = $data[$i];

            for ($j = 0; $j <= count($row); $j++)
            {
                if(isset($row[$j])){
                    $value = $row[$j];

                    $objGet->setCellValueByColumnAndRow($j, ($i + 1), $value);
                }

                if($i == 0){
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->applyFromArray($styleForHeaders);
                }
            }
        }

        $filename = 'test.xlsx';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('test.xlsx');

        Yii::$app->response->sendFile('test.xlsx');
    }

    public function actionContacts($providerId)
    {
        $this->layout = '@app/views/layouts/single.php';
        $this->view->params['filterEnable'] = false;
        $this->view->params['filterActive'] = false;
        $this->view->params['providerId'] = $providerId;

        $branchSearchModel = new BranchSearch();
        $branchDataProvider = $branchSearchModel->search(Yii::$app->request->queryParams);
        $branchDataProvider->query->andWhere(['company_id' => $providerId]);
        $branchDataProvider->pagination = false;

        $provider = Company::findOne($providerId);

        $branches = Branch::find()->where(['company_id' => $providerId])->andWhere(['is not', 'coords', null])->all();

        return $this->render('provider', [
            'provider' => $provider,
            'branches' => $branches,
            'branchSearchModel' => $branchSearchModel,
            'branchDataProvider' => $branchDataProvider
        ]);
    }

    public function actionGallery($providerId)
    {
        $this->layout = '@app/views/layouts/single.php';
        $this->view->params['filterEnable'] = false;
        $this->view->params['filterActive'] = false;
        $this->view->params['providerId'] = $providerId;

        $branchSearchModel = new BranchSearch();
        $branchDataProvider = $branchSearchModel->search(Yii::$app->request->queryParams);
        $branchDataProvider->query->andWhere(['company_id' => $providerId]);
        $branchDataProvider->pagination = false;

        $provider = Company::findOne($providerId);

        $images = ImageCompany::find()->where(['company_id' => $providerId])->all();

        return $this->render('gallery', [
            'provider' => $provider,
            'images' => $images,
        ]);
    }

    public function actionSinglePage($providerId, $headerAccessoryId = null, $accessoryId = null)
    {
//        if(Yii::$app->user->isGuest){
//            return $this->redirect(['site/register', 'redirect_url' => Url::toRoute(['report/single-page', 'headerAccessoryId' => $headerAccessoryId, 'providerId' => $providerId, 'accessoryId' => $accessoryId])]);
//        }

        $this->layout = '@app/views/layouts/single.php';
        $this->view->params['filterEnable'] = false;
        $this->view->params['filterActive'] = true;
        $this->view->params['providerId'] = $providerId;

        $cityFilterValue = null;
        $metroFilterValue = null;


        $headers = ArrayHelper::getColumn(Accessories::find()
            ->where([
                'id' => ArrayHelper::getColumn(Price::find()->where(['company_id' => $providerId, 'is_order' => 0])->all(), 'accessory_id'),
            ])
            ->all(), 'accessories_id');

        $headers = ArrayHelper::getColumn(Accessories::find()
            ->where([
                'id' => ArrayHelper::getColumn(Accessories::find()->where(['id' => $headers])->all(), 'accessories_id'),
            ])
            ->all(), 'id');

        if(isset($_GET['Fld'])){
            foreach ($_GET['Fld'] as $fldName => $fldValue){
                if($fldValue){
                    $this->view->params['filterActive'] = true;
                }
            }
        }

//        var_dump($this->view->params['filterActive']);
//        exit;

        $searchModel = new ReportFilterForm();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = new ArrayDataProvider();

        $searchCitiesPks = [];

        $query = null;

        $pricesPks = [];

        if($accessoryId){
            $this->view->params['filterEnable'] = true;
            $accessory = Accessories::findOne($accessoryId);
//            $searchModel->accessoryId = intval($accessory->accessories_id);
            $searchModel->accessoryId = $accessoryId;

//            $pricesPks = ArrayHelper::getColumn(Accessories::find()->where(['accessories_id' => $accessory->accessories_id])->all(), 'id');
            $pricesPks = ArrayHelper::getColumn(Accessories::find()->where(['accessories_id' => $accessory->id])->all(), 'id');
            $pricesModels = Price::find()->where(['accessory_id' => $pricesPks])->joinWith(['priceCities', 'priceMetros', 'branch']);


            $sPricesModels = Price::find()->where(['accessory_id' => $pricesPks])->joinWith(['priceCities', 'priceMetros', 'branch'])->all();



            if(isset($_GET['FldPrice'])) {
//                if(isset($_GET['FldPrice']['cities'])){
//                    $pricesModels->andFilterWhere(['branch.city_id' => $_GET['FldPrice']['cities']]);
//                    $cityFilterValue = $_GET['FldPrice']['cities'];
//                    if($cityFilterValue){
//                        $this->view->params['filterActive'] = true;
//                    }
//                }

                $cityFilterValue = Yii::$app->session->get('user__city');

                $pricesModels->andFilterWhere(['branch.city_id' => $cityFilterValue]);
//                $cityFilterValue = $_GET['FldPrice']['cities'];

                if(isset($_GET['FldPrice']['metro'])){
                    $pricesModels->andFilterWhere(['branch.metro_id' => $_GET['FldPrice']['metro']]);
                    $metroFilterValue = $_GET['FldPrice']['metro'];
                    if($metroFilterValue){
                        $this->view->params['filterActive'] = true;
                    }
                }
            }

//            \Yii::warning($pricesModels->createCommand()->getRawSql());
            $pricesModels = $pricesModels->all();

            $pricesPks = ArrayHelper::getColumn($pricesModels, 'id');

            // Модели для поиска по городам
            $sPricesPks = ArrayHelper::getColumn($sPricesModels, 'id');
            $sModels = PriceParam::find()->select('price_param.*, company.name as company_name, company.id as companyId')->where(['price_id' => $sPricesPks, 'company.id' => $providerId])->joinWith('price.company as company')->asArray()->all();
            $branchesPks = ArrayHelper::getColumn($sModels, 'price.branch_id');
            $branches = \app\models\Branch::find()->where(['id' => $branchesPks])->all();
            $searchCitiesPks = ArrayHelper::getColumn(\app\models\City::find()->where(['id' => ArrayHelper::getColumn($branches, 'city_id')])->all(), 'id');


//            $pricesPks = ArrayHelper::getColumn(Price::find()->where(['accessory_id' => $pricesPks])->all(), 'id');

            $params = PriceParam::find()->select('price_param.*, company.name as company_name, company.id as companyId')->where(['price_id' => $pricesPks, 'company.id' => $providerId])->joinWith('price.company as company');


            if(isset($_GET['Fld'])){
//                VarDumper::dump($_GET['Fld'], 10, true);
//                exit;
                foreach ($_GET['Fld'] as $id => $value){
                    if($value == null) continue;
                    if($value != -1){
                        $params->andWhere(['like', 'params', '"'.$id.'":"'.$value.'"']);
                    } else {
                        $params->andWhere(['like', 'params', '"'.$id.'":""']);
                    }
                }
            }

            $query = clone $params;

//            echo $params->createCommand()->getRawSql();
//            exit;


            $params = $params->asArray()->all();
        } else {
            $params = [];
            $pricesPks = [];
        }


        $dataProvider->pagination = false;
        $dataProvider->setModels($params);



        return $this->render('single', [
            'headers' => $headers,
            'searchModel' => $searchModel,
            'query' => $query,
            'dataProvider' => $dataProvider,
            'providerId' => $providerId,
            'pricesPks' => $pricesPks,
            'headerAccessoryId' => $headerAccessoryId,
            'accessoryId' => $accessoryId,
            'cityFilterValue' => $cityFilterValue,
            'metroFilterValue' => $metroFilterValue,
            'searchCitiesPks' => $searchCitiesPks,
        ]);
    }


    public function actionServices($providerId)
    {
        $this->layout = '@app/views/layouts/single.php';
        $this->view->params['filterEnable'] = false;
        $this->view->params['filterActive'] = false;
        $this->view->params['providerId'] = $providerId;

        if(isset($_GET['Fld'])){
            foreach ($_GET['Fld'] as $fldName => $fldValue){
                if($fldValue){
                    $this->view->params['filterActive'] = true;
                }
            }
        }

        $company = Company::findOne($providerId);

        if($company == false){
            throw new NotFoundHttpException();
        }

        if($company->accessories == null){
            $accessories = [];
            $parentAccessories = [];
            $otherAccessories = [];
        } else {
            $accessories = Accessories::find()->where(['id' => json_decode($company->accessories)])->all();
            $parentAccessories = Accessories::find()->where(['id' => array_unique(ArrayHelper::getColumn($accessories, 'accessories_id'))])->all();
            if($company->accessories_other != null){
                $otherAccessories = json_decode($company->accessories_other);
            } else {
                $otherAccessories = [];
            }
        }


        return $this->render('services', [
            'parentAccessories' => $parentAccessories,
            'accessories' => $accessories,
            'otherAccessories' => $otherAccessories,
            'company' => $company,
        ]);
    }

    public function actionAction($providerId)
    {
        $this->layout = '@app/views/layouts/single.php';
        $this->view->params['filterEnable'] = true;
        $this->view->params['filterActive'] = false;
        $this->view->params['providerId'] = $providerId;

        return $this->render('action', [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => Advert::find()->where(['company_id' => $providerId])->orderBy('rand()')->all(),
                'pagination' => false,
            ]),
        ]);
    }

    public function actionOrder()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        if(isset($_POST['orders_items'])){


            $price = new Price([
                'name' => 'Заказ',
                'accessory_id' => $_POST['accessory'],
                'is_provider_order' => true,
                'order_items' => $_POST['orders_items'],
                'company_id' => Yii::$app->user->identity->company->id,
                'provider_id' => $_POST['providerId'],
            ]);

            $price->save(false);

            Yii::$app->session->setFlash('success', 'Заказ успешно создан');

            $userCompany = \app\models\User::find()->where(['company_id' => $_POST['providerId'], 'branch_id' => $_POST['orders_branch']])->one();

            if($userCompany){
                (new ChatMessage([
                    'sender_id' => Yii::$app->user->getId(),
                    'receiver_id' => $userCompany->id,
                    'content' => "Новая заявка ".Html::a(Url::toRoute(['my-order/view', 'id' => $price->id], true), Url::toRoute(['my-order/view', 'id' => $price->id], true)),
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);

                $email = explode('@', $userCompany->email);
                if(strlen($email[0]) > 3){
                    $email[0] = substr($email[0], 0, 3);
                    $email[0] .= "...";
                }

                $email = implode('@', $email);


                $messageParams = [
                  'from'    => \Yii::$app->params['adminEmail'],
                  'to'      => 'info@stampato.ru',
                  'subject' => 'Заказ от пользователя '.$email,
                  'html'    => "Пользователь {$email} сделал заказ. <br>".Html::a("Перейти для просмотра в чат", Url::toRoute(['my-order/view', 'id' => $price->id], true)),
                ];
                \Yii::$app->mailgun->instance->messages()->send('stampato.ru', $messageParams);

                $messageParams = [
                  'from'    => \Yii::$app->params['adminEmail'],
                  'to'      => $userCompany->email,
                  'subject' => 'Заказ от пользователя '.$email,
                  'html'    => "Пользователь {$email} сделал заказ. <br>".Html::a("Перейти для просмотра в чат", Url::toRoute(['my-order/view', 'id' => $price->id], true)),
                ];
                \Yii::$app->mailgun->instance->messages()->send('stampato.ru', $messageParams);

                \Yii::warning('Email sending');
            }

            return $this->render('order-success', [
            ]);
//            return $this->redirect($_POST['redirect']);
        }
    }

    public function actionParamsOrder($accessory, $providerId, $branch_id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $pricesPks = ArrayHelper::getColumn(Accessories::find()->where(['accessories_id' => $accessory])->all(), 'id');
        $pricesModels = Price::find()->where(['accessory_id' => $pricesPks])->joinWith(['priceCities', 'priceMetros', 'branch'])->all();

        $pricesPks = ArrayHelper::getColumn($pricesModels, 'id');

        $params = PriceParam::find()->select('price_param.*, company.name as company_name, company.id as companyId')->where(['price_id' => $pricesPks, 'company.id' => $providerId])->joinWith('price.company as company')->all();

        if(isset($params[0])){

            $price = new Price([
                'name' => 'Заказ',
                'accessory_id' => $accessory,
                'is_provider_order' => true,
                'order_items' => $params[0]->id,
                'company_id' => Yii::$app->user->identity->company->id,
                'provider_id' => $providerId,
            ]);

            $price->save(false);

            Yii::$app->session->setFlash('success', 'Заказ успешно создан');

            $userCompany = \app\models\User::find()->where(['company_id' => $providerId, 'branch_id' => $branch_id])->one();

            if($userCompany){
                (new ChatMessage([
                    'sender_id' => Yii::$app->user->getId(),
                    'receiver_id' => $userCompany->id,
                    'content' => "Новая заявка ".Html::a(Url::toRoute(['my-order/view', 'id' => $price->id], true), Url::toRoute(['my-order/view', 'id' => $price->id], true)),
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }


            return $this->render('order-success', [
            ]);
//            return $this->redirect(['user/chat', 'userId' => $userCompany->id]);
//            return $this->redirect($_POST['redirect']);
        }
    }

    public function actionToggleMy($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $toggle = null;
        $id = strval($id);
        $company = Yii::$app->user->identity->company;

        $my = explode(',', $company->providers);

        if(in_array($id, $my)){
            $pks = [];

            foreach ($my as $m)
            {
                if($m != $id){
                    $pks[] = $m;
                }
            }

            $company->providers = implode(',', $pks);
            $toggle = false;
        } else {
            $company->providers = implode(',', ArrayHelper::merge($my, [$id]));
            $toggle = true;
        }

        $company->save(false);

        return ['toggle' => $toggle];
    }

}
