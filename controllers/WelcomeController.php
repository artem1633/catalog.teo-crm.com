<?php

namespace app\controllers;

use app\components\helpers\Bitrix;

use app\models\Company;
use app\models\forms\ForgetPasswordForm;
use app\models\forms\ResetPasswordForm;
use app\models\forms\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;

/**
 * Class WelcomeController
 * @package app\controllers
 */
class WelcomeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }
}
