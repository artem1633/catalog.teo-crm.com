<?php

namespace app\controllers;

use app\models\Accessories;
use app\models\PriceParam;
use app\models\PriceParamSearch;
use Yii;
use app\models\Price;
use app\models\PriceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\TemplateFields;
use \yii\helpers\ArrayHelper;
use app\models\forms\PriceImportForm;
use \yii\web\UploadedFile;

/**
 * PriceController implements the CRUD actions for Price model.
 */
class PriceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Price models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->params['left'] = true;
        $this->view->params['left-content'] = $this->renderAjax('@app/views/user/left-profile');
        $searchModel = new PriceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFld($id, $priceId)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            if($request->isPost){
                $changes = $_POST['changes'];
                $models = PriceParam::find()->where(['price_id' => $priceId])->all();

                $counter = 0;
                foreach (explode("\n", $changes) as $change) {
                    
                    if(isset($models[$counter])){
                        $params = json_decode($models[$counter]->params, true);

                        $params[$id] = $change;

                        $models[$counter]->params = json_encode($params, JSON_UNESCAPED_UNICODE);
                        $models[$counter]->save(false);
                    }

                    $counter++;
                }

            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                        'title'=> "",
                        'content'=>$this->renderAjax('fld', [
                            'model' => $this->findModel($id),
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['id' => 'submit-button-form', 'class'=>'btn btn-primary','type'=>"submit"])
                    ]; 
            }
        }
    }


    /**
     * Displays a single Price model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;

        $paramSearchModel = new PriceParamSearch();
        $paramDataProvider = $paramSearchModel->search(Yii::$app->request->queryParams);
        $paramDataProvider->query->where(['price_id' => $id]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'paramSearchModel' => $paramSearchModel,
                'paramDataProvider' => $paramDataProvider,
            ]);
        }
    }

    public function actionExport($id)
    {
        $request = Yii::$app->request;

        $model = $this->findModel($id);

        $paramSearchModel = new PriceParamSearch();
        $paramDataProvider = $paramSearchModel->search(Yii::$app->request->queryParams);
        $paramDataProvider->query->where(['price_id' => $id]);
        $paramDataProvider->pagination = false;

        $fields = TemplateFields::find()->where(['accessories_id' => $model->accessory_id])->all();


        $data = [];


        foreach ($fields as $field){
            $output = $field->label;

            $data[0][] = $output;
        }

        foreach($paramDataProvider->models as $model)
        {
            $row = [];

            foreach ($fields as $field){

                $params = json_decode($model->params, true);

                $output = ArrayHelper::getValue($params, $field->id);

                $row[] = $output;
            }

            $data[] = $row;
        }

        // \yii\helpers\VarDumper::dump($data, 10, true);
    
        // Генерация Excel
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $objGet = $objPHPExcel->getActiveSheet();

        $styleForHeaders = array('font' => array('size' => 11,'bold' => true,'color' => array('rgb' => '000000')));

//        $objGet->getStyle('B'.$index)->applyFromArray($styleForHeaders);

//        $objGet->setCellValueByColumnAndRow(0, 1, 'Привет как дела');

        $objGet->getColumnDimension('A')->setWidth(30);
        $objGet->getColumnDimension('B')->setWidth(30);
        $objGet->getColumnDimension('C')->setWidth(30);
        $objGet->getColumnDimension('D')->setWidth(30);
        $objGet->getColumnDimension('E')->setWidth(30);
        $objGet->getColumnDimension('F')->setWidth(30);
        $objGet->getColumnDimension('G')->setWidth(30);
        $objGet->getColumnDimension('H')->setWidth(30);
        $objGet->getColumnDimension('I')->setWidth(30);
        $objGet->getColumnDimension('J')->setWidth(30);
        $objGet->getColumnDimension('L')->setWidth(30);
        $objGet->getColumnDimension('M')->setWidth(30);
        $objGet->getColumnDimension('N')->setWidth(30);
        $objGet->getColumnDimension('O')->setWidth(30);
        $objGet->getColumnDimension('P')->setWidth(30);
        $objGet->getColumnDimension('Q')->setWidth(30);
        $objGet->getColumnDimension('R')->setWidth(30);
        $objGet->getColumnDimension('S')->setWidth(30);
        $objGet->getColumnDimension('T')->setWidth(30);
        $objGet->getColumnDimension('U')->setWidth(30);
        $objGet->getColumnDimension('V')->setWidth(30);
        $objGet->getColumnDimension('W')->setWidth(30);
        $objGet->getColumnDimension('X')->setWidth(30);
        $objGet->getColumnDimension('Y')->setWidth(30);
        $objGet->getColumnDimension('Z')->setWidth(30);
        $objGet->getColumnDimension('AA')->setWidth(30);
        $objGet->getColumnDimension('AB')->setWidth(30);
        $objGet->getColumnDimension('AC')->setWidth(30);

        for ($i = 0; $i <= count($data); $i++)
        {
            if(isset($data[$i]) == false){
                continue;
            }

            $row = $data[$i];

            for ($j = 0; $j <= count($row); $j++)
            {
                if(isset($row[$j])){
                    $value = $row[$j];

                    $objGet->setCellValueByColumnAndRow($j, ($i + 1), $value);
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->getAlignment()->setWrapText(true);
                }

                if($i == 0){
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->applyFromArray($styleForHeaders);
                }
            }
        }

        $filename = 'test.xlsx';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('test.xlsx');

        Yii::$app->response->sendFile('test.xlsx');
    }

    public function actionImport($id)
    {
        $request = Yii::$app->request;
        $model = new PriceImportForm();
        $model->priceId = $id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('import', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['id' => 'submit-button-form', 'class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate()){

                $model->file = UploadedFile::getInstance($model, 'file');

                $model->upload(false);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "",
                    'content'=>'<span class="text-success">Прайс успешно создан</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                \Yii::warning($model->errors, 'Errors');
                return [
                    'title'=> "23123123",
                    'content'=>$this->renderAjax('import', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('import', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Displays a single Price model.
     * @param integer $id
     * @return mixed
     */
    public function actionCopy($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $params = PriceParam::find()->where(['price_id' => $id])->all();

        $copy = new Price();
        $copy->attributes = $model->attributes;
        $copy->name = $copy->name.' (Копия)';
        $copy->save(false);

        foreach ($params as $param)
        {
            $copyParam = new PriceParam();
            $copyParam->attributes = $param->attributes;
            $copyParam->price_id = $copy->id;
            $copyParam->save(false);
        }

        return [
            'forceReload'=>'#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    /**
     * Creates a new Price model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Price();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['id' => 'submit-button-form', 'class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){

                $accessoryModel = Accessories::findOne($model->accessory_id);


                if($accessoryModel){
                    $accessoryId = $accessoryModel->accessories_id;

                    if($accessoryId){
                        $company = Yii::$app->user->identity->company;
                        if($company){
                            $acccessoriesList = [];
                            if($company->accessories != null){
                                $acccessoriesList = json_decode($company->accessories);
                            }

                            if(in_array($accessoryId, $acccessoriesList) == false){
                                $acccessoriesList[] = $accessoryId;
                                $company->accessories = json_encode($acccessoriesList);
                                $company->save(false);
                            }
                        }
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "",
                    'content'=>'<span class="text-success">Прайс успешно создан</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Price model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            }else{
                 return [
                    'title'=> "",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }


    /**
     * Delete an existing Price model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionPublish($id, $reloadPjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->order_status = Price::ORDER_STATUS_PUBLISHED;
        $model->save(false);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>$reloadPjaxContainer];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete an existing Price model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionInBlack($id, $reloadPjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->order_status = Price::ORDER_STATUS_BLACK;
        $model->save(false);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>$reloadPjaxContainer];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete an existing Price model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Price model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Price model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Price the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Price::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
