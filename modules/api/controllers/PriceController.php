<?php

namespace app\modules\api\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use \yii\helpers\ArrayHelper;
use \app\models\Price;
use \app\models\PriceParam;
use \app\models\TemplateFields;
use \app\models\Accessories;
use \app\models\City;
use \app\models\Company;
use \yii\web\NotFoundHttpException;

/**
 * Debtor controller for the `api` module
 */
class PriceController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $models = Price::find()->where(['company_id' => $this->user->company_id])->all();


        return $models;
    }

    public function actionCreate()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        if($this->user == null){
            return null;
        }

        $request = Yii::$app->request;
        $model = new Price();
        $model->company_id = $this->user->company_id;
        
        $content = file_get_contents('php://input');
        $data = json_decode($content, true);

        foreach($data as $key => $value)
        {
            if($key == 'token')
                continue;

            $model->$key = $value;
        }

        if($model->save()){

                $accessoryModel = \app\models\Accessories::findOne($model->accessory_id);


                if($accessoryModel){
                    $accessoryId = $accessoryModel->accessories_id;

                    if($accessoryId){
                        $company = $this->user->company;
                        if($company){
                            $acccessoriesList = [];
                            if($company->accessories != null){
                                $acccessoriesList = json_decode($company->accessories);
                            }

                            if(in_array($accessoryId, $acccessoriesList) == false){
                                $acccessoriesList[] = $accessoryId;
                                $company->accessories = json_encode($acccessoriesList);
                                $company->save(false);
                            }
                        }
                    }
                }


                $fields = TemplateFields::find()->where(['accessories_id' => $model->accessory_id])->all();
                // $params = json_decode($model->params, true);

                $fieldsData = [];

                foreach ($fields as $fld) {
                    $type = null;
                    if($fld->type == TemplateFields::TYPE_TEXT){
                        $type = 'text';
                        $items = [];
                    } elseif($fld->type == TemplateFields::TYPE_NUMBER){
                        $type = 'number';
                        $items = [];
                    } elseif($fld->type == TemplateFields::TYPE_DROPDOWN){
                        $type = 'dropdown';
                        $items = explode(',', $fld->data);
                        $items = array_combine($items, $items);
                    }

                    $data = [
                        'field' => (string) $fld->id,
                        'type' => $type,
                        'label' => $fld->label,
                        'hintTitle' => $fld->hint_title,
                        'hintContent' => $fld->hint_content,
                        'required' => (boolean) $fld->is_main,
                        'dropdownData' => $items,
                    ];

                    $fieldsData[] = $data;
                }

                if($this->user->company->type != \app\models\Company::TYPE_BUYER){
                    $fieldsData[] = [
                        'field' => 'count',
                        'type' => 'number',
                        'label' => "Кол-во создаваемых строк",
                        'hintTitle' => null,
                        'hintContent' => null,
                        'required' => true,
                        'dropdownData' => [],
                    ];
                }

                return ['success' => true, 'errors' => [], 'fields' => $fieldsData];
       
            } else {
                return ['success' => false, 'errors' => $model->errors];
            }
    }

    public function actionCopy($pks)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;

        $pks = explode(',', $pks);

        $copiedPks = [];

        foreach($pks as $id)
        {
	        $model = \app\models\Price::findOne($id);

	        $params = \app\models\PriceParam::find()->where(['price_id' => $id])->all();

	        $copy = new \app\models\Price();
	        $copy->attributes = $model->attributes;
	        $copy->name = $copy->name.' (Копия)';
	        $copy->save(false);
            

	        foreach ($params as $param)
	        {
	            $copyParam = new PriceParam();
	            $copyParam->attributes = $param->attributes;
	            $copyParam->price_id = $copy->id;
	            $copyParam->save(false);
	        }

	        $copiedPks[] = $copy->id;
        }

        return ['result' => true, 'copied_pks' => $copiedPks];
    }

    public function actionCopyParams($pks)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;

        $pks = explode(',', $pks);

        $copiedPks = [];

        foreach($pks as $id)
        {
	        $model = \app\models\PriceParam::findOne($id);

	        $copyParam = new \app\models\PriceParam();
	        $copyParam->attributes = $model->attributes;
	        $copyParam->save(false);

	        $copiedPks[] = $copyParam->id;
        }

        return ['result' => true, 'copied_pks' => $copiedPks];
    }    

    public function actionImport($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;

        $model = new \app\models\forms\PriceImportForm();
        $model->priceId = $id;

        $model->file = \yii\web\UploadedFile::getInstanceByName('file');


        $model->upload(false);

        return ['result' => true];
    }

    public function actionExport($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;

        $model = \app\models\Price::findOne($id);

        $paramSearchModel = new \app\models\PriceParamSearch();
        $paramDataProvider = $paramSearchModel->search(Yii::$app->request->queryParams);
        $paramDataProvider->query->where(['price_id' => $id]);
        $paramDataProvider->pagination = false;

        $fields = \app\models\TemplateFields::find()->where(['accessories_id' => $model->accessory_id])->all();


        $data = [];


        foreach ($fields as $field){
            $output = $field->label;

            $data[0][] = $output;
        }

        foreach($paramDataProvider->models as $model)
        {
            $row = [];

            foreach ($fields as $field){

                $params = json_decode($model->params, true);

                $output = ArrayHelper::getValue($params, $field->id);

                $row[] = $output;
            }

            $data[] = $row;
        }

        // \yii\helpers\VarDumper::dump($data, 10, true);
    
        // Генерация Excel
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $objGet = $objPHPExcel->getActiveSheet();

        $styleForHeaders = array('font' => array('size' => 11,'bold' => true,'color' => array('rgb' => '000000')));


        $objGet->getColumnDimension('A')->setWidth(30);
        $objGet->getColumnDimension('B')->setWidth(30);
        $objGet->getColumnDimension('C')->setWidth(30);
        $objGet->getColumnDimension('D')->setWidth(30);
        $objGet->getColumnDimension('E')->setWidth(30);
        $objGet->getColumnDimension('F')->setWidth(30);
        $objGet->getColumnDimension('G')->setWidth(30);
        $objGet->getColumnDimension('H')->setWidth(30);
        $objGet->getColumnDimension('I')->setWidth(30);
        $objGet->getColumnDimension('J')->setWidth(30);
        $objGet->getColumnDimension('L')->setWidth(30);
        $objGet->getColumnDimension('M')->setWidth(30);
        $objGet->getColumnDimension('N')->setWidth(30);
        $objGet->getColumnDimension('O')->setWidth(30);
        $objGet->getColumnDimension('P')->setWidth(30);
        $objGet->getColumnDimension('Q')->setWidth(30);
        $objGet->getColumnDimension('R')->setWidth(30);
        $objGet->getColumnDimension('S')->setWidth(30);
        $objGet->getColumnDimension('T')->setWidth(30);
        $objGet->getColumnDimension('U')->setWidth(30);
        $objGet->getColumnDimension('V')->setWidth(30);
        $objGet->getColumnDimension('W')->setWidth(30);
        $objGet->getColumnDimension('X')->setWidth(30);
        $objGet->getColumnDimension('Y')->setWidth(30);
        $objGet->getColumnDimension('Z')->setWidth(30);
        $objGet->getColumnDimension('AA')->setWidth(30);
        $objGet->getColumnDimension('AB')->setWidth(30);
        $objGet->getColumnDimension('AC')->setWidth(30);

        for ($i = 0; $i <= count($data); $i++)
        {
            if(isset($data[$i]) == false){
                continue;
            }

            $row = $data[$i];

            for ($j = 0; $j <= count($row); $j++)
            {
                if(isset($row[$j])){
                    $value = $row[$j];

                    $objGet->setCellValueByColumnAndRow($j, ($i + 1), $value);
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->getAlignment()->setWrapText(true);
                }

                if($i == 0){
                    $objGet->getStyleByColumnAndRow($j, ($i + 1))->applyFromArray($styleForHeaders);
                }
            }
        }

        $filename = 'test.xlsx';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('test.xlsx');

        Yii::$app->response->sendFile('test.xlsx');
    }


    public function actionUpdate($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        if($this->user == null){
            return null;
        }

        $request = Yii::$app->request;
        $model = Price::findOne($id);

        if($model == null){
            throw new NotFoundHttpException('Запись не найдена');
        }

        if($model->company_id != $this->user->company_id){
            throw new NotFoundHttpException('Запись не найдена');
        }
        
        $content = file_get_contents('php://input');
        $data = json_decode($content, true);

        foreach($data as $key => $value)
        {
            if($key == 'token')
                continue;

            $model->$key = $value;
        }

        if($model->save()){

                $accessoryModel = \app\models\Accessories::findOne($model->accessory_id);


                if($accessoryModel){
                    $accessoryId = $accessoryModel->accessories_id;

                    if($accessoryId){
                        $company = $this->user->company;
                        if($company){
                            $acccessoriesList = [];
                            if($company->accessories != null){
                                $acccessoriesList = json_decode($company->accessories);
                            }

                            if(in_array($accessoryId, $acccessoriesList) == false){
                                $acccessoriesList[] = $accessoryId;
                                $company->accessories = json_encode($acccessoriesList);
                                $company->save(false);
                            }
                        }
                    }
                }


                return ['success' => true, 'errors' => []];
       
            } else {
                return ['success' => false, 'errors' => $model->errors];
            }
    }

    public function actionDelete($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;
        $model = Price::findOne($id);
        if($model == null){
            throw new NotFoundHttpException('Запись не найдена');
        }

        if($model->company_id != $this->user->company_id){
            throw new NotFoundHttpException('Запись не найдена');
        }

        return ['success' => $model->delete()];   
    }

    /**
     * @return mixed
     */
    public function actionPublish($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;
        $model = Price::findOne($id);
        if($model == null){
            throw new NotFoundHttpException('Запись не найдена');
        }

        if($model->company_id != $this->user->company_id){
            throw new NotFoundHttpException('Запись не найдена');
        }

        $model->order_status = Price::ORDER_STATUS_PUBLISHED;

        return ['success' => $model->save(false)];
    }

    /**
     * @return mixed
     */
    public function actionCancelPublish($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;
        $model = Price::findOne($id);
        if($model == null){
            throw new NotFoundHttpException('Запись не найдена');
        }

        if($model->company_id != $this->user->company_id){
            throw new NotFoundHttpException('Запись не найдена');
        }

        $model->order_status = Price::ORDER_STATUS_BLACK;

        return ['success' => $model->save(false)];
    }


    public function actionPriceParamsFields($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = Price::findOne($id);
        if($model == null){
            throw new NotFoundHttpException('Запись не найдена');
        }

        if($model->company_id != $this->user->company_id){
            throw new NotFoundHttpException('Запись не найдена');
        }

        $fields = TemplateFields::find()->where(['accessories_id' => $model->accessory_id])->all();
        // $params = json_decode($model->params, true);

        $returnData = [];

        foreach ($fields as $fld) {
            $type = null;
            if($fld->type == TemplateFields::TYPE_TEXT){
                $type = 'text';
                $items = [];
            } elseif($fld->type == TemplateFields::TYPE_NUMBER){
                $type = 'number';
                $items = [];
            } elseif($fld->type == TemplateFields::TYPE_DROPDOWN){
                $type = 'dropdown';
                $items = explode(',', $fld->data);
                $items = array_combine($items, $items);
            }

            $data = [
                'field' => (string) $fld->id,
                'type' => $type,
                'label' => $fld->label,
                'hintTitle' => $fld->hint_title,
                'hintContent' => $fld->hint_content,
                'required' => (boolean) $fld->is_main,
                'dropdownData' => $items,
            ];

            $returnData[] = $data;
        }

        if($this->user->company->type != \app\models\Company::TYPE_BUYER){
            $returnData[] = [
                'field' => 'count',
                'type' => 'number',
                'label' => "Кол-во создаваемых строк",
                'hintTitle' => null,
                'hintContent' => null,
                'required' => true,
                'dropdownData' => [],
            ];
        }

        return $returnData;
    }

    public function actionAccessoryList()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');
        
        $list = [];
        $type = \app\models\Accessories::find()->where(['is', 'accessories_id', null])->all();
        foreach ($type as $item) {
            $kings = \app\models\Accessories::find()->where(['accessories_id' => $item->id])->all();
            foreach ($kings as $king) {
                $item = \app\models\Accessories::find()->where(['accessories_id' => $king->id])->one();
                if($item){
                    $list[$item->name][$item->id] = $king->name;
                }
            }
        }

        return $list;
    }

    public function actionAddParams($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        if($this->user == false){
            return;
        }

        $request = Yii::$app->request;
        $model = new PriceParam();
        $model->price_id = $id;
        $model->company_id = $this->user->company_id;
        $model->check = 0;


        $content = file_get_contents('php://input');
        $data = json_decode($content, true);

        $params = [];

        foreach ($data as $key => $value) {
            if($key == 'token')
                continue;

            if($key == 'count'){
                $model->$key = $value;
            } else {
                $params[$key] = $value;
            }
        }

        $model->params = json_encode($params, JSON_UNESCAPED_UNICODE);

        return ['success' => $model->saveMulti(false), 'errors' => $model->errors];
    }

    public function actionUpdateParams($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;
        $model = PriceParam::findOne($id);

        if($model == null){
            throw new NotFoundHttpException('Запись не найдена');
        }

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);

        if($data == null){
        	return [];
        }

        $params = json_decode($model->params, true);

        foreach ($data as $key => $value) {
            if($key == 'token')
                continue;

            if($key == 'count'){
                $model->$key = $value;
            } else {
                $params[$key] = $value;
            }
        }

        $model->params = json_encode($params, JSON_UNESCAPED_UNICODE);


        return ['success' => $model->save(false), 'errors' => $model->errors];
    }

    public function actionDeleteParam($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $request = Yii::$app->request;
        $model = PriceParam::findOne($id);

        if($model == null){
            throw new NotFoundHttpException('Запись не найдена');
        }

        return ['success' => $model->delete()];
    }

    public function actionDeleteParamsTotal($pks)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $pks = explode(',', $pks);

        $request = Yii::$app->request;
        $model = PriceParam::deleteAll(['id' => $pks]);

        return ['success' => true];
    }

    public function actionDeleteTotal($pks)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $pks = explode(',', $pks);

        $request = Yii::$app->request;
        $model = Price::deleteAll(['id' => $pks]);

        return ['success' => true];
    }

    public function actionViewParams($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');
        
        $model = Price::findOne($id);
        if($model == null){
            throw new NotFoundHttpException('Запись не найдена');
        }

        if($model->company_id != $this->user->company_id){
            throw new NotFoundHttpException('Запись не найдена');
        }

        $fields = TemplateFields::find()->where(['accessories_id' => $model->accessory_id])->all();
        $params = PriceParam::find()->where(['price_id' => $model->id])->all();

        $returnData = [];


        $fieldsData = [];

        foreach ($fields as $fld) {
            $type = null;
            if($fld->type == TemplateFields::TYPE_TEXT){
                $type = 'text';
                $items = [];
            } elseif($fld->type == TemplateFields::TYPE_NUMBER){
                $type = 'number';
                $items = [];
            } elseif($fld->type == TemplateFields::TYPE_DROPDOWN){
                $type = 'dropdown';
                $items = explode(',', $fld->data);
                $items = array_combine($items, $items);
            }

            $data = [
                'field' => (string) $fld->id,
                'type' => $type,
                'label' => $fld->label,
                'hintTitle' => $fld->hint_title,
                'hintContent' => $fld->hint_content,
                'required' => (boolean) $fld->is_main,
                'dropdownData' => $items,
            ];

            $fieldsData[] = $data;
        }


        foreach ($params as $param) {
            $returnDatum = ['id' => $param->id];
            $data = json_decode($param['params'], true);

            foreach ($data as $key => $value) {
                foreach ($fields as $fld) {
                    if($fld->id == $key)
                    {
                        $returnDatum[$fld->label] = $value;
                    } else {
                        if(isset($returnDatum[$fld->label]) == false){
                            $returnDatum[$fld->label] = null;
                        }
                    }
                }
            }

            $returnData[] = $returnDatum;
        }




        // return ['data' => $returnData, 'fields' => $fieldsData];

        return $returnData;
    }

    public function beforeAction($action)
    {
        if($action->id != 'register' && $action->id != 'login')
        {
            if(isset($_GET['token'])){

                if(isset($_GET['token']) == false){
                     return ['error' => "Токен не найден"];
                }

                if($_GET['token'] == null){
                     return ['error' => "Токен пуст"];
                }

                $token = $_GET['token'];

                $user = \app\models\User::find()->where(['token' => $token])->one();

                if($user == false){
                     return ['error' => "Токен некорректный"];
                }

                $this->user = $user;

            //} elseif (Yii::$app->request->isPost){
            } else{

                $content = file_get_contents('php://input');
                $data = json_decode($content, true);

                \Yii::warning($data);



                if(isset($data['token']) == false){
                    // Yii::warning('heere');
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                     return ['error' => "Токен не найден"];
                }

                if($data['token'] == null){
                    // Yii::warning('heere2');
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                     return ['error' => "Токен пуст"];
                }

                $token = $data['token'];

                $user = \app\models\User::find()->where(['token' => $token])->one();

                if($user == false){
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                     return ['error' => "Токен некорректный"];
                }

                $this->user = $user;

            }
        }

        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
}
