<?php

namespace app\modules\api\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use \yii\helpers\ArrayHelper;
use \app\models\Price;
use \app\models\Accessories;
use \app\models\City;
use \app\models\Company;

/**
 * Debtor controller for the `api` module
 */
class V1Controller extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionCategories()
    {
        header('Access-Control-Allow-Origin: *');

        $mainCategories = \yii\helpers\ArrayHelper::getColumn(\app\models\Accessories::find()->where(['accessories_id' => null])->andWhere(['is_main' => true])->all(), 'id');
        $categories = \app\models\Accessories::find()->where(['id' => $mainCategories])->andWhere(['is_main' => true])->all();

        return $categories;
    }

    public function actionCompanies($category_id, $city = null, $metro = null)
    {
        header('Access-Control-Allow-Origin: *');
        
         if($city == null){
            $city = City::find()->where(['name' => 'Санкт-Петербург'])->one();
            if($city){
                $city = $city->id;
            }
        }


        $firstAccess = Accessories::find()->where(['id' => $category_id])->andWhere(['is', 'accessories_id', null])->all();
        $secondAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($firstAccess, 'id')])->all();
        $thirdAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($secondAccess, 'id')])->all();


        $branches = \app\models\Branch::find()->andFilterWhere(['city_id' => $city, 'metro_id' => $metro])->all();

        $companiesPks = array_unique(ArrayHelper::getColumn($branches, 'company_id'));

        $companies = Company::find()->where(['type' => Company::TYPE_PROVIDER])->andWhere(['id' => $companiesPks])->andWhere(['!=', 'id', 1])->asArray()->all();

        $branchesPks = ArrayHelper::getColumn($branches, 'id');

        $allowAttrs = ['id', 'prices', 'branches', 'name', 'accessories', 'accessories_other', 'avatar', 'description'];


        array_walk($companies, function(&$company) use ($thirdAccess, $branches, $branchesPks, $allowAttrs){

            $company['prices'] = [];
            $company['branches'] = \app\models\Branch::find()->where(['company_id' => $company['id']])->asArray()->all();


            array_walk($company['branches'], function(&$model){
                $metro = \app\models\Metro::findOne($model['metro_id']);
                if($metro){
                    $model['metroName'] = $metro->name;
                }
            });


            foreach ($thirdAccess as $access) {

                                $price = Price::find()->where(['accessory_id' => $access->id, 'company_id' => $company['id'], 'branch_id' => $branchesPks])->one();

                                $pAccess = Accessories::find()->where(['id' => $access->accessories_id])->one();

                                if($price){
                                    if($price->order_status == Price::ORDER_STATUS_PUBLISHED) // Если прайс опубликован
                                    {

                                        $company['prices'][] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Рассчитать'];

                                    } else {

                                        // Уточняем есть ли в услугах компании данная услуга
                                        if($company['accessories'] == null){
                                            $dataAccessories = [];
                                        } else {
                                            $dataAccessories = json_decode($company['accessories']);
                                        }

                                        if(in_array($access->accessories_id, $dataAccessories)){
                                            $user = \app\models\User::find()->where(['company_id' => $company['id']])->one();
                                            if($user) {
                                                $company['prices'][] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Консультация'];
                                                
                                            } else {
                                                $company['prices'][] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Консультация'];
                                            }
                                        } else {
                                            $company['prices'][] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => '—'];
                                        }
                                    }

                                } else {

                                    // Уточняем есть ли в услугах компании данная услуга
                                    if($company['accessories'] == null){
                                        $dataAccessories = [];
                                    } else {
                                        $dataAccessories = json_decode($company['accessories']);
                                    }

                                    if(in_array($access->accessories_id, $dataAccessories)){
                                        $user = \app\models\User::find()->where(['company_id' => $company['id']])->one();
                                        if($user) {
                                            $company['prices'][] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Консультация'];
                                        } else {
                                            $company['prices'][] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Консультация'];
                                        }
                                    } else {
                                        $company['prices'][] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => '—'];
                                    }

                                }                
            }


            foreach ($company as $key => $value) {
                if(!in_array($key, $allowAttrs)){
                    unset($company[$key]);
                }
            }

        });


        $companies = array_values(array_filter($companies, function($model){
            $filtered = false;
            
            foreach ($model['prices'] as $price) {
                if($price['type'] != '—'){
                    $filtered = true;
                }               
            }

            return $filtered;
        }));

        return ['companies' => $companies, 'headers' => \yii\helpers\ArrayHelper::map($secondAccess, 'id', 'name')];
    }

    public function actionSub($category_id)
    {
        header('Access-Control-Allow-Origin: *');

        $models = Accessories::find()->where(['accessories_id' => $category_id])->andWhere(['is_main' => true])->asArray()->all();

        array_walk($models, function(&$model){
            $model['icon'] = \yii\helpers\Url::toRoute('/access_icon.svg', true);
        });

        return $models;
    }

    public function actionTestOb()
    {
        echo 'Begin ...<br />';
        for( $i = 0 ; $i < 10 ; $i++ )
        {
            echo $i . '<br />';
            flush();
            ob_flush();
            sleep(1);
        }

        echo 'End ...<br />';
    }

    public function actionFieldsFilter($category_id, $company_id = null)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT');

        $model = Accessories::find()->where(['accessories_id' => $category_id])->one();


        if($model){
            /** @var TemplateFields[] $fields */
            $fields = \app\models\TemplateFields::find()->where(['accessories_id' => $model->id])->orderBy('sort asc')->all();

            $pricesPks = ArrayHelper::getColumn(\app\models\Accessories::find()->where(['accessories_id' => $category_id])->all(), 'id');
            $pricesModel = \app\models\Price::find()
                ->where(['accessory_id' => $pricesPks])
                ->andWhere(['order_status' => Price::ORDER_STATUS_PUBLISHED])
                ->andWhere(['!=', 'price.company_id', 1])
                ->joinWith(['priceCities', 'priceMetros', 'branch']);

            $pricesModel = $pricesModel->all();




            $pricesPks = ArrayHelper::getColumn($pricesModel,'id');

            // \yii\helpers\VarDumper::dump($pricesPks, 10, true);
            // exit;

            $params = \app\models\PriceParam::find()
                ->select('price_param.*, company.name as company_name, company.rate_id as company_rate_id')
                ->where(['price_id' => $pricesPks])
                ->andWhere(['!=', 'company.type', \app\models\Company::TYPE_BUYER])
                ->andFilterWhere(['price_param.company_id' => $company_id])
                ->joinWith('price.accessory as accessory')
                ->joinWith('price.branch as branch')
                ->joinWith('price.company as company');

            $params->andWhere(['!=', 'company.rate_id', 0]);

            $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
            $report = json_decode($content, true); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
                \Yii::warning($report);



            if ($report == null){
                return "json null"; 
            }



            foreach ($report as $id => $value){
                if($value == null) continue;
                if($value != "Нет"){
                    // $params->andWhere(['or', ['like', 'params', '"'.$id.'":"'.trim($value).'"'], ['like', 'params', '"'.$id.'":"'.$value.'"'], ['like', 'params', '"'.$id.'":" '.$value.'"']]);
                    $params->andWhere(['like', 'params', '"'.$id.'":"'.trim($value).'"']);
                } else {
                    $params->andWhere(['like', 'params', '"'.$id.'":""']);
                }
            }


            $data = $params->asArray()->all();

            $fieldsData = \yii\helpers\ArrayHelper::map($fields, 'id', 'label');


            array_walk($data, function(&$model) use ($fieldsData){
                $params = json_decode($model['params']);
                    
                $formatParams = [];

                foreach ($params as $id => $value) {
                    $formatParams[$fieldsData[$id]] = $value;
                }

                $model = \yii\helpers\ArrayHelper::merge($model, $formatParams);
                $model['company'] = \app\models\Company::find()->where(['id' => $model['company_id']])->asArray()->one();
                $model['company']['branches'] = \app\models\Branch::find()->where(['company_id' => $model['company_id']])->asArray()->all();

                $price = \app\models\Price::findOne($model['price_id']);

                $model['branch_id'] = $price ? $price['branch_id'] : null;


                array_walk($model['company']['branches'], function(&$model){
                    $metro = \app\models\Metro::findOne($model['metro_id']);
                    if($metro){
                        $model['metroName'] = $metro->name;
                    }
                });

                $allowAttrs = ['id', 'prices', 'branches', 'name', 'accessories', 'accessories_other', 'avatar'];

                foreach ($model['company'] as $key => $value) {
                    if(!in_array($key, $allowAttrs)){
                        unset($model['company'][$key]);
                    }
                }

                unset($model['params']);
                unset($model['price']);
            });

            $companyPks = array_values(array_unique(ArrayHelper::getColumn($data, 'company.id')));

            $companies = Company::find()->where(['id' => $companyPks])->andWhere(['!=', 'id', 1])->asArray()->all();

            array_walk($companies, function(&$company) use ($data){
                $company['branches'] = \app\models\Branch::find()->where(['company_id' => $company['id']])->asArray()->all();


                $allowAttrs = ['id', 'prices', 'branches', 'name', 'accessories', 'accessories_other', 'avatar', 'data'];

                array_walk($company['branches'], function(&$model){
                    $metro = \app\models\Metro::findOne($model['metro_id']);
                    if($metro){
                        $model['metroName'] = $metro->name;
                    }
                });


                $company['data'] = array_values(array_filter($data, function(&$model) use ($company){
                    unset($model['company']);
                    return $model['company_id'] == $company['id'];
                }));

                foreach ($company as $key => $value) {
                    if(!in_array($key, $allowAttrs)){
                        unset($company[$key]);
                    }
                }

                unset($company['params']);
                unset($company['price']);
            });



            // return $data;
            return $companies;
        }

        throw new \yii\web\NotFoundHttpException("Категория не найдена");
    }

    public function actionFieldsFilterHeaders($category_id)
    {
        header('Access-Control-Allow-Origin: *');


        $model = Accessories::find()->where(['accessories_id' => $category_id])->one();


        if($model){
            /** @var TemplateFields[] $fields */
            $fields = \app\models\TemplateFields::find()->where(['accessories_id' => $model->id])->orderBy('sort asc')->all();

            $pricesPks = ArrayHelper::getColumn(\app\models\Accessories::find()->where(['accessories_id' => $category_id])->all(), 'id');
            $pricesModel = \app\models\Price::find()
                ->where(['accessory_id' => $pricesPks])
                ->andWhere(['order_status' => Price::ORDER_STATUS_PUBLISHED])
                ->andWhere(['!=', 'price.company_id', 1])
                ->joinWith(['priceCities', 'priceMetros', 'branch']);

            $pricesModel = $pricesModel->all();




            $pricesPks = ArrayHelper::getColumn($pricesModel,'id');


            // \yii\helpers\VarDumper::dump($pricesPks, 10, true);
            // exit;

            $params = \app\models\PriceParam::find()
                ->select('price_param.*, company.name as company_name, company.rate_id as company_rate_id')
                ->where(['price_id' => $pricesPks])
                ->andWhere(['!=', 'company.type', \app\models\Company::TYPE_BUYER])
                ->joinWith('price.accessory as accessory')
                ->joinWith('price.branch as branch')
                ->joinWith('price.company as company');

            $params->andWhere(['!=', 'company.rate_id', 0]);

            foreach ($_POST as $id => $value){
                if($value == null) continue;
                if($value != -1){
                    $params->andWhere(['like', 'params', '"'.$id.'":"'.$value.'"']);
                } else {
                    $params->andWhere(['like', 'params', '"'.$id.'":""']);
                }
            }


            // array_walk($fields, function(&$field) use ($params){
            //     $query = clone $params;
            //     $field['data'] = explode(',', $field['data']);

            //     $parent = $field->parent;


            //     $disabled = false;

            //     if($parent){
            //         $disabled = true;
            //         if(isset($_GET[$parent->id])){
            //             if($_GET[$parent->id]){
            //                 $disabled = false;
            //             }
            //         }
            //     }

            //     $field['data'] = array_filter($field['data'], function($value) use ($query, $field, $parent){

            //                    $query = clone $query;


            //    if($parent){
            //        $filterValue = null;

            //         if(isset($_GET[$parent->id])){
            //             if($_GET[$parent->id]){
            //                 $filterValue = $_GET[$parent->id];
            //             }
            //         }


            //        $prices = $query->andWhere(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['like', 'params', '"'.$parent->id.'":"'.$filterValue.'"'])->count();
            //    } else {
            //        $prices = $query->andWhere(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->count();
            //    }


                               

            //                    // if($fldValue){
            //                    //     return $fldValue == $value;
            //                    // }


            //     //                 $prices = \app\models\PriceParam::find()->where(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['price_id' => $pricesPks, 'company.id' => $providerId])->count();

            //                    return $prices > 0;
            //                });
            
            //     $field['data'] = array_values($field['data']);
            // });

            // $data = $params->asArray()->all();

            $fieldsData = \yii\helpers\ArrayHelper::map($fields, 'id', 'label');


            return $fieldsData;
        }

        throw new \yii\web\NotFoundHttpException("Категория не найдена");
    }

    public function actionFieldsUrl()
    {
        $category_id = 53;

        header('Access-Control-Allow-Origin: *');

        $model = Accessories::find()->where(['accessories_id' => $category_id])->one();


        if($model){


            /** @var TemplateFields[] $fields */
            $fields = \app\models\TemplateFields::find()->where(['accessories_id' => $model->id])->orderBy('sort asc')->all();

            $pricesPks = ArrayHelper::getColumn(\app\models\Accessories::find()->where(['accessories_id' => $category_id])->all(), 'id');
            $pricesModel = \app\models\Price::find()
                ->where(['accessory_id' => $pricesPks])
                ->andWhere(['order_status' => Price::ORDER_STATUS_PUBLISHED])
                ->andWhere(['!=', 'price.company_id', 1])
                ->joinWith(['priceCities', 'priceMetros', 'branch']);

            $pricesModel = $pricesModel->all();




            $pricesPks = ArrayHelper::getColumn($pricesModel,'id');


            // \yii\helpers\VarDumper::dump($pricesPks, 10, true);
            // exit;

            $params = \app\models\PriceParam::find()
                ->select('price_param.*, company.name as company_name, company.rate_id as company_rate_id')
                ->where(['price_id' => $pricesPks])
                ->andWhere(['!=', 'company.type', \app\models\Company::TYPE_BUYER])
                ->joinWith('price.accessory as accessory')
                ->joinWith('price.branch as branch')
                ->joinWith('price.company as company');

            $params->andWhere(['!=', 'company.rate_id', 0]);




            array_walk($fields, function(&$field) use ($params){
                $query = clone $params;
                $field['data'] = explode(',', $field['data']);


                $field['data'] = array_filter($field['data'], function($value) use ($query, $field){

                               $query = clone $query;

                               $prices = $query->andWhere(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->count();

                               // if($fldValue){
                               //     return $fldValue == $value;
                               // }


                //                 $prices = \app\models\PriceParam::find()->where(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['price_id' => $pricesPks, 'company.id' => $providerId])->count();

                               return $prices > 0;
                           });


            
                $field['data'] = array_values($field['data']);
            
                // $field['data'] = ArrayHelper::merge(['Нет'], $field['data']);


            });


            // set_time_limit(0);

            $urls = [];

            foreach ($fields as $fld) {
                $urls[] = $this->urlRecursive($fields, $fld, $urls);    
                break;            
            }

            return $urls;
        }     
    }

    private function urlRecursive($fields, $fld, $params = [], $counter = 0)
    {
        foreach ($fld['data'] as $value) {
            // $localUrls = [
            //     [
            //         $fld['id'] => $value,
            //     ],
            // ];
            // $urls[] = Url::toRoute(['/api/v1/fields-update', 'category_id' => $category_id, "filters[{$fld['id']}]" => $value], true);



            $params[$fld['id']] = $value;
            foreach ($fields as $subFld) {
                if($subFld['id'] == $fld['id']){
                    continue;
                }
                if($subFld['sort'] < $fld['sort']){
                    continue;
                }
                if(($subFld['sort'] + 1) == count($fields)){
                    break;
                }
                $params = $this->urlRecursive($fields, $subFld, $params);
                // break;
                // $params = ArrayHelper::merge($params, [$fld['id'] => $value]);
                // $params = $this->urlRecursive($fields, $subFld, $params);
            }


            break;

            // if($counter == 2){
                // break;
            // }

            // break;
        } 

        return $params;
    }

    public function actionTestRec()
    {
        $arr = [
            [1, 2],
            [3, 4],
        ];

        // [1, 3]
        // [1, 4]
        // [2, 3]
        // [2, 4]

        $output = $this->rec($arr);

        return $output;
    }


    private function rec($arr, $output = [], $counter = 0)
    {
        foreach ($arr as $row) {
            foreach ($row as $el) {
                $output[] = $el;
                $counter++;
                $output = $this->rec(array_slice($arr, $counter), $output, $counter);
            }
        }

        return $output;
    }

    public function actionFields($category_id)
    {
        header('Access-Control-Allow-Origin: *');

        $model = Accessories::find()->where(['accessories_id' => $category_id])->one();


        if($model){

            // if(Yii::$app->cache->exists('API_FIELDS_'.$category_id)){
            //     $fields = Yii::$app->cache->get('API_FIELDS_'.$category_id);
            //     return $fields;
            // }

            /** @var TemplateFields[] $fields */
            $fields = \app\models\TemplateFields::find()->where(['accessories_id' => $model->id, 'is_main' => 1])->orderBy('sort asc')->all();

            $pricesPks = ArrayHelper::getColumn(\app\models\Accessories::find()->where(['accessories_id' => $category_id])->all(), 'id');
            $pricesModel = \app\models\Price::find()
                ->where(['accessory_id' => $pricesPks])
                ->andWhere(['order_status' => Price::ORDER_STATUS_PUBLISHED])
                ->andWhere(['!=', 'price.company_id', 1])
                ->joinWith(['priceCities', 'priceMetros', 'branch']);

            $pricesModel = $pricesModel->all();




            $pricesPks = ArrayHelper::getColumn($pricesModel,'id');


            // \yii\helpers\VarDumper::dump($pricesPks, 10, true);
            // exit;

            $params = \app\models\PriceParam::find()
                ->select('price_param.*, company.name as company_name, company.rate_id as company_rate_id')
                ->where(['price_id' => $pricesPks])
                ->andWhere(['!=', 'company.type', \app\models\Company::TYPE_BUYER])
                ->joinWith('price.accessory as accessory')
                ->joinWith('price.branch as branch')
                ->joinWith('price.company as company');

            $params->andWhere(['!=', 'company.rate_id', 0]);




            array_walk($fields, function(&$field) use ($params){
                $query = clone $params;
                $field['data'] = explode(',', $field['data']);


                $field['data'] = array_filter($field['data'], function($value) use ($query, $field){

                               $query = clone $query;

                               $prices = $query->andWhere(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->count();

                               // if($fldValue){
                               //     return $fldValue == $value;
                               // }


                //                 $prices = \app\models\PriceParam::find()->where(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['price_id' => $pricesPks, 'company.id' => $providerId])->count();

                               return $prices > 0;
                           });


            
                $field['data'] = array_values($field['data']);
            
                // $field['data'] = ArrayHelper::merge(['Нет'], $field['data']);


            });


            // Yii::$app->cache->set('API_FIELDS_'.$category_id, $fields);

            return $fields;
        }

        throw new \yii\web\NotFoundHttpException("Категория не найдена");
    }


    public function actionFieldsUpdate($category_id)
    {
        header('Access-Control-Allow-Origin: *');

        $model = Accessories::find()->where(['accessories_id' => $category_id])->one();


        $filterParams = $_GET['filters'];


        if($model){

            $filterParamsJson = json_encode($filterParams);

            // if(Yii::$app->cache->exists('API_FIELDS_JSON')){
            //     $fields = Yii::$app->cache->get('API_FIELDS_JSON');

            //     if(isset($fields[$filterParamsJson])){
            //         return $fields[$filterParamsJson];
            //     }
            // }

            /** @var TemplateFields[] $fields */
            // $fields = \app\models\TemplateFields::find()->where(['accessories_id' => $model->id])->orderBy('sort asc')->asArray()->all();


            $fields = \app\models\TemplateFields::find()->where(['accessories_id' => $model->id, 'is_main' => 1])->orderBy('sort asc')->all();

            $pricesPks = ArrayHelper::getColumn(\app\models\Accessories::find()->where(['accessories_id' => $category_id])->all(), 'id');
            $pricesModel = \app\models\Price::find()
                ->where(['accessory_id' => $pricesPks])
                ->andWhere(['order_status' => Price::ORDER_STATUS_PUBLISHED])
                ->andWhere(['!=', 'price.company_id', 1])
                ->joinWith(['priceCities', 'priceMetros', 'branch']);

            $pricesModel = $pricesModel->all();




            $pricesPks = ArrayHelper::getColumn($pricesModel,'id');


            // \yii\helpers\VarDumper::dump($pricesPks, 10, true);
            // exit;

            $params = \app\models\PriceParam::find()
                ->select('price_param.*, company.name as company_name, company.rate_id as company_rate_id')
                ->where(['price_id' => $pricesPks])
                ->andWhere(['!=', 'company.type', \app\models\Company::TYPE_BUYER])
                ->joinWith('price.accessory as accessory')
                ->joinWith('price.branch as branch')
                ->joinWith('price.company as company');

            $params->andWhere(['!=', 'company.rate_id', 0]);

            // $params->andWhere(['like', 'params', '"'.$id.'":"'.$value.'"']);

            foreach ($filterParams as $paramId => $paramValue) {
                $params->andWhere(['like', 'params', '"'.$paramId.'":"'.trim($paramValue).'"']);
            }


            array_walk($fields, function(&$field) use ($params){
                $query = clone $params;
                $field['data'] = explode(',', $field['data']);

                $field['data'] = array_filter($field['data'], function($value) use ($query, $field){

                               $query = clone $query;

                               $prices = $query->andWhere(['like', 'params', '"'.$field['id'].'":"'.trim($value).'"'])->count();

                               // if($fldValue){
                               //     return $fldValue == $value;
                               // }


                //                 $prices = \app\models\PriceParam::find()->where(['like', 'params', '"'.$field->id.'":"'.$value.'"'])->andWhere(['price_id' => $pricesPks, 'company.id' => $providerId])->count();

                               return $prices > 0;
                           });
            
                $field['data'] = array_values($field['data']);

                // \Yii::warning($field);

                // var_dump($field);

                // echo serialize($field);

                // echo json_encode((object) $field, JSON_UNESCAPED_UNICODE);
                // flush();
                // ob_flush();
                // sleep(1);

                // $field['data'] = ArrayHelper::merge(['Нет'], $field['data']);


            });


            // Yii::$app->cache->set('API_FIELDS_JSON', ArrayHelper::merge(
            //     Yii::$app->cache->exists('API_FIELDS_JSON') ? Yii::$app->cache->get('API_FIELDS_JSON') : [],
            //     [$filterParamsJson => $fields])
            // );

            return $fields;
        }

        throw new \yii\web\NotFoundHttpException("Категория не найдена");
    }

    public function actionOrder($token, $id, $branch_id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT');

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content, true); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)

        $user = \app\models\User::find()->where(['token' => $token])->one();

        // $pricesPks = ArrayHelper::getColumn(Accessories::find()->where(['accessories_id' => $accessory])->all(), 'id');
        // $pricesModels = Price::find()->where(['accessory_id' => $pricesPks])->joinWith(['priceCities', 'priceMetros', 'branch'])->all();

        // $pricesPks = ArrayHelper::getColumn($pricesModels, 'id');

        // $params = PriceParam::find()->select('price_param.*, company.name as company_name, company.id as companyId')->where(['price_id' => $pricesPks, 'company.id' => $providerId])->joinWith('price.company as company')->all();

        \Yii::warning('123', '123');

        // if($report == null){
            // return ['result' => false];
        // }

        // if(count($report)){
        if(true){

            // $price = new Price([
            //     'name' => 'Заказ',
            //     'accessory_id' => $accessory,
            //     'is_provider_order' => true,
            //     'order_items' => $params[0]->id,
            //     'company_id' => Yii::$app->user->identity->company->id,
            //     'provider_id' => $providerId,
            // ]);

            // $price->save(false);

            \Yii::warning('456', '456');


            $model = \app\models\PriceParam::findOne($id);

            if($model == null){
                return ['result' => false];
            }

            \Yii::warning('789', '789');
            $paramsData = json_decode($model['params'], true);
            $price = Price::findOne($model->price_id);
            if($price == null){
                return ['result' => false];
            }
            $accessory = Accessories::findOne($price->accessory_id);

            Yii::$app->session->setFlash('success', 'Заказ успешно создан');

            \Yii::warning('10', '10');

            // $userCompany = \app\models\User::find()->where(['company_id' => $providerId, 'branch_id' => $branch_id])->one();
            $userCompany = \app\models\User::find()->where(['company_id' => $model->company_id, 'branch_id' => $branch_id])->one();

            \Yii::warning($userCompany, 'userCompany');

            $text = "Категория: «{$accessory->name}», Параметры:\n";

            // foreach ($report as $id => $value) {
            //     $fld = \app\models\TemplateFields::findOne($id);
            //     if($fld){
            //         $text .= "{$fld->name}: {$value}\n";
            //     }
            // }


            foreach ($paramsData as $datumId => $datumValue) {
                $fld = \app\models\TemplateFields::findOne($datumId);

                if($fld){
                    $text .= "{$fld->name}: {$datumValue}\n";
                }
            }

            if($userCompany){

                $messageContent = "Новая заявка!\n{$text}";

                \Yii::warning($messageContent, 'messageContent');

                (new \app\models\ChatMessage([
                    'sender_id' => $user->id,
                    'receiver_id' => $userCompany->id,
                    'content' => $messageContent,
                    // 'is_read' => true,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }


            return ['result' => true];
//            return $this->redirect(['user/chat', 'userId' => $userCompany->id]);
//            return $this->redirect($_POST['redirect']);
        }

        return ['result' => false];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
}
