<?php

namespace app\modules\api\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use \yii\helpers\ArrayHelper;
use \app\models\Price;
use \app\models\Accessories;
use \app\models\City;
use \app\models\Company;

/**
 * Debtor controller for the `api` module
 */
class CompanyController extends Controller
{

    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     * Login action.
     *
     * @return string
     */
    public function actionRegister()
    {

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        // if (!Yii::$app->user->isGuest) {
            // return ["error" => "не авторизованы"];    
        // }

        $model = new \app\models\forms\SignupForm();


        $model->type = 1;




        // if ($model->load(Yii::$app->request->post()['SignupForm']) && $model->signup()) {
        if (Yii::$app->request->isPost) {
            // (new LoginForm(['username' => $model->email, 'password' => $model->password]))->login();

            $content = file_get_contents('php://input');
            $attributes = json_decode($content, true);

            $model->attributes = $attributes;

            $token = $model->signup(true);

            // if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
            //     return $this->redirect(['branch/index']);
            // }

            // if($redirect_url){
            //     return $this->redirect($redirect_url);
            // } else {
            //     return $this->goHome();
            // }

            return ['token' => $token, 'errors' => $model->errors];
        }


        return ['token' => null, 'errors' => []];
    }

    public function actionSendCreditionals()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);

        if($data == null){
        	return ['success' => false];
        }

        if(isset($data['email']) == false){
        	throw new BadRequestHttpException('Параметр "email" отправлен не корректно');
        }

        $newPassword = Yii::$app->security->generateRandomString(10);

        $user = \app\models\User::find()->where(['email' => $data['email']])->one();

        if($user == null){
            throw new BadRequestHttpException('Параметр "email" отправлен не корректно');
        }

        $user->password = $newPassword;
        $user->save(false);

		$messageParams = [
		  // 'from'    => \Yii::$app->params['adminEmail'],
		  'from'    => 'admin@stampato.ru',
		  'to'      => $data['email'],
		  'subject' => 'Данные для входа',
		  'html'    => "Ваши данные для входа в личный кабинет<br><b>Email:</b> {$data['email']}<br><b>Новый пароль:</b> {$newPassword}",
		  'attachment' => [],
		];

        $result = \Yii::$app->mailgun->instance->messages()->send('stampato.ru', $messageParams);

        \Yii::warning($newPassword, 'New password');
        \Yii::warning($result, 'Mail sending result');

        return ['success' => true];
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionRegisterClient()
    {

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        // if (!Yii::$app->user->isGuest) {
            // return ["error" => "не авторизованы"];    
        // }

        $model = new \app\models\forms\SignupForm();


        $model->type = 0;




        // if ($model->load(Yii::$app->request->post()['SignupForm']) && $model->signup()) {
        if (Yii::$app->request->isPost) {
            // (new LoginForm(['username' => $model->email, 'password' => $model->password]))->login();

            $content = file_get_contents('php://input');
            $attributes = json_decode($content, true);

            $model->attributes = $attributes;

            $token = $model->signup(true);

            // if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
            //     return $this->redirect(['branch/index']);
            // }

            // if($redirect_url){
            //     return $this->redirect($redirect_url);
            // } else {
            //     return $this->goHome();
            // }

            return ['token' => $token, 'errors' => $model->errors];
        }


        return ['token' => null, 'errors' => []];
    }

    public function actionCityList()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');
        
        $models = \app\models\City::find()->joinWith(['region'])->andWhere(['region.name' => 'Санкт-Петербург и Ленинградская область'])->all();

        return $models;
    }

    public function actionMetroList()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');
        
        $models = \app\models\Metro::find()->where(['city_id' => 540])->all(); // 540 — Санкт-Петербург

        return $models;
    }

    public function actionLogin()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);

        \Yii::warning($content);
        \Yii::warning($data);

        if($data == null){
            \Yii::warning("нету данных");
            return ['error' => "нету данных"];
        }


        if(isset($data['email']) == false || isset($data['password']) == false){
            return ["error" => "Логин или пароль не указаны"];    
        }

        $hash = md5($data['email'].$data['password']);

        $model = \app\models\User::find()->where(['token' => $hash])->one();

        if($model == null){

            return ["error" => "Неверный логин или пароль"];    
        }


         \Yii::warning("вернули токен {$hash}");
        return ['token' => $hash, 'role' => $model->company->type];
    }

    public function actionAdminPage()
    {

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $company = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();

        
        return [
            'email' => $company->email, 
            'name' => $company->name,
            'phone' => $company->phone, 
            'avatar' => $company->avatar, 
        ];
    }

    public function actionAdminSave()
    {

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);


        $allowAttributes = ['email', 'name', 'phone', 'password'];

        if (Yii::$app->request->isPost) {

            $user = \app\models\User::find()->where(['company_id' => $this->user->company_id])->one();
            
            foreach ($data as $attribute => $value) {
                if(in_array($attribute, $allowAttributes) == false){
                    continue;
                }

                if($attribute == 'password'){
                    if($user){
                        $user->password = $value;
                        $user->save(false);
                    }
                } else {
                    $model->$attribute = $value;
                }

            }


            $model->save();

            return [
                'email' => $model->email, 
                'name' => $model->name,
                'phone' => $model->phone, 
                'token' => $user ? $user->token : null, 
            ];
        } else {
            return $model->errors;
        }
    }

    public function actionCompany($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $id])->one();

        if($model == null){
        	throw new \yii\web\NotFoundHttpException('Компания не найдена');
        }


        $branches = \app\models\Branch::find()->where(['company_id' => $model->id])->asArray(true)->all();

        $allowAttributes = ['id', 'name', 'phone', 'email', 'social_site', 'address', 'city_id', 'social_site', 'social_vk', 'social_instagram', 'file', 'metro_id','avatar', 'coords'];

        for ($i=0; $i < count($branches); $i++) { 
            foreach ($branches[$i] as $attr => $value) {
                if(in_array($attr, $allowAttributes) == false){
                    unset($branches[$i][$attr]);
                }
            }
        }

        array_walk($branches, function(&$branch){
        	$metro = \app\models\Metro::findOne($branch['metro_id']);

        	if($metro){
        		$branch['metroName'] = $metro->name;
        	} else {
        		$branch['metroName'] = null;
        	}
        });



        $images = \app\models\ImageCompany::find()->where(['company_id' => $id])->all();

        $data = [];

        foreach ($images as $image) {
            $data[] = Url::toRoute('/'.$image->path, true);
        }



        $accessories = \app\models\Accessories::find()->where(['accessories_id' => null])->all();


        $accesserData = [];

        foreach ($accessories as $accessory) {
             $childAccessories = \app\models\Accessories::find()->where(['accessories_id' => $accessory->id])->all();

             $accesserData[$accessory->id] = [
                'name' => $accessory->name,
                // 'items' => [],
             ];

             foreach ($childAccessories as $childAccessory){
                $checked = false;

                $accessData = json_decode($model->accessories);

                if(is_array($accessData)){
                    $checked = in_array($childAccessory->id, $accessData);
                }


                $accesserData[$accessory->id]['items'][] = $childAccessory->name;
             }
        }


        $firstAccess = Accessories::find()->where(['is', 'accessories_id', null])->all();
        $secondAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($firstAccess, 'id')])->all();
        $thirdAccess = Accessories::find()->where(['accessories_id' => \yii\helpers\ArrayHelper::getColumn($secondAccess, 'id')])->all();

        $prices = [];


        foreach ($thirdAccess as $access) {
                                $price = Price::find()->where(['accessory_id' => $access->id, 'company_id' => $id])->one();

                                $pAccess = Accessories::find()->where(['id' => $access->accessories_id])->one();

                                if($price){
                                    if($price->order_status == Price::ORDER_STATUS_PUBLISHED) // Если прайс опубликован
                                    {

                                        $prices[] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Рассчитать'];

                                    } else {

                                        // Уточняем есть ли в услугах компании данная услуга
                                        if($model->accessories == null){
                                            $dataAccessories = [];
                                        } else {
                                            $dataAccessories = json_decode($model->accessories);
                                        }

                                        if(in_array($access->accessories_id, $dataAccessories)){
                                            $user = \app\models\User::find()->where(['company_id' => $id])->one();
                                            if($user) {
                                                $prices[] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Консультация'];
                                                
                                            } else {
                                                $prices[] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Консультация'];
                                            }
                                        } else {
                                            $prices[] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => '—'];
                                        }
                                    }

                                } else {

                                    // Уточняем есть ли в услугах компании данная услуга
                                    if($model->accessories == null){
                                        $dataAccessories = [];
                                    } else {
                                        $dataAccessories = json_decode($model->accessories);
                                    }

                                    if(in_array($access->accessories_id, $dataAccessories)){
                                        $user = \app\models\User::find()->where(['company_id' => $id])->one();
                                        if($user) {
                                            $prices[] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Консультация'];
                                        } else {
                                            $prices[] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => 'Консультация'];
                                        }
                                    } else {
                                        $prices[] = ['id' => $pAccess->id, 'name' => $pAccess->name, 'type' => '—'];
                                    }

                                }  

        }



        return [
            'branches' => $branches,
            'images' => $data,
            'description' => $model->description,
            'accessories' => $accesserData,
            'accessories_other' => json_decode($model->accessories_other, true),
            'prices' => $prices,
        ];
    }

    public function actionGetChatList()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $users = \app\models\User::find()->where(['!=', 'user.id', $this->user->id])->joinWith(['company'])->asArray()->all();

        array_walk($users, function(&$user){

            $lastMessage = \app\models\ChatMessage::find()->where([
                'or',
                ['sender_id' => $this->user->id, 'receiver_id' => $user['id']],
                ['sender_id' => $user['id'], 'receiver_id' => $this->user->id]
            ])->orderBy('created_at desc')->one();

            $branch = \app\models\Branch::findOne($user['branch_id']);

            $user['avatar'] = $user['company']['avatar'];
            $user['branch_name'] = $branch ? $branch->name : null;
            $user['branch_coords'] = $branch ? $branch->coords : null;

            $user['unread_messages'] = \app\models\ChatMessage::find()->where(['receiver_id' => $this->user->id, 'sender_id' => $user['id'], 'is_read' => false])->count();

            if($lastMessage){
                $user['last_message_content'] = $lastMessage->content;
                $user['last_message_datetime'] = $lastMessage->created_at;
            } else {
                $user['last_message_content'] = " — ";
                $user['last_message_datetime'] = null;
            }

            $user['company']['metroId'] = $branch ? $branch->metro_id : null;
            $user['company']['metroName'] = $branch ? ArrayHelper::getValue($branch, 'metro.name') : null;

        });


        $users = array_values(array_filter($users, function($user){
            return $user['last_message_datetime'];
        }));


        return $users;
    }

    public function actionGetChat($userId)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');



        if($this->user->company->type == 0){ // Клиент
            $branch = \app\models\Branch::findOne($userId);

            $user = \app\models\User::find()->where(['branch_id' => $userId])->one();
            $userId = $user->id;


        } elseif($this->user->company->type == 1){ // Компания

        }


        $messages = \app\models\ChatMessage::find()->where([
            'or',
            ['sender_id' => $this->user->id, 'receiver_id' => $userId],
            ['sender_id' => $userId, 'receiver_id' => $this->user->id]
        ])->orderBy('created_at desc')->asArray()->all();

        array_walk($messages, function(&$model) use($userId){
        	$model['income'] = (int) $model['sender_id'] == $userId;
        });

        return $messages;
    }


    public function actionChatNewMessage($userId)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);

        if($this->user == null){
            return ['result' => false];
        }


        if($this->user->company->type == 0){ // Клиент
            // $branch = \app\models\Branch::findOne($userId);

            $user = \app\models\User::find()->where(['branch_id' => $userId])->one();
            $userId = $user->id;


        } elseif($this->user->company->type == 1){ // Компания

        }

        $message = new \app\models\ChatMessage([
            'sender_id' => $this->user->id,
            'receiver_id' => $userId,
            'content' => $data['content'],
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        return ['result' => $message->save(false)];
    }

    public function actionChatSendFile($userId)
    {

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        // $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();

        // $content = file_get_contents('php://input');
        // $data = json_decode($content, true);



        $file = \yii\web\UploadedFile::getInstanceByName('file');

        if($file){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $attachmentBasename = $file->baseName.'.'.$file->extension;
            $attachment = 'uploads/'.Yii::$app->security->generateRandomString().'.'.$file->extension;
            $file->saveAs($attachment);
        }

        (new \app\models\ChatMessage([
            'sender_id' => $this->user->id,
            'receiver_id' => $userId,
            'content' => '',
            'attachment' => $attachment,
            'attachment_base_name' => $attachmentBasename,
        ]))->save(false);

        return ['success' => true];
    }


    public function actionAdminLogoSave()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();

        // $content = file_get_contents('php://input');
        // $data = json_decode($content, true);


        if(isset($_POST['file']) && $_POST['file'] == 'undefined')
        {
            $model->avatar = null;
            $model->save(false);
        } else {
            $file = \yii\web\UploadedFile::getInstanceByName('file');

            if($file){
                $model->file = $file;
            }


            $model->save(false);
        }
    }

    public function actionOrderLoadFile()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $file = \yii\web\UploadedFile::getInstanceByName('file');

        if($file){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = 'uploads/'.\Yii::$app->security->generateRandomString().'.'.$file->extension;

            $file->saveAs($path);

            return ['result' => true, 'path' => \yii\helpers\Url::toRoute('/'.$path, true)];
        }

        return ['result' => false];
    }

    public function actionOrderSend()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);

        if($data == null){
        	return null;
        }

        $request = Yii::$app->request;
        $model = new \app\modules\api\models\OrderForm();

        // if(Yii::$app->session->has('user__city')){
            // $city = Yii::$app->session->get('user__city');
        // } else {
        $cityModel = City::find()->where(['name' => 'Санкт-Петербург'])->one();
        if($cityModel){
            $city = $cityModel->id;
        } else {
            $city = null;
        }
        // }

        $model->city = $city;
        // $model->agree = true;

        // $model->files = \yii\web\UploadedFile::getInstancesByName('files');



        foreach ($data as $key => $value) {
            $model->$key = $value;
        }

        if($model->send()){
            return ['result' => true];
        } else {
            return ['result' => false, 'errors' => $model->errors];
        }
    }

    public function actionOrderCategoryList()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $json = json_decode(file_get_contents('params.json'), true);

        $firstParam = array_combine(array_keys($json), array_keys($json));

        return $firstParam;
    }

    public function actionOrderSubcategoryList($category)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $data = json_decode(file_get_contents('params.json'), true);

        $secondData = $data[$category];

        $output = [];

        foreach ($secondData as $key => $value)
        {
            $output[$key] = $key;
        }

        return $output;
    }

    public function actionUserFileSave()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = $this->user;

        $file = \yii\web\UploadedFile::getInstanceByName('file');

        if($file){
            $model->file = $file;
        }


        $model->save(false);

        return ['result' => true];
    }

    public function actionBranchLogoSave($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Branch::find()->where(['id' => $id])->one();

        // $content = file_get_contents('php://input');
        // $data = json_decode($content, true);

        $file = \yii\web\UploadedFile::getInstanceByName('file');

        if($file){
            $model->file = $file;
        }


        $model->save(false);
    }


    public function actionDescriptionPage()
    {

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $company = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();
        return [
            'description' => $company->description, 
        ];
    }

    public function actionDescriptionSave()
    {

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);


        $allowAttributes = ['description'];

        if (Yii::$app->request->isPost) {
            
            foreach ($data as $attribute => $value) {
                if(in_array($attribute, $allowAttributes) == false){
                    continue;
                }

                $model->$attribute = $value;
            }

            $model->save(false);

            return [
                'description' => $model->description, 
            ];
        } else {
            return $model->errors;
        }
    }

    public function actionAccessoriesPage()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();


        $accessories = \app\models\Accessories::find()->where(['accessories_id' => null])->all();


        $data = [];

        foreach ($accessories as $accessory) {
        	 $childAccessories = \app\models\Accessories::find()->where(['accessories_id' => $accessory->id])->all();

        	 $data[$accessory->id] = [
        	 	'name' => $accessory->name,
        	 	// 'items' => [],
        	 ];

        	 foreach ($childAccessories as $childAccessory){
                $checked = false;

                $accessData = json_decode($model->accessories);

                if(is_array($accessData)){
                    $checked = in_array($childAccessory->id, $accessData);
                }


                $data[$accessory->id]['items'][] = [
                	'id' => $childAccessory->id,
                	'name' => $childAccessory->name,
                	'checked' => $checked,
                ];
        	 }
        }

        return [
            'accessories' => $data, 
        ];
    }

    public function actionAccessoriesSave()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        if($this->user == null){
            return null;
        }

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);


        $allowAttributes = ['accessories'];

        if (Yii::$app->request->isPost) {
            
            foreach ($data as $attribute => $value) {
                if(in_array($attribute, $allowAttributes) == false){
                    continue;
                }

                if($attribute == 'accessories'){
                	$model->accessories = json_encode($data['accessories']);
                }
            }

            $model->save(false);

            return [
                'accessories' => $model->accessories, 
            ];
        } else {
            return $model->errors;
        }
    }

    public function actionAccessoriesOtherPage()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();


        // $accessories = \app\models\Accessories::find()->where(['accessories_id' => null])->all();


		
        // $accessories_other = json_decode($model->accessories_other);
		$accessories_other = json_decode($model->accessories_other);


        return [
            // 'accessories_other' => $accessories_other, 
            'accessories_other' => $model->accessories_other, 
        ];
    }

    public function actionAccessoriesOtherSave()
    {

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();

        $content = file_get_contents('php://input');
        $data = json_decode($content, true);


        $allowAttributes = ['accessories_other'];

        if (Yii::$app->request->isPost) {
            
            foreach ($data as $attribute => $value) {
                if(in_array($attribute, $allowAttributes) == false){
                    continue;
                }


            }

            // $model->accessories_other = json_encode($value, JSON_UNESCAPED_UNICODE);
            $model->accessories_other = $value;

            $model->save(false);

            return [
                'accessories_other' => $value, 
            ];
        } else {
            return $model->errors;
        }
    }

    public function actionGalleryPage()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();


        $images = \app\models\ImageCompany::find()->where(['company_id' => $model->id])->all();

        $data = [];

        foreach ($images as $image) {
        	$data[] = Url::toRoute($image->path, true);
        }

        return [
            'images' => $data, 
        ];
    }

    public function actionGalleryAdd()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $file = \yii\web\UploadedFile::getInstanceByName('file');

        $path = 'uploads/'.Yii::$app->security->generateRandomString().'.'.$file->extension;

        $file->saveAs($path);

        $model = new \app\models\ImageCompany([
        	'company_id' => $this->user->company_id,
        	'path' => $path,
        	'name' => $file->name
        ]);

        $model->save(false);
    }

    public function actionGalleryRemove($path)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $pathArr = explode('uploads/', $path);

        if(count($pathArr) == 2){
        	$relativePath = 'uploads/'.$pathArr[1];

        	$img = \app\models\ImageCompany::find()->where(['path' => $relativePath, 'company_id' => $this->user->company_id])->one();

        	if($img == null){
        		throw new NotFoundHttpException('Файл не найден');
        	}

        	$img->delete();

        	if(file_exists('/'.$relativePath)){
        		unlink('/'.$relativePath);
        	}

        	return ['result' => true];
        }
    }

    public function actionBranches()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();


        $branches = \app\models\Branch::find()->where(['company_id' => $model->id])->asArray(true)->all();

        $allowAttributes = ['id', 'name', 'phone', 'email', 'social_site', 'address', 'city_id', 'social_site', 'social_vk', 'social_instagram', 'file', 'metro_id','avatar', 'coords'];

        for ($i=0; $i < count($branches); $i++) { 
        	foreach ($branches[$i] as $attr => $value) {
        		if(in_array($attr, $allowAttributes) == false){
        			unset($branches[$i][$attr]);
        		}
        	}
        }

        array_walk($branches, function(&$model){
            $model['metroName'] = null;
            $model['cityName'] = null;

            if($model['metro_id']){
                $metro = \app\models\Metro::findOne($model['metro_id']);
                if($metro){
                    $model['metroName'] = $metro->name;
                }
            }

            if($model['city_id']){
                $city = \app\models\City::findOne($model['city_id']);
                if($city){
                    $model['cityName'] = $city->name;
                }
            }
        });


        return $branches;
    }

    public function actionBranchAdd()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');
        $model = new \app\models\Branch();

        if (Yii::$app->request->isPost) {

            $content = file_get_contents('php://input');
            $attributes = json_decode($content, true);

            if(count($attributes) == 0){
            	return ['success' => false];
            }

            unset($attributes['token']);

            $model->attributes = $attributes;

            \Yii::warning($this->user);

            $model->company_id = $this->user->company_id;
            $saved = $model->save(false);

            return ['success' => $saved, 'errors' => $model->errors];
        }
    }

    public function actionBranchUpdate($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');
        $model = \app\models\Branch::findOne($id);

        if (Yii::$app->request->isPost) {

            $content = file_get_contents('php://input');
            $attributes = json_decode($content, true);

            unset($attributes['token']);

            $model->attributes = $attributes;

            \Yii::warning($this->user);

            $model->company_id = $this->user->company_id;
            $saved = $model->save(false);

            return ['success' => $saved, 'errors' => $model->errors];
        }
    }

    public function actionDeleteBranchesTotal($pks)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $pks = explode(',', $pks);

        $request = Yii::$app->request;
        $model = \app\models\Branch::deleteAll(['id' => $pks]);

        return ['success' => true];
    }

    public function actionDeleteUsersTotal($pks)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $pks = explode(',', $pks);

        $request = Yii::$app->request;
        $model = \app\models\User::deleteAll(['id' => $pks]);

        return ['success' => true];
    }

    // public function actionBranchCoords($id, $coords = null)
    // {
    //     $model = \app\models\Branch::findOne($id);
    //     if($coords == null){
    //         return ['coords' => $model->coords];
    //     } else {
    //         $model->coords = $coords;
    //         $model->save();
    //     }

    //     return ['coords' => $model->coords];
    // }

    public function actionUserAdd()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = new \app\models\User();

        if (Yii::$app->request->isPost) {

            $content = file_get_contents('php://input');
            $attributes = json_decode($content, true);

            unset($attributes['token']);

            $model->attributes = $attributes;

            \Yii::warning($this->user);

            $model->company_id = $this->user->company_id;
            $saved = $model->save(false);

            return ['success' => $saved, 'errors' => $model->errors];
        }
    }

    public function actionUserUpdate($id)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\User::findOne($id);

        if (Yii::$app->request->isPost) {

            $content = file_get_contents('php://input');
            $attributes = json_decode($content, true);

            unset($attributes['token']);

            $model->attributes = $attributes;

            \Yii::warning($this->user);

            $saved = $model->save(false);

            return ['success' => $saved, 'errors' => $model->errors];
        }
    }

    public function actionFaq()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');




        $faqs = \app\models\Faq::find()->asArray()->all();

       	for ($i=0; $i < count($faqs); $i++) { 
       		$faqs[$i]['file'] = $faqs[$i]['file'] ? \yii\helpers\Url::toRoute('/'.$faqs[$i]['file'], true) : null;
       	}

        $faqsGroups = array_unique(ArrayHelper::getColumn($faqs, 'grouping'));

        $output = [];

        foreach ($faqsGroups as $groupName) {
            if(isset($output[$groupName]) == false){
                $output[$groupName] = [];
            }
            foreach ($faqs as $faq) {
                if($faq['grouping'] == $groupName){
                    $output[$groupName][] = $faq;
                }                
            }
        }

        return $output;
    }

    public function actionUsers()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');

        $model = \app\models\Company::find()->where(['id' => $this->user->company_id])->one();


        $users = \app\models\User::find()->where(['company_id' => $model->id])->asArray(true)->all();

        // $allowAttributes = ['name', 'phone', 'email', 'social_site', 'address', 'city_id', 'social_site', 'social_vk', 'social_instagram', 'file'];

        // foreach ($users as $user) {
        // 	foreach ($user as $attr => $value) {
        // 		if(in_array($attr, $allowAttributes) == false){
        // 			unset($user[$attr]);
        // 		}
        // 	}
        // }

        return $users;
    }

    public function beforeAction($action)
    {
        if($action->id != 'register' && $action->id != 'login' && $action->id != 'faq' && $action->id != 'order-category-list' && $action->id != 'order-subcategory-list' && $action->id != 'order-load-file' && $action->id != 'order-send' && $action->id != 'send-creditionals')
        {
            if(isset($_GET['token'])){

                if(isset($_GET['token']) == false){
                     return ['error' => "Токен не найден"];
                }

                if($_GET['token'] == null){
                     return ['error' => "Токен пуст"];
                }

                $token = $_GET['token'];

                $user = \app\models\User::find()->where(['token' => $token])->one();

                if($user == false){
                     return ['error' => "Токен некорректный"];
                }

                $this->user = $user;

            //} elseif (Yii::$app->request->isPost){
            } else{

                $content = file_get_contents('php://input');
                $data = json_decode($content, true);

                \Yii::warning($data);



                if(isset($data['token']) == false){
                    // Yii::warning('heere');
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                     return ['error' => "Токен не найден"];
                }

                if($data['token'] == null){
                    // Yii::warning('heere2');
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                     return ['error' => "Токен пуст"];
                }

                $token = $data['token'];

                $user = \app\models\User::find()->where(['token' => $token])->one();

                if($user == false){
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                     return ['error' => "Токен некорректный"];
                }

                $this->user = $user;

            }
        }

        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
}
